package main

import (
	"os" //"strconv"
	domain "ps_meta/a_domain"
	data_provider "ps_meta/c_impl_psprovider_ftpserver"
	data_reader "ps_meta/d_impl_psdatareader"
	my_sql_rep "ps_meta/e_impl_repo_mysql"
	imageProxy "ps_meta/f_impl_imagerepo_filesys"
	appl_logger "ps_meta/y_impl_logger"
	bootstrap "ps_meta/z_bootstrap"
)

/*===============================================================================
func getResources
===============================================================================*/
func getResources(iApplConf bootstrap.ApplConfStruct) (
	eDatabase domain.PSDataBase,
	eImageRepo domain.PSImageArchivePort,
	eDataprovider domain.PSDataProviderPort,
	eDatareader domain.PSProviderDataReaderPort,
	eApplLogger domain.PSLogPort) {

	////println("eDatabase = domain.PSDataBase{DataBasePort: my_sql_rep.GetNewMySQLRep()}")

	eDatabase = domain.PSDataBase{
		AuthDBPort:    my_sql_rep.GetNewMySQLRepoAuth(),
		UsrDBPort:     my_sql_rep.GetNewMySQLRepoUsr(),
		PSDBPort:      my_sql_rep.GetNewMySQLRepoPS(),
		MeldungDBPort: my_sql_rep.GetNewMySQLRepoMeldung(),
		ArticleDBPort: my_sql_rep.GetNewMySQLRepoArtikel()}

	// get imagearchiv
	eImageRepo = imageProxy.ImageRepo{} //
	//ImageArchivPort: image_repo_impl.PSImageArchiv}

	// get dataprovider
	eDataprovider = data_provider.ProviderFTPServerPort{}

	// get datareader
	eDatareader = data_reader.DataReader
	eApplLogger = appl_logger.GetLogger(iApplConf.LogOnlyErrors)

	//
	return
}

/*===============================================================================
func SetUpWorkDoneLogsDirPaths
===============================================================================*/
func SetUpWorkDoneLogsDirPaths(applConf bootstrap.ApplConfStruct, iLogger domain.PSLogPort) {
	////println("huhuh")
	logTransactionBuffer := iLogger.SetStartTransaction("CheckArchivFileSystem")
	logTransactionBuffer = iLogger.LogStep(logTransactionBuffer, "Work, Done, Logs Verzeichnisse überprüfen", "")

	lPathExists := false
	lErr := *new(error)

	lPathExists = existsSubpath(applConf.LocationLogs, "")
	if !lPathExists {
		lErr = createSubpath(applConf.LocationLogs, "")
		if lErr != nil {
			panic("lErr: " + lErr.Error())
		}
	}

	////println("s:"+applConf.LocationPDFArchivePS)
	if lErr == nil {
		lPathExists = existsSubpath(applConf.ImportWorkDir, "")
		if !lPathExists {
			lErr = createSubpath(applConf.ImportWorkDir, "")
		}
	}
	if lErr == nil {
		lPathExists = existsSubpath(applConf.DataDonePath, "")
		if !lPathExists {
			lErr = createSubpath(applConf.DataDonePath, "")
		}
	}

	lprocessinginfo := domain.ProcessingInfo{}
	if lErr != nil {
		lprocessinginfo.DetectingComponentName = "SetUpWorkDoneLogsDirPaths"
		lprocessinginfo.InfoDescription = "Pfade(Workdir,DoneDir,Logs) konnten nicht erstellt werden:" + lErr.Error()
		logTransactionBuffer = iLogger.LogStepResult(logTransactionBuffer, "Work, Done, Logs Verzeichnisse überprüfen", "FAILED", lprocessinginfo)
		return
	}
	logTransactionBuffer = iLogger.LogStepResult(logTransactionBuffer, "Work, Done, Logs Verzeichnisse überprüfen", "OK", lprocessinginfo)
	iLogger.SetEndTransaction(logTransactionBuffer)

}

/*===============================================================================
func existsSubpath(iPathBase string, iSubpath string)
===============================================================================*/
func existsSubpath(iPathBase string, iSubpath string) (eExists bool) {
	eExists = false
	filepath := ""
	if len(iSubpath) > 0 {
		filepath = iPathBase + string(os.PathSeparator) + iSubpath
	} else {
		filepath = iPathBase
	}
	_, err := os.Stat(filepath)
	if err == nil {
		eExists = true
	}
	return eExists
}

/*===============================================================================
func createSubpath(iPathBase string, iSubpath string)
===============================================================================*/
func createSubpath(iPathBase string, iSubpath string) (err error) {
	filepath := ""
	if len(iSubpath) > 0 {
		filepath = iPathBase + string(os.PathSeparator) + iSubpath
	} else {
		filepath = iPathBase
	}
	err = os.Mkdir(filepath, 0777)
	return err
}

/*===============================================================================
func SetUpDatabase
===============================================================================*/
func SetUpDatabase(applConf bootstrap.ApplConfStruct, iDBHostIP, iDBHostPort string) {
	define_database_table(applConf, iDBHostIP, iDBHostPort)
}
