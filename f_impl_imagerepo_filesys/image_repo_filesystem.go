package f_impl_imagerepo_filesys

import model "ps_meta/a_domain"

//import imageRep "ps_meta/i_ps_image"

type ImageRepo struct {
}

func (r ImageRepo) TransferImagesToImageRepProxy(iWorkdir string, iPSImageName string, iPSESD string, iPSExtID string,
	iArtikelImageList model.ArtikelImageList) (eTransferResult model.ImageTransferResult) {

	portPSImageService := getPortPSImageService()
	lParamAsJson, err := translateEntityInfosToJson(iWorkdir, iPSImageName, iPSESD, iPSExtID, iArtikelImageList)
	//    //println("lParamAsJson:"+lParamAsJson)
	if err == nil {
		eTransferResult = callServiceTransferToPSImage(lParamAsJson, portPSImageService)

	}
	return
}

func (r ImageRepo) BuildMeldungImageToImageRepProxy(iPSESD string, iMelExtID string) (eTransferResult model.ImageTransferResult) {

	portPSImageService := getPortPSImageService()
	//    //println("lParamAsJson:"+lParamAsJson)
	eTransferResult = callServiceBuildMeldungImage(iPSESD, iMelExtID, portPSImageService)

	return
}

func (r ImageRepo) BuildMeldungenImagesToImageRepProxy(iPSESD string, iMeldungenExtIds []string) (eTransferResult model.ImageTransferResult) {

	portPSImageService := getPortPSImageService()
	//    //println("lParamAsJson:"+lParamAsJson)
	eTransferResult = callServiceBuildMeldungenImages(iPSESD, iMeldungenExtIds, portPSImageService)

	return
}

func (r ImageRepo) CancelTransferedImagesToRepProxy(iPSESD string) (eTransferCancelResult model.ImageCancelTransferResult) {
	portPSImageService := getPortPSImageService()
	//    //println("lParamAsJson:"+lParamAsJson)
	eTransferCancelResult = callServiceCancelTransferredImages(iPSESD, portPSImageService)

	return
}

func (r ImageRepo) GetEntityImageLocationProxy(iEntityextid string, iEntititytype string) (eImageRepoStatus, eRepoPath string, err error, ok bool) {
	//var imageStorage = imageRep.PSImageArchiv{}
	//return imageStorage.GetImageLocation(iEntityextid,	iEntititytype)
	return "", "", nil, true
}

func (r ImageRepo) GetPSImageLocationsProxy(iPSESD string) (eImageRepoInfo model.ListEntityImageDescriptor, err error, ok bool) {
	//var imageStorage = imageRep.PSImageArchiv{}

	//return imageStorage.GetImageLocation(iEntityextid,	iEntititytype)
	return model.ListEntityImageDescriptor{}, nil, true
}

func (r ImageRepo) GetMeldungImageLocationProxy(iPSESD, iMelExtID string) (eImageRepoInfo model.ListEntityImageDescriptor, err error, ok bool) {
	//var imageStorage = imageRep.PSImageArchiv{}

	//return imageStorage.GetImageLocation(iEntityextid,	iEntititytype)
	return model.ListEntityImageDescriptor{}, nil, true
}

func (r ImageRepo) GetCollectionImageLocationProxy(iCollectionID string) (eImageRepoInfo model.ListEntityImageDescriptor, err error, ok bool) {
	//var imageStorage = imageRep.PSImageArchiv{}

	//return imageStorage.GetImageLocation(iEntityextid,	iEntititytype)
	return model.ListEntityImageDescriptor{}, nil, true
}

func (r ImageRepo) GetTempCollectionImageLocationProxy(iTempCollection []model.ArtikelSelection) (eImageRepoInfo model.ListEntityImageDescriptor, err error, ok bool) {
	//var imageStorage = imageRep.PSImageArchiv{}

	//return imageStorage.GetImageLocation(iEntityextid,	iEntititytype)
	return model.ListEntityImageDescriptor{}, nil, true
}
