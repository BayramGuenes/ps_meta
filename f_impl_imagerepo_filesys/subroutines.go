package f_impl_imagerepo_filesys

import (
	model "ps_meta/a_domain"
	bootinfo "ps_meta/z_bootstrap"

	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	syscommanager "ps_meta/h_service_communication_manager"
)

//import "strconv"

type EntityInfoAsJsonStruct struct {
	Workdir              string                 `json:"workdir"`
	PSImageFileName      string                 `json:"psimagefilename"`
	PSESD                string                 `json:"psesd"`
	PSExtID              string                 `json:"psextid"`
	ArtikelImageListInfo model.ArtikelImageList `json:"artikelimagelistinfo"`
}

func getPortPSImageService() (ePSImagePort string) {
	if bootinfo.ApplConf.UsePSSysregAsRegistrator {
		lResultGetPortMS := syscommanager.ApplServiceCom.GetPortMicroservice(syscommanager.MicroServiceNamePsImage)
		ePSImagePort = lResultGetPortMS.Port
	} else {
		ePSImagePort = bootinfo.PortPSImage
	}
	return
}

func translateEntityInfosToJson(iWorkdir string, iPSImageName string, iPSESD string, iPSExtID string, iArtikelImageList model.ArtikelImageList) (eInfosAsJson string, err error) {

	eInfosAsJson = ""
	structToJson := EntityInfoAsJsonStruct{}
	structToJson.Workdir = iWorkdir
	structToJson.PSImageFileName = iPSImageName
	structToJson.PSESD = iPSESD
	structToJson.PSExtID = iPSExtID
	structToJson.ArtikelImageListInfo = iArtikelImageList

	infosAsJsonByte, err := json.Marshal(structToJson)
	if err == nil {
		eInfosAsJson = string(infosAsJsonByte)
	}
	return
}

func callServiceTransferToPSImage(ientityInfosAsJson string, iPortOfService string) (eDetails model.ImageTransferResult) {
	println("20200608 callServiceTransferToPSImage CALLED")

	eDetails = model.ImageTransferResult{}

	body := []byte(ientityInfosAsJson)

	//serviceadr := "localhost:" + iPortOfService+"/Hello"  //TransferPDFsToImageStorage"
	serviceadr := bootinfo.ThisCommunnicationProtokoll + "://" + bootinfo.ServiceLocationIDPSImage + ":" + iPortOfService + "/ImageTransfer"
	resp, err := http.Post(serviceadr, "application/json", bytes.NewBuffer(body))
	//resp, err := http.Get(serviceadr)
	println("20200608 callServiceTransferToPSImage CALLED II")

	if err != nil {
		eDetails.TransferWarningErrorText = "Technical error: http.Post failed." + err.Error()
		eDetails.TransferOK = false
		return

	}
	println("20200608 callServiceTransferToPSImage CALLED III")

	defer resp.Body.Close()
	body, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		eDetails.TransferWarningErrorText = "Technical error: ioutil.ReadAll(resp.Body) failed." + err.Error()
		eDetails.TransferOK = false
		return
	}
	println("20200608 callServiceTransferToPSImage CALLED IV")

	var respStruct model.ImageTransferResult
	if err = json.Unmarshal(body, &respStruct); err != nil {
		eDetails.TransferWarningErrorText = "Technical error: json.Unmarshal(body, &respStruct)) failed." + err.Error()
		eDetails.TransferOK = false
		return

	}
	println("20200608 callServiceTransferToPSImage CALLED V")

	eDetails = respStruct
	return

}

func callServiceBuildMeldungImage(iPSESD string, iMelExtID string, iPortOfService string) (eDetails model.ImageTransferResult) {

	eDetails = model.ImageTransferResult{}

	body := []byte{}

	println("20200608 callServiceBuildMeldungImage CALLED")

	serviceadr := bootinfo.ThisCommunnicationProtokoll + "://" + bootinfo.ServiceLocationIDPSImage + ":" + iPortOfService + "/MeldungImage/Build/" + iPSESD + "/" + iMelExtID
	resp, err := http.Post(serviceadr, "application/json", bytes.NewBuffer(body))
	if err != nil {
		println("20200608 err:" + err.Error())

		eDetails.TransferWarningErrorText = "Technical error: http.Post failed." + err.Error()
		eDetails.TransferOK = false
		return

	}
	println("20200608 callServiceBuildMeldungImage CALLED II")

	defer resp.Body.Close()
	body, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		eDetails.TransferWarningErrorText = "Technical error: ioutil.ReadAll(resp.Body) failed." + err.Error()
		eDetails.TransferOK = false
		return
	}
	println("20200608 callServiceBuildMeldungImage CALLED III")

	var respStruct model.ImageTransferResult
	if err = json.Unmarshal(body, &respStruct); err != nil {
		eDetails.TransferWarningErrorText = "Technical error: json.Unmarshal(body, &respStruct)) failed." + err.Error()
		eDetails.TransferOK = false
		return
	}
	eDetails = respStruct
	return
}

func callServiceBuildMeldungenImages(iPSESD string, iMeldungenExtIds []string, iPortOfService string) (eDetails model.ImageTransferResult) {
	type typMeldungslisteAsStruct struct {
		MeldungenExtIds []string
	}
	meldungslisteAsStruct := model.TypMeldungslisteAsStruct{MeldungenExtIds: iMeldungenExtIds}
	eDetails = model.ImageTransferResult{}
	infosAsJsonByte, err := json.Marshal(meldungslisteAsStruct)
	////println("iMeldungenExtIds.len"+strconv.Itoa(len(iMeldungenExtIds)))
	body := infosAsJsonByte

	//serviceadr := "localhost:" + iPortOfService+"/Hello"  //TransferPDFsToImageStorage"
	serviceadr := bootinfo.ThisCommunnicationProtokoll + "://" + bootinfo.ServiceLocationIDPSImage + ":" + iPortOfService + "/MeldungenImages/Build/PSESD/" + iPSESD
	resp, err := http.Post(serviceadr, "application/json", bytes.NewBuffer(body))
	//resp, err := http.Get(serviceadr)

	if err != nil {
		eDetails.TransferWarningErrorText = "Technical error: http.Post failed." + err.Error()
		eDetails.TransferOK = false
		return

	}
	defer resp.Body.Close()
	body, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		eDetails.TransferWarningErrorText = "Technical error: ioutil.ReadAll(resp.Body) failed." + err.Error()
		eDetails.TransferOK = false
		return
	}
	var respStruct model.ImageTransferResult
	if err = json.Unmarshal(body, &respStruct); err != nil {
		eDetails.TransferWarningErrorText = "Technical error: json.Unmarshal(body, &respStruct)) failed." + err.Error()
		eDetails.TransferOK = false
		return
	}
	eDetails = respStruct
	return

}

func callServiceCancelTransferredImages(iPSESD string, iPortOfService string) (eDetails model.ImageCancelTransferResult) {

	eDetails = model.ImageCancelTransferResult{}

	//serviceadr := "localhost:" + iPortOfService+"/Hello"  //TransferPDFsToImageStorage"
	serviceadr := bootinfo.ThisCommunnicationProtokoll + "://" + bootinfo.ServiceLocationIDPSImage + ":" + iPortOfService +
		"/ResetImageTransfer/" + iPSESD

	// Create client
	client := &http.Client{}

	// Create request
	req, err := http.NewRequest("DELETE", serviceadr, nil)
	if err != nil {
		//fmt.//println(err)
		eDetails.CancelOK = false
		eDetails.Details = "http error:" + err.Error()
		return
	}

	// Fetch Request
	resp, err := client.Do(req)
	if err != nil {
		eDetails.CancelOK = false
		eDetails.Details = "http error:" + err.Error()
		return
	}
	defer resp.Body.Close()

	// Read Response Body
	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		eDetails.CancelOK = false
		eDetails.Details = "http error:" + err.Error()
		return

	}
	var respStruct model.ImageCancelTransferResult
	if err = json.Unmarshal(respBody, &respStruct); err != nil {
		eDetails.Details = "Technical error: json.Unmarshal(body, &respStruct)) failed." + err.Error()
		eDetails.CancelOK = false
		return
	}
	eDetails = respStruct

	return

}
