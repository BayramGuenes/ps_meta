package z_bootstrap

type ApplConfStruct struct {
	UsePSSysregAsRegistrator          bool
	UseGorillaMuxRouter               bool
	UseGinGonicRouter                 bool
	ImportFromSys                     string
	FTPHostName                       string
	FTPHostPort                       string
	FTPUsr                            string
	FTPPwd                            string
	FTPDeleteDataFromServerAfterFetch bool
	ImportWorkDir                     string
	DataDonePath                      string
	QueryXMLFileName                  string
	DB_UserPwd                        string
	DB_SQLApplschemaName              string
	LocationLogs                      string
	LogOnlyErrors                     bool
	SkipFTPAndReadDataWorkDir         bool
	TriggerBuildMeldungImages         bool
	AlwaysLogImportActivate           bool
	AlwaysLogExecQueryArticle         bool
	AnonymUsrDefaultAccessInterval    int
	CreateDatabase                    bool
	StrictTokenValidateOn             bool
}

var ApplConf ApplConfStruct

var (
	ThisCommunnicationProtokoll string
	ThisServerAddr              string
	ThisPort                    string
	ThisHttpsPort               string
	ThisConfLocation            string
	ThisDbhostIP                string
	ThisDbhostPORT              string
)

var (
	ServiceLocationIDPSMeta    string
	ServiceLocationIDSysReg    string
	ServiceLocationIDPSImage   string
	ServiceLocationIDPSQuery   string
	ServiceLocationIDPSCollect string
	SysRegPort                 string
	PortPSImage                string
	PortPSQuery                string
	PortPSCollect              string
)

var (
	FrontConsKCloakDef              string
	FrontConsKCloakCookieCaller     string
	FrontConsKCloakCookieAccessTok  string
	FrontConsKCloakCookieRefreshTok string
	FrontConsKCloakDomainCookies    string
	FrontConsKCloakPathCookies      string
	ServerKCloakClientID            string
	ServerKCloakClioentSecret       string
	ServerKCloakConfigURL           string
	ServerKCloakConfigURLScheme     string
)
