package z_bootstrap

import (
	"flag"
	"os"
	"strings"
)

func loadExternalSettings() {

	loadFromOSFlags()

	// Überschreiben OS Flags durch ENV Param
	loadFromOSEnv()
	loadFromOSEnvKeyCloak()

	// Überschreiben Parameter, falls durch args mitgegeben
	loadFromOSArgs()
	loadFromOSArgsKeycloak()

	return
}

func loadFromOSFlags() {
	protokollPointer := flag.String("protokoll", "http", "application-communication protokoll")

	servLocaIDPSMetaPointer := flag.String("serviceLocationIDPSMeta", "127.0.0.1", "service location id PSMeta")
	servLocaIDSysRegPointer := flag.String("serviceLocationIDSysReg", "127.0.0.1", "service location id PSSysReg")
	servLocaIDPSImagePointer := flag.String("serviceLocationIDPSImage", "127.0.0.1", "service location id PSImage")
	servLocaIDPSQueryPointer := flag.String("serviceLocationIDPSQuery", "127.0.0.1", "service location id PSQuery")
	servLocaIDPSCollectPointer := flag.String("serviceLocationIDPSCollect", "127.0.0.1", "service location id PSCollect")

	portPointer := flag.String("port", "8800", "application port")
	portPointerSysReg := flag.String("port_Sysreg", "5555", "ps syscomponent registrator service port")
	portPointerPsImage := flag.String("port_PsImage", "9080", "Ps_Image MS  port")
	portPointerPsQuery := flag.String("port_PsQuery", "6080", "Ps_Image MS  port")
	portPointerPsCollect := flag.String("port_PsCollect", "7080", "Ps_Collect MS  port")

	httpsportPointer := flag.String("httpsport", "443", "application https port")
	confLocationPointer := flag.String("confLocation", "", "conf.json path")
	dbserverhostIPPointer := flag.String("dbhostIP", "127.0.0.1", "database host-Ip:Port ")
	dbserverPORTPointer := flag.String("dbserverPORT", "3306", "database host-Ip:Port ")

	flag.Parse()
	protokoll := *protokollPointer
	servLocaIDPSMeta := *servLocaIDPSMetaPointer
	servLocaIDSysReg := *servLocaIDSysRegPointer
	servLocaIDPSImage := *servLocaIDPSImagePointer
	servLocaIDPSQuery := *servLocaIDPSQueryPointer
	servLocaIDPSCollect := *servLocaIDPSCollectPointer

	port := *portPointer
	portSysReg := *portPointerSysReg
	portPsImage := *portPointerPsImage
	portPsQuery := *portPointerPsQuery
	portPsCollect := *portPointerPsCollect

	httpsport := *httpsportPointer
	confLocation := *confLocationPointer
	dbserverhostIP := *dbserverhostIPPointer
	dbserverPORT := *dbserverPORTPointer

	ThisCommunnicationProtokoll = protokoll
	ThisPort = port
	ThisHttpsPort = httpsport
	ThisConfLocation = confLocation
	if ThisCommunnicationProtokoll == "http" {
		ThisServerAddr = ThisCommunnicationProtokoll + "://" + ServiceLocationIDPSMeta + ":" + ThisPort
	} else {
		ThisServerAddr = ThisCommunnicationProtokoll + "://" + ServiceLocationIDPSMeta + ":" + ThisHttpsPort
	}
	ThisDbhostIP = dbserverhostIP
	ThisDbhostPORT = dbserverPORT

	ServiceLocationIDPSMeta = servLocaIDPSMeta
	ServiceLocationIDSysReg = servLocaIDSysReg
	ServiceLocationIDPSImage = servLocaIDPSImage
	ServiceLocationIDPSQuery = servLocaIDPSQuery
	ServiceLocationIDPSCollect = servLocaIDPSCollect
	SysRegPort = portSysReg
	PortPSImage = portPsImage
	PortPSQuery = portPsQuery
	PortPSCollect = portPsCollect
	return
}

func loadFromOSEnv() {

	servLocaIDPSMeta, servLocaIDSysReg, servLocaIDPSImage, servLocaIDPSQuery, servLocaIDPSCollect, dbserverhostIP :=
		ServiceLocationIDPSMeta, ServiceLocationIDSysReg, ServiceLocationIDPSImage, ServiceLocationIDPSQuery, ServiceLocationIDPSCollect, ThisDbhostIP

	protokoll, port, httpsport, portSysReg, portPsImage, portPsQuery, portPsCollect, confLocation, dbserverPORT := ThisCommunnicationProtokoll,
		ThisPort, ThisHttpsPort, SysRegPort, PortPSImage, PortPSQuery, PortPSCollect, ThisConfLocation, ThisDbhostPORT

	environList := os.Environ()
	leng := len(environList)
	for i := 0; i < leng; i++ {
		if i > 0 {
			osenvparamval := environList[i]
			splittedString := strings.Split(osenvparamval, "=")
			paramname := splittedString[0]
			paramval := splittedString[1]

			if paramname == "port" || paramname == "PORT" || paramname == "Port" ||
				paramname == "port_psmeta" || paramname == "PORT_PSMETA" || paramname == "Port_PSMeta" || paramname == "port_PSMeta" {
				port = paramval
			}
			if paramname == "httpsport" || paramname == "HTTPSPORT" || paramname == "Httpsport" {
				httpsport = paramval
			}
			if paramname == "protokoll" || paramname == "Protokoll" || paramname == "PROTOKOLL" {
				protokoll = paramval
			}

			if paramname == "serviceLocationIDPSMeta" || paramname == "SERVICELOCATIONIDPSMETA" {
				servLocaIDPSMeta = paramval
			}
			if paramname == "serviceLocationIDSysReg" || paramname == "SERVICELOCATIONIDSYSREG" {
				servLocaIDSysReg = paramval
			}
			if paramname == "serviceLocationIDPSImage" || paramname == "SERVICELOCATIONIDPSIMAGE" {
				servLocaIDPSImage = paramval
			}
			if paramname == "serviceLocationIDPSQuery" || paramname == "SERVICELOCATIONIDPSQUERY" {
				servLocaIDPSQuery = paramval
			}
			if paramname == "serviceLocationIDPSCollect" || paramname == "SERVICELOCATIONIDPSCOLLECT" {
				servLocaIDPSCollect = paramval
			}

			if paramname == "port_Sysreg" || paramname == "PORT_SYSREG" || paramname == "Port_Sysreg" {
				portSysReg = paramval
			}
			if paramname == "port_Psimage" || paramname == "PORT_PSIMAGE" || paramname == "Port_PsImage" {
				portPsImage = paramval
			}
			if paramname == "port_Psquery" || paramname == "PORT_PSQUERY" || paramname == "Port_PsQuery" {
				portPsQuery = paramval
			}
			if paramname == "port_Pscollect" || paramname == "PORT_PSCOLLECT" || paramname == "Port_PsCollect" {
				portPsCollect = paramval
			}
			if paramname == "confLocation" || paramname == "ConfLocation" ||
				paramname == "conflocation" || paramname == "Conflocation" || paramname == "CONFLOCATION" {
				confLocation = paramval
			}
			if paramname == "dbhostIP" || paramname == "DBHOSTIP" {
				dbserverhostIP = paramval
			}
			if paramname == "dbserverPORT" || paramname == "DBSERVERPORT" {
				dbserverPORT = paramval
			}

		}
	}

	ServiceLocationIDPSMeta, ServiceLocationIDSysReg, ServiceLocationIDPSImage, ServiceLocationIDPSQuery,
		ServiceLocationIDPSCollect, ThisDbhostIP =
		servLocaIDPSMeta, servLocaIDSysReg, servLocaIDPSImage, servLocaIDPSQuery, servLocaIDPSCollect, dbserverhostIP

	ThisCommunnicationProtokoll, ThisPort,
		ThisHttpsPort, SysRegPort, PortPSImage, PortPSQuery, PortPSCollect, ThisConfLocation, ThisDbhostPORT =
		protokoll, port, httpsport, portSysReg, portPsImage, portPsQuery, portPsCollect, confLocation, dbserverPORT

	if ThisCommunnicationProtokoll == "http" {
		ThisServerAddr = ThisCommunnicationProtokoll + "://" + ServiceLocationIDPSMeta + ":" + ThisPort
	} else {
		ThisServerAddr = ThisCommunnicationProtokoll + "://" + ServiceLocationIDPSMeta + ":" + ThisHttpsPort

	}
}

func loadFromOSEnvKeyCloak() {

	environList := os.Environ()
	leng := len(environList)
	for i := 0; i < leng; i++ {
		if i > 0 {
			osenvparamval := environList[i]
			splittedString := strings.Split(osenvparamval, "=")
			paramname := splittedString[0]
			paramval := splittedString[1]

			if paramname == "FRONTEND_CONS_KCLOAK_DEFAULT" {
				FrontConsKCloakDef = paramval
			}
			if paramname == "FRONTEND_CONS_KCLOAK_COOKI_NAME_CALLER" {
				FrontConsKCloakCookieCaller = paramval
			}
			if paramname == "FRONTEND_CONS_KCLOAK_COOKI_NAME_ACCESTOK" {
				FrontConsKCloakCookieAccessTok = paramval
			}

			if paramname == "FRONTEND_CONS_KCLOAK_COOKI_NAME_REFRESHTOK" {
				FrontConsKCloakCookieRefreshTok = paramval
			}
			if paramname == "FRONTEND_CONS_KCLOAK_COOKI_DOMAIN" {
				FrontConsKCloakDomainCookies = paramval
			}
			if paramname == "FRONTEND_CONS_KCLOAK_COOKI_PATH" {
				FrontConsKCloakPathCookies = paramval
			}
			if paramname == "KCLOAK_SERVER_CLIENT_ID" {
				ServerKCloakClientID = paramval
			}
			if paramname == "KCLOAK_SERVER_CLIENT_SECRET" {
				ServerKCloakClioentSecret = paramval
			}

			if paramname == "KCLOAK_SERVER_CONFIG_URL" {
				ServerKCloakConfigURL = paramval
			}
			if paramname == "KCLOAK_SERVER_CONFIG_URL_SCHEME" {
				ServerKCloakConfigURLScheme = paramval
			}

		}
	}

}

func loadFromOSArgs() {

	servLocaIDPSMeta, servLocaIDSysReg, servLocaIDPSImage, servLocaIDPSQuery, servLocaIDPSCollect, dbserverhostIP :=
		ServiceLocationIDPSMeta, ServiceLocationIDSysReg, ServiceLocationIDPSImage, ServiceLocationIDPSQuery, ServiceLocationIDPSCollect, ThisDbhostIP

	protokoll, port, httpsport, portSysReg, portPsImage, portPsQuery, portPsCollect, confLocation, dbserverPORT := ThisCommunnicationProtokoll,
		ThisPort, ThisHttpsPort, SysRegPort, PortPSImage, PortPSQuery, PortPSCollect, ThisConfLocation, ThisDbhostPORT

	leng := len(os.Args)
	for i := 0; i < leng; i++ {
		if i > 0 {
			osparam := os.Args[i]
			splittedString := strings.Split(osparam, "=")
			var namevalues []string
			namevalues = append(namevalues, splittedString...)
			leng := len(namevalues)
			if leng > 1 {
				paramname := splittedString[0]
				paramval := splittedString[1]

				if paramname == "serviceLocationIDPSMeta" || paramname == "SERVICELOCATIONIDPSMETA" {
					servLocaIDPSMeta = paramval
				}
				if paramname == "serviceLocationIDSysReg" || paramname == "SERVICELOCATIONIDSYSREG" {
					servLocaIDSysReg = paramval
				}
				if paramname == "serviceLocationIDPSImage" || paramname == "SERVICELOCATIONIDPSIMAGE" {
					servLocaIDPSImage = paramval
				}
				if paramname == "serviceLocationIDPSQuery" || paramname == "SERVICELOCATIONIDPSQUERY" {
					servLocaIDPSQuery = paramval
				}
				if paramname == "serviceLocationIDPSCollect" || paramname == "SERVICELOCATIONIDPSCOLLECT" {
					servLocaIDPSCollect = paramval
				}

				if paramname == "port" || paramname == "PORT" || paramname == "Port" ||
					paramname == "port_psmeta" || paramname == "PORT_PSMETA" || paramname == "Port_PSMeta" || paramname == "port_PSMeta" {
					port = paramval
				}
				if paramname == "httpsport" || paramname == "HTTPSPORT" || paramname == "Httpsport" {
					httpsport = paramval
				}
				if paramname == "protokoll" || paramname == "Protokoll" || paramname == "PROTOKOLL" {
					protokoll = paramval
				}
				if paramname == "port_Sysreg" || paramname == "PORT_SYSREG" || paramname == "Port_Sysreg" {
					portSysReg = paramval
				}
				if paramname == "port_Psimage" || paramname == "PORT_PSIMAGE" || paramname == "Port_PsImage" {
					portPsImage = paramval
				}
				if paramname == "port_Psquery" || paramname == "PORT_PSQUERY" || paramname == "Port_PsQuery" {
					portPsQuery = paramval
				}
				if paramname == "port_Pscollect" || paramname == "PORT_PSCOLLECT" || paramname == "Port_PsCollect" {
					portPsCollect = paramval
				}
				if paramname == "confLocation" || paramname == "ConfLocation" ||
					paramname == "conflocation" || paramname == "Conflocation" || paramname == "CONFLOCATION" {
					confLocation = paramval
				}
				if paramname == "dbhostIP" || paramname == "DBHOSTIP" {
					dbserverhostIP = paramval
				}
				if paramname == "dbserverPORT" || paramname == "DBSERVERPORT" {
					dbserverPORT = paramval
				}

			}

		}
	}

	ServiceLocationIDPSMeta, ServiceLocationIDSysReg, ServiceLocationIDPSImage, ServiceLocationIDPSQuery,
		ServiceLocationIDPSCollect, ThisDbhostIP =
		servLocaIDPSMeta, servLocaIDSysReg, servLocaIDPSImage, servLocaIDPSQuery, servLocaIDPSCollect, dbserverhostIP

	ThisCommunnicationProtokoll, ThisPort,
		ThisHttpsPort, SysRegPort, PortPSImage, PortPSQuery, PortPSCollect, ThisConfLocation, ThisDbhostPORT =
		protokoll, port, httpsport, portSysReg, portPsImage, portPsQuery, portPsCollect, confLocation, dbserverPORT

	if ThisCommunnicationProtokoll == "http" {
		ThisServerAddr = ThisCommunnicationProtokoll + "://" + ServiceLocationIDPSMeta + ":" + ThisPort
	} else {
		ThisServerAddr = ThisCommunnicationProtokoll + "://" + ServiceLocationIDPSMeta + ":" + ThisHttpsPort

	}

}

func loadFromOSArgsKeycloak() {

	leng := len(os.Args)
	for i := 0; i < leng; i++ {
		if i > 0 {
			osparam := os.Args[i]
			splittedString := strings.Split(osparam, "=")
			var namevalues []string
			namevalues = append(namevalues, splittedString...)
			leng := len(namevalues)
			if leng > 1 {
				paramname := splittedString[0]
				paramval := splittedString[1]

				if paramname == "FRONTEND_CONS_KCLOAK_DEFAULT" {
					FrontConsKCloakDef = paramval
				}
				if paramname == "FRONTEND_CONS_KCLOAK_COOKI_NAME_CALLER" {
					FrontConsKCloakCookieCaller = paramval
				}
				if paramname == "FRONTEND_CONS_KCLOAK_COOKI_NAME_ACCESTOK" {
					FrontConsKCloakCookieAccessTok = paramval
				}

				if paramname == "FRONTEND_CONS_KCLOAK_COOKI_NAME_REFRESHTOK" {
					FrontConsKCloakCookieRefreshTok = paramval
				}
				if paramname == "FRONTEND_CONS_KCLOAK_COOKI_DOMAIN" {
					FrontConsKCloakDomainCookies = paramval
				}
				if paramname == "FRONTEND_CONS_KCLOAK_COOKI_PATH" {
					FrontConsKCloakPathCookies = paramval
				}
				if paramname == "KCLOAK_SERVER_CLIENT_ID" {
					ServerKCloakClientID = paramval
				}
				if paramname == "KCLOAK_SERVER_CLIENT_SECRET" {
					ServerKCloakClioentSecret = paramval
				}

				if paramname == "KCLOAK_SERVER_CONFIG_URL" {
					ServerKCloakConfigURL = paramval
				}
				if paramname == "KCLOAK_SERVER_CONFIG_URL_SCHEME" {
					ServerKCloakConfigURLScheme = paramval
				}

			}
		}

	}

}

func GetThisServerAddr() {

	loadExternalSettings()
}
