package z_bootstrap

import (
	"encoding/json"
	//"log"
  "errors"
	"os"
)

func GetApplConf() (eConf ApplConfStruct, err error) {
	ApplConf = ApplConfStruct{}
  if len(ThisConfLocation)==0{
		errString:=
     "Achtung !!!  Service konnte nicht gestartet werden."+
		 " Bitte Pfad der Konfigurationsdatei conf.json  über Umgebungsparameter"+
		 " 'confLocation' bzw als Startargument(osparam) 'confLocation=<path>' angeben."
     ////println(errString)
		 return 	ApplConf, errors.New(errString)
	}
	//file, _ := os.Open("z_bootstrap/conf.json") //relativ zum Ausführungspfad
	filePathAndName:=ThisConfLocation+string(os.PathSeparator)+"conf.json"
	file, _ := os.Open(filePathAndName)
	// guinvi_server.exe
	decoder := json.NewDecoder(file)
	err = decoder.Decode(&ApplConf)
	if err != nil {
		return 	ApplConf, err
	}
	return ApplConf,nil

}
