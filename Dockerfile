#FROM scratch
#COPY main /
#ENV  confLocation /epsFileSys/Settings
#ENTRYPOINT [ "./main" ]
#ENTRYPOINT [ "docker-entrypoint.sh" ]


FROM alpine

COPY main /
COPY docker-entrypoint.sh /usr/local/bin/
RUN ln -s /usr/local/bin/docker-entrypoint.sh


# security updates, useful packages/tools
RUN apk update && \
    apk add \
        bash \
        curl \
        wget && \
    rm -rf /var/cache/apk/*

# ENV  confLocation /epsFileSys/Settings

RUN chmod u+x docker-entrypoint.sh

ENTRYPOINT [ "docker-entrypoint.sh" ]

# =================================================
# Exec  go build via:
# CGO_ENABLED=0 GOOS=linux go build -o main
