package main

import (
	"log"
	rest_adapter "ps_meta/g_adapter_rest"
	bootstrap "ps_meta/z_bootstrap"
)

func main() {

	applConf, err := bootstrap.GetApplConf()
	if err != nil {
		log.Fatal(err.Error() + ". Möglicherweise fehlt die conf.json Datei im Pfad " + bootstrap.ThisConfLocation)
		return
	}

	database, imagearchiv, dataprovider, datareader, applLogger = getResources(applConf)
	applLogger.SetLocationLogs(applConf.LocationLogs)

	if applConf.CreateDatabase {
		go SetUpDatabase(applConf, bootstrap.ThisDbhostIP, bootstrap.ThisDbhostPORT)
	}
	SetUpWorkDoneLogsDirPaths(applConf, applLogger)

	//
	if applConf.UseGorillaMuxRouter {
		rest_adapter.RegisterUCsAndRun(database, imagearchiv,
			dataprovider, datareader, applLogger)
	}
	if applConf.UseGinGonicRouter {
		rest_adapter.RegisterUCsAndRunGinGonic(database, imagearchiv,
			dataprovider, datareader, applLogger)
	}

}
