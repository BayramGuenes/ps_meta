package g_adapter_rest

import (
	"errors"
	"net/http"
	"ps_meta/a_domain"
	domain "ps_meta/a_domain"
	uc "ps_meta/b_usecases"
	sec "ps_meta/s_security"
	bootstrap "ps_meta/z_bootstrap"

	//"strings"
	"github.com/gin-gonic/gin"
	//"github.com/gorilla/mux"
)

func importPSHandlerGin(c *gin.Context) {
	c.Header("Content-Type", "application/json")
	c.Header("Access-Control-Allow-Origin", "*")

	//  Get Usecase
	UC_ImportPS := uc.NewPSDataImporter(restServerDataprovider, restServerDatareader, restServerDatabase,
		restServerImageRepo, restServerLogger)

	// 1. HTTP-> Intern
	lLogicalNameImportsystem, lToken, lKindOfAuth, err := readQueryParamImportPS(c)
	if err != nil {
		lErrMsg := domain.ProcessingInfo{}
		logTransactionBuffer := restServerLogger.SetStartTransaction("ImportPSData")
		logTransactionBuffer = restServerLogger.LogStep(logTransactionBuffer, "readQueryParamImportPS", "")
		lErrMsg.DetectingComponentName = "adapter_rest: importPSHandlerGin"
		lErrMsg.InfoDescription = "wrong call rest service"
		logTransactionBuffer = restServerLogger.LogStepResult(logTransactionBuffer, "readQueryParamImportPS", "FAILED", lErrMsg)
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	lUser, lUserRoles, err := uc.GetUsrInfoFromToken(lToken, lKindOfAuth, restServerDatabase.UsrDBPort)
	lCanDataImport := a_domain.CheckAuthority(a_domain.AuthDataImport, lUserRoles, restServerDatabase.AuthDBPort, restServerLogger)
	if !lCanDataImport {
		lErrMsg := domain.ProcessingInfo{}
		logTransactionBuffer := restServerLogger.SetStartTransaction("ImportPSData")
		logTransactionBuffer = restServerLogger.LogStep(logTransactionBuffer, "GetUsr", lUser.Loginname)
		lErrMsg.DetectingComponentName = "adapter_rest: importPSHandlerGin"
		lErrMsg.InfoDescription = "rest servicecall  with not authorised user"
		logTransactionBuffer = restServerLogger.LogStepResult(logTransactionBuffer, "GetUsr", "FAILED", lErrMsg)
		c.AbortWithStatus(http.StatusBadRequest)
		return

	}

	lLoginname := lUser.Loginname

	lServiceResult, ltMsg, ok :=
		UC_ImportPS.ImportPSData(lLoginname, bootstrap.ApplConf.SkipFTPAndReadDataWorkDir, bootstrap.ApplConf.TriggerBuildMeldungImages, bootstrap.ApplConf.ImportWorkDir, bootstrap.ApplConf.QueryXMLFileName, lLogicalNameImportsystem)

	if ok {
		c.JSON(http.StatusOK, lServiceResult)
	} else {
		var contentStr = ""
		for _, v := range ltMsg {
			contentStr += v.InfoDescription
		}
		errmsg := "{ \"PspEsd\":\"\",	\"Message\":\"Import konnte nicht durchgeführt werden.Fehler aufgetreten! \", \"Details\": \"" + contentStr + "\"}"

		c.JSON(http.StatusOK, errmsg)
	}

}

// ------------------------------------------------------------------------- //
//  UC ImportPS
// ------------------------------------------------------------------------- //

// HTTP ->Int
func readQueryParamImportPS(c *gin.Context) (eLogicalNameImportsystem string, eToken string, eKindOfAuth string, err error) {

	/*body, err := ioutil.ReadAll(r.Body)*/

	eLogicalNameImportsystem = ""
	eToken = ""
	err = nil

	eToken, eKindOfAuth = sec.ExtractTokenFromHTTPRequestGin(c)
	eLogicalNameImportsystem = c.Param("LogicalNameImportsystem")

	if eLogicalNameImportsystem == "" {
		////println("LogicalNameImportsystem als Query-Parameter nicht übergeben.")
		err = errors.New("LogicalNameImportsystem als Query-Parameter nicht übergeben.")
	}

	if eToken == "" {
		////println("Sowohl Importusr als auch Token wurden nicht als als Query-Parameter übergeben.")

		err = errors.New("Token wurde nicht als als Query-Parameter übergeben.")
	}

	return eLogicalNameImportsystem, eToken, eKindOfAuth, err

}
