package g_adapter_rest

import (
	//"encoding/json"
	//"errors"
	"net/http"
	uc "ps_meta/b_usecases"
	sec "ps_meta/s_security"

	"github.com/gin-gonic/gin"
)

func getPSDateBeforeHandlerGin(c *gin.Context) {

	if c.Request.Method == "GET" {

		lToken, lKindOfAuth := sec.ExtractTokenFromHTTPRequestGin(c)
		lPSErscheinungsdatum := ""
		lPSErscheinungsdatum = c.Param("Referenzdatum")
		if len(lPSErscheinungsdatum) == 0 {
			c.AbortWithStatus(http.StatusBadRequest)
			return
		}

		UC_GetPSDateBefore := uc.NewPSGetter(restServerDatabase, restServerLogger)

		lRefDate := lPSErscheinungsdatum
		lPSDate, err := UC_GetPSDateBefore.GetDateBeforePS(lToken, lKindOfAuth, lRefDate)

		if err != nil {
			c.AbortWithStatus(http.StatusInternalServerError)
			return
		}

		c.Header("Content-Type", "application/json")
		c.Header("Access-Control-Allow-Origin", "*")

		lPSDateFormatted := formatDateYYYYminMMminDD(lPSDate)
		c.JSON(http.StatusOK, lPSDateFormatted)
		// 3. result intern ->HTTP
	}

}
