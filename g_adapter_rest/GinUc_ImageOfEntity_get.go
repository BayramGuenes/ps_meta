package g_adapter_rest

import (
	//"io/ioutil"
	//"log"

	"encoding/json"
	"net/http"
	"strings"

	domain "ps_meta/a_domain"
	uc "ps_meta/b_usecases"
	syscommanager "ps_meta/h_service_communication_manager"
	bootstrap "ps_meta/z_bootstrap"

	//"time"

	"github.com/gin-gonic/gin"
)

type EntityImageDescriptor struct {
	EIDID                      int
	Entityextid                string
	Entitytype                 string //ps, article,meldung, collection
	RepoPath                   string
	ImageRepoStatus            string "ImageNotFound, transferOK, transferFAILED, nolongerAvailable"
	DateRepoRegistration       string
	TimeRepoRegistration       string
	RepoStatusDetails          string
	MovedToHistoryArchiveLocat string
	DateMoveToHistoryArchiv    string
	TimeMoveToHistoryArchiv    string
	DateRestore                string
	TimeRestore                string
	Psesd                      string
	Melextid                   string
}
type FrontEndReqGetImageColItemlist struct {
	Collection      string
	ItemlistID      string
	Itemlist        uc.CollectionItemList
	WithTabContents bool
	Heading         string
}

type PsImageReqGetImageColItemlist struct {
	Collection               string
	ItemlistID               string
	Itemlist                 uc.CollectionItemList
	WithTabContents          bool
	Heading                  string
	ArticleDataForContentTab []ArticleStruct
}
type ArticleStruct struct {
	Extid     string
	Path      string
	Title     string
	Source    string
	Date      string
	PageCount int
	Valid     bool
}

type GetImageResult struct {
	GetImageOK bool
	Details    string
	ImageInfos EntityImageDescriptor
}

func handleGetImageData(c *gin.Context) {
	c.Header("Content-Type", "application/json")
	c.Header("Access-Control-Allow-Origin", "*")

	ENTITYID := c.Param("ENTITYID")
	ENTITYTYPE := c.Param("ENTITYTYPE")
	resGetPort := syscommanager.ResultGetPortMicroservice{}
	if bootstrap.ApplConf.UsePSSysregAsRegistrator {
		resGetPort = syscommanager.ApplServiceCom.GetPortMicroservice("ps_image")
	} else {
		resGetPort.Port = bootstrap.PortPSImage
	}
	redirectServiceGetImage := ":" + resGetPort.Port + "/ImageData/Entity/" + ENTITYTYPE + "/" + ENTITYID

	getImageResult := GetImageResult{}
	getImageResult, err := GinCallImageService(redirectServiceGetImage)
	if err != nil {
		getImageResult.GetImageOK = false
		getImageResult.Details = "Error Call GinCallRestService ps_image: " + err.Error()
	}

	c.Header("Content-Type", "application/json")
	c.Header("Access-Control-Allow-Origin", "*")

	c.JSON(http.StatusOK, getImageResult)

}
func handleGetPSMeldungImage(c *gin.Context) {
	c.Header("Content-Type", "application/json")
	c.Header("Access-Control-Allow-Origin", "*")

	ENTITYID := c.Param("MELEXTID")
	PSESD := c.Param("PSESD")
	//		//println("PSESD:"+PSESD)

	if strings.Contains(PSESD, "-") {
		PSESD = strings.Replace(PSESD, "-", "", -1)
		PSESD = strings.Replace(PSESD, " ", "", -1)
	}
	resGetPort := syscommanager.ResultGetPortMicroservice{}
	if bootstrap.ApplConf.UsePSSysregAsRegistrator {
		resGetPort = syscommanager.ApplServiceCom.GetPortMicroservice("ps_image")
	} else {
		resGetPort.Port = bootstrap.PortPSImage
	}
	redirectServiceGetImage := ":" + resGetPort.Port + "/ImageData/MeldungImage/" + PSESD + "/" + ENTITYID

	println("20200608 redirectServiceGetImage :" + redirectServiceGetImage)
	getImageResult := GetImageResult{}
	getImageResult, err := GinCallImageService(redirectServiceGetImage)
	if err != nil {
		getImageResult.GetImageOK = false
		getImageResult.Details = "Error Call GinCallRestService ps_image: " + err.Error()
	}

	if getImageResult.GetImageOK &&
		getImageResult.ImageInfos.ImageRepoStatus != "transferOK" {
		//println("CALL BUILD MELDUNG_IMAGE")
		buildMeldungImageService := ":" + resGetPort.Port + "/MeldungImage/Build/" + PSESD + "/" + ENTITYID
		getBuildImageResult := GetImageResult{}
		getBuildImageResult, err := GinCallBuildMeldungImageService(buildMeldungImageService)
		if err != nil {
			getBuildImageResult.GetImageOK = false
			getBuildImageResult.Details = "Error Call GinCallBuildMeldungImageService ps_image: " + err.Error()
		}
		getImageResult = getBuildImageResult
	}

	c.Header("Content-Type", "application/json")
	c.Header("Access-Control-Allow-Origin", "*")

	c.JSON(http.StatusOK, getImageResult)

}
func handleGetImageDataArtItemlist(c *gin.Context) {
	//println(" handleGetImageDataArtItemlist CALLED")
	resGetPort := syscommanager.ResultGetPortMicroservice{}
	if bootstrap.ApplConf.UsePSSysregAsRegistrator {
		resGetPort = syscommanager.ApplServiceCom.GetPortMicroservice("ps_image")
	} else {
		resGetPort.Port = bootstrap.PortPSImage
	}
	redirectServiceGetImage := ":" + resGetPort.Port + "/CollItemlistImage/Build"

	getImageResult := GetImageResult{}
	getImageResult.GetImageOK = true

	requestBody, _ := c.GetRawData()
	requestParamFrontend := FrontEndReqGetImageColItemlist{}
	err := json.Unmarshal(requestBody, &requestParamFrontend)
	if err != nil {
		getImageResult.GetImageOK = false
		getImageResult.Details = "Error json.Unmarshal: " + err.Error()
	}
	requestParamPsImage := PsImageReqGetImageColItemlist{
		Collection:               requestParamFrontend.Collection,
		ItemlistID:               requestParamFrontend.ItemlistID,
		Itemlist:                 requestParamFrontend.Itemlist,
		WithTabContents:          requestParamFrontend.WithTabContents,
		Heading:                  requestParamFrontend.Heading,
		ArticleDataForContentTab: []ArticleStruct{},
	}
	if requestParamPsImage.WithTabContents {
		for i := 0; i < len(requestParamFrontend.Itemlist); i++ {
			lItem := requestParamFrontend.Itemlist[i]
			ArtExtId := lItem.ItemIdField1
			ArtESD := lItem.ItemIdField2
			lArtikel := domain.Artikel{}
			err := *new(error)
			UC_GetArt := uc.NewArticleGetter(restServerDatabase, restServerLogger)
			lArtikel, err = UC_GetArt.GetArtViaIDAndESD(ArtExtId, ArtESD)
			if err != nil {
				getImageResult.GetImageOK = false
				getImageResult.Details = "Error Call UC_GetArt.GetArtViaIDAndESD: " + err.Error()
				break
			}
			lArtikelFormatted := formatArticle(lArtikel)
			lArticleImage := ArticleStruct{
				Extid:     lArtikel.ExtID,
				Path:      "",
				Title:     lArtikelFormatted.Title,
				Source:    lArtikelFormatted.Source,
				Date:      ArtESD,
				PageCount: 0,
				Valid:     false,
			}
			requestParamPsImage.ArticleDataForContentTab =
				append(requestParamPsImage.ArticleDataForContentTab, lArticleImage)

		}
	}
	if err == nil {
		requestBody, err = json.Marshal(requestParamPsImage)
		if err != nil {
			getImageResult.GetImageOK = false
			getImageResult.Details = "Error Call json.Marshal(requestParamPsImage): " + err.Error()
		}
		if err == nil {
			getImageResult, err = GinCallImgServColItemlist(redirectServiceGetImage, requestBody)

			if err != nil {
				getImageResult.GetImageOK = false
				getImageResult.Details = "Error Call GinCallRestService ps_image: " + err.Error()
			}
		}
	}
	c.Header("Content-Type", "application/json")
	c.Header("Access-Control-Allow-Origin", "*")
	println("20200610 getImageResult:" + getImageResult.ImageInfos.RepoPath)
	c.JSON(http.StatusOK, getImageResult)

}
func formatDDdotMMdotYYYY(iDate string) string {
	return iDate
}
