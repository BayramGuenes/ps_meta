package g_adapter_rest

import (
	"fmt"
	"net/http"
	domain "ps_meta/a_domain"
	uc "ps_meta/b_usecases"
	sec "ps_meta/s_security"
	"strings"

	"github.com/gin-gonic/gin"
)

func getMelCollHandlerGin(c *gin.Context) {

	lToken, lKindOfAuth := sec.ExtractTokenFromHTTPRequestGin(c)
	lESD := c.Param("Erscheinungsdatum")
	lESD = formatDateYYYYMMDD(lESD)

	lDisplayOnlyRubrics := false
	if strings.Contains(c.Request.URL.Path, "DisplayOnlyRubrics") {
		lDisplayOnlyRubrics = true
	}

	if len(lESD) == 0 {

		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	UC_GetMelCollection := uc.NewMeldungCollGetter(restServerDatabase, restServerLogger)

	// 2. Exec domain function

	////println("lESD:"+lESD)
	lPSHeader, lMelCollection, err := UC_GetMelCollection.GetMeldungCollection(lToken, lKindOfAuth, lESD)
	if err != nil {
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	// 3. result intern ->HTTP
	if lDisplayOnlyRubrics {
		GinWriteHTTPResponseRubrColl(lPSHeader, lMelCollection, c)
	} else {
		//println("GinWriteHTTPResponseMelColl" + lToken)
		GinWriteHTTPResponseMelColl(lToken, lKindOfAuth, lPSHeader, lMelCollection, c)
	}

}

// Result Int ->HTTP
func GinWriteHTTPResponseMelColl(iToken string, iKindOfAuth string, ePSHeader domain.Pressespiegel, eMelColl []domain.Meldung, c *gin.Context) {

	var lTopicsServiceOutput TopicsServiceOutput
	lTopicsServiceOutput.Id = ePSHeader.PSExtID
	lTopicsServiceOutput.Importsystem = ePSHeader.LogicalNameImportsystem
	if ePSHeader.IsActivated {
		lTopicsServiceOutput.Status = "aktiv"
	} else {
		lTopicsServiceOutput.Status = "inaktiv"
	}
	////println("lTopicsServiceOutput.Importsystem:" + lTopicsServiceOutput.Importsystem)

	serviceDateBefore := "/ps_meta/PSDateBeforeService/Referenzdatum/" + ePSHeader.PSESD

	esdBeforeInterface, _ := GinCallRestService(serviceDateBefore, iToken, iKindOfAuth)

	esdBefore := fmt.Sprintf("%v", esdBeforeInterface)
	esdBefore = strings.Replace(esdBefore, "\"", "", -1)

	lTopicsServiceOutput.Prev = esdBefore

	serviceDateNext := "/ps_meta/PSDateNextService/Referenzdatum/" + ePSHeader.PSESD

	esdNextInterface, _ := GinCallRestService(serviceDateNext, iToken, iKindOfAuth)
	esdNext := fmt.Sprintf("%v", esdNextInterface)
	esdNext = strings.Replace(esdNext, "\"", "", -1)
	lTopicsServiceOutput.Next = esdNext

	lTopicsServiceOutput.Rubrics = getRubricsAndMelContents(eMelColl)

	c.Header("Content-Type", "application/json")
	c.Header("Access-Control-Allow-Origin", "*")

	c.JSON(http.StatusOK, lTopicsServiceOutput)

}

// Result Int ->HTTP
func GinWriteHTTPResponseRubrColl(ePSHeader domain.Pressespiegel, eMelColl []domain.Meldung, c *gin.Context) {

	lRubricServiceOutput := getRubricList(eMelColl)

	c.Header("Content-Type", "application/json")
	c.Header("Access-Control-Allow-Origin", "*")

	c.JSON(http.StatusOK, lRubricServiceOutput)

}
