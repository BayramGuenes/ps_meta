package g_adapter_rest

import (
	"encoding/json"
	"errors"
	"net/http"
	domain "ps_meta/a_domain"
	uc "ps_meta/b_usecases"
	"strings"

	"github.com/gorilla/mux"
)

//import domain "ps_meta/a_domain"

// HTTP ->Int
func (a Adapter) readHTTPQueryParamMeldung(r *http.Request) (ePSExtId string, eMelExtId string, err error) {

	psextid := ""
	melextid := ""

	err = nil
	vars := mux.Vars(r)

	for i, v := range vars {
		switch i {

		case "PSExtId":
			psextid = strings.SplitAfter(v, "=")[1]
			break
		case "MelExtId":
			melextid = strings.SplitAfter(v, "=")[1]
		}
		////println("readHTTPQueryParamMeldung",i, v)
	}


	if psextid == "" {
		err = errors.New("PSExtId  als Query-Parameter nicht übergeben.")
	}
	if melextid == "" {
		err = errors.New("MelExtId als Query-Parameter nicht übergeben.")
	}
	ePSExtId = psextid
	eMelExtId = melextid
	return

}

// Result Int ->HTTP
func (a Adapter) writeHTTPResponseMeldung(iMeldung domain.Meldung, w http.ResponseWriter) error {

	//	b, err := json.Marshal(eMelColl)
	b, err := json.Marshal(iMeldung)

	if err != nil {
		return err
	}

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")

	w.Write(b)
	return nil
}

func (a Adapter) MakeMeldungHandler(iMeldungClass uc.MeldungClass) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		////println(" MakeMelCollHandler called")
		// 1. HTTP-> Intern
		lpsextid, lmelextid, err := a.readHTTPQueryParamMeldung(r)

		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		// 2. Exec domain function
		lMeldung, err := iMeldungClass.GetMeldung(lpsextid, lmelextid)

		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		////println("lPSHeader:" + lPSHeader.PSExtID)
		/*for i, v := range lMelCollection {
			//println("i=" + strconv.Itoa(i))
			//println("v=" + v.MelExtID)
		}*/

		// 3. result intern ->HTTP
		if err = a.writeHTTPResponseMeldung(lMeldung, w); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
		}

	}
}
