package g_adapter_rest

import (
	"encoding/json"
	"net/http"
	domain "ps_meta/a_domain"
	uc "ps_meta/b_usecases"
	sec "ps_meta/s_security"
	//utils "ps_meta/u_utils"
)

// ------------------------------------------------------------------------- //
// UC Getusr
// ------------------------------------------------------------------------- //
// HTTP ->Int
func (a Adapter) readHTTPFormParamCalendar(r *http.Request) (eDateFrom, eDateTo string, err error) {

	eDateFrom = r.FormValue("DateFrom")
	eDateTo = r.FormValue("DateTo")

	err = nil

	return

}

// Result Int ->HTTP
func (a Adapter) writeHTTPResponseCalendar(iCalendar domain.Calendar, w http.ResponseWriter) error {

	var outtype domain.Calendar
	outtype = iCalendar
	//b, err := json.Marshal(eUsr)
	b, err := json.Marshal(outtype)

	if err != nil {
		return err
	}

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	////println("usr:" + string(b))
	w.Write(b)
	return nil
}

func (a Adapter) MakeCalendarGetHandler(iCalendarClass uc.CalendarClass) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// 1. HTTP-> Intern
		lToken := sec.ExtractTokenFromHTTPRequest(r)
		lFromDate, lToDate, _ := a.readHTTPFormParamCalendar(r)

		lKindOfAuth := ""
		lCalendar, err := iCalendarClass.GetCalendar(lToken, lKindOfAuth, lFromDate, lToDate)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		// 3. result intern ->HTTP
		if err = a.writeHTTPResponseCalendar(lCalendar, w); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
		}

	}
}
