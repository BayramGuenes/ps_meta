package g_adapter_rest

import (
	"encoding/json"
	"net/http"
	"ps_meta/a_domain"
	uc "ps_meta/b_usecases"
	sec "ps_meta/s_security"
)

// Result Int ->HTTP
func (a Adapter) writeHTTPResponseDateLast(psESD string, w http.ResponseWriter) error {
	date := formatDateYYYYminMMminDD(psESD)
	b, err := json.Marshal(date)
	if err != nil {
		return err
	}

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")

	w.Write(b)
	return nil
}

func (a Adapter) MakePSGetDateLastHandler(iPSEntityClass uc.PSEntityClass) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		// 1. HTTP-> Intern
		lToken := sec.ExtractTokenFromHTTPRequest(r)

		lWithInactiv := false
		if len(lToken) > 0 {
			lUser, err := iPSEntityClass.Repository.UsrDBPort.GetUsrViaToken(lToken)
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
			lUserRoles, err := iPSEntityClass.Repository.UsrDBPort.GetUsrRoles(lUser.Loginname, lUser.Rfa) //GetUsrRoles
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
			lWithInactiv = a_domain.CheckAuthority(a_domain.AuthSearchWithInactive, lUserRoles, iPSEntityClass.Repository.AuthDBPort, iPSEntityClass.Logger)
		}
		println(lWithInactiv)
		// 2. Exec domain function
		lPSDate, err := iPSEntityClass.GetDateLastPS(lToken, "")
		//lPSDate = strings.Replace(lPSDate, "\"", "", -1)

		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		// 3. result intern ->HTTP
		if err = a.writeHTTPResponseDateNext(lPSDate, w); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
		}

	}
}
