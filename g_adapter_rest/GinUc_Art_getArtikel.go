package g_adapter_rest

import (
	//"encoding/json"
	//"errors"
	"net/http"
	domain "ps_meta/a_domain"

	uc "ps_meta/b_usecases"

	"github.com/gin-gonic/gin"
)

func getArtHandlerGin(c *gin.Context) {
	lResult := ResponseStructGetSingleArticleData{}
	lResult.ProcessOK = true

	PSExtId := c.Param("PSExtId")
	ArtExtId := c.Param("ArtExtId")
	ArtESD := c.Param("ArtESD")

	/*if len(PSExtId) == 0 || len(ArtExtId) == 0 {
		lResult.ProcessOK = false
		lResult.Details = "missing param, check PSExtId and ArtExtId"
	}*/
	if len(PSExtId) > 0 && len(ArtExtId) > 0 {
		lArtikel := domain.Artikel{}
		err := *new(error)
		if lResult.ProcessOK {
			UC_GetArt := uc.NewArticleGetter(restServerDatabase, restServerLogger)
			lArtikel, err = UC_GetArt.GetArticle(PSExtId, ArtExtId)
			if err != nil {
				lResult.ProcessOK = false
				lResult.Details = "UC_GetArt.GetArticle: " + err.Error()
			}
		}
		lResult.ArtikelData = formatArticle(lArtikel)
	} else if len(ArtESD) > 0 && len(ArtExtId) > 0 {
		lArtikel := domain.Artikel{}
		err := *new(error)
		if lResult.ProcessOK {
			UC_GetArt := uc.NewArticleGetter(restServerDatabase, restServerLogger)
			lArtikel, err = UC_GetArt.GetArtViaIDAndESD(ArtExtId, ArtESD)
			if err != nil {
				lResult.ProcessOK = false
				lResult.Details = "UC_GetArt.GetArtViaIDAndESD: " + err.Error()
			}
		}
		lResult.ArtikelData = formatArticle(lArtikel)
	} else {
		lResult.ProcessOK = false
		lResult.Details = "missing param, check PSExtId and ArtExtId"
	}
	c.Header("Content-Type", "application/json")
	c.Header("Access-Control-Allow-Origin", "*")
	c.JSON(http.StatusOK, lResult) // 3. result intern ->HTTP

}

/*type ArtContentsService struct {
	Id         string
	Source     string
	Date       string
	Title      string
	Othertitle string
	Subtitle   string
	Teaser     string
	IsDictated string
	Pagenrtext string
	Text       string
}*/
