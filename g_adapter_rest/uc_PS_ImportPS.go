package g_adapter_rest

import (
	"errors"
	"net/http"
	domain "ps_meta/a_domain"
	uc "ps_meta/b_usecases"
	sec "ps_meta/s_security"
	bootstrap "ps_meta/z_bootstrap"
	"strings"

	"github.com/gorilla/mux"
)

// ------------------------------------------------------------------------- //
//  UC ImportPS
// ------------------------------------------------------------------------- //

// HTTP ->Int
func (a Adapter) readQueryParamImportPS(r *http.Request) (eLogicalNameImportsystem string, eImportusr string, eRFA string, ePWD string, eToken string, err error) {

	/*body, err := ioutil.ReadAll(r.Body)*/

	eLogicalNameImportsystem = ""
	eImportusr = ""
	ePWD = ""
	eToken = ""
	err = nil
	eRFA = ""

	eToken = sec.ExtractTokenFromHTTPRequest(r)

	vars := mux.Vars(r)
	for i, v := range vars {
		switch i {

		case "LogicalNameImportsystem":
			lstringTab := strings.SplitAfter(v, "=")
			if len(lstringTab) > 1 {
				eLogicalNameImportsystem = lstringTab[1]
			} else {
				eLogicalNameImportsystem = v
			}
		case "Importusr":
			eImportusr = strings.SplitAfter(v, "=")[1]
		case "UsrPwd":
			ePWD = strings.SplitAfter(v, "=")[1]
		case "RFA":
			eRFA = strings.SplitAfter(v, "=")[1]

		}

		////println(i, v)
	}

	/*
		lLogicalNameImportsystemKV, _ := mux.Vars(r)["LogicalNameImportsystem"]
		eLogicalNameImportsystem = strings.SplitAfter(lLogicalNameImportsystemKV, "=")[1]
		lImportusrKV, _ := mux.Vars(r)["Importusr"]
		eImportusr = strings.SplitAfter(lImportusrKV, "=")[1]
		lPWDKV, _ := mux.Vars(r)["UsrPwd"]
		ePWD = strings.SplitAfter(lPWDKV, "=")[1]
		lRFAKV, _ := mux.Vars(r)["RFA"]
		eRFA = strings.SplitAfter(lRFAKV, "=")[1]
	*/
	if eLogicalNameImportsystem == "" {

		err = errors.New("LogicalNameImportsystem als Query-Parameter nicht übergeben.")
	}

	if eImportusr == "" && eToken == "" {

		err = errors.New("Sowohl Importusr als auch Token wurden nicht als als Query-Parameter übergeben.")
	}

	if eToken == "" && (eImportusr == "" || ePWD == "" || eRFA == "") {

		err = errors.New("Query-Parameter 'Importusr' oder 'RFA' oder 'UsrPwd' wurde nicht übergeben oder ist initial.")
	}

	return eLogicalNameImportsystem, eImportusr, eRFA, ePWD, eToken, err

}

//  (a Adapter) MakePSImportHandler ...

func (a Adapter) MakePSImportHandler(eImportPSObject uc.PSDataImporterClass) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		// 1. HTTP-> Intern
		lLogicalNameImportsystem, lImportusr, lRFA, lPWD, lToken, err := a.readQueryParamImportPS(r)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		// check user has import rights
		lUser := domain.User{}
		if len(lToken) > 0 {
			lUser, err = eImportPSObject.Repository.UsrDBPort.GetUsrViaToken(lToken)
			if err != nil {
				w.WriteHeader(http.StatusBadRequest)
				return
			}
			lImportusr = lUser.Loginname
		} else {
			lUser, err = eImportPSObject.Repository.UsrDBPort.GetUsr(lImportusr, lRFA)
			if err != nil || lUser.Pwd != lPWD {
				w.WriteHeader(http.StatusBadRequest)
				return
			}
		}

		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("Access-Control-Allow-Origin", "*")

		////println("MakePSImportHandler:lImportusr" + lImportusr)

		if len(lRFA) == 0 {
			lRFA = lUser.Rfa
		}
		lServiceResult, ltMsg, ok :=
			eImportPSObject.ImportPSData(lUser.Loginname, bootstrap.ApplConf.SkipFTPAndReadDataWorkDir, bootstrap.ApplConf.TriggerBuildMeldungImages, bootstrap.ApplConf.ImportWorkDir, bootstrap.ApplConf.QueryXMLFileName, lLogicalNameImportsystem)

		content := resultToJson(lServiceResult, ltMsg, ok)

		w.Write(content)

	}
}
