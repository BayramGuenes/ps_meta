package g_adapter_rest

import (
	"net/http"
	"net/url"
	domain "ps_meta/a_domain"
	uc "ps_meta/b_usecases"
	sec "ps_meta/s_security"
	"strings"

	"github.com/gin-gonic/gin"
)

func getAuthKeysHandlerGin(c *gin.Context) {
	c.Header("Content-Type", "application/json")
	c.Header("Access-Control-Allow-Origin", "*")

	UsrRoles, _ := url.QueryUnescape(c.Param("UsrRoles"))
	UsrRolesArr := strings.Split(UsrRoles, ",")
	authSet := []string{}
	for i := 0; i < len(UsrRolesArr); i++ {
		lAuthObjSet, _ := restServerDatabase.AuthDBPort.GetAuthSet(UsrRolesArr[i])
		for j := 0; j < len(lAuthObjSet); j++ {
			authSet = append(authSet, lAuthObjSet[j])
		}
	}

	outtype := strings.Join(authSet, ",")

	c.JSON(http.StatusOK, outtype)
}

func updateUsrHandlerGin(c *gin.Context) {
	c.Header("Content-Type", "application/json")
	c.Header("Access-Control-Allow-Origin", "*")

	lUsr := domain.User{}
	lUsr.Loginname = c.PostForm("PSUsr.Loginname")
	lUsr.Name = c.PostForm("PSUsr.Name")
	lUsr.Pwd = c.PostForm("PSUsr.Pwd")
	lUsr.Email = c.PostForm("PSUsr.Email")
	lUsr.Rfa = c.PostForm("PSUsr.Rfa")
	if len(lUsr.Loginname) == 0 || len(lUsr.Rfa) == 0 {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	UC_UpdateUsr := uc.NewUsrClassInstanceGetter(restServerDatabase, restServerLogger)
	// 2. Exec domain function
	_, err := UC_UpdateUsr.UpdateUsr(lUsr)
	if err != nil {
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	//lUsrObj, err := a.readHTTPQueryParamPOST(r)

	lUser, lAuthkeys, err := UC_UpdateUsr.GetUsr(lUsr.Loginname, lUsr.Rfa, lUsr.Email)
	if len(lAuthkeys) > 0 {
		//println(" lAuthkeys:" + lAuthkeys[0])
	}
	if err != nil {
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}
	// 3. result intern ->HTTP
	lUserToken, lSavedUserToken, err := UC_UpdateUsr.Repository.GetUsrToken(lUser.Loginname, lUser.Rfa)
	if err == nil {
		if !lSavedUserToken {
			lValidFrom, lValidTo := sec.GetValidFromTo()

			err = UC_UpdateUsr.Repository.UsrDBPort.SetUserToken(lUser.Loginname, lUser.Rfa, lUserToken.Token, lValidFrom, lValidTo)
			if err == nil {
				lUserToken.Loginname = lUser.Loginname
				lUserToken.Rfa = lUser.Rfa
				lUserToken.Validdatefrom = lValidFrom
				lUserToken.Validdateto = lValidTo
			}

		}

	}

	var outtype UsrOutput
	outtype.User = lUser
	outtype.UserToken = lUserToken
	outtype.UserAuthkeys = lAuthkeys
	c.JSON(http.StatusOK, outtype)

}

func createUsrHandlerGin(c *gin.Context) {
	////println("MakeUsrCreateHandler called")
	// 1. HTTP-> Intern
	c.Header("Content-Type", "application/json")
	c.Header("Access-Control-Allow-Origin", "*")

	lUser := domain.User{}
	lUser.Loginname = c.PostForm("PSUsr.Loginname")
	lUser.Name = c.PostForm("PSUsr.Name")
	lUser.Pwd = c.PostForm("PSUsr.Pwd")
	lUser.Email = c.PostForm("PSUsr.Email")
	lUser.Rfa = c.PostForm("PSUsr.Rfa")
	if len(lUser.Loginname) == 0 || len(lUser.Rfa) == 0 {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}
	UC_CreateUsr := uc.NewUsrClassInstanceGetter(restServerDatabase, restServerLogger)

	// 2. Exec domain function
	_, err := UC_CreateUsr.CreateUsr(lUser.Loginname, lUser.Rfa, lUser.Name, lUser.Pwd, lUser.Email, "web")
	if err != nil {
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	// 3. result intern ->HTTP
	lUserToken, lSavedUserToken, err := UC_CreateUsr.Repository.GetUsrToken(lUser.Loginname, lUser.Rfa)
	if err == nil {
		if !lSavedUserToken {
			lValidFrom, lValidTo := sec.GetValidFromTo()

			err = UC_CreateUsr.Repository.UsrDBPort.SetUserToken(lUser.Loginname, lUser.Rfa, lUserToken.Token, lValidFrom, lValidTo)
			if err == nil {
				lUserToken.Loginname = lUser.Loginname
				lUserToken.Rfa = lUser.Rfa
				lUserToken.Validdatefrom = lValidFrom
				lUserToken.Validdateto = lValidTo
			}

		}

	}

	var outtype UsrOutput
	outtype.User = lUser
	outtype.UserToken = lUserToken
	outtype.UserAuthkeys = domain.UserAuthKeys{}

	c.JSON(http.StatusOK, outtype)

}

func existsEmailHandlerGin(c *gin.Context) {
	c.Header("Content-Type", "application/json")
	c.Header("Access-Control-Allow-Origin", "*")

	lUsr := domain.User{}
	lUsr.Loginname = c.PostForm("PSUsr.Loginname")
	lUsr.Name = c.PostForm("PSUsr.Name")
	lUsr.Pwd = c.PostForm("PSUsr.Pwd")
	lUsr.Email = c.PostForm("PSUsr.Email")
	lUsr.Rfa = c.PostForm("PSUsr.Rfa")
	if len(lUsr.Loginname) == 0 || len(lUsr.Rfa) == 0 {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	UC_CheckEmail := uc.NewUsrClassInstanceGetter(restServerDatabase, restServerLogger)

	// 2. Exec domain function
	lUsrObj, lAuthkeys, err := UC_CheckEmail.GetUsr(lUsr.Loginname, lUsr.Rfa, lUsr.Email)
	//fmt.//println("Lusr-rfa:" + lUsrObj.Name)

	if err != nil {
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	// 3. result intern ->HTTP

	c.Header("Content-Type", "application/json")
	c.Header("Access-Control-Allow-Origin", "*")

	// 3. result intern ->HTTP
	lUsrToken := domain.UserToken{}
	var outtype UsrOutput
	outtype.User = lUsrObj
	outtype.UserToken = lUsrToken
	outtype.UserAuthkeys = lAuthkeys

	c.JSON(http.StatusOK, outtype)
	// 3. result intern ->HTTP
}

func existsLoginnameHandlerGin(c *gin.Context) {
	c.Header("Content-Type", "application/json")
	c.Header("Access-Control-Allow-Origin", "*")

	lUsr := domain.User{}
	lUsr.Loginname = c.PostForm("PSUsr.Loginname")
	lUsr.Name = c.PostForm("PSUsr.Name")
	lUsr.Pwd = c.PostForm("PSUsr.Pwd")
	lUsr.Email = c.PostForm("PSUsr.Email")
	lUsr.Rfa = c.PostForm("PSUsr.Rfa")
	if len(lUsr.Loginname) == 0 || len(lUsr.Rfa) == 0 {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}
	UC_CheckLoginname := uc.NewUsrClassInstanceGetter(restServerDatabase, restServerLogger)

	// 2. Exec domain function
	lUsrObj, lAuthkeys, err := UC_CheckLoginname.GetUsr(lUsr.Loginname, lUsr.Rfa, lUsr.Email)
	//fmt.//println("Lusr-rfa:" + lUsrObj.Name)

	if err != nil {
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	// 3. result intern ->HTTP
	lUsrToken := domain.UserToken{}
	var outtype UsrOutput
	outtype.User = lUsrObj
	outtype.UserToken = lUsrToken
	outtype.UserAuthkeys = lAuthkeys
	c.JSON(http.StatusOK, outtype)
	// 3. result intern ->HTTP

}

func getLoginHandlerGin(c *gin.Context) {
	c.Header("Content-Type", "application/json")
	c.Header("Access-Control-Allow-Origin", "*")

	lUsrObj := domain.User{}
	lUsrObj.Loginname = c.PostForm("PSUsr.Loginname")
	lUsrObj.Name = c.PostForm("PSUsr.Name")
	lUsrObj.Pwd = c.PostForm("PSUsr.Pwd")
	lUsrObj.Email = c.PostForm("PSUsr.Email")
	lUsrObj.Rfa = c.PostForm("PSUsr.Rfa")
	if len(lUsrObj.Loginname) == 0 || len(lUsrObj.Rfa) == 0 {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}
	UC_LoginUsr := uc.NewUsrClassInstanceGetter(restServerDatabase, restServerLogger)

	lUser, lUserToken, lUserAuthkeys, err := UC_LoginUsr.LoginUsr(lUsrObj.Loginname, lUsrObj.Rfa, lUsrObj.Email)
	if err != nil {
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	var outtype UsrOutput
	outtype.User = lUser
	outtype.UserToken = lUserToken
	outtype.UserAuthkeys = lUserAuthkeys
	c.JSON(http.StatusOK, outtype)
	// 3. result intern ->HTTP

}

func getUsrHandlerGin(c *gin.Context) {
	if c.Request.Method == "POST" {
		c.Header("Content-Type", "application/json")
		c.Header("Access-Control-Allow-Origin", "*")

		lUsrObj := domain.User{}
		lUsrObj.Loginname = c.PostForm("PSUsr.Loginname")
		lUsrObj.Name = c.PostForm("PSUsr.Name")
		lUsrObj.Pwd = c.PostForm("PSUsr.Pwd")
		lUsrObj.Email = c.PostForm("PSUsr.Email")
		lUsrObj.Rfa = c.PostForm("PSUsr.Rfa")

		if len(lUsrObj.Loginname) == 0 || len(lUsrObj.Rfa) == 0 {
			c.AbortWithStatus(http.StatusBadRequest)
			return
		}

		UC_GetUsr := uc.NewUsrClassInstanceGetter(restServerDatabase, restServerLogger)

		lUser, lUserAuthkeys, err := UC_GetUsr.GetUsr(lUsrObj.Loginname, lUsrObj.Rfa, lUsrObj.Email)
		if err != nil {
			c.AbortWithStatus(http.StatusInternalServerError)
			return
		}
		// 3. result intern ->HTTP
		//lUserToken := domain.UserToken{}
		lUserToken, lSavedUserToken, err := UC_GetUsr.Repository.GetUsrToken(lUser.Loginname, lUser.Rfa)
		if err == nil {
			if !lSavedUserToken {
				lValidFrom, lValidTo := sec.GetValidFromTo()

				err = UC_GetUsr.Repository.UsrDBPort.SetUserToken(lUser.Loginname, lUser.Rfa, lUserToken.Token, lValidFrom, lValidTo)
				if err == nil {
					lUserToken.Loginname = lUser.Loginname
					lUserToken.Rfa = lUser.Rfa
					lUserToken.Validdatefrom = lValidFrom
					lUserToken.Validdateto = lValidTo
				}

			}

		}

		var outtype UsrOutput
		outtype.User = lUser
		outtype.UserToken = lUserToken
		outtype.UserAuthkeys = lUserAuthkeys
		c.JSON(http.StatusOK, outtype)

	}
}

func getUsrViaTokenHandlerGin(c *gin.Context) {
	c.Header("Content-Type", "application/json")
	c.Header("Access-Control-Allow-Origin", "*")

	lToken, lKindOfAuth := sec.ExtractTokenFromHTTPRequestGin(c)
	err := *new(error)

	lUser := domain.User{}
	lUserToken := domain.UserToken{}
	lAuthkeys := domain.UserAuthKeys{}

	UC_GetUsrViaToken := uc.NewUsrClassInstanceGetter(restServerDatabase, restServerLogger)

	lUser, lAuthkeys, err = UC_GetUsrViaToken.GetUsrViaToken(lToken, lKindOfAuth)

	if err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	lUserToken, lSavedUserToken, err := UC_GetUsrViaToken.Repository.GetUsrToken(lUser.Loginname, lUser.Rfa)
	if err == nil {
		if !lSavedUserToken {
			lValidFrom, lValidTo := sec.GetValidFromTo()

			err = UC_GetUsrViaToken.Repository.UsrDBPort.SetUserToken(lUser.Loginname, lUser.Rfa, lUserToken.Token, lValidFrom, lValidTo)
			if err == nil {
				lUserToken.Loginname = lUser.Loginname
				lUserToken.Rfa = lUser.Rfa
				lUserToken.Validdatefrom = lValidFrom
				lUserToken.Validdateto = lValidTo
			}
		}
	}
	if err != nil {
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	var outtype UsrOutput
	outtype.User = lUser
	outtype.UserToken = lUserToken
	outtype.UserAuthkeys = lAuthkeys

	c.JSON(http.StatusOK, outtype)

}
