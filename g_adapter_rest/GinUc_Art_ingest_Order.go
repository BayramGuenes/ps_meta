package g_adapter_rest

import (
	"net/http"
	uc "ps_meta/b_usecases"

	"github.com/gin-gonic/gin"
)

func ingestOrderHandlerGin(c *gin.Context) {

	PSESD := c.Param("PSESD")
	PSESDFROM := c.Param("PSESDFROM")
	PSESDTO := c.Param("PSESDTO")
	ARTEXTID := c.Param("ARTEXTID")
	INGESTOP := c.Param("INGESTOP")
	if len(INGESTOP) == 0 {
		c.JSON(http.StatusOK, "Parameter INGESTOP not provided. No DataTransfer")
		return
	}
	if len(PSESD) == 0 && (len(PSESDFROM) == 0 || len(PSESDTO) == 0) {
		c.JSON(http.StatusOK, "Parameter PSESD respectively  PSESDFROM or  PSESDTO not provided. No DataTransfer")
		return
	}

	if len(PSESD) > 0 && len(ARTEXTID) == 0 {
		orderBuildResult := uc.BuildIngestOrder(restServerDatabase, restServerLogger, PSESD, INGESTOP, 1)
		c.JSON(http.StatusOK, orderBuildResult)
		return
	}
	if len(PSESDFROM) > 0 && len(PSESDTO) > 0 {
		orderBuildResult := uc.BuildIngestOrderInterval(restServerDatabase, restServerLogger, PSESDFROM, PSESDTO, INGESTOP)
		c.JSON(http.StatusOK, orderBuildResult)
		return
	}
	if len(ARTEXTID) > 0 && len(PSESD) > 0 {
		orderBuildResult := uc.BuildIngestOrderArticle(restServerDatabase, restServerLogger, PSESD, ARTEXTID, INGESTOP)
		c.JSON(http.StatusOK, orderBuildResult)
		return
	}
}
