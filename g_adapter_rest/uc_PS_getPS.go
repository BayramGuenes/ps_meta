package g_adapter_rest

import (
	"encoding/json"
	"net/http"
	domain "ps_meta/a_domain"
	uc "ps_meta/b_usecases"
	sec "ps_meta/s_security"
	"strings"

	"github.com/gorilla/mux"
)

// ------------------------------------------------------------------------- //
// UC GetPS
// ------------------------------------------------------------------------- //
// HTTP ->Int
func (a Adapter) readPSErscheinungsdatum(r *http.Request) (eErscheinungsdatum string, err error) {

	lErscheinungsdatumKV, _ := mux.Vars(r)["Erscheinungsdatum"]
	eErscheinungsdatum = strings.SplitAfter(lErscheinungsdatumKV, "=")[1]
	return eErscheinungsdatum, nil

}

// Result Int ->HTTP
func (a Adapter) writePSHeader(psHeader domain.Pressespiegel, w http.ResponseWriter) error {

	b, err := json.Marshal(psHeader)
	if err != nil {
		return err
	}

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")

	w.Write(b)
	return nil
}

func (a Adapter) MakePSGetHandler(iPSEntityClass uc.PSEntityClass) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		// 1. HTTP-> Intern
		lPSErscheinungsdatum, err := a.readPSErscheinungsdatum(r)
		lToken := sec.ExtractTokenFromHTTPRequest(r)

		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		// 2. Exec domain function

		lPSHeader, err := iPSEntityClass.GetPS(lPSErscheinungsdatum, lToken, "")
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		// 3. result intern ->HTTP
		if err = a.writePSHeader(lPSHeader, w); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
		}

	}
}
