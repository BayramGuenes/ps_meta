package g_adapter_rest

import (
	"encoding/json"
	"errors"
	"net/http"
	"ps_meta/a_domain"
	uc "ps_meta/b_usecases"
	"strings"

	"github.com/gorilla/mux"
)

type ArtContentsService struct {
	Id         string
	Source     string
	Date       string
	Title      string
	Othertitle string
	Subtitle   string
	Teaser     string
	IsDictated string
	Pagenrtext string
	Text       string
}

// ------------------------------------------------------------------------- //
// UC GetArtikel
// ------------------------------------------------------------------------- //
// HTTP ->Int
func (a Adapter) readHTTPQueryParamArt(r *http.Request) (ePSExtId string, eArtExtId string, err error) {

	ePSExtId = ""
	eArtExtId = ""
	err = nil

	vars := mux.Vars(r)
	for i, v := range vars {
		switch i {

		case "PSExtId":
			ePSExtId = strings.SplitAfter(v, "=")[1]
			break
		case "ArtExtId":
			eArtExtId = strings.SplitAfter(v, "=")[1]
		}
		////println(i, v)
	}

	if ePSExtId == "" {
		err = errors.New("PSExtId als Query-Parameter nicht übergeben.")
	}
	if eArtExtId == "" {
		err = errors.New("ArtExtId  als Query-Parameter nicht übergeben.")
	}

	return

}

// Result Int ->HTTP
func (a Adapter) writeHTTPResponseArt(iArt a_domain.Artikel, w http.ResponseWriter) error {

	//	b, err := json.Marshal(eMelColl)
	b, err := json.Marshal(iArt)

	if err != nil {
		return err
	}

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")

	w.Write(b)
	return nil

}

func (a Adapter) MakeArtHandler(iArtClass uc.ArticleClass) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		////println(" MakeMelCollHandler called")
		// 1. HTTP-> Intern
		lPSExtId, lArtExtId, err := a.readHTTPQueryParamArt(r)

		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		// 2. Exec domain function
		lArtikel, err := iArtClass.GetArticle(lPSExtId, lArtExtId)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		// 3. result intern ->HTTP
		if err = a.writeHTTPResponseArt(lArtikel, w); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
		}

	}
}

func getArtContentsArticle(iPSExtId string, iArtikel a_domain.Artikel) (eResult ArtContentsService) {

	lArtikelContent := ArtContentsService{}
	lArtikelContent.Id = iArtikel.ExtID
	lArtikelArtQuelleName := encodeSpecialSigns(iArtikel.QuelleName)
	lArtikelContent.Source = lArtikelArtQuelleName 
	lArtikelContent.Date = iArtikel.ESD

	lArtikelContentArtTitel := encodeSpecialSigns(iArtikel.Titel)
	lArtikelContent.Title = lArtikelContentArtTitel
	lArtikelContentArtSonstTitel := encodeSpecialSigns(iArtikel.SonstTitel)
	lArtikelContent.Othertitle = lArtikelContentArtSonstTitel

	lArtikelContentArtUntertitel := encodeSpecialSigns(iArtikel.Untertitel)
	lArtikelContent.Subtitle = lArtikelContentArtUntertitel
	//lResultRow.Teaser=lArtikel.
	lArtikelContent.Pagenrtext = iArtikel.QuelleSeitennr
	if ldiktiert, _ := getArtBelongsToDictatedMeldung(iPSExtId, iArtikel.MelExtID); ldiktiert {
		lArtikelContent.IsDictated = "true"
	} else {
		lArtikelContent.IsDictated = "false"
	}
	lArtikelContent.Teaser = iArtikel.Inhalttext
	if len(iArtikel.Inhalttext) > 240 {
		lArtikelContent.Teaser = lArtikelContent.Teaser[:240]
	}
	lArtikelContent.Teaser = encodeSpecialSigns(lArtikelContent.Teaser)
	lArtikelContent.Text = iArtikel.Inhalttext

	eResult = lArtikelContent

	return eResult
}
