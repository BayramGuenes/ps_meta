package g_adapter_rest

import (
	//"encoding/json"
	"net/http"
	//domain "ps_meta/a_domain"
	uc "ps_meta/b_usecases"
	sec "ps_meta/s_security"

	"github.com/gin-gonic/gin"
)

func getCalendarHandlerGin(c *gin.Context) {

	c.Header("Content-Type", "application/json")
	c.Header("Access-Control-Allow-Origin", "*")
	lToken, lKindOfAuth := sec.ExtractTokenFromHTTPRequestGin(c)
	//println("getCalendarHandlerGin CALLED-lKindOfAuth:" + lKindOfAuth)

	//println("getCalendarHandlerGin-Token" + lToken)
	lFromDate := c.PostForm("DateFrom")
	lToDate := c.PostForm("DateTo")

	UC_GetCalendar := uc.NewCalendarInstanceGetter(restServerDatabase, restServerLogger)

	//lCalendar, err := UC_GetCalendar.GetCalendar(lToken, lFromDate, lToDate)
	lCalendar, err := UC_GetCalendar.GetCalendarViaOneSelect(lToken, lKindOfAuth, lFromDate, lToDate)

	if err != nil {
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	c.JSON(http.StatusOK, lCalendar)

}
