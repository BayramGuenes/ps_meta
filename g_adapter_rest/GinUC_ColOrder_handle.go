package g_adapter_rest

import (
	"encoding/json"
	"net/http"
	uc "ps_meta/b_usecases"
	sec "ps_meta/s_security"

	"github.com/gin-gonic/gin"
)

func collectionGetActiveHandlerGin(c *gin.Context) {
	c.Header("Content-Type", "application/json")
	c.Header("Access-Control-Allow-Origin", "*")

	lToken, lKindOfAuth := sec.ExtractTokenFromHTTPRequestGin(c)
	colHandleResult := uc.CollectionGetActiveResult{}
	collHandler := uc.NewCollectionHandler(restServerLogger, restServerDatabase)
	colHandleResult = collHandler.HandleGetActivColl(lToken, lKindOfAuth)
	c.JSON(http.StatusOK, colHandleResult)
}

func collectionOrderHandlerGin(c *gin.Context) {
	c.Header("Content-Type", "application/json")
	c.Header("Access-Control-Allow-Origin", "*")

	lToken, lKindOfAuth := sec.ExtractTokenFromHTTPRequestGin(c)
	ORDERTYPE := c.Param("ORDERTYPE")
	COLLNAME := c.PostForm("COLLNAME")
	colHandleResult := uc.CollectionObjOrderTransferResult{}

	collHandler := uc.NewCollectionHandler(restServerLogger, restServerDatabase)

	switch ORDERTYPE {
	case uc.COColOrderGet:
		colHandleResult = collHandler.HandleGetUsrCollections(lToken, lKindOfAuth)
	case uc.COColOrderCheckNewPerm:
		colHandleResult = collHandler.HandleNewCollPermitted(lToken, lKindOfAuth, COLLNAME)
	case uc.COColOrderNew:
		colHandleResult = collHandler.HandleNewColl(lToken, lKindOfAuth, COLLNAME)
	case uc.COColOrderDel:
		colHandleResult = collHandler.HandleDelColl(lToken, lKindOfAuth, COLLNAME)
	case uc.COColOrderActivate:
		colHandleResult = collHandler.HandleActivateColl(lToken, lKindOfAuth, COLLNAME)

	}
	c.JSON(http.StatusOK, colHandleResult)
}

func collectionItemOrderHandlerGin(c *gin.Context) {
	c.Header("Content-Type", "application/json")
	c.Header("Access-Control-Allow-Origin", "*")

	lToken, lKindOfAuth := sec.ExtractTokenFromHTTPRequestGin(c)
	ORDERTYPE := c.Param("ORDERTYPE")
	ITEMIDFIELD1 := c.PostForm("ITEMIDFIELD1")
	ITEMIDFIELD2 := c.PostForm("ITEMIDFIELD2")
	colHandleResult := uc.CollectionItemOrderTransferResult{}

	collHandler := uc.NewCollectionHandler(restServerLogger, restServerDatabase)

	switch ORDERTYPE {
	case uc.COColItemNew:
		colHandleResult = collHandler.HandleNewCollItem(lToken, lKindOfAuth, ITEMIDFIELD1, ITEMIDFIELD2)

	case uc.COColItemDel:
		colHandleResult = collHandler.HandleDelCollItem(lToken, lKindOfAuth, ITEMIDFIELD1, ITEMIDFIELD2)

	}
	c.JSON(http.StatusOK, colHandleResult)
}
func collectionItemListOrderHandlerGin(c *gin.Context) {
	c.Header("Content-Type", "application/json")
	c.Header("Access-Control-Allow-Origin", "*")

	ORDERTYPE := c.Param("ORDERTYPE")
	lToken, lKindOfAuth := sec.ExtractTokenFromHTTPRequestGin(c)

	data, _ := c.GetRawData()

	collItemListOrderGUI := uc.GUICollItemListOrder{}
	json.Unmarshal(data, &collItemListOrderGUI)

	if len(lToken) == 0 {
		lToken = collItemListOrderGUI.Token
	}
	lItemlist := collItemListOrderGUI.Itemlist

	colHandleResult := uc.CollectionItemListOrderTransferResult{}

	collHandler := uc.NewCollectionHandler(restServerLogger, restServerDatabase)

	switch ORDERTYPE {
	case uc.COColItemNew:
	case uc.COColItemDel:
		colHandleResult = collHandler.HandleDelCollItemList(lToken, lKindOfAuth, lItemlist)

	}
	c.JSON(http.StatusOK, colHandleResult)

}

func collectionGetItemListGin(c *gin.Context) {
	c.Header("Content-Type", "application/json")
	c.Header("Access-Control-Allow-Origin", "*")

	lToken, lKindOfAuth := sec.ExtractTokenFromHTTPRequestGin(c)
	collHandler := uc.NewCollectionHandler(restServerLogger, restServerDatabase)
	colHandleResult := collHandler.HandleGetItems(lToken, lKindOfAuth)

	c.JSON(http.StatusOK, colHandleResult)

}

func collectionGetArticleListGin(c *gin.Context) {
	c.Header("Content-Type", "application/json")
	c.Header("Access-Control-Allow-Origin", "*")

	lToken, lKindOfAuth := sec.ExtractTokenFromHTTPRequestGin(c)
	collHandler := uc.NewCollectionHandler(restServerLogger, restServerDatabase)

	colHandleResult := collHandler.HandleGetCollArticleList(lToken, lKindOfAuth)

	c.JSON(http.StatusOK, colHandleResult)

}

func itemlistGetArticleListGin(c *gin.Context) {
	c.Header("Content-Type", "application/json")
	c.Header("Access-Control-Allow-Origin", "*")

	data, _ := c.GetRawData()

	lItemlist := uc.CollectionItemList{}
	json.Unmarshal(data, &lItemlist)

	collHandler := uc.NewCollectionHandler(restServerLogger, restServerDatabase)

	colHandleResult := collHandler.HandleGetItemlistArticles(lItemlist)
	//println(colHandleResult.Articlelist[0].Title)
	c.JSON(http.StatusOK, colHandleResult)

}
