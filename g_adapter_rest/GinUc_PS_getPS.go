package g_adapter_rest

import (
	//"encoding/json"
	"net/http"
	//domain "ps_meta/a_domain"
	uc "ps_meta/b_usecases"
	sec "ps_meta/s_security"

	//"strings"

	"github.com/gin-gonic/gin"
)

func getPSHandlerGin(c *gin.Context) {

	if c.Request.Method == "GET" {

		lToken, lKindOfAuth := sec.ExtractTokenFromHTTPRequestGin(c)
		lPSErscheinungsdatum := ""
		lPSErscheinungsdatum = c.Param("Erscheinungsdatum")

		if len(lPSErscheinungsdatum) == 0 {
			c.AbortWithStatus(http.StatusBadRequest)
			return
		}

		UC_GetPS := uc.NewPSGetter(restServerDatabase, restServerLogger)

		// 2. Exec domain function

		lPSHeader, err := UC_GetPS.GetPS(lPSErscheinungsdatum, lToken, lKindOfAuth)
		if err != nil {
			c.AbortWithStatus(http.StatusInternalServerError)
			return
		}

		// 3. result intern ->HTTP

		c.Header("Content-Type", "application/json")
		c.Header("Access-Control-Allow-Origin", "*")

		c.JSON(http.StatusOK, lPSHeader)

	}
}
