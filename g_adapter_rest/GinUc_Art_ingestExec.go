package g_adapter_rest

import (
	"net/http"
	uc "ps_meta/b_usecases"
	"strconv"

	"github.com/gin-gonic/gin"
)

func execIngestHandlerGin(c *gin.Context) {

	NumOrders := c.Param("NumOrders")

	if len(NumOrders) == 0 {
		c.JSON(http.StatusOK, "Parameter NumOrders not provided. Exec Ingest not permitted")
		return
	}

	num, _ := strconv.Atoi(NumOrders)

	execIngestResult := uc.ExecOrderToQueryMicroService(num)
	c.JSON(http.StatusOK, execIngestResult)
	return
}
