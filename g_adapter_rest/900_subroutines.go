package g_adapter_rest

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"ps_meta/a_domain"
	"ps_meta/b_usecases"
	bootstrap "ps_meta/z_bootstrap"
	"regexp"
	"strings"
)

/*type ImportServiceResponseStruct struct {
	PspEsd  string
	Message string
	Details string
}
*/
func resultToJson(iServiceResult b_usecases.ImportServiceResponseStruct, itErrorMsgs []a_domain.ProcessingInfo, iOk bool) []byte {
	var content []byte
	errmsg := ""
	ok := iOk
	if ok {
		b, err := json.Marshal(iServiceResult)
		if err == nil {
			content = b
		} else {
			ok = false
			errmsg = "{ \"PspEsd\":\"\",	\"Message\":\"Import durchgeführt, aber Generierung der Antwortmeldung (nach JSON) konnte nicht durchgeführt werden.\", \"Details\": \"" + err.Error() + "\"}"
		}

	}
	if !ok {
		if len(errmsg) == 0 {
			var contentStr = ""
			for _, v := range itErrorMsgs {
				contentStr += v.InfoDescription
			}
			errmsg = "{ \"PspEsd\":\"\",	\"Message\":\"Import konnte nicht durchgeführt werden.Fehler aufgetreten! \", \"Details\": \"" + contentStr + "\"}"
		}
		content = []byte(errmsg)
	}
	return content

}

func callRestService(iServiceUrl string) (eResult interface{}, err error) {
	eResult = ""
	//client := &http.Client{}
	lServiceUrl := bootstrap.ThisServerAddr + iServiceUrl
	////println(lServiceUrl)
	resp, err := http.Get(lServiceUrl) //<--
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		//println("err:" + err.Error())

		return "", err
	}
	eResult = string(body)

	return eResult, nil

}

func formatDateYYYYminMMminDD(iDate string) string {

	var result = ""
	////println("formatDateYYYYminMMminDD=" + iDate)
	if !strings.Contains(iDate, "-") && len(iDate) >= 8 {
		result = iDate[:4] + "-" + iDate[4:6] + "-" + iDate[6:8]
		strings.TrimSpace(result)
	} else {
		result = iDate
	}
	return result
}

func formatDateYYYYMMDD(iDate string) string {

	var result = ""
	if strings.Contains(iDate, "-") && len(iDate) > 9 {
		result = iDate[:4] + iDate[5:7] + iDate[8:10]
	} else {
		result = iDate
	}
	return result
}

func encodeSpecialSigns(iInput string) string {
	eResult := iInput
	eResult = strings.Replace(eResult, "\\", "\\\\\\", -1)
	var re = regexp.MustCompile(`\t`)
	eResult = re.ReplaceAllString(eResult, " ")
	eResult = strings.Replace(eResult, "&quot;", "\"", -1)

	//var reQuot = regexp.MustCompile(`"`)
	//eResult = reQuot.ReplaceAllString(eResult, `\"`)
	eResult = strings.Replace(eResult, "\"", "\\\"", -1)
	return eResult
}
