package g_adapter_rest

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"

	domain "ps_meta/a_domain"

	//"ps_meta/b_usecases"
	bootstrap "ps_meta/z_bootstrap"
	//"regexp"
	//"strings"
)

//===============================================================================
//===============================================================================
func GinCallRestService(iServiceUrl string, iToken string, iKindOfAuth string) (eResult interface{}, err error) {
	////println("GinCallRestService:lServiceUrl" + iServiceUrl)

	client := &http.Client{
		//CheckRedirect: redirectPolicyFunc,
	}
	eResult = ""

	//lServiceUrl := bootstrap.ThisCommunnicationProtokoll + "://" + bootstrap.ThisHostname + iServiceUrl
	lServiceUrl := bootstrap.ThisCommunnicationProtokoll + "://:" + bootstrap.ThisPort + iServiceUrl + "?KindOfAuth=" + iKindOfAuth

	req, err := http.NewRequest(http.MethodGet, lServiceUrl, bytes.NewBuffer([]byte{}))
	if err != nil {
		return "", err
	}
	req.Header.Add("Authorization", "Bearer "+iToken)

	resp, err := client.Do(req)
	if err != nil {
		println("client.Do(req):" + err.Error())
		return
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		//println("err:" + err.Error())

		return "", err
	}
	eResult = string(body)
	//println("eResult :" + string(body))
	return eResult, nil

}
func GinCallImageService(iServiceUrl string) (eResult GetImageResult, err error) {
	////println("GinCallRestService:lServiceUrl" + iServiceUrl)
	eResult = GetImageResult{}
	//client := &http.Client{}
	//lServiceUrl := bootstrap.ThisServerAddr + iServiceUrl
	lServiceUrl := bootstrap.ThisCommunnicationProtokoll + "://" + bootstrap.ServiceLocationIDPSImage + iServiceUrl
	//lServiceUrl = "http://localhost:39080/ImageData/Entity/article/2020011640020"
	//println("20200608 GinCallImageService:" + lServiceUrl)

	////println(lServiceUrl)
	resp, err := http.Get(lServiceUrl) //<--
	if err != nil {
		return eResult, err
	}
	//	//println(lServiceUrl)

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		//println("err:" + err.Error())

		return eResult, err
	}

	err = json.Unmarshal(body, &eResult)
	//println("20200608 GinCallImageService:" + eResult.ImageInfos.RepoPath)

	return eResult, err

}
func GinCallImgServColItemlist(iServiceUrl string, iData []byte) (eResult GetImageResult, err error) {
	////println("GinCallRestService:lServiceUrl" + iServiceUrl)

	eResult = GetImageResult{}
	//client := &http.Client{}
	//lServiceUrl := bootstrap.ThisServerAddr + iServiceUrl
	lServiceUrl := bootstrap.ThisCommunnicationProtokoll + "://" + bootstrap.ServiceLocationIDPSImage + iServiceUrl
	////println(lServiceUrl)
	resp, err := http.Post(lServiceUrl, "application/json", bytes.NewBuffer(iData)) //<--
	if err != nil {
		return eResult, err
	}
	//println(lServiceUrl)

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		//println("err:" + err.Error())

		return eResult, err
	}

	err = json.Unmarshal(body, &eResult)

	return eResult, err

}

func GinCallBuildMeldungImageService(iServiceUrl string) (eResult GetImageResult, err error) {
	////println("GinCallRestService:lServiceUrl" + iServiceUrl)

	eResult = GetImageResult{}
	//client := &http.Client{}
	//lServiceUrl := bootstrap.ThisServerAddr + iServiceUrl
	lServiceUrl := bootstrap.ThisCommunnicationProtokoll + "://" + bootstrap.ServiceLocationIDPSImage + iServiceUrl
	//println(lServiceUrl)

	b := []byte("")
	resp, err := http.Post(lServiceUrl, "application/json", bytes.NewBuffer(b)) //<--
	if err != nil {
		return eResult, err
	}
	//println(lServiceUrl)

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		//println("err:" + err.Error())

		return eResult, err
	}

	err = json.Unmarshal(body, &eResult)

	return eResult, err

}

func formatArticle(iArticle domain.Artikel) (eResult IF_psArticleTextInfo) {
	lResultRow := IF_psArticleTextInfo{}
	lArtikel := iArticle
	////println("lArtikel.ArtExtID:" + lArtikel.ArtExtID + " Subtitle:" + lArtikel.ArtUntertitel)

	lResultRow.Id = lArtikel.ExtID
	lArtikel.QuelleName = encodeAklContentSpecialSigns(lArtikel.QuelleName)
	lResultRow.Source = lArtikel.QuelleName
	lResultRow.Date = lArtikel.ESD

	lArtikel.Titel = encodeAklContentSpecialSigns(lArtikel.Titel)
	lResultRow.Title = lArtikel.Titel
	lArtikel.SonstTitel = encodeAklContentSpecialSigns(lArtikel.SonstTitel)
	lResultRow.Othertitle = lArtikel.SonstTitel

	lArtikel.Untertitel = encodeAklContentSpecialSigns(lArtikel.Untertitel)
	lResultRow.Subtitle = lArtikel.Untertitel
	//lResultRow.Teaser=lArtikel.
	lResultRow.Pagenrtext = lArtikel.QuelleSeitennr

	lResultRow.Teaser = lArtikel.Inhalttext
	if len(lArtikel.Inhalttext) > teaserLength {
		lOffsetLastWord, llastWord := getLastWordComplete(lArtikel.Inhalttext)
		////println("lOffsetLastWort:" + strconv.Itoa(lOffsetLastWord) + " llastWord:" + llastWord)
		lResultRow.Teaser = lResultRow.Teaser[:lOffsetLastWord] + " " + llastWord + " ..."
	}
	lResultRow.Teaser = encodeAklContentSpecialSigns(lResultRow.Teaser)
	lResultRow.Text = lArtikel.Inhalttext
	eResult = lResultRow

	return eResult
}
