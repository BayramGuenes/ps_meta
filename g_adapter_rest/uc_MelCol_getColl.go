package g_adapter_rest

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"ps_meta/a_domain"
	domain "ps_meta/a_domain"
	uc "ps_meta/b_usecases"
	sec "ps_meta/s_security"
	"regexp"
	"sort"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
)

type TopicsServiceArticles struct {
	Id   string `json:"id"`
	Text string `json:"text"`
}

type TopicsServiceRubric struct {
	Name          string                  `json:"name"`
	HasUndictated bool                    `json:"hasUndictated"`
	Articles      []TopicsServiceArticles `json:"articles"`
}

type TopicsServiceOutput struct {
	Id           string                `json:"id"`
	Status       string                `json:"status"`
	Importsystem string                `json:"importsystem"`
	Prev         string                `json:"prev"`
	Next         string                `json:"next"`
	Rubrics      []TopicsServiceRubric `json:"rubrics"`
}

type RubricServiceOutput []string

// ------------------------------------------------------------------------- //
// UC
// ------------------------------------------------------------------------- //

// HTTP ->Int
func (a Adapter) readHTTPQueryParamMelColl(r *http.Request) (eESD string, eDisplayOnlyRubrics bool, err error) {

	eESD = ""
	err = nil
	eDisplayOnlyRubrics = false
	vars := mux.Vars(r)
	for i, v := range vars {
		switch i {

		case "Erscheinungsdatum":
			////println("v="+v)
			lstringTab := strings.SplitAfter(v, "=")
			if len(lstringTab) > 1 {
				eESD = lstringTab[1]
			} else {
				eESD = v
			}
		case "DisplayOnlyRubrics":
			eDisplayOnlyRubrics = true
		}

		////println(i, v)
	}

	//if eToken == "" {
	//	err = errors.New("Token als Query-Parameter nicht übergeben.")
	//}
	if eESD == "" {
		err = errors.New("Erscheinungsdatum als Query-Parameter nicht übergeben.")
	}

	return

}

// Result Int ->HTTP
func (a Adapter) writeHTTPResponseMelColl(iToken string, ePSHeader domain.Pressespiegel, eMelColl []domain.Meldung, w http.ResponseWriter) error {

	var lTopicsServiceOutput TopicsServiceOutput
	lTopicsServiceOutput.Id = ePSHeader.PSExtID
	lTopicsServiceOutput.Importsystem = ePSHeader.LogicalNameImportsystem
	if ePSHeader.IsActivated {
		lTopicsServiceOutput.Status = "aktiv"
	} else {
		lTopicsServiceOutput.Status = "inaktiv"
	}
	////println("lTopicsServiceOutput.Importsystem:" + lTopicsServiceOutput.Importsystem)
	serviceDateBefore := "/ps_meta/PSDateBeforeService/Referenzdatum=" + ePSHeader.PSESD + "/Token=" + iToken
	esdBeforeInterface, _ := callRestService(serviceDateBefore)
	esdBefore := fmt.Sprintf("%v", esdBeforeInterface)
	esdBefore = strings.Replace(esdBefore, "\"", "", -1)
	lTopicsServiceOutput.Prev = esdBefore

	serviceDateNext := "/ps_meta/PSDateNextService/Referenzdatum=" + ePSHeader.PSESD + "/Token=" + iToken
	esdNextInterface, _ := callRestService(serviceDateNext)
	esdNext := fmt.Sprintf("%v", esdNextInterface)
	esdNext = strings.Replace(esdNext, "\"", "", -1)
	lTopicsServiceOutput.Next = esdNext

	lTopicsServiceOutput.Rubrics = getRubricsAndMelContents(eMelColl)

	//	b, err := json.Marshal(eMelColl)
	b, err := json.Marshal(lTopicsServiceOutput)

	if err != nil {
		return err
	}

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")

	w.Write(b)
	return nil
}

// Result Int ->HTTP
func (a Adapter) writeHTTPResponseRubrColl(ePSHeader domain.Pressespiegel, eMelColl []domain.Meldung, w http.ResponseWriter) error {

	lRubricServiceOutput := getRubricList(eMelColl)

	//	b, err := json.Marshal(eMelColl)
	b, err := json.Marshal(lRubricServiceOutput)

	if err != nil {
		return err
	}

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")

	w.Write(b)
	return nil
}

func (a Adapter) MakeMelCollHandler(iMelCollClass uc.MeldungCollClass) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		////println(" MakeMelCollHandler called")
		// 1. HTTP-> Intern
		lToken := sec.ExtractTokenFromHTTPRequest(r)
		lESD, lDisplayOnlyRubrics, err := a.readHTTPQueryParamMelColl(r)
		lESD = formatDateYYYYMMDD(lESD)

		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		// 2. Exec domain function
		lWithInactiv := false
		if len(lToken) > 0 {
			lUser, err := iMelCollClass.PSDataBase.GetUsrViaToken(lToken)
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
			lUserRoles, err := iMelCollClass.PSDataBase.UsrDBPort.GetUsrRoles(lUser.Loginname, lUser.Rfa) //GetUsrRoles
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
			lWithInactiv = a_domain.CheckAuthority(a_domain.AuthSearchWithInactive, lUserRoles, iMelCollClass.PSDataBase.AuthDBPort, iMelCollClass.Logger)
			println(lWithInactiv)
		}

		////println("lESD:"+lESD)
		lPSHeader, lMelCollection, err := iMelCollClass.GetMeldungCollection(lToken, "", lESD)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		// 3. result intern ->HTTP
		if lDisplayOnlyRubrics {
			if err = a.writeHTTPResponseRubrColl(lPSHeader, lMelCollection, w); err != nil {
				w.WriteHeader(http.StatusInternalServerError)
			}
		} else {
			if err = a.writeHTTPResponseMelColl(lToken, lPSHeader, lMelCollection, w); err != nil {
				w.WriteHeader(http.StatusInternalServerError)
			}
		}
	}
}

// sort mt_meldungen BY posnr psp rubrik.
type ByPosnrRubrik []domain.Meldung

func (pr ByPosnrRubrik) Len() int { return len(pr) }

func (pr ByPosnrRubrik) Swap(i, j int) { pr[i], pr[j] = pr[j], pr[i] }
func (pr ByPosnrRubrik) Less(i, j int) bool {
	intMelPSPosnr_i, _ := strconv.Atoi(pr[i].MelPSPosnr)
	intMelPSPosnr_j, _ := strconv.Atoi(pr[j].MelPSPosnr)

	if intMelPSPosnr_i < intMelPSPosnr_j {
		return true
	}
	if intMelPSPosnr_i == intMelPSPosnr_j {
		if strings.Compare(pr[i].MelPSRubrik, pr[j].MelPSRubrik) == -1 {
			return true
		}
	}
	return false
}

func getRubricsAndMelContents(iMelColl []a_domain.Meldung) (eResult []TopicsServiceRubric) {
	lMelcoll := []a_domain.Meldung{}
	for i := 0; i < len(iMelColl); i++ {
		lMelcoll = append(lMelcoll, iMelColl[i])
		////println("lMelcoll:" + lMelcoll[i].MelInhalttext)
	}
	sort.Sort(ByPosnrRubrik(lMelcoll))

	lRubrikBefore := ""
	var lrowResult TopicsServiceRubric
	var lrowArticle TopicsServiceArticles
	for i := 0; i < len(lMelcoll); i++ {
		if strings.Compare(lRubrikBefore, lMelcoll[i].MelPSRubrik) != 0 {
			if len(lrowResult.Name) > 0 {
				eResult = append(eResult, lrowResult)
				lrowResult = TopicsServiceRubric{}
			}
			lrowResult.Name = lMelcoll[i].MelPSRubrik
			lrowResult.HasUndictated = hasUndiktiert(lMelcoll, lrowResult.Name)
		}
		lrowArticle.Id = lMelcoll[i].MelExtID
		lrowArticle.Text = lMelcoll[i].MelInhalttext
		var re = regexp.MustCompile(`[^[:print:]äÄöÖüÜß]`)
		lrowArticle.Text = re.ReplaceAllString(lrowArticle.Text, "")
		lrowResult.Articles = append(lrowResult.Articles, lrowArticle)
		lRubrikBefore = lMelcoll[i].MelPSRubrik
	}
	if len(lrowResult.Name) > 0 {
		eResult = append(eResult, lrowResult)
	}
	return eResult
}

func getRubricList(iMelColl []a_domain.Meldung) (eResult RubricServiceOutput) {
	lMelcoll := []a_domain.Meldung{}
	for i := 0; i < len(iMelColl); i++ {
		lMelcoll = append(lMelcoll, iMelColl[i])
		////println("lMelcoll:" + lMelcoll[i].MelInhalttext)
	}
	sort.Sort(ByPosnrRubrik(lMelcoll))

	lRubrikBefore := ""
	var lrowResult string
	for i := 0; i < len(lMelcoll); i++ {
		if strings.Compare(lRubrikBefore, lMelcoll[i].MelPSRubrik) != 0 {
			if len(lrowResult) > 0 {
				eResult = append(eResult, lrowResult)
				lrowResult = ""
			}
			lrowResult = lMelcoll[i].MelPSRubrik

		}

		lRubrikBefore = lMelcoll[i].MelPSRubrik
	}
	if len(lrowResult) > 0 {
		eResult = append(eResult, lrowResult)
	}
	return eResult
}

func hasUndiktiert(iMelColl []a_domain.Meldung, iRubrik string) bool {
	for i := 0; i < len(iMelColl); i++ {
		if strings.Compare(iMelColl[i].MelPSRubrik, iRubrik) == 0 {
			if strings.Compare(iMelColl[i].MelInhalttext, "undiktiert") == 0 {
				return true
			}
		}
	}
	return false
}
