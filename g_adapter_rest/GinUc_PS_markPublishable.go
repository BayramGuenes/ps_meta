package g_adapter_rest

import (
	"net/http"
	uc "ps_meta/b_usecases"
	sec "ps_meta/s_security"

	"github.com/gin-gonic/gin"
)

// ------------------------------------------- //

func getPublishMarkerHandlerGin(c *gin.Context) {
	// 1. HTTP-> Intern
	lToken, lKindOfAuth := sec.ExtractTokenFromHTTPRequestGin(c)
	lPSDate := c.PostForm("PS.Date")
	lESD := formatDateYYYYMMDD(lPSDate)

	UC_PublishPS := uc.NewPSGetter(restServerDatabase, restServerLogger)

	serviceResult := UC_PublishPS.MarkPublishable(lESD, lToken, lKindOfAuth)

	// 3. result intern ->HTTP
	//date := formatDateYYYYminMMminDD(lESD)
	c.Header("Content-Type", "application/json")
	c.Header("Access-Control-Allow-Origin", "*")

	c.JSON(http.StatusOK, serviceResult)

}

//MessageType string //Info,Error
//	PspEsd      string
//	MessageText string
//	Details     string
