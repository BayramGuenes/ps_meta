package g_adapter_rest

import (
	"fmt"
	"net/http"
	domain "ps_meta/a_domain"
	uc "ps_meta/b_usecases"
	sec "ps_meta/s_security"
	"strings"

	"github.com/gin-gonic/gin"
)

func getArtCollHandlerGin(c *gin.Context) {

	//c.Header("Content-Type", "application/json")
	//c.Header("Access-Control-Allow-Origin", "*")

	lToken, lKindOfAuth := sec.ExtractTokenFromHTTPRequestGin(c)

	lESD := c.Param("Erscheinungsdatum")
	////println("getArtCollHandlerGin calledIII: ", lESD)

	if strings.Contains(lESD, "-") {
		lESD = formatDateYYYYMMDD(lESD)
	}
	if len(lESD) == 0 {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}
	////println("getArtCollHandlerGin called IV")

	UC_GetArtCollection := uc.NewArticleCollGetter(restServerDatabase, restServerLogger)

	lPSHeader, lArtCollection, err := UC_GetArtCollection.GetArticleCollection(lToken, lKindOfAuth, lESD)
	if err != nil {
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}
	c.Header("Content-Type", "application/json")
	c.Header("Access-Control-Allow-Origin", "*")
	GinWriteHTTPResponseArtColl(lToken, lKindOfAuth, lPSHeader, lArtCollection, c)
}

func GinWriteHTTPResponseArtColl(iToken string, iKindOfAuth string, iPSHeader domain.Pressespiegel, iArtColl []domain.Artikel, c *gin.Context) {

	var lartContentsServiceOutput ArtContentsServiceOutput

	lartContentsServiceOutput.Date = iPSHeader.PSESD
	serviceDateBefore := "/ps_meta/PSDateBeforeService/Referenzdatum/" + iPSHeader.PSESD

	esdBeforeInterface, _ := GinCallRestService(serviceDateBefore, iToken, iKindOfAuth)
	esdBefore := fmt.Sprintf("%v", esdBeforeInterface)
	esdBefore = strings.Replace(esdBefore, "\"", "", -1)

	lartContentsServiceOutput.Prev = esdBefore

	serviceDateNext := "/ps_meta/PSDateNextService/Referenzdatum/" + iPSHeader.PSESD

	esdNextInterface, _ := GinCallRestService(serviceDateNext, iToken, iKindOfAuth)
	esdNext := fmt.Sprintf("%v", esdNextInterface)
	esdNext = strings.Replace(esdNext, "\"", "", -1)

	lartContentsServiceOutput.Next = esdNext

	lartContentsServiceOutput.Rubrics = getRubricsAndArtContents(iPSHeader.PSExtID, iArtColl)

	c.Header("Content-Type", "application/json")
	c.Header("Access-Control-Allow-Origin", "*")

	c.JSON(http.StatusOK, lartContentsServiceOutput)

}
