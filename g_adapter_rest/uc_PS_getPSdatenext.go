package g_adapter_rest

import (
	"encoding/json"
	"errors"
	"net/http"
	"ps_meta/a_domain"
	uc "ps_meta/b_usecases"
	sec "ps_meta/s_security"
	"strings"

	"github.com/gorilla/mux"
)

// ------------------------------------------------------------------------- //
// UC GetPS
// ------------------------------------------------------------------------- //
// HTTP ->Int
func (a Adapter) readHTTPQueryParamDateNext(r *http.Request) (eReferenceDate string, err error) {

	/*body, err := ioutil.ReadAll(r.Body)
	if err != nil {
	  return domain.Invoice{}, err
	}
	var invoice domain.Invoice
	if err := json.Unmarshal(body, &invoice); err != nil {
	return domain.Invoice{}, err
	}*/

	eReferenceDate = ""
	err = nil

	vars := mux.Vars(r)
	for i, v := range vars {
		switch i {
		case "Referenzdatum":
			eReferenceDate = strings.SplitAfter(v, "=")[1]

		}

	}
	if eReferenceDate == "" {

		err = errors.New("Referenzdatum als Query-Parameter nicht übergeben.")
	}

	return

}

// Result Int ->HTTP
func (a Adapter) writeHTTPResponseDateNext(psESD string, w http.ResponseWriter) error {
	date := formatDateYYYYminMMminDD(psESD)
	b, err := json.Marshal(date)
	if err != nil {
		return err
	}

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")

	w.Write(b)
	return nil
}

func (a Adapter) MakePSGetDateNextHandler(iPSEntityClass uc.PSEntityClass) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		// 1. HTTP-> Intern
		lRefDate, err := a.readHTTPQueryParamDateNext(r)
		lToken := sec.ExtractTokenFromHTTPRequest(r)

		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		// 2. Exec domain function
		lWithInactiv := false
		if len(lToken) > 0 {
			lUser, err := iPSEntityClass.Repository.UsrDBPort.GetUsrViaToken(lToken)
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
			lUserRoles, err := iPSEntityClass.Repository.UsrDBPort.GetUsrRoles(lUser.Loginname, lUser.Rfa) //GetUsrRoles
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
			lWithInactiv = a_domain.CheckAuthority(a_domain.AuthSearchWithInactive, lUserRoles, iPSEntityClass.Repository.AuthDBPort, iPSEntityClass.Logger)
		}

		lPSDate, _, err := iPSEntityClass.GetDateNextPS(lToken, "", lRefDate)
		println(lWithInactiv)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		// 3. result intern ->HTTP
		if err = a.writeHTTPResponseDateNext(lPSDate, w); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
		}

	}
}
