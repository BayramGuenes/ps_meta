package g_adapter_rest

import (
	"net/http"
	uc "ps_meta/b_usecases"
	sec "ps_meta/s_security"
	"strings"

	"github.com/gin-gonic/gin"
)

type ResultArtikel struct {
	EXTID   string
	PSEXTID string
	ESD     string
}

type ResponseStructSearchEngine struct {
	ProcessOK         bool //Info,Error
	Details           string
	CountResultsTotal string
	ListArtikel       []ResultArtikel
}

type ResponseStructGetSingleArticleData struct {
	ProcessOK   bool //Info,Error
	Details     string
	ArtikelData IF_psArticleTextInfo
}
type ResponseStructGetQueryArticleData struct {
	ProcessOK   bool //Info,Error
	Details     string
	ListArtikel []IF_psArticleTextInfo
}

type IF_psArticleTextInfo struct {
	Id            string
	Source        string
	Date          string
	Title         string
	Othertitle    string
	Subtitle      string
	Teaser        string
	IsDictated    string
	Pagenrtext    string
	Text          string
	isLastArticle bool
}

// ------------------------------------------- //

// ------------------------------------------- //

func getArticleQueryHitlistHdGin(c *gin.Context) {
	// 1. HTTP-> Intern

	/*==========================================================*/
	/* Prepare Call GinCallQueryRestService
	/*==========================================================*/
	lToken, lKindOfAuth := sec.ExtractTokenFromHTTPRequestGin(c)
	lQueryString := ""
	c.Header("Content-Type", "application/json")
	c.Header("Access-Control-Allow-Origin", "*")

	//IS Used usually:
	//println("c.Request.Method:" + c.Request.Method)
	if c.Request.Method == "GET" {
		lQueryString = c.Param("QueryFor")
		////println("getArticleQueryHandlerGin.lQueryString:" + lQueryString)
	}
	if c.Request.Method == "POST" {
		lQueryString = c.PostForm("QueryFor")
	}

	respSearchEngine := uc.ResponseStructSearchEngine{}
	if strings.Contains(c.Request.URL.String(), "ArticleQueryHitlistEXT") {
		respSearchEngine = uc.ExecArticleQueryExtendedSearch(restServerDatabase, restServerLogger, lQueryString, lToken, lKindOfAuth)
	} else {
		respSearchEngine = uc.ExecArticleQueryPlainSearch(restServerDatabase, restServerLogger, lQueryString, lToken, lKindOfAuth)
	}
	//println("respSearchEngine:" + respSearchEngine.Details)

	c.JSON(http.StatusOK, respSearchEngine)

}
