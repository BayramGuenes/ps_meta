package g_adapter_rest

import (
	"encoding/json"
	"errors"
	"net/http"
	"ps_meta/a_domain"
	uc "ps_meta/b_usecases"
	sec "ps_meta/s_security"

	//"strconv"
	"strings"
	"time"

	"github.com/gorilla/mux"
)

// ------------------------------------------------------------------------- //
// UC GetPS
// ------------------------------------------------------------------------- //
// HTTP ->Int
func (a Adapter) readHTTPQueryParamDateBefore(r *http.Request) (eReferenceDate string, err error) {

	eReferenceDate = ""
	err = nil

	vars := mux.Vars(r)
	for i, v := range vars {
		switch i {
		case "Referenzdatum":
			eReferenceDate = strings.SplitAfter(v, "=")[1]

		}

	}
	if eReferenceDate == "" {

		err = errors.New("Referenzdatum als Query-Parameter nicht übergeben.")
	}

	return

}

// Result Int ->HTTP
func (a Adapter) writeHTTPResponseDateBefore(psESD string, w http.ResponseWriter) error {
	date := formatDateYYYYminMMminDD(psESD)
	b, err := json.Marshal(date)
	if err != nil {
		return err
	}

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")

	w.Write(b)
	return nil
}

func (a Adapter) MakePSGetDateBeforeHandler(iPSEntityClass uc.PSEntityClass) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		// 1. HTTP-> Intern
		lRefDate, err := a.readHTTPQueryParamDateBefore(r)
		lToken := sec.ExtractTokenFromHTTPRequest(r)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		// 2. Exec domain function

		//lWithInactiv := false
		lUserRoles := []string{}
		if len(lToken) > 0 {
			lUser, err := iPSEntityClass.Repository.UsrDBPort.GetUsrViaToken(lToken)
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
			lUserRoles, err = iPSEntityClass.Repository.UsrDBPort.GetUsrRoles(lUser.Loginname, lUser.Rfa) //GetUsrRoles
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
			//lWithInactiv = a_domain.CheckAuthority(a_domain.AuthSearchWithInactive, lUserRoles, iPSEntityClass.Repository.AuthDBPort, iPSEntityClass.Logger)
		}

		lPSDate, err := iPSEntityClass.GetDateBeforePS(lToken, "", lRefDate)

		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		lOneYearSearch := a_domain.CheckAuthority(a_domain.AuthSearchOneYear, lUserRoles, iPSEntityClass.Repository.AuthDBPort, iPSEntityClass.Logger)
		////println("lOneYearSearch :" + strconv.FormatBool(lOneYearSearch))
		lUnrestrictedSearch := a_domain.CheckAuthority(a_domain.AuthSearchUnlimited, lUserRoles, iPSEntityClass.Repository.AuthDBPort, iPSEntityClass.Logger)
		////println("lUnrestrictedSearch :" + strconv.FormatBool(lUnrestrictedSearch))

		if !lUnrestrictedSearch {
			dateCompare := ""
			if lOneYearSearch {
				dateCompare = dateBeforeOneYear()
			} else {
				dateCompare = dateBeforeOneMonth()
			}
			if dateLessThan(lPSDate, dateCompare) {
				lPSDate = ""
			}
		}

		// 3. result intern ->HTTP
		if err = a.writeHTTPResponseDateBefore(lPSDate, w); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
		}

	}
}

func dateBeforeNumDays(iDays int) string {
	currentTime := time.Now()
	newtime := currentTime.AddDate(0, 0, -(iDays)) //30 day before
	return newtime.Format("20060102")
}

func dateBeforeOneMonth() string {
	return (dateBeforeNumDays(30))

}
func dateBeforeOneYear() string {
	return (dateBeforeNumDays(365))
}
func dateLessThan(iDateLeft, iDateRight string) bool {
	leftTime, _ := time.Parse("20060102", iDateLeft)
	rightTime, _ := time.Parse("20060102", iDateRight)
	if leftTime.Before(rightTime) {
		return true
	}
	return false
}
