package g_adapter_rest

import (
	"io/ioutil"
	"log"
	"net/http"
	domain "ps_meta/a_domain"
	syscommanager "ps_meta/h_service_communication_manager"
	bootstrap "ps_meta/z_bootstrap"
	"time"

	sec "ps_meta/s_security"

	"github.com/gin-gonic/gin"
	"golang.org/x/oauth2"

	//"github.com/coreos/go-oidc"

	oidc "github.com/coreos/go-oidc"
)

var (
	oauth2Config oauth2.Config
	state        string
	oidcConfig   *oidc.Config
	configURL    string
)

type GinAdapter struct {
	router *gin.Engine
}

func GinNewAdapter() GinAdapter {
	router := gin.New()
	return GinAdapter{router}
}

//https://github.com/gin-gonic/gin/issues/1799
func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		//c.Writer.Header().Set("Access-Control-Allow-Origin", "http://localhost:3000")
		//c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Origin", c.Request.Header.Get("Origin"))
		c.Writer.Header().Set("Access-Control-Max-Age", "86400")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, UPDATE")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Access-Control-Allow-Origin, Origin, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
		c.Writer.Header().Set("Access-Control-Expose-Headers", "Content-Length")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")

		//fmt.Println(c.Request.Method)

		if c.Request.Method == "OPTIONS" {
			//fmt.Println("OPTIONS")
			c.AbortWithStatus(200)
		} else {
			c.Next()
		}
	}
}
func RegisterUCsAndRunGinGonic(iDatabase domain.PSDataBase,
	iImageRepo domain.PSImageArchivePort,
	iDataprovider domain.PSDataProviderPort,
	iDatareader domain.PSProviderDataReaderPort,
	iLogger domain.PSLogPort) {

	/*----------------------------------------------------------*/
	restServerDatabase = iDatabase
	restServerImageRepo = iImageRepo
	restServerDataprovider = iDataprovider
	restServerDatareader = iDatareader
	restServerLogger = iLogger
	/*----------------------------------------------------------*/
	restAdapter := GinNewAdapter()

	restAdapter.router.Use(CORSMiddleware())

	restAdapter.router.Static("/epsFileSys", "./epsFileSys")
	/*----------------------------------------------------------*/
	restAdapter.router.GET("/", HandleHealthCheck)
	restAdapter.router.GET("/ps_image", handleCallHelloPSImageApplGin)
	restAdapter.router.GET("/ps_image/ImageData/Entity/:PSESD/:ENTITYTYPE/:ENTITYID", handleGetImageData)
	restAdapter.router.GET("/ps_image/ImageData/MeldungImage/PSESD/:PSESD/MELID/:MELEXTID", handleGetPSMeldungImage)

	restAdapter.router.POST("/ps_image/ImageDataCollItemlist", handleGetImageDataArtItemlist)

	restAdapter.router.POST("/ps_meta/CalendarService", getCalendarHandlerGin)
	/*----------------------------------------------------------*/
	restAdapter.router.GET("/ps_meta/PSDataService/Erscheinungsdatum/:Erscheinungsdatum", getPSHandlerGin)
	/*----------------------------------------------------------*/
	restAdapter.router.GET("/ps_meta/PSDateBeforeService/Referenzdatum/:Referenzdatum", getPSDateBeforeHandlerGin)
	/*----------------------------------------------------------*/
	restAdapter.router.GET("/ps_meta/PSDateNextService/Referenzdatum/:Referenzdatum", getPSDateNextHandlerGin)
	/*----------------------------------------------------------*/
	restAdapter.router.POST("/ps_meta/PSDateLastService", getPSDateLastHandlerGin)
	/*----------------------------------------------------------*/
	restAdapter.router.POST("/ps_meta/UserDataService", getUsrHandlerGin)
	restAdapter.router.POST("/ps_meta/UserDataViaToken", getUsrViaTokenHandlerGin)
	/*----------------------------------------------------------*/
	restAdapter.router.GET("/ps_meta/TopicsService/Erscheinungsdatum/:Erscheinungsdatum", getMelCollHandlerGin)
	restAdapter.router.GET("/ps_meta/TopicsService/DisplayOnlyRubrics", getMelCollHandlerGin)
	restAdapter.router.POST("/ps_meta/TopicsService/Erscheinungsdatum/:Erscheinungsdatum", getMelCollHandlerGin)
	restAdapter.router.POST("/ps_meta/TopicsService/DisplayOnlyRubrics", getMelCollHandlerGin)
	/*----------------------------------------------------------*/
	restAdapter.router.POST("/ps_meta/ContentsService/Erscheinungsdatum/:Erscheinungsdatum", getArtCollHandlerGin)
	restAdapter.router.GET("/ps_meta/PSArticleService/:PSExtId/:ArtExtId", getArtHandlerGin)

	/*----------------------------------------------------------*/
	restAdapter.router.GET("/ps_meta/GetAuthKeys/UsrRoles/:UsrRoles", getAuthKeysHandlerGin)

	restAdapter.router.POST("/ps_meta/UserLogin", getLoginHandlerGin)
	restAdapter.router.POST("/ps_meta/UserServiceExistsLoginname", existsLoginnameHandlerGin)
	restAdapter.router.POST("/ps_meta/UserServiceExistsEmail", existsEmailHandlerGin)
	restAdapter.router.POST("/ps_meta/CreateUser", createUsrHandlerGin)
	restAdapter.router.POST("/ps_meta/UpdateUser", updateUsrHandlerGin)

	/*----------------------------------------------------------*/
	restAdapter.router.POST("/ps_meta/PSImportService/LogicalNameImportsystem/:LogicalNameImportsystem", importPSHandlerGin)
	restAdapter.router.POST("/ps_meta/PSPublishMarkService", getPublishMarkerHandlerGin)

	/*----------------------------------------------------------*/
	restAdapter.router.GET("/ps_meta/ArticleQueryHitlistSTD/:QueryFor", getArticleQueryHitlistHdGin)
	restAdapter.router.POST("/ps_meta/ArticleQueryHitlistSTD/:QueryFor", getArticleQueryHitlistHdGin)
	restAdapter.router.GET("/ps_meta/ArticleQueryHitlistEXT/:QueryFor", getArticleQueryHitlistHdGin)
	restAdapter.router.POST("/ps_meta/ArticleQueryHitlistEXT/:QueryFor", getArticleQueryHitlistHdGin)
	/*----------------------------------------------------------*/
	restAdapter.router.GET("/ps_meta/ArticleIngestOrder/INGESTOP/:INGESTOP/PSESD/:PSESD", ingestOrderHandlerGin)
	restAdapter.router.GET("/ps_meta/ArticleIngestOrder/INGESTOP/:INGESTOP/PSESD/:PSESD/ARTEXTID/:ARTEXTID", ingestOrderHandlerGin)
	restAdapter.router.GET("/ps_meta/ArticleIngestOrder/INGESTOP/:INGESTOP/PSESDFROM/:PSESDFROM/PSESDTO/:PSESDTO", ingestOrderHandlerGin)
	restAdapter.router.GET("/ps_meta/execIngest/:NumOrders", execIngestHandlerGin)

	/*----------------------------------------------------------*/
	restAdapter.router.GET("/ps_meta/Collection/GetActive", collectionGetActiveHandlerGin)
	restAdapter.router.POST("/ps_meta/Collection/GetActive", collectionGetActiveHandlerGin)
	restAdapter.router.POST("/ps_meta/CollectionOrder/ORDERTYPE/:ORDERTYPE", collectionOrderHandlerGin) //NEW,DEL,ACTIVATE
	restAdapter.router.POST("/ps_meta/CollectionItemOrder/ORDERTYPE/:ORDERTYPE", collectionItemOrderHandlerGin)
	restAdapter.router.POST("/ps_meta/CollectionItemListOrder/ORDERTYPE/:ORDERTYPE", collectionItemListOrderHandlerGin)

	restAdapter.router.POST("/ps_meta/CollectionGetItemList", collectionGetItemListGin)
	restAdapter.router.POST("/ps_meta/CollectionGetArticleList", collectionGetArticleListGin)
	restAdapter.router.POST("/ps_meta/ItemlistArticles", itemlistGetArticleListGin)

	/*----------------------------------------------------------*/
	restAdapter.router.GET("/ps_meta/authViaKeycloak/login", sec.GolangKeyCloakLoginGin)
	restAdapter.router.GET("/ps_meta/authViaKeycloak/oidc/callback", sec.GolangKeyCloakOIDCCallbackGin)
	restAdapter.router.POST("/ps_meta/authViaKeycloak/refreshToken", sec.GolangKeyCloakRefreshTokenGin)
	restAdapter.router.GET("/ps_meta/authViaKeycloakLogout", sec.GolangKeyCloakLogoutGin)
	restAdapter.router.POST("/ps_meta/authViaKeycloakLogout", sec.GolangKeyCloakLogoutGin)

	/*----------------------------------------------------------*/
	if bootstrap.ApplConf.UsePSSysregAsRegistrator {
		lResultRegisterMe := syscommanager.ApplServiceCom.RegisterMe()

		if !lResultRegisterMe.ResultOk {
			log.Fatal("Self-Registration for Health-Checks FAILED. Application will be stopped. Reason: " + lResultRegisterMe.ResultDetails)
		} else {
			log.Print("Self-Registration for Health-Checks done.")
		}
	}
	restAdapter.ListenAndServe()
	/*----------------------------------------------------------*/
}

func (a GinAdapter) ListenAndServe() {
	log.Printf("Listening on " + bootstrap.ThisPort)

	if bootstrap.ApplConf.UsePSSysregAsRegistrator {
		go func() {
			time.Sleep(10 * time.Second)
			lResultGetPortMS := syscommanager.ApplServiceCom.GetPortMicroservice(syscommanager.MicroServiceNamePsImage)
			lPSImagePort := lResultGetPortMS.Port
			log.Printf("Using ServiceLocation for ps_image:  " + bootstrap.ServiceLocationIDPSImage + ":" + lPSImagePort)
		}()
		go func() {
			time.Sleep(10 * time.Second)
			lResultGetPortMS := syscommanager.ApplServiceCom.GetPortMicroservice(syscommanager.MicroServiceNamePsQuery)
			lPSQueryPort := lResultGetPortMS.Port
			log.Printf("Using ServiceLocation for ps_query:  " + bootstrap.ServiceLocationIDPSQuery + ":" + lPSQueryPort)
		}()
		go func() {
			time.Sleep(10 * time.Second)
			lResultGetPortMS := syscommanager.ApplServiceCom.GetPortMicroservice(syscommanager.MicroServiceNamePsCollect)
			lPSCollectPort := lResultGetPortMS.Port
			log.Printf("Using ServiceLocation for ps_collect:  " + bootstrap.ServiceLocationIDPSCollect + ":" + lPSCollectPort)
		}()
	} else {
		log.Printf("Using Using ServiceLocation  for ps_image:  " + bootstrap.ServiceLocationIDPSImage + ":" + bootstrap.PortPSImage)
		log.Printf("Using Using ServiceLocation  for ps_query:  " + bootstrap.ServiceLocationIDPSQuery + ":" + bootstrap.PortPSQuery)
		log.Printf("Using Using ServiceLocation  for ps_collect:  " + bootstrap.ServiceLocationIDPSCollect + ":" + bootstrap.PortPSCollect)
	}
	mysqldbserverRef := bootstrap.ThisDbhostIP + ":" + bootstrap.ThisDbhostPORT
	log.Printf("Using sqldbserverRef:" + mysqldbserverRef)

	http.ListenAndServe(":"+bootstrap.ThisPort, a.router)
}

func HandleHealthCheck(c *gin.Context) {

	c.JSON(http.StatusOK, "ps_meta (micro-)service is alive.")
}

func handleCallHelloPSImageApplGin(c *gin.Context) {

	lPSImagePort := ""
	if bootstrap.ApplConf.UsePSSysregAsRegistrator {
		lResultGetPortMS := syscommanager.ApplServiceCom.GetPortMicroservice(syscommanager.MicroServiceNamePsImage)
		lPSImagePort = lResultGetPortMS.Port
	} else {
		lPSImagePort = bootstrap.PortPSImage
	}
	serviceadr := bootstrap.ThisCommunnicationProtokoll + "://" + bootstrap.ServiceLocationIDPSImage + ":" + lPSImagePort + "/Hello"
	resp, err := http.Get(serviceadr)
	if err != nil {
		//println(err.Error())
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	c.JSON(http.StatusOK, string(body))
}

/*
UC_DeactivateUsr := uc.NewUsrClassInstanceGetter(iDatabase, iLogger)
deactivateUsrHandler := restAdapter.MakeUsrDeactivateHandler(UC_DeactivateUsr)
restAdapter.router.HandleFunc("/ps_meta/UserDataService/Deactivate", deactivateUsrHandler)
UC_ReactivateUsr := uc.NewUsrClassInstanceGetter(iDatabase, iLogger)
reactivateUsrHandler := restAdapter.MakeUsrReactivateHandler(UC_ReactivateUsr)
restAdapter.router.HandleFunc("/ps_meta/UserDataService/Reactivate", reactivateUsrHandler)
// ------------------------------------------- //
UC_GetArt := uc.NewArticleGetter(iDatabase, iLogger)
getArtHandler := restAdapter.MakeArtHandler(UC_GetArt)
restAdapter.router.HandleFunc("/ps_meta/PSArticleService/{PSExtId}/{ArtExtId}", getArtHandler)

// ------------------------------------------- //
UC_GetMeldung := uc.NewMeldungGetter(iDatabase, iLogger)
getMeldungHandler := restAdapter.MakeMeldungHandler(UC_GetMeldung)
restAdapter.router.HandleFunc("/ps_meta/PSMeldungService/{PSExtId}/{MelExtId}", getMeldungHandler)*/
