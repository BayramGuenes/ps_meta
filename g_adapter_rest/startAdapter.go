package g_adapter_rest

import (
	"log"
	"net/http"
	domain "ps_meta/a_domain"
	//image_archiv "ps_meta/i_model_image_interface"
	uc "ps_meta/b_usecases"

	bootstrap "ps_meta/z_bootstrap"
	syscommanager "ps_meta/h_service_communication_manager"

	"github.com/gorilla/mux"
)

type Adapter struct {
	router *mux.Router
}

func NewAdapter() Adapter {
	router := mux.NewRouter()
	return Adapter{router}
}

func (a Adapter) HandleFunc(path string, f func(http.ResponseWriter,
	*http.Request)) *mux.Route {
	return a.router.NewRoute().Path(path).HandlerFunc(f)
}

func (a Adapter) ListenAndServe() {
	log.Printf("Listening on " + bootstrap.ThisServerAddr)

	lResultGetPortMS:= syscommanager.ApplServiceCom.GetPortMicroservice(syscommanager.MicroServiceNamePsImage)
	lPSImagePort:=lResultGetPortMS.Port

	log.Printf("Using port for ps_image:  " + lPSImagePort)
	//http.ListenAndServe(bootstrap.ThisHostname+":"+bootstrap.ThisPort, a.router)
	http.ListenAndServe(":"+bootstrap.ThisPort, a.router)
}

func RegisterUCsAndRun(iDatabase domain.PSDataBase,
	iImageRepo domain.PSImageArchivePort,
	iDataprovider domain.PSDataProviderPort,
	iDatareader domain.PSProviderDataReaderPort,
	iLogger domain.PSLogPort) {

	restAdapter := NewAdapter()

	// ------------------------------------------- //
	UC_ImportPS := uc.NewPSDataImporter(iDataprovider, iDatareader, iDatabase,
		  iImageRepo,
		  iLogger)
	importPSHandler := restAdapter.MakePSImportHandler(UC_ImportPS)
	restAdapter.router.HandleFunc("/ps_meta/PSImportService/{LogicalNameImportsystem}", importPSHandler)
	restAdapter.router.HandleFunc("/ps_meta/PSImportService/{LogicalNameImportsystem}/{Token}", importPSHandler)
	restAdapter.router.HandleFunc("/ps_meta/PSImportService/LogicalNameImportsystem/{LogicalNameImportsystem}/Token/{Token}", importPSHandler)
	restAdapter.router.HandleFunc("/ps_meta/PSImportService/{LogicalNameImportsystem}/{Importusr}/{RFA}/{UsrPwd}", importPSHandler)

	// ------------------------------------------- //
	UC_GetUsr := uc.NewUsrClassInstanceGetter(iDatabase, iLogger)
	getUsrHandler := restAdapter.MakeUsrGetHandler(UC_GetUsr)
	restAdapter.router.HandleFunc("/ps_meta/UserDataService", getUsrHandler)
	restAdapter.router.HandleFunc("/ps_meta/UserDataService/{Token}", getUsrHandler)
	UC_GetUsrViaToken := uc.NewUsrClassInstanceGetter(iDatabase, iLogger)
	getUsrViaTokenHandler := restAdapter.MakeUsrGetViaTokenHandler(UC_GetUsrViaToken)
	restAdapter.router.HandleFunc("/ps_meta/UserDataViaToken", getUsrViaTokenHandler)

	UC_LoginUsr := uc.NewUsrClassInstanceGetter(iDatabase, iLogger)
	getLoginHandler := restAdapter.MakeLoginUsrHandler(UC_LoginUsr)
	restAdapter.router.HandleFunc("/ps_meta/UserLogin", getLoginHandler)

	UC_CheckLoginname := uc.NewUsrClassInstanceGetter(iDatabase, iLogger)
	existsLoginnameHandler := restAdapter.MakeLoginnameExistsHandler(UC_CheckLoginname)
	restAdapter.router.HandleFunc("/ps_meta/UserServiceExistsLoginname", existsLoginnameHandler)

	UC_CheckEmail := uc.NewUsrClassInstanceGetter(iDatabase, iLogger)
	existsEmailHandler := restAdapter.MakeEmailExistsHandler(UC_CheckEmail)
	restAdapter.router.HandleFunc("/ps_meta/UserServiceExistsEmail", existsEmailHandler)

	UC_CreateUsr := uc.NewUsrClassInstanceGetter(iDatabase, iLogger)
	createUsrHandler := restAdapter.MakeUsrCreateHandler(UC_CreateUsr)
	restAdapter.router.HandleFunc("/ps_meta/CreateUser", createUsrHandler)
	//restAdapter.router.HandleFunc("/ps_meta/UserDataService", createUsrHandler)

	UC_UpdateUsr := uc.NewUsrClassInstanceGetter(iDatabase, iLogger)
	updateUsrHandler := restAdapter.MakeUsrUpdateHandler(UC_UpdateUsr)
	restAdapter.router.HandleFunc("/ps_meta/UpdateUser", updateUsrHandler)

	UC_DeactivateUsr := uc.NewUsrClassInstanceGetter(iDatabase, iLogger)
	deactivateUsrHandler := restAdapter.MakeUsrDeactivateHandler(UC_DeactivateUsr)
	restAdapter.router.HandleFunc("/ps_meta/UserDataService/Deactivate", deactivateUsrHandler)
	UC_ReactivateUsr := uc.NewUsrClassInstanceGetter(iDatabase, iLogger)
	reactivateUsrHandler := restAdapter.MakeUsrReactivateHandler(UC_ReactivateUsr)
	restAdapter.router.HandleFunc("/ps_meta/UserDataService/Reactivate", reactivateUsrHandler)

	// ------------------------------------------- //
	UC_GetCalendar := uc.NewCalendarInstanceGetter(iDatabase, iLogger)
	getCalendarHandler := restAdapter.MakeCalendarGetHandler(UC_GetCalendar)
	restAdapter.router.HandleFunc("/ps_meta/CalendarService", getCalendarHandler)
	restAdapter.router.HandleFunc("/ps_meta/CalendarService/{Token}", getCalendarHandler)

	// ------------------------------------------- //
	UC_GetPS := uc.NewPSGetter(iDatabase, iLogger)
	getPSHandler := restAdapter.MakePSGetHandler(UC_GetPS)
	restAdapter.router.HandleFunc("/ps_meta/PSDataService/{Erscheinungsdatum}", getPSHandler)
	restAdapter.router.HandleFunc("/ps_meta/PSDataService/{Erscheinungsdatum}/{Token}", getPSHandler)

	// ------------------------------------------- //
	UC_GetPSDateBefore := uc.NewPSGetter(iDatabase, iLogger)
	getPSDateBeforeHandler := restAdapter.MakePSGetDateBeforeHandler(UC_GetPSDateBefore)
	restAdapter.router.HandleFunc("/ps_meta/PSDateBeforeService/{Referenzdatum}", getPSDateBeforeHandler)
	restAdapter.router.HandleFunc("/ps_meta/PSDateBeforeService/{Referenzdatum}/{Token}", getPSDateBeforeHandler)

	// ------------------------------------------- //
	UC_GetPSDateNext := uc.NewPSGetter(iDatabase, iLogger)
	getPSDateNextHandler := restAdapter.MakePSGetDateNextHandler(UC_GetPSDateNext)
	restAdapter.router.HandleFunc("/ps_meta/PSDateNextService/{Referenzdatum}", getPSDateNextHandler)
	restAdapter.router.HandleFunc("/ps_meta/PSDateNextService/{Referenzdatum}/{Token}", getPSDateNextHandler)

	// ------------------------------------------- //
	UC_GetPSDateLast := uc.NewPSGetter(iDatabase, iLogger)
	getPSDateLastHandler := restAdapter.MakePSGetDateLastHandler(UC_GetPSDateLast)
	restAdapter.router.HandleFunc("/ps_meta/PSDateLastService", getPSDateLastHandler)
	restAdapter.router.HandleFunc("/ps_meta/PSDateLastService/{Token}", getPSDateLastHandler)

	// ------------------------------------------- //
	UC_GetMelCollection := uc.NewMeldungCollGetter(iDatabase, iLogger)
	getMelCollHandler := restAdapter.MakeMelCollHandler(UC_GetMelCollection)
	restAdapter.router.HandleFunc("/ps_meta/TopicsService/{Erscheinungsdatum}", getMelCollHandler)
	restAdapter.router.HandleFunc("/ps_meta/TopicsService/Erscheinungsdatum/{Erscheinungsdatum}", getMelCollHandler)
	restAdapter.router.HandleFunc("/ps_meta/TopicsService/{Erscheinungsdatum}/{Token}", getMelCollHandler)
	restAdapter.router.HandleFunc("/ps_meta/TopicsService/{DisplayOnlyRubrics}/{Erscheinungsdatum}", getMelCollHandler)
	restAdapter.router.HandleFunc("/ps_meta/TopicsService/{DisplayOnlyRubrics}/{Erscheinungsdatum}/{Token}", getMelCollHandler)

	// ------------------------------------------- //
	UC_GetArtCollection := uc.NewArticleCollGetter(iDatabase, iLogger)
	getArtCollHandler := restAdapter.MakeArtCollHandler(UC_GetArtCollection)
	restAdapter.router.HandleFunc("/ps_meta/ContentsService/{Erscheinungsdatum}", getArtCollHandler)
	restAdapter.router.HandleFunc("/ps_meta/ContentsService/Erscheinungsdatum/{Erscheinungsdatum}", getArtCollHandler)
	restAdapter.router.HandleFunc("/ps_meta/ContentsService/{Token}/{Erscheinungsdatum}", getArtCollHandler)

	// ------------------------------------------- //
	UC_GetArt := uc.NewArticleGetter(iDatabase, iLogger)
	getArtHandler := restAdapter.MakeArtHandler(UC_GetArt)
	restAdapter.router.HandleFunc("/ps_meta/PSArticleService/{PSExtId}/{ArtExtId}", getArtHandler)

	// ------------------------------------------- //
	UC_GetMeldung := uc.NewMeldungGetter(iDatabase, iLogger)
	getMeldungHandler := restAdapter.MakeMeldungHandler(UC_GetMeldung)
	restAdapter.router.HandleFunc("/ps_meta/PSMeldungService/{PSExtId}/{MelExtId}", getMeldungHandler)

	// ------------------------------------------- //
	UC_PublishPS := uc.NewPSGetter(iDatabase, iLogger)
	getPublishMarkerHandler := restAdapter.MakePublishMarkerHandler(UC_PublishPS)
	restAdapter.router.HandleFunc("/ps_meta/PSPublishMarkService", getPublishMarkerHandler)


	/*----------------------------------------------------------*/
	lResultRegisterMe := syscommanager.ApplServiceCom.RegisterMe()

	if !lResultRegisterMe.ResultOk {
		log.Fatal("Die Selbstregistrierung für Health-Checks ist fehlgeschlagen, Anwendung wird gestoppt. Ursache: " + lResultRegisterMe.ResultDetails)
	}
	// ------------------------------------------- //
	//bootstrap.GetThisServerAddr()
	restAdapter.ListenAndServe()
	// ------------------------------------------- //
}
