package g_adapter_rest

import (
	//"encoding/json"
	//"errors"
	"net/http"
	uc "ps_meta/b_usecases"
	sec "ps_meta/s_security"

	//"strings"

	"github.com/gin-gonic/gin"
)

func getPSDateNextHandlerGin(c *gin.Context) {

	if c.Request.Method == "GET" {

		lPSErscheinungsdatum := ""
		lPSErscheinungsdatum = c.Param("Referenzdatum")
		if len(lPSErscheinungsdatum) == 0 {
			c.AbortWithStatus(http.StatusBadRequest)
			return
		}

		UC_GetPSDateNext := uc.NewPSGetter(restServerDatabase, restServerLogger)
		//lWithInactiv := false

		lToken, lKindOfAuth := sec.ExtractTokenFromHTTPRequestGin(c)

		lRefDate := lPSErscheinungsdatum
		lPSDate, _, err := UC_GetPSDateNext.GetDateNextPS(lToken, lKindOfAuth, lRefDate)

		if err != nil {
			c.AbortWithStatus(http.StatusInternalServerError)
			return
		}

		c.Header("Content-Type", "application/json")
		c.Header("Access-Control-Allow-Origin", "*")
		lPSDateFormatted := formatDateYYYYminMMminDD(lPSDate)
		c.JSON(http.StatusOK, lPSDateFormatted)

	}
}
