package g_adapter_rest

import (
	"net/http"
	uc "ps_meta/b_usecases"
	sec "ps_meta/s_security"

	"github.com/gin-gonic/gin"
)

func contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

func getPSDateLastHandlerGin(c *gin.Context) {
	////println("getPSDateLastHandlerGin called")

	c.Header("Content-Type", "application/json")
	c.Header("Access-Control-Allow-Origin", "*")

	UC_GetPSDateLast := uc.NewPSGetter(restServerDatabase, restServerLogger)

	lToken, lKindOfAuth := sec.ExtractTokenFromHTTPRequestGin(c)

	lPSDate, err := UC_GetPSDateLast.GetDateLastPS(lToken, lKindOfAuth)
	if err != nil {
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}
	lPSDateFormatted := formatDateYYYYminMMminDD(lPSDate)
	c.JSON(http.StatusOK, lPSDateFormatted)

}
