package g_adapter_rest

import (
	"encoding/json"
	"net/http"
	domain "ps_meta/a_domain"
	uc "ps_meta/b_usecases"
	sec "ps_meta/s_security"
)

/*type RoleType struct {
	Role string `json:"role"`
}*/
type UsrOutput struct {
	User         domain.User
	UserToken    domain.UserToken
	UserAuthkeys domain.UserAuthKeys
}

/*struct {
	User a_domain.User
	Role string `json:"user"`
}*/

// ------------------------------------------------------------------------- //
// UC Getusr
// ------------------------------------------------------------------------- //

// Result Int ->HTTP
func (a Adapter) writeHTTPResponseUsr(iUsr domain.User, iUsrToken domain.UserToken, iAuthKeys domain.UserAuthKeys, w http.ResponseWriter) error {

	var outtype UsrOutput
	outtype.User = iUsr
	outtype.UserToken = iUsrToken
	outtype.UserAuthkeys = iAuthKeys
	/*if iUsr.HasExpertRights {
		outtype.Role = domain.RoleExpert
	} else {
		outtype.Role = domain.RoleNormal
	}*/
	////println("iUsr-pwd " + iUsr.Pwd)
	////println(outtype.Pwd)
	//b, err := json.Marshal(eUsr)
	b, err := json.Marshal(outtype)

	if err != nil {
		return err
	}

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	////println("usr:" + string(b))
	w.Write(b)
	return nil
}

func (a Adapter) MakeLoginUsrHandler(iUsrClass uc.UsrClass) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		lUsrObj, err := a.readHTTPQueryParamPOST(r)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		lUser, lUserToken, lUserAuthkeys, err := iUsrClass.LoginUsr(lUsrObj.Loginname, lUsrObj.Rfa, lUsrObj.Email)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		// 3. result intern ->HTTP
		if err = a.writeHTTPResponseUsr(lUser, lUserToken, lUserAuthkeys, w); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
		}

	}
}

func (a Adapter) MakeUsrGetHandler(iUsrClass uc.UsrClass) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		lUsrObj, err := a.readHTTPQueryParamPOST(r)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		lUser, lUserAuthkeys, err := iUsrClass.GetUsr(lUsrObj.Loginname, lUsrObj.Rfa, lUsrObj.Email)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		// 3. result intern ->HTTP
		//lUserToken := domain.UserToken{}
		lUserToken, lSavedUserToken, err := iUsrClass.Repository.GetUsrToken(lUser.Loginname, lUser.Rfa)
		if err == nil {
			if !lSavedUserToken {
				lValidFrom, lValidTo := sec.GetValidFromTo()

				err = iUsrClass.Repository.UsrDBPort.SetUserToken(lUser.Loginname, lUser.Rfa, lUserToken.Token, lValidFrom, lValidTo)
				if err == nil {
					lUserToken.Loginname = lUser.Loginname
					lUserToken.Rfa = lUser.Rfa
					lUserToken.Validdatefrom = lValidFrom
					lUserToken.Validdateto = lValidTo
				}

			}

		}

		if err = a.writeHTTPResponseUsr(lUser, lUserToken, lUserAuthkeys, w); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
		}
	}
}

func (a Adapter) MakeUsrGetViaTokenHandler(iUsrClass uc.UsrClass) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		lToken := sec.ExtractTokenFromHTTPRequest(r)
		//lUsrObj, err := a.readHTTPQueryParamPOST(r)

		lUser, lAuthkeys, err := iUsrClass.GetUsrViaToken(lToken, "")

		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		////println("lUser.Loginname:" + lUser.Loginname)
		// 3. result intern ->HTTP
		lUserToken, lSavedUserToken, err := iUsrClass.Repository.GetUsrToken(lUser.Loginname, lUser.Rfa)
		if err == nil {
			if !lSavedUserToken {
				lValidFrom, lValidTo := sec.GetValidFromTo()

				err = iUsrClass.Repository.UsrDBPort.SetUserToken(lUser.Loginname, lUser.Rfa, lUserToken.Token, lValidFrom, lValidTo)
				if err == nil {
					lUserToken.Loginname = lUser.Loginname
					lUserToken.Rfa = lUser.Rfa
					lUserToken.Validdatefrom = lValidFrom
					lUserToken.Validdateto = lValidTo
				}

			}

		}
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		if err = a.writeHTTPResponseUsr(lUser, lUserToken, lAuthkeys, w); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
		}
	}
}

func (a Adapter) MakeLoginnameExistsHandler(iUsrClass uc.UsrClass) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		//fmt.//println("MakeLoginnameExistsHandler called")
		// 1. HTTP-> Intern
		lUsr, err := a.readHTTPQueryParamPOST(r)

		// 2. Exec domain function
		lUsrObj, lAuthkeys, err := iUsrClass.GetUsr(lUsr.Loginname, lUsr.Rfa, lUsr.Email)
		//fmt.//println("Lusr-rfa:" + lUsrObj.Name)

		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		// 3. result intern ->HTTP
		lUsrToken := domain.UserToken{}
		if err = a.writeHTTPResponseUsr(lUsrObj, lUsrToken, lAuthkeys, w); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
		}

	}
}

func (a Adapter) MakeEmailExistsHandler(iUsrClass uc.UsrClass) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		//fmt.//println("MakeLoginnameExistsHandler called")
		// 1. HTTP-> Intern
		lUsr, err := a.readHTTPQueryParamPOST(r)

		// 2. Exec domain function
		lUsrObj, lAuthkeys, err := iUsrClass.GetUsr(lUsr.Loginname, lUsr.Rfa, lUsr.Email)
		//fmt.//println("Lusr-rfa:" + lUsrObj.Name)

		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		// 3. result intern ->HTTP
		lUsrToken := domain.UserToken{}
		if err = a.writeHTTPResponseUsr(lUsrObj, lUsrToken, lAuthkeys, w); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
		}

	}
}

func (a Adapter) MakeUsrCreateHandler(iUsrClass uc.UsrClass) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		////println("MakeUsrCreateHandler called")
		// 1. HTTP-> Intern
		lUser, err := a.readHTTPQueryParamPOST(r)

		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		// 2. Exec domain function
		_, err = iUsrClass.CreateUsr(lUser.Loginname, lUser.Rfa, lUser.Name, lUser.Pwd, lUser.Email, "web")
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		// 3. result intern ->HTTP
		lUserToken, lSavedUserToken, err := iUsrClass.Repository.GetUsrToken(lUser.Loginname, lUser.Rfa)
		if err == nil {
			if !lSavedUserToken {
				lValidFrom, lValidTo := sec.GetValidFromTo()

				err = iUsrClass.Repository.UsrDBPort.SetUserToken(lUser.Loginname, lUser.Rfa, lUserToken.Token, lValidFrom, lValidTo)
				if err == nil {
					lUserToken.Loginname = lUser.Loginname
					lUserToken.Rfa = lUser.Rfa
					lUserToken.Validdatefrom = lValidFrom
					lUserToken.Validdateto = lValidTo
				}

			}

		}
		if err = a.writeHTTPResponseUsr(lUser, lUserToken, domain.UserAuthKeys{}, w); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
		}

	}
}

func (a Adapter) MakeUsrUpdateHandler(iUsrClass uc.UsrClass) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		// 1. HTTP-> Intern
		lUsr, err := a.readHTTPQueryParamPOST(r)

		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		// 2. Exec domain function
		_, err = iUsrClass.UpdateUsr(lUsr)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		//lUsrObj, err := a.readHTTPQueryParamPOST(r)

		lUser, lAuthkeys, err := iUsrClass.GetUsr(lUsr.Loginname, lUsr.Rfa, lUsr.Email)
		if len(lAuthkeys) > 0 {
			////println(" lAuthkeys:" + lAuthkeys[0])
		}
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		// 3. result intern ->HTTP
		lUserToken, lSavedUserToken, err := iUsrClass.Repository.GetUsrToken(lUser.Loginname, lUser.Rfa)
		if err == nil {
			if !lSavedUserToken {
				lValidFrom, lValidTo := sec.GetValidFromTo()

				err = iUsrClass.Repository.UsrDBPort.SetUserToken(lUser.Loginname, lUser.Rfa, lUserToken.Token, lValidFrom, lValidTo)
				if err == nil {
					lUserToken.Loginname = lUser.Loginname
					lUserToken.Rfa = lUser.Rfa
					lUserToken.Validdatefrom = lValidFrom
					lUserToken.Validdateto = lValidTo
				}

			}

		}

		////println(" lUserToken:" + lUserToken.Token)
		if err = a.writeHTTPResponseUsr(lUser, lUserToken, lAuthkeys, w); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
		}

	}
}
func (a Adapter) MakeUsrDeactivateHandler(iUsrClass uc.UsrClass) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		// 1. HTTP-> Intern
		lUsr, err := a.readHTTPQueryParamPOST(r)

		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		// 2. Exec domain function
		_, err = iUsrClass.DeactivateUsr(lUsr.Loginname, lUsr.Rfa, lUsr.Loginname, "web")
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		// 3. result intern ->HTTP
		lUsrToken := domain.UserToken{}
		if err = a.writeHTTPResponseUsr(lUsr, lUsrToken, domain.UserAuthKeys{}, w); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
		}

	}
}

func (a Adapter) MakeUsrReactivateHandler(iUsrClass uc.UsrClass) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		// 1. HTTP-> Intern
		lUsr, err := a.readHTTPQueryParamPOST(r)

		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		// 2. Exec domain function
		_, err = iUsrClass.ActivateUsr(lUsr.Loginname, lUsr.Rfa, lUsr.Loginname, "web")
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		// 3. result intern ->HTTP
		lUsrToken := domain.UserToken{}
		if err = a.writeHTTPResponseUsr(lUsr, lUsrToken, domain.UserAuthKeys{}, w); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
		}

	}
}

// HTTP ->Int
func (a Adapter) readHTTPQueryParamPOST(r *http.Request) (eUser domain.User, err error) {

	eUser = domain.User{}
	err = nil
	eUser.Loginname = r.FormValue("PSUsr.Loginname")
	eUser.Name = r.FormValue("PSUsr.Name")
	eUser.Pwd = r.FormValue("PSUsr.Pwd")
	eUser.Email = r.FormValue("PSUsr.Email")
	eUser.Rfa = r.FormValue("PSUsr.Rfa")

	return

}
