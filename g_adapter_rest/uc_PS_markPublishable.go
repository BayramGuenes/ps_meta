package g_adapter_rest

import (
	"encoding/json"
	"errors"
	"net/http"
	uc "ps_meta/b_usecases"
	sec "ps_meta/s_security"
	"strings"

	"github.com/gorilla/mux"
)

// ------------------------------------------------------------------------- //
// UC GetPS
// ------------------------------------------------------------------------- //
// HTTP ->Int
func (a Adapter) readHTTPQueryParamMarkPublishable(r *http.Request) (eErscheinungsdatum string, err error) {

	/*body, err := ioutil.ReadAll(r.Body)
	if err != nil {
	  return domain.Invoice{}, err
	}
	var invoice domain.Invoice
	if err := json.Unmarshal(body, &invoice); err != nil {
	return domain.Invoice{}, err
	}*/

	eErscheinungsdatum = ""
	err = nil

	vars := mux.Vars(r)
	for i, v := range vars {
		switch i {
		case "Erscheinungsdatum":
			eErscheinungsdatum = strings.SplitAfter(v, "=")[1]

		}
		////println(i, v)
	}
	if eErscheinungsdatum == "" {

		err = errors.New("Erscheinungsdatum als Query-Parameter nicht übergeben.")
	}

	return

}

// Result Int ->HTTP
func (a Adapter) writeHTTPResponseMarkPublishable(psESD string, w http.ResponseWriter) error {
	date := formatDateYYYYminMMminDD(psESD)
	b, err := json.Marshal(date)
	if err != nil {
		return err
	}

	w.Header().Set("Content-Type", "text/plain")
	w.Header().Set("Access-Control-Allow-Origin", "*")

	w.Write(b)
	return nil
}

func (a Adapter) MakePublishMarkerHandler(iPSEntityClass uc.PSEntityClass) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		////println(" MakePublishMarkerHandler called")
		// 1. HTTP-> Intern
		lToken := sec.ExtractTokenFromHTTPRequest(r)
		//lESD, err := a.readHTTPQueryParamMarkPublishable(r)
		lESD, err := a.readHTTPQueryParamPSPOST(r)
		////println(" lRefDate=" + lRefDate)
		////println("(a Adapter) MakePublishMarkerHandler-lESD" + lESD)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		lESD = formatDateYYYYMMDD(lESD)
		iPSEntityClass.MarkPublishable(lESD, lToken, "")

		// 3. result intern ->HTTP
		if err = a.writeHTTPResponseMarkPublishable(lESD, w); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
		}

	}
}

// HTTP ->Int
func (a Adapter) readHTTPQueryParamPSPOST(r *http.Request) (ePSDate string, err error) {

	ePSDate = r.FormValue("PS.Date")
	////println("(a Adapter) readHTTPQueryParamPSPOST" + ePSDate)
	err = nil

	return

}
