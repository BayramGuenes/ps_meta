package g_adapter_rest

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"ps_meta/a_domain"
	domain "ps_meta/a_domain"
	uc "ps_meta/b_usecases"
	sec "ps_meta/s_security"
	"regexp"
	"sort"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
)

type ArtContentsServiceArticles struct {
	Id         string
	Source     string
	Date       string
	Title      string
	Othertitle string
	Subtitle   string
	Teaser     string
	IsDictated string
	Pagenrtext string
	Text       string
}

type ArtContentsServiceRubric struct {
	Name     string
	Articles []ArtContentsServiceArticles
}

type ArtContentsServiceOutput struct {
	Prev    string
	Date    string
	Next    string
	Rubrics []ArtContentsServiceRubric
}

const teaserLength int = 267

// ------------------------------------------------------------------------- //
// UC GetPS
// ------------------------------------------------------------------------- //
// HTTP ->Int
func (a Adapter) readHTTPQueryParamArtColl(r *http.Request) (eESD string, err error) {

	eESD = ""
	err = nil
	vars := mux.Vars(r)
	for i, v := range vars {
		switch i {

		case "Erscheinungsdatum":

			lstringTab := strings.SplitAfter(v, "=")
			if len(lstringTab) > 1 {
				eESD = lstringTab[1]
			} else {
				eESD = v
			}
			if strings.Contains(eESD, "-") {
				eESD = formatDateYYYYMMDD(eESD)
			}

			/*		eESD = strings.SplitAfter(v, "=")[1]
					if strings.Contains(eESD, "-") {
						eESD = formatDateYYYYMMDD(eESD)
					}*/

		}
		////println(i, v)
	}

	if eESD == "" {
		err = errors.New("Erscheinungsdatum als Query-Parameter nicht übergeben.")
	}

	return

}

// Result Int ->HTTP
func (a Adapter) writeHTTPResponseArtColl(iToken string, iPSHeader a_domain.Pressespiegel, iArtColl []a_domain.Artikel, w http.ResponseWriter) error {

	var lartContentsServiceOutput ArtContentsServiceOutput

	lartContentsServiceOutput.Date = iPSHeader.PSESD
	serviceDateBefore := "/ps_meta/PSDateBeforeService/Referenzdatum=" + iPSHeader.PSESD + "/Token=" + iToken
	esdBeforeInterface, _ := callRestService(serviceDateBefore)
	////println("iPSHeader.PSErscheinungsdatum =" + iPSHeader.PSErscheinungsdatum + " iName =" + iName)
	esdBefore := fmt.Sprintf("%v", esdBeforeInterface)
	esdBefore = strings.Replace(esdBefore, "\"", "", -1)
	lartContentsServiceOutput.Prev = esdBefore
	serviceDateNext := "/ps_meta/PSDateNextService/Referenzdatum=" + iPSHeader.PSESD + "/Token=" + iToken
	esdNextInterface, _ := callRestService(serviceDateNext)
	esdNext := fmt.Sprintf("%v", esdNextInterface)
	esdNext = strings.Replace(esdNext, "\"", "", -1)

	lartContentsServiceOutput.Next = esdNext

	lartContentsServiceOutput.Rubrics = getRubricsAndArtContents(iPSHeader.PSExtID, iArtColl)

	//	b, err := json.Marshal(eMelColl)
	b, err := json.Marshal(lartContentsServiceOutput)

	if err != nil {
		return err
	}

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")

	w.Write(b)
	return nil

}

func (a Adapter) MakeArtCollHandler(iArtCollClass uc.ArticleCollClass) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		////println(" MakeArtCollHandler called")
		// 1. HTTP-> Intern
		lToken := sec.ExtractTokenFromHTTPRequest(r)
		lESD, err := a.readHTTPQueryParamArtColl(r)

		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		lWithInactiv := false
		if len(lToken) > 0 {
			lUser, err := iArtCollClass.PSDataBase.GetUsrViaToken(lToken)
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
			lUserRoles, err := iArtCollClass.PSDataBase.UsrDBPort.GetUsrRoles(lUser.Loginname, lUser.Rfa) //GetUsrRoles
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
			lWithInactiv = a_domain.CheckAuthority(a_domain.AuthSearchWithInactive, lUserRoles, iArtCollClass.PSDataBase.AuthDBPort, iArtCollClass.Logger)
			println(lWithInactiv)
		}

		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		lPSHeader, lArtCollection, err := iArtCollClass.GetArticleCollection(lToken, "", lESD)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		// 3. result intern ->HTTP
		if err = a.writeHTTPResponseArtColl(lToken, lPSHeader, lArtCollection, w); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
		}

	}
}

//SORT mt_article BY posnr psp rubrik .
type ArtByPosnrRubrik []a_domain.Artikel

func (pr ArtByPosnrRubrik) Len() int { return len(pr) }

func (pr ArtByPosnrRubrik) Swap(i, j int) { pr[i], pr[j] = pr[j], pr[i] }
func (pr ArtByPosnrRubrik) Less(i, j int) bool {
	intArtPSPosnr_i, _ := strconv.Atoi(pr[i].PSPosnr)
	intArtPSPosnr_j, _ := strconv.Atoi(pr[j].PSPosnr)

	if intArtPSPosnr_i < intArtPSPosnr_j {
		return true
	}
	if intArtPSPosnr_i == intArtPSPosnr_j {
		if strings.Compare(pr[i].PSRubrik, pr[j].PSRubrik) == -1 {
			return true
		}
	}
	return false
}

func getRubricsAndArtContents(iPSExtId string, iArtColl []a_domain.Artikel) (eResult []ArtContentsServiceRubric) {
	lArtcoll := []a_domain.Artikel{}
	for i := 0; i < len(iArtColl); i++ {
		lArtcoll = append(lArtcoll, iArtColl[i])
		////println(iArtColl[i].ArtPSPosnr + " " + iArtColl[i].ArtPSRubrik)
	}

	sort.Sort(ArtByPosnrRubrik(lArtcoll))
	/*//println("Later:")
	for i := 0; i < len(lArtcoll); i++ {
		//println(lArtcoll[i].ArtPSPosnr + " " + lArtcoll[i].ArtPSRubrik)
	}
	*/
	var lrowResult ArtContentsServiceRubric
	var rubrBef = ""
	for i := 0; i < len(lArtcoll); i++ {
		if rubrBef != lArtcoll[i].PSRubrik {
			lrowResult.Name = lArtcoll[i].PSRubrik
			lrowResult.Articles = getArtContentsArticlesRubrik(iPSExtId, lArtcoll, i, lArtcoll[i].PSRubrik)
			eResult = append(eResult, lrowResult)
		}
		rubrBef = lArtcoll[i].PSRubrik
	}

	return eResult
}

func getArtContentsArticlesRubrik(iPSExtId string, iArtCol []a_domain.Artikel, iRubrIndex int, iRubrik string) (eResult []ArtContentsServiceArticles) {
	lExit := false
	lResultRow := ArtContentsServiceArticles{}
	for i := iRubrIndex; !lExit && i < len(iArtCol); i++ {
		lArtikel := iArtCol[i]
		////println("lArtikel.ArtExtID:" + lArtikel.ArtExtID + " Subtitle:" + lArtikel.ArtUntertitel)
		if lArtikel.PSRubrik != iRubrik {
			//eResult = append(eResult, lResultRow)
			//lResultRow = ArtContentsServiceArticles{}
			lExit = true
		} else {
			lResultRow.Id = lArtikel.ExtID
			lArtikel.QuelleName = encodeAklContentSpecialSigns(lArtikel.QuelleName)
			lResultRow.Source = lArtikel.QuelleName
			lResultRow.Date = lArtikel.ESD

			lArtikel.Titel = encodeAklContentSpecialSigns(lArtikel.Titel)
			lResultRow.Title = lArtikel.Titel
			lArtikel.SonstTitel = encodeAklContentSpecialSigns(lArtikel.SonstTitel)
			lResultRow.Othertitle = lArtikel.SonstTitel

			lArtikel.Untertitel = encodeAklContentSpecialSigns(lArtikel.Untertitel)
			lResultRow.Subtitle = lArtikel.Untertitel
			//lResultRow.Teaser=lArtikel.
			lResultRow.Pagenrtext = lArtikel.QuelleSeitennr

			if ldiktiert, _ := getArtBelongsToDictatedMeldung(iPSExtId, lArtikel.MelExtID); ldiktiert {
				lResultRow.IsDictated = "true"
			} else {
				lResultRow.IsDictated = "false"
			}
			lResultRow.Teaser = lArtikel.Inhalttext
			if len(lArtikel.Inhalttext) > teaserLength {
				lOffsetLastWord, llastWord := getLastWordComplete(lArtikel.Inhalttext)
				////println("lOffsetLastWort:" + strconv.Itoa(lOffsetLastWord) + " llastWord:" + llastWord)
				lResultRow.Teaser = lResultRow.Teaser[:lOffsetLastWord] + " " + llastWord + " ..."
			}
			lResultRow.Teaser = encodeAklContentSpecialSigns(lResultRow.Teaser)
			lResultRow.Text = lArtikel.Inhalttext
			eResult = append(eResult, lResultRow)
		}
	}

	return eResult
}

func getLastWordComplete(iText string) (eOffsetLastword int, eLastWord string) {
	checkOfs := teaserLength - 2
	startOfs := teaserLength - 2
	lastWord := ""
	doExit := false
	for checkOfs > -1 && !doExit {
		s := iText[checkOfs:(checkOfs + 1)]
		blank := strings.TrimSpace(s) == ""
		if !blank {
			lastWord = iText[checkOfs:(checkOfs+1)] + lastWord
			startOfs = checkOfs
			checkOfs--

		} else {
			doExit = true
		}
	}
	if len(lastWord) > 0 {
		checkOfs := teaserLength - 1
		doExit := false
		for checkOfs < len(iText) && !doExit {
			s := iText[checkOfs:(checkOfs + 1)]
			blank := strings.TrimSpace(s) == ""

			if !blank {
				lastWord = lastWord + iText[checkOfs:(checkOfs+1)]
				checkOfs++
			} else {
				doExit = true
			}
		}
	}
	eOffsetLastword = startOfs
	eLastWord = lastWord
	return eOffsetLastword, eLastWord
}

func getArtBelongsToDictatedMeldung(iPSExtId string, iMelExtId string) (eResult bool, err error) {
	serviceGetMeldung := "/ps_meta/PSMeldungService/PSExtId=" + iPSExtId + "/MelExtId=" + iMelExtId
	lMeldungAsJsonInterface, _ := callRestService(serviceGetMeldung)
	lMeldungAsJson := fmt.Sprintf("%v", lMeldungAsJsonInterface)
	lsMeldung := domain.Meldung{}
	var b = []byte(string(lMeldungAsJson))
	err = json.Unmarshal(b, &lsMeldung)
	if err != nil {
		return false, err
	}
	eResult = (lsMeldung.MelInhalttext != "undiktiert")
	return eResult, nil
}

func encodeAklContentSpecialSigns(iInput string) string {
	eResult := iInput
	eResult = strings.Replace(eResult, "\\", "\\\\\\", -1)
	var re = regexp.MustCompile(`\t`)
	eResult = re.ReplaceAllString(eResult, " ")
	eResult = strings.Replace(eResult, "&quot;", "\"", -1)

	//var reQuot = regexp.MustCompile(`"`)
	//eResult = reQuot.ReplaceAllString(eResult, `\"`)
	//eResult = strings.Replace(eResult, "\"", "\\\"", -1)
	return eResult
}
