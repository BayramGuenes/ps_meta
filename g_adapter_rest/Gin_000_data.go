
package g_adapter_rest
import domain "ps_meta/a_domain"

var(
  restServerDatabase domain.PSDataBase
  restServerImageRepo domain.PSImageArchivePort
  restServerDataprovider domain.PSDataProviderPort
  restServerDatareader domain.PSProviderDataReaderPort
  restServerLogger domain.PSLogPort
)
