package e_impl_repo_mysql

import domain "ps_meta/a_domain"

type PSErscheinungsdatum string

type DBUsrRole struct {
	loginname    string
	rfa          string
	authrole     string
	createddate  string
	createdtime  string
	createdbyusr string
}

type DBUsr struct {
	intid int
	domain.User
	createddate   string
	createdtime   string
	createdbyusr  string
	createdbycomp string
	updatedate    string
	updatetime    string
	updatebyusr   string
	updatedbycomp string
}

// Pressespiegel intern
type DBPressespiegel struct {
	intid int
	domain.Pressespiegel
}
type DBMeldung struct {
	intid   int
	Meldung domain.Meldung
	PSExtID string
}
type DBArtikel struct {
	intid   int
	PSExtID string
	Artikel domain.Artikel
	
}

type RepoArtikel struct {
	ArtikelSet []DBArtikel
}

type RepoAuth struct {
	UsrRoleSet []DBUsrRole
}

type RepoUsr struct {
	UsrSet []DBUsr
}

type RepoPS struct {
	PsSet []DBPressespiegel
}

type RepoMeldung struct {
	MeldungSet []DBMeldung
}

var (
	mysqlApplschemaName string
	mysqldbserverRef    string
	databaselocation    string
)
