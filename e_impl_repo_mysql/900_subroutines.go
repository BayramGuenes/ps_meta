package e_impl_repo_mysql

import "time"

func getNowDate() string {
	currentDate := time.Now()
	datePostfix := currentDate.Format("20060102")
	return datePostfix
}
func getNowTime() string {
	currentTime := time.Now()
	timeAsString := currentTime.Format("15:04:05")
	return timeAsString
}
