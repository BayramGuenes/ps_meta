package e_impl_repo_mysql

import (
	"database/sql"
	domain "ps_meta/a_domain"
)

func (r RepoAuth) CreateUserAuthRole(iLoginname, iRfa, iAuthRoleName string) (err error) {
	return nil
}

/*GetAuthSet====================================================== */
/*                                                                  */
/* ================================================================ */
func (r RepoAuth) GetAuthRoleNames() (eAuthRoles []string, err error) {

	getDBRef()

	db, err := sql.Open("mysql", databaselocation)
	if err != nil {
		return []string{}, err
	}
	defer db.Close()
	////println("iLoginname:" + iLoginname + " iRfa:" + iRfa)
	headerQuery := "SELECT name FROM psauthrole;"
	//rows, err := db.Query(headerQuery)
	rows, err := db.Query(headerQuery)

	if err != nil {
		//println("err:" + err.Error())
		return []string{}, err
	}
	defer rows.Close()
	var (
		authrolename string
	)
	for rows.Next() {

		err := rows.Scan(&authrolename)
		if err != nil {
			return []string{}, err
		}
		////println("lAuthkey:" + lAuthkey)
		eAuthRoles = append(eAuthRoles, authrolename)

	}

	return eAuthRoles, nil
}

/*GetAuthSet====================================================== */
/*                                                                  */
/* ================================================================ */
func (r RepoAuth) GetAuthSet(iAuthRoleName string) (eAuthObjects []string, err error) {
	getDBRef()

	db, err := sql.Open("mysql", databaselocation)
	if err != nil {
		return domain.UserRoles{}, err
	}
	defer db.Close()
	////println("iLoginname:" + iLoginname + " iRfa:" + iRfa)
	headerQuery := "SELECT * FROM pauthroleauthobj WHERE authrolename=?"
	//rows, err := db.Query(headerQuery)
	rows, err := db.Query(headerQuery, iAuthRoleName)

	if err != nil {
		//println("err:" + err.Error())
		return domain.UserRoles{}, err
	}
	defer rows.Close()
	var (
		authrolename string
		authobjname  string
	)
	//println("GetAuthSet MYPRINT iAuthRoleName:" + iAuthRoleName)

	for rows.Next() {

		err := rows.Scan(&authrolename, &authobjname)
		if err != nil {
			return []string{}, err
		}
		//println("MYPRINT lAuthkey:" + authobjname)
		eAuthObjects = append(eAuthObjects, authobjname)

	}

	return eAuthObjects, nil
}

func (r RepoAuth) GetUserAuthRoles(iLoginname, iRfa string) (eAuthRoles []string, err error) {
	return []string{}, nil
}

func (r RepoAuth) RemoveUserAuthRole(iLoginname, iRfa, iAuthRoleName string) (err error) {
	return nil
}
