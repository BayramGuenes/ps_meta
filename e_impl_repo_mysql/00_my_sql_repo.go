package e_impl_repo_mysql

import (
	bootstrap "ps_meta/z_bootstrap"
	//"log"

	_ "github.com/go-sql-driver/mysql"
)

func getDBRef() {
	//	log.Printf(" getDBRefI")

	mysqlApplschemaName = bootstrap.ApplConf.DB_SQLApplschemaName
	////println("II")

	mysqldbserverRef = bootstrap.ApplConf.DB_UserPwd + "@tcp(" + bootstrap.ThisDbhostIP + ":" + bootstrap.ThisDbhostPORT + ")"
	////println("IIIIIIIIIIIIIIIIIIIIIIIIIIIIIII   mysqldbserverRef:"+mysqldbserverRef, " IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII")
	databaselocation = mysqldbserverRef + "/" + mysqlApplschemaName
	////println("database location:" + databaselocation)
}

func GetNewMySQLRepoAuth() RepoAuth {

	return RepoAuth{}

}

func GetNewMySQLRepoUsr() RepoUsr {

	return RepoUsr{}

}

func GetNewMySQLRepoPS() RepoPS {

	return RepoPS{}

}

func GetNewMySQLRepoMeldung() RepoMeldung {

	return RepoMeldung{}

}

func GetNewMySQLRepoArtikel() RepoArtikel {

	return RepoArtikel{}

}
