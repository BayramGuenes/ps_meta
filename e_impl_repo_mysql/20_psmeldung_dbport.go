package e_impl_repo_mysql

import (
	"database/sql"
	domain "ps_meta/a_domain"
)

func (r RepoMeldung) CreateMeldung(iPSExtID string, iMeldung domain.Meldung) (err error) {
	db, err := sql.Open("mysql", databaselocation)
	if err != nil {
		return err
	}
	insstmt := "INSERT psmeldung SET" +
		" melextid=?, melpsrubrik=?, melinhalttext=?, melpsposnr=?, psextid=?"
	defer db.Close()
	stmt, err := db.Prepare(insstmt)
	if err != nil {
		return err
	}
	////println("iPSExtID:" + iPSExtID)
	_, err = stmt.Exec(
		iMeldung.MelExtID,
		iMeldung.MelPSRubrik,
		iMeldung.MelInhalttext,
		iMeldung.MelPSPosnr,
		iPSExtID,
	)
	if err != nil {
		return err
	}

	return nil
}

func (r RepoMeldung) GetMeldung(iPSExtID string, iMelExtID string) (eMeldung domain.Meldung, err error) {
	getDBRef()
	db, err := sql.Open("mysql", databaselocation)
	if err != nil {
		return domain.Meldung{}, err
	}

	defer db.Close()

	headerQuery := `SELECT * FROM psmeldung WHERE psextid=? and melextid=?`
	rows, err := db.Query(headerQuery, iPSExtID, iMelExtID)
	if err != nil {
		return domain.Meldung{}, err
	}
	defer rows.Close()

	var (
		intid         int64
		psextid       string
		melextid      string
		melpsposnr    string
		melinhalttext string
		melpsrubrik   string
		lMeldung      domain.Meldung
	)
	for rows.Next() {
		err := rows.Scan(&intid,  &psextid,   &melextid, &melpsposnr, &melinhalttext, &melpsrubrik )
		if err != nil {
			return domain.Meldung{}, err
		}
		lMeldung.MelExtID = melextid
		lMeldung.MelInhalttext = melinhalttext
		lMeldung.MelPSPosnr = melpsposnr
		lMeldung.MelPSRubrik = melpsrubrik
	}

	return lMeldung, nil
}

func (r RepoMeldung) DeleteMeldung(iPSExtID string, iMelExtID string) (err error) {
	getDBRef()
	db, err := sql.Open("mysql", databaselocation)
	defer db.Close()
	////println("(r Repo) DeleteMeldung(iPSExtID called")
	stmt, err := db.Prepare("DELETE FROM psmeldung WHERE melextid=? and psextid=?")
	if err != nil {
		return err
	}
	_, err = stmt.Exec(iMelExtID, iPSExtID)
	////println("leaving (r Repo) DeleteMeldung(iPSExtID called")
	if err != nil {
		////println(err.Error())
	}
	return err
}

func (r RepoMeldung) GetMeldungCollection(iPSExtID string) (eCollection []domain.Meldung, err error) {
	getDBRef()
	////println("GetMeldungCollection:"+iPSExtID)
	db, err := sql.Open("mysql", databaselocation)
	defer db.Close()
	if err != nil {
		////println("err=" + err.Error())
		return []domain.Meldung{}, err
	}

	////println("iPSExtID=" + iPSExtID)
	headerQuery := "SELECT * FROM psmeldung WHERE psextid=?"
	rows, err := db.Query(headerQuery, iPSExtID)
	if err != nil {
		return []domain.Meldung{}, err
	}
	defer rows.Close()

	var (
		intid         int//int64
		melextid      string
		melpsrubrik   string
		melinhalttext string
		melpsposnr    string
		psextid       string
		lMeldung      domain.Meldung
	)
	

	for rows.Next() {
		////println("rows.Next()")
		err := rows.Scan(&intid,  &psextid,   &melextid, &melpsposnr, &melinhalttext, &melpsrubrik )
		if err != nil {
			return []domain.Meldung{}, err

		}
		////println("psextid:"+psextid)
		lMeldung.MelExtID = melextid
		lMeldung.MelInhalttext = melinhalttext
		lMeldung.MelPSPosnr = melpsposnr
		lMeldung.MelPSRubrik = melpsrubrik
		////println("melinhalttext:" + melinhalttext)
		eCollection = append(eCollection, lMeldung)

	}
	////println("leaving r Repo) GetMeldungCollection")
	return eCollection, nil
}
