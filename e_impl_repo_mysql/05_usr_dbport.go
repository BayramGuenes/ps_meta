package e_impl_repo_mysql

import (
	"database/sql"
	"errors"
	domain "ps_meta/a_domain"
	"reflect"
	"time"
)

/*CreateUser =========================================================== */
/*                                                                       */
/* ===================================================================== */
func (r RepoUsr) CreateUser(iLoginname string, iRfa string, iName string, iPwd string, iEmail string, iCreatedbyusr string, iCreatedbycomp string) (err error) {
	getDBRef()
	db, err := sql.Open("mysql", databaselocation)
	if err != nil {
		return err
	}
	defer db.Close()
	insstmt := "INSERT psusr SET" +
		" loginname=?, rfa=?, name=?, pwd=?, email=?, " +
		" isdeactivated=?, deactivatedby=?," +
		" deactivateddate=?, deactivatedtime=?, reactivatedby=?, reactivateddate=?," +
		" reactivatedtime=?, createddate=?,  createdtime=?, createdbyusr=?, createdbycomp=?, updatedate=?, updatetime=?, updatebyusr=?, updatedbycomp=? "
	stmt, err := db.Prepare(insstmt)
	if err != nil {
		return err
	}
	currentTime := time.Now()
	////println("CreateUser called:"+iLoginname+ iRfa)
	_, err = stmt.Exec(
		iLoginname,
		iRfa,
		iName,
		iPwd,
		iEmail,
		false,
		"", "", "", "", "", "", //deactivate and reactvate info
		currentTime.Format("2006-01-02"),
		currentTime.Format("15:04:05"),
		iCreatedbyusr,
		iCreatedbycomp,
		"", "", "", "") //update info

	if err != nil {
		return err
	}
	return nil
}

/*SetUserToken  ========================================================== */
/*                                                                         */
/* ======================================================================= */
func (r RepoUsr) SetUserToken(iLoginname string, iRfa string, iToken string, iValidFrom, iValidTo string) (err error) {
	getDBRef()
	db, err := sql.Open("mysql", databaselocation)
	if err != nil {
		return err
	}
	defer db.Close()

	headerQuery := "SELECT * FROM psusrtoken WHERE loginname=? and rfa=?"
	rows, err := db.Query(headerQuery, iLoginname, iRfa)

	defer rows.Close()

	var (
		loginname     string
		rfa           string
		token         string
		validdatefrom string
		validdateto   string
	)
	lfound := false
	for rows.Next() {
		err := rows.Scan(&loginname, &rfa, &token, &validdatefrom, &validdateto)
		if err != nil {
			return err
		}
		lfound = true
	}
	if lfound {
		stmt, err := db.Prepare(`
	      UPDATE  psusrtoken
	      SET token=?,  validdatefrom=?,  validdateto=?
          WHERE loginname=? and rfa=? `)
		if err != nil {
			return err
		}
		_, err = stmt.Exec(
			iToken,
			iValidFrom,
			iValidTo,
			iLoginname,
			iRfa)
		return err
	}
	if !lfound {
		////println("(r RepoUsr) SetUserToken insert" + iLoginname + iRfa + iValidFrom + iValidTo)

		insstmt := "INSERT psusrtoken SET" +
			" loginname=?, rfa=?, token=?, validdatefrom=?, validdateto=?"
		stmt, err := db.Prepare(insstmt)
		if err != nil {
			return err
		}

		_, err = stmt.Exec(
			iLoginname,
			iRfa,
			iToken,
			iValidFrom,
			iValidTo)

		return err
	}
	return nil
}

/*SetUserTokenViaToken  ========================================================== */
/*                                                                         */
/* ======================================================================= */
func (r RepoUsr) SetUserTokenViaToken(iToken string, iValidFrom, iValidTo string) (err error) {
	getDBRef()
	db, err := sql.Open("mysql", databaselocation)
	if err != nil {
		return err
	}
	defer db.Close()

	headerQuery := "SELECT * FROM psusrtoken WHERE token=?"
	rows, err := db.Query(headerQuery, iToken)

	defer rows.Close()

	var (
		loginname     string
		rfa           string
		token         string
		validdatefrom string
		validdateto   string
	)
	lfound := false
	for rows.Next() {
		err := rows.Scan(&loginname, &rfa, &token, &validdatefrom, &validdateto)
		if err != nil {
			return err
		}
		lfound = true
	}
	if lfound {
		stmt, err := db.Prepare(`
	      UPDATE  psusrtoken
	      SET token=?,  validdatefrom=?,  validdateto=?
          WHERE loginname=? and rfa=? `)
		if err != nil {
			return err
		}
		_, err = stmt.Exec(
			iToken,
			iValidFrom,
			iValidTo,
			loginname,
			rfa)
		return err
	} else {
		err = errors.New("Token with no user entry. ")

		return err
	}

}

/*GetUsrViaToken ========================================================== */
/*                                                                          */
/* ======================================================================== */
func (r RepoUsr) GetUsrViaToken(iToken string) (eUser domain.User, err error) {

	getDBRef()

	db, err := sql.Open("mysql", databaselocation)
	if err != nil {
		return domain.User{}, err
	}
	defer db.Close()

	headerQuery := "SELECT * FROM psusrtoken WHERE token=?;"
	rows, err := db.Query(headerQuery, iToken)
	if err != nil {
		return domain.User{}, err
	}
	defer rows.Close()
	var (
		loginname     string
		rfa           string
		token         string
		validdatefrom string
		validdateto   string
	)
	for rows.Next() {

		err := rows.Scan(&loginname, &rfa, &token, &validdatefrom, &validdateto)
		if err != nil {
			return domain.User{}, err
		}

	}

	eUser, err = r.GetUsr(loginname, rfa)

	return eUser, err
}

/*GetUsrToken====================================================== */
/*                                                                  */
/* ================================================================ */
func (r RepoUsr) GetUsrToken(iLoginname string, iRfa string) (eUserToken domain.UserToken, err error) {
	getDBRef()

	db, err := sql.Open("mysql", databaselocation)
	if err != nil {
		return domain.UserToken{}, err
	}
	defer db.Close()

	headerQuery := "SELECT * FROM psusrtoken WHERE loginname=? and rfa=?"
	//rows, err := db.Query(headerQuery)
	rows, err := db.Query(headerQuery, iLoginname, iRfa)

	if err != nil {
		//println("err:" + err.Error())
		return domain.UserToken{}, err
	}
	defer rows.Close()
	var (
		loginname     string
		rfa           string
		token         string
		validdatefrom string
		validdateto   string
	)
	for rows.Next() {

		err := rows.Scan(&loginname, &rfa, &token, &validdatefrom, &validdateto)
		if err != nil {
			return domain.UserToken{}, err
		}

	}
	eUserToken.Loginname = loginname
	eUserToken.Rfa = rfa
	eUserToken.Token = token
	eUserToken.Validdatefrom = validdatefrom
	eUserToken.Validdateto = validdateto

	return eUserToken, nil
}

/*GetUsrRoles====================================================== */
/*                                                                  */
/* ================================================================ */
func (r RepoUsr) GetUsrRoles(iLoginname string, iRfa string) (eUserRoles domain.UserRoles, err error) {
	getDBRef()

	db, err := sql.Open("mysql", databaselocation)
	if err != nil {
		return domain.UserRoles{}, err
	}
	defer db.Close()
	////println("iLoginname:" + iLoginname + " iRfa:" + iRfa)
	headerQuery := "SELECT * FROM psusrrole WHERE loginname=? and rfa=?"
	//rows, err := db.Query(headerQuery)
	rows, err := db.Query(headerQuery, iLoginname, iRfa)

	if err != nil {
		//println("err:" + err.Error())
		return domain.UserRoles{}, err
	}
	defer rows.Close()
	var (
		loginname   string
		rfa         string
		authrole    sql.NullString // string
		createddate sql.NullString //string
		createdtime sql.NullString //string
		createdby   sql.NullString //string
	)
	for rows.Next() {

		err := rows.Scan(&loginname, &rfa, &authrole, &createddate, &createdtime, &createdby)
		if err != nil {
			return domain.UserRoles{}, err
		}
		lUserRole := ""
		if reflect.TypeOf(authrole) == nil {
			lUserRole = ""
		} else {
			lUserRole = authrole.String
		}
		//lUserRole := authrole
		////println("lUserRole:" + lUserRole)
		eUserRoles = append(eUserRoles, lUserRole)
	}

	return eUserRoles, nil
}

/*GetUsr ========================================================== */
/*                                                                  */
/* ================================================================ */
func (r RepoUsr) GetUsr(iLoginname, iRfa string) (eUser domain.User, err error) {
	getDBRef()

	db, err := sql.Open("mysql", databaselocation)
	if err != nil {
		return domain.User{}, err
	}
	defer db.Close()

	headerQuery := "SELECT * FROM psusr WHERE loginname=? and rfa=?"
	//rows, err := db.Query(headerQuery)
	rows, err := db.Query(headerQuery, iLoginname, iRfa)

	if err != nil {
		//println("err:" + err.Error())
		return domain.User{}, err
	}
	defer rows.Close()
	var (
		intid           int64
		loginname       string
		rfa             string
		name            string
		pwd             string
		email           string
		isdeactivated   bool
		deactivatedby   string
		deactivateddate string
		deactivatedtime string
		reactivatedby   string
		reactivateddate string
		reactivatedtime string
		createddate     string
		createdtime     string
		createdbyusr    string
		createdbycomp   string
		updatedate      string
		updatetime      string
		updatebyusr     string
		updatedbycomp   string
	)
	for rows.Next() {

		err := rows.Scan(&intid, &loginname, &rfa, &name, &pwd, &email, &isdeactivated, &deactivatedby,
			&deactivateddate, &deactivatedtime, &reactivatedby, &reactivateddate, &reactivatedtime,
			&createddate, &createdtime, &createdbyusr, &createdbycomp, &updatedate, &updatetime, &updatebyusr, &updatedbycomp)
		if err != nil {
			return domain.User{}, err
		}

	}
	eUser.Loginname = loginname
	eUser.Rfa = rfa
	eUser.Name = name
	eUser.Pwd = pwd
	eUser.Email = email
	eUser.IsDeactivated = isdeactivated
	eUser.DeactivatedBy = deactivatedby
	eUser.DeactivatedDate = deactivateddate
	eUser.DeactivatedTime = deactivatedtime
	eUser.ReactivatedBy = reactivatedby
	eUser.ReactivatedDate = reactivateddate
	eUser.ReactivatedTime = reactivatedtime

	return eUser, nil

}

/*GetUsr ========================================================== */
/*                                                                  */
/* ================================================================ */
func (r RepoUsr) GetUsrViaEmail(iEmail string) (eUser domain.User, err error) {
	getDBRef()

	db, err := sql.Open("mysql", databaselocation)
	if err != nil {
		return domain.User{}, err
	}
	defer db.Close()

	headerQuery := "SELECT * FROM psusr WHERE email=?"
	//rows, err := db.Query(headerQuery)
	rows, err := db.Query(headerQuery, iEmail)

	if err != nil {
		//println("err:" + err.Error())
		return domain.User{}, err
	}
	defer rows.Close()
	var (
		intid           int64
		loginname       string
		rfa             string
		name            string
		pwd             string
		email           string
		isdeactivated   bool
		deactivatedby   string
		deactivateddate string
		deactivatedtime string
		reactivatedby   string
		reactivateddate string
		reactivatedtime string
		createddate     string
		createdtime     string
		createdbyusr    string
		createdbycomp   string
		updatedate      string
		updatetime      string
		updatebyusr     string
		updatedbycomp   string
	)
	for rows.Next() {

		err := rows.Scan(&intid, &loginname, &rfa, &name, &pwd, &email, &isdeactivated, &deactivatedby,
			&deactivateddate, &deactivatedtime, &reactivatedby, &reactivateddate, &reactivatedtime,
			&createddate, &createdtime, &createdbyusr, &createdbycomp, &updatedate, &updatetime, &updatebyusr, &updatedbycomp)
		if err != nil {
			return domain.User{}, err
		}

	}
	eUser.Loginname = loginname
	eUser.Rfa = rfa
	eUser.Name = name
	eUser.Pwd = pwd
	eUser.Email = email
	eUser.IsDeactivated = isdeactivated
	eUser.DeactivatedBy = deactivatedby
	eUser.DeactivatedDate = deactivateddate
	eUser.DeactivatedTime = deactivatedtime
	eUser.ReactivatedBy = reactivatedby
	eUser.ReactivatedDate = reactivateddate
	eUser.ReactivatedTime = reactivatedtime

	return eUser, nil

}

/*UpdateUser ========================================================== */
/*                                                                      */
/* ==================================================================== */
func (r RepoUsr) UpdateUser(iUser domain.User, iUpdatebyusr string, iUpdatedbycomp string) (err error) {
	getDBRef()
	db, err := sql.Open("mysql", databaselocation)
	defer db.Close()

	stmt, err := db.Prepare(`
	  UPDATE  psusr
	  SET name=?,  pwd=?,  email=?,
		  isdeactivated=?,
		  deactivatedby=?,
		  deactivateddate=?,
		  deactivatedtime=?,
		  reactivatedby=?,
		  reactivateddate=?,
		  reactivatedtime=?,
		  updatedate=?,
		  updatetime=?,
		  updatebyusr=?,
          updatedbycomp=?
      WHERE loginname=? and rfa=? `)
	if err != nil {
		return err
	}

	currentTime := time.Now()
	_, err = stmt.Exec(
		iUser.Name,
		iUser.Pwd,
		iUser.Email,
		iUser.IsDeactivated,
		iUser.DeactivatedBy,
		iUser.DeactivatedDate,
		iUser.DeactivatedTime,
		iUser.ReactivatedBy,
		iUser.ReactivatedDate,
		iUser.ReactivatedTime,
		currentTime.Format("2006-01-02"),
		currentTime.Format("15:04:05"),
		iUpdatebyusr,
		iUpdatedbycomp,
		iUser.Loginname,
		iUser.Rfa,
	)

	return err

}

/*GetUserRole ========================================================== */
/*                                                                       */
/* ===================================================================== */
/*func (r RepoUsr) GetUserRole(iLoginname, iRfa string) (eRole string, err error) {
	lUser, err := r.GetUsr(iLoginname, iRfa)
	if err != nil {
		return "", err
	}
	eRole = lUser.GetUsrRole()
	return
}
*/
