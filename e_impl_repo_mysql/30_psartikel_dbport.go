package e_impl_repo_mysql

import (
	"database/sql"
	domain "ps_meta/a_domain"
)

func (r RepoArtikel) CreateArticle(iPSExtID string, iArtikel domain.Artikel) (err error) {
	db, err := sql.Open("mysql", databaselocation)
	if err != nil {
		return err
	}
	defer db.Close()
	////println("CreateArticle ArtUntertitel:" + iArtikel.ArtUntertitel)
	insstmt := "INSERT psarticle SET" +
		" psrubrik=?," +
		"	psposnr=?," +
		" melextid=?," +
		" extid=?," +
		" quellename=?," +
		"	quellekuerzel=?," +
		"	quellepubnr=?," +
		"	quelleseitennr=?," +
		" esd=?," +
		" print_online=?," +
		" autor=?," +
		" schlagwort=?," +
		" anzahlfotos=?," +
		" titel=?," +
		" untertitel=?," +
		" sonsttitel=?," +
		" seite1titel=?," +
		" inhalttext=?," +
		" autoabstract=?," +
		" ausgabe=?," +
		" bemerkung=?," +
		" imagetyp=?," +
		" imagetypversion=?," +
		" imageuri=?," +
		" psextid=?"

	stmt, err := db.Prepare(insstmt)
	if err != nil {
		return err
	}
	////println("iPSExtID:" + iPSExtID)
	_, err = stmt.Exec(
		iArtikel.PSRubrik,
		iArtikel.PSPosnr,
		iArtikel.MelExtID,
		iArtikel.ExtID,
		iArtikel.QuelleName,
		iArtikel.QuelleKuerzel,
		iArtikel.QuellePubnr,
		iArtikel.QuelleSeitennr,
		iArtikel.ESD,
		iArtikel.Print_Online,
		iArtikel.Autor,
		iArtikel.Schlagwort,
		iArtikel.AnzahlFotos,
		iArtikel.Titel,
		iArtikel.Untertitel,
		iArtikel.SonstTitel,
		iArtikel.Seite1Titel,
		iArtikel.Inhalttext,
		iArtikel.Autoabstract,
		iArtikel.Ausgabe,
		iArtikel.Bemerkung,
		iArtikel.ImageTyp,
		iArtikel.ImageTypVersion,
		iArtikel.ImageURI,
		iPSExtID,
	)
	if err != nil {
		return err
	}

	return nil

}

func (r RepoArtikel) DeleteArticle(iPSExtID string, iArtExtID string) (err error) {
	getDBRef()
	db, err := sql.Open("mysql", databaselocation)
	////println("(r Repo) DeleteMeldung(iPSExtID called")
	defer db.Close()
	stmt, err := db.Prepare("DELETE FROM psarticle WHERE extid=? and psextid=?")
	if err != nil {
		return err
	}
	_, err = stmt.Exec(iArtExtID, iPSExtID)
	////println("leaving (r Repo) DeleteMeldung(iPSExtID called")
	if err != nil {
		////println(err.Error())
	}
	return err

}

func (r RepoArtikel) GetArticleCollection(iPSExtID string) (eCollection []domain.Artikel, err error) {

	getDBRef()
	db, err := sql.Open("mysql", databaselocation)
	if err != nil {
		////println("err=" + err.Error())
		return []domain.Artikel{}, err
	}
	defer db.Close()

	//	//println("iPSExtID=" + iPSExtID)
	headerQuery := "SELECT * FROM psarticle WHERE psextid=?"
	rows, err := db.Query(headerQuery, iPSExtID)
	if err != nil {
		return []domain.Artikel{}, err
	}
	defer rows.Close()

	var (
		lDBArtikel DBArtikel
	)
	////println("eeeerows.Next()")

	for rows.Next() {
		////println("rows.Next()")
		err := rows.Scan(&lDBArtikel.intid,
			&lDBArtikel.PSExtID,
			&lDBArtikel.Artikel.MelExtID,
			&lDBArtikel.Artikel.ExtID,
			&lDBArtikel.Artikel.PSPosnr,
			&lDBArtikel.Artikel.PSRubrik,
			&lDBArtikel.Artikel.ESD,
			&lDBArtikel.Artikel.Print_Online,
			&lDBArtikel.Artikel.AnzahlFotos,
			&lDBArtikel.Artikel.Ausgabe,
			&lDBArtikel.Artikel.Titel,
			&lDBArtikel.Artikel.Seite1Titel,
			&lDBArtikel.Artikel.Untertitel,
			&lDBArtikel.Artikel.SonstTitel,
			&lDBArtikel.Artikel.Inhalttext,
			&lDBArtikel.Artikel.Autoabstract,
			&lDBArtikel.Artikel.Schlagwort,
			&lDBArtikel.Artikel.Autor,
			&lDBArtikel.Artikel.Bemerkung,
			&lDBArtikel.Artikel.QuelleKuerzel,
			&lDBArtikel.Artikel.QuelleName,
			&lDBArtikel.Artikel.QuellePubnr,
			&lDBArtikel.Artikel.QuelleSeitennr,
			&lDBArtikel.Artikel.ImageURI,
			&lDBArtikel.Artikel.ImageTyp,
			&lDBArtikel.Artikel.ImageTypVersion,
		)
		if err != nil {
			return []domain.Artikel{}, err

		}

		eCollection = append(eCollection, lDBArtikel.Artikel)

	}
	////println("leaving r Repo) GetMeldungCollection")
	return eCollection, nil

}

func (r RepoArtikel) GetArtViaIDAndESD(iArtExtID string, iArtESD string) (eArtikel domain.Artikel, err error) {

	getDBRef()
	db, err := sql.Open("mysql", databaselocation)
	if err != nil {
		return domain.Artikel{}, err
	}

	defer db.Close()
	////println("iArtExtID, iPSExtID:" + iArtExtID + " " + iPSExtID)
	headerQuery := "SELECT * FROM psarticle WHERE extid=? and esd=?"
	rows, err := db.Query(headerQuery, iArtExtID, iArtESD)
	if err != nil {
		return domain.Artikel{}, err
	}
	defer rows.Close()

	var (
		lDBArtikel DBArtikel
	)
	for rows.Next() {
		err := rows.Scan(&lDBArtikel.intid,
			&lDBArtikel.PSExtID,
			&lDBArtikel.Artikel.MelExtID,
			&lDBArtikel.Artikel.ExtID,
			&lDBArtikel.Artikel.PSPosnr,
			&lDBArtikel.Artikel.PSRubrik,
			&lDBArtikel.Artikel.ESD,
			&lDBArtikel.Artikel.Print_Online,
			&lDBArtikel.Artikel.AnzahlFotos,
			&lDBArtikel.Artikel.Ausgabe,
			&lDBArtikel.Artikel.Titel,
			&lDBArtikel.Artikel.Seite1Titel,
			&lDBArtikel.Artikel.Untertitel,
			&lDBArtikel.Artikel.SonstTitel,
			&lDBArtikel.Artikel.Inhalttext,
			&lDBArtikel.Artikel.Autoabstract,
			&lDBArtikel.Artikel.Schlagwort,
			&lDBArtikel.Artikel.Autor,
			&lDBArtikel.Artikel.Bemerkung,
			&lDBArtikel.Artikel.QuelleKuerzel,
			&lDBArtikel.Artikel.QuelleName,
			&lDBArtikel.Artikel.QuellePubnr,
			&lDBArtikel.Artikel.QuelleSeitennr,
			&lDBArtikel.Artikel.ImageURI,
			&lDBArtikel.Artikel.ImageTyp,
			&lDBArtikel.Artikel.ImageTypVersion,
		)
		if err != nil {
			return domain.Artikel{}, err

		}
	}
	return lDBArtikel.Artikel, nil

}

func (r RepoArtikel) GetArticle(iPSExtID string, iArtExtID string) (eArtikel domain.Artikel, err error) {

	getDBRef()
	db, err := sql.Open("mysql", databaselocation)
	if err != nil {
		return domain.Artikel{}, err
	}

	defer db.Close()
	////println("iArtExtID, iPSExtID:" + iArtExtID + " " + iPSExtID)
	headerQuery := "SELECT * FROM psarticle WHERE extid=? and psextid=?"
	rows, err := db.Query(headerQuery, iArtExtID, iPSExtID)
	if err != nil {
		return domain.Artikel{}, err
	}
	defer rows.Close()

	var (
		lDBArtikel DBArtikel
	)
	for rows.Next() {
		err := rows.Scan(&lDBArtikel.intid,
			&lDBArtikel.PSExtID,
			&lDBArtikel.Artikel.MelExtID,
			&lDBArtikel.Artikel.ExtID,
			&lDBArtikel.Artikel.PSPosnr,
			&lDBArtikel.Artikel.PSRubrik,
			&lDBArtikel.Artikel.ESD,
			&lDBArtikel.Artikel.Print_Online,
			&lDBArtikel.Artikel.AnzahlFotos,
			&lDBArtikel.Artikel.Ausgabe,
			&lDBArtikel.Artikel.Titel,
			&lDBArtikel.Artikel.Seite1Titel,
			&lDBArtikel.Artikel.Untertitel,
			&lDBArtikel.Artikel.SonstTitel,
			&lDBArtikel.Artikel.Inhalttext,
			&lDBArtikel.Artikel.Autoabstract,
			&lDBArtikel.Artikel.Schlagwort,
			&lDBArtikel.Artikel.Autor,
			&lDBArtikel.Artikel.Bemerkung,
			&lDBArtikel.Artikel.QuelleKuerzel,
			&lDBArtikel.Artikel.QuelleName,
			&lDBArtikel.Artikel.QuellePubnr,
			&lDBArtikel.Artikel.QuelleSeitennr,
			&lDBArtikel.Artikel.ImageURI,
			&lDBArtikel.Artikel.ImageTyp,
			&lDBArtikel.Artikel.ImageTypVersion,
		)
		if err != nil {
			return domain.Artikel{}, err

		}
	}
	return lDBArtikel.Artikel, nil

}

func (r RepoArtikel) SetArticleImageLocation(iPSExtID string, iArticleEXTID string, iLocation string) (ok bool, err error) {
	getDBRef()
	found := false

	return found, nil
}
