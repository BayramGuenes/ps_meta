package e_impl_repo_mysql

import (
	"database/sql"
	domain "ps_meta/a_domain"
	"reflect"
	"strings"
	"time"
)

func (r RepoPS) ExistsPS(iPSErscheinungsdatum string) (eEXTId string, eFound bool, err error) {
	getDBRef()
	////println("(r Repo) ExistsPS(iPSErscheinungsdatum string) called")
	eFound = false
	db, err := sql.Open("mysql", databaselocation)

	if err != nil {
		////println("err=" + err.Error())
		return "", false, err
	}

	defer db.Close()

	err = db.QueryRow("SELECT psextid FROM psheader WHERE psesd=?", iPSErscheinungsdatum).Scan(&eEXTId)
	if err != nil {
		eFound = false
	} else {
		eFound = true
	}

	return eEXTId, eFound, nil
}

func (r RepoPS) CreatePS(iPressespiegelWrapper domain.PressespiegelWrapper, iLogicalNameImportsystem string, iImportusr string) (err error) {
	getDBRef()
	db, err := sql.Open("mysql", databaselocation)
	if err != nil {
		return err
	}
	defer db.Close()
	////println("iLogicalNameImportsystem=" + iLogicalNameImportsystem,)

	////println(" (r Repo) CreatePS(iPressespiegelWrapper called")
	insstmt := "INSERT psheader SET" +
		" psimageuri=?, psart=?, psimagetyp=?, psimagetypversion=?, psausgabenr=?," +
		" psextid=?, psesd=?, psrfa=?," +
		" isactivated=?, activatedbyuser=?, activateddate=?, activatedtime=?," +
		" logicalnameimpsys=?, importusrname=?,  importtime=? "
	stmt, err := db.Prepare(insstmt)
	if err != nil {
		////println("err:" + err.Error())
		return err
	}
	currentTime := time.Now()
	timePostfix := currentTime.Format("2006-01-02 15:04:05")
	lImporttime := timePostfix

	_, err = stmt.Exec(
		iPressespiegelWrapper.PSHeader.PSImageURI,
		iPressespiegelWrapper.PSHeader.PSArt,
		iPressespiegelWrapper.PSHeader.PSImageTyp,
		iPressespiegelWrapper.PSHeader.PSImageTypVersion,
		iPressespiegelWrapper.PSHeader.PSAusgabenr,
		iPressespiegelWrapper.PSHeader.PSExtID,
		iPressespiegelWrapper.PSHeader.PSESD,
		iPressespiegelWrapper.PSHeader.PSRFA,
		iPressespiegelWrapper.PSHeader.IsActivated,
		iPressespiegelWrapper.PSHeader.ActivatedByUser,
		iPressespiegelWrapper.PSHeader.ActivatedDate,
		iPressespiegelWrapper.PSHeader.ActivatedTime,
		iLogicalNameImportsystem,
		iImportusr,
		lImporttime,
	)
	if err != nil {
		return err
	}
	////println(" leaving (r Repo) CreatePS(iPressespiegelWrapper called")

	return nil
}

func (r RepoPS) SetPSImageLocation(iPSExtID string, iLocation string) (ok bool, err error) {
	getDBRef()
	found := false
	return found, nil
}

func (r RepoPS) DeletePS(iPSExtID string) (err error) {
	////println(" (r Repo) DeletePS( called")
	getDBRef()
	db, err := sql.Open("mysql", databaselocation)
	defer db.Close()
	stmt, err := db.Prepare("DELETE FROM psheader WHERE psextid=? ")
	if err != nil {
		return err
	}
	_, err = stmt.Exec(iPSExtID)
	////println(" leaving (r Repo) DeletePS(")

	return err

}

func (r RepoPS) SetPSPublishmark(iPSErscheinungsdatum string, iUname string) (err error) {
	getDBRef()
	db, err := sql.Open("mysql", databaselocation)
	defer db.Close()
	stmt, err := db.Prepare("UPDATE  psheader SET isactivated=1, activatedbyuser=?, activateddate=?,  activatedtime=? WHERE psesd=? ")
	if err != nil {
		return err
	}
	timeOfOp := time.Now().Format("20060102 150405")
	////println("timeOfOp:" + timeOfOp)
	activateddate := timeOfOp[0:8]
	activatedtime := timeOfOp[9:15]
	_, err = stmt.Exec(iUname, activateddate, activatedtime, iPSErscheinungsdatum)

	return err
}

func (r RepoPS) GetPS(iPSErscheinungsdatum string, iAlsoInactive bool) (ePSHeader domain.Pressespiegel, err error) {
	getDBRef()
	////println("GetPS():" + iPSErscheinungsdatum + " " + iRole)
	db, err := sql.Open("mysql", databaselocation)
	defer db.Close()

	headerQuery := `SELECT * FROM psheader WHERE psesd=?`
	if !iAlsoInactive {
		headerQuery += " and isactivated=1 "
	}
	lPSErscheinungsdatum := iPSErscheinungsdatum
	rows, err := db.Query(headerQuery, lPSErscheinungsdatum)
	if err != nil {
		////println("err  " + err.Error())
		return domain.Pressespiegel{}, err
	}
	defer rows.Close()

	var (
		intid             int64
		psextid           string
		psart             string
		psausgabenr       string
		psesd             string
		psrfa             string
		psimageuri        string
		psimagetyp        string
		psimagetypversion string
		isactivated       bool
		activatedbyuser   string
		activateddate     string
		activatedtime     string
		logicalnameimpsys string
		importusrname     string
		importtime        string
	)
	for rows.Next() {

		err := rows.Scan(&intid, &psextid, &psart, &psausgabenr, &psesd, &psrfa, &psimageuri, &psimagetyp, &psimagetypversion,
			&isactivated, &activatedbyuser, &activateddate, &activatedtime, &logicalnameimpsys, &importusrname, &importtime)
		if err != nil {
			return domain.Pressespiegel{}, err
		}
		////println("rows.Next():" + lPSErscheinungsdatum + " " + pserscheinungsdatum)

	}
	ePSHeader.PSImageURI = psimageuri
	ePSHeader.PSArt = psart
	ePSHeader.PSImageTyp = psimagetyp
	ePSHeader.PSImageTypVersion = psimagetypversion
	ePSHeader.PSAusgabenr = psausgabenr
	ePSHeader.PSExtID = psextid
	ePSHeader.PSESD = psesd
	ePSHeader.PSRFA = psrfa
	ePSHeader.IsActivated = isactivated
	ePSHeader.ActivatedByUser = activatedbyuser
	ePSHeader.ActivatedDate = activateddate
	ePSHeader.ActivatedTime = activatedtime
	ePSHeader.LogicalNameImportsystem = logicalnameimpsys

	return ePSHeader, nil
}

func (r RepoPS) GetPSESDsFromTo(iPSFrom string, iPSTo string, iAlsoInactive bool) (eListESD []string, err error) {
	//	//println("CALLED (r RepoPS) GetPSESDsFromTo")
	getDBRef()
	////println("GetPS():" + iPSErscheinungsdatum + " " + iRole)
	db, err := sql.Open("mysql", databaselocation)
	defer db.Close()

	headerQuery := `SELECT psesd FROM psheader WHERE psesd>=? and psesd<=?`
	if !iAlsoInactive {
		headerQuery += " and isactivated=1 "
	}
	//lPSErscheinungsdatum := iPSErscheinungsdatum
	rows, err := db.Query(headerQuery, iPSFrom, iPSTo)
	if err != nil {
		////println("err  " + err.Error())
		return []string{}, err
	}
	defer rows.Close()

	var (
		psesd string
	)
	for rows.Next() {

		err := rows.Scan(&psesd)
		if err != nil {
			return []string{}, err
		}
		////println("rows.Next():" + lPSErscheinungsdatum + " " + pserscheinungsdatum)

		eListESD = append(eListESD, psesd)
	}

	return eListESD, nil
}

func (r RepoPS) GetDateBeforePSOBSOLET(iAlsoInactive bool, iDateInRelationTo string, iMaxNumBack int) (ePSDate string, err error) {
	getDBRef()
	s := iDateInRelationTo
	t, _ := time.Parse("20060102", s)
	found := false
	ePSDate = ""
	err = nil
	for i := 0; i < iMaxNumBack && !found; i++ {
		nt := t.AddDate(0, 0, -1)
		t = nt
		datePostfix := nt.Format("20060102")
		lPS, _ := r.GetPS(datePostfix, iAlsoInactive)
		if len(strings.TrimSpace(lPS.PSExtID)) > 0 {
			ePSDate = datePostfix
			found = true
		}

	}

	return ePSDate, err
}
func (r RepoPS) GetDateNextPSOBSOLETE(iAlsoInactive bool, iDateInRelationTo string, iMaxNumFore int) (ePSDate string, err error) {
	getDBRef()
	s := iDateInRelationTo
	t, _ := time.Parse("20060102", s)
	found := false
	ePSDate = ""
	err = nil
	for i := 0; i < iMaxNumFore && !found; i++ {
		nt := t.AddDate(0, 0, 1)
		t = nt
		datePostfix := nt.Format("20060102")
		lPS, _ := r.GetPS(datePostfix, iAlsoInactive)
		if len(strings.TrimSpace(lPS.PSExtID)) > 0 {
			ePSDate = datePostfix
			////println("ePSDate=" + ePSDate)
			found = true
		}

	}
	return ePSDate, err
}

func (r RepoPS) GetDateLastPSOBSOLETE(iAlsoInactive bool) (ePSDate string, err error) {
	getDBRef()
	////println("(r Repo) ExistsPS(iPSErscheinungsdatum string) called")
	db, err := sql.Open("mysql", databaselocation)
	if err != nil {
		////println("err=" + err.Error())
		return "", err
	}

	defer db.Close()
	esd := ""
	sqlstat := "SELECT MAX(psesd) FROM psheader"
	if iAlsoInactive {
		sqlstat += " and isactivated=1 "
	}
	err = db.QueryRow(sqlstat).Scan(&esd)

	return esd, err
}

func (r RepoPS) GetDateLastPS(iAlsoInactive bool) (ePSDate string, err error) {
	getDBRef()
	////println("GetDateLastPS(iAlsoInactive bool) CALLED")
	////println("GetPS():" + iPSErscheinungsdatum + " " + iRole)
	db, err := sql.Open("mysql", databaselocation)
	if err != nil {
		//println("err sql.Open " + err.Error())
		return "", err
	}
	defer db.Close()

	headerQuery := `SELECT max(psesd) FROM psheader `
	if !iAlsoInactive {
		headerQuery += " where isactivated=1 "
	}
	headerQuery += ";"
	rows, err := db.Query(headerQuery)
	if err != nil {
		//println("err  db.Query(headerQuery) " + err.Error())
		return "", err
	}
	defer rows.Close()

	var (
		pserscheinungsdatum string
	)
	for rows.Next() {

		err := rows.Scan(&pserscheinungsdatum)
		if err != nil {
			return "", err
		}
		////println("rows.Next():" + lPSErscheinungsdatum + " " + pserscheinungsdatum)

	}
	////println(" pserscheinungsdatum:" + pserscheinungsdatum)
	ePSDate = pserscheinungsdatum

	return ePSDate, nil

}
func (r RepoPS) GetDateBeforePS(iAlsoInactive bool, iDateInRelationTo string) (ePSDate string, err error) {
	getDBRef()
	db, err := sql.Open("mysql", databaselocation)
	defer db.Close()

	headerQuery := `SELECT max(psesd) FROM psheader WHERE psesd<?`
	if !iAlsoInactive {
		headerQuery += " and isactivated=1 "
	}
	lPSErscheinungsdatum := iDateInRelationTo
	rows, err := db.Query(headerQuery, lPSErscheinungsdatum)
	if err != nil {
		////println("err  " + err.Error())
		return "", err
	}
	defer rows.Close()

	var (
		pserscheinungsdatum sql.NullString
	)
	for rows.Next() {

		err := rows.Scan(&pserscheinungsdatum)
		if err != nil {
			return "", err
		}
		////println("rows.Next():" + lPSErscheinungsdatum + " " + pserscheinungsdatum)
	}
	if reflect.TypeOf(pserscheinungsdatum) == nil {
		ePSDate = ""
	} else {
		ePSDate = pserscheinungsdatum.String
	}
	return ePSDate, nil
}
func (r RepoPS) GetDateNextPS(iAlsoInactive bool, iDateInRelationTo string) (ePSDate string, err error) {

	getDBRef()
	////println("GetPS():" + iPSErscheinungsdatum + " " + iRole)
	db, err := sql.Open("mysql", databaselocation)
	defer db.Close()

	headerQuery := `SELECT min(psesd) FROM psheader WHERE psesd > ?`
	if !iAlsoInactive {
		headerQuery += " and isactivated=1 "
	}
	lPSErscheinungsdatum := iDateInRelationTo
	rows, err := db.Query(headerQuery, lPSErscheinungsdatum)
	if err != nil {
		////println("err  " + err.Error())
		return "", err
	}
	defer rows.Close()

	var (
		pserscheinungsdatum sql.NullString
	)
	for rows.Next() {

		err := rows.Scan(&pserscheinungsdatum)
		if err != nil {
			return "", err
		}
		////println("rows.Next():" + lPSErscheinungsdatum + " " + pserscheinungsdatum)

	}
	if reflect.TypeOf(pserscheinungsdatum) == nil {
		ePSDate = ""
	} else {
		ePSDate = pserscheinungsdatum.String
	}

	return ePSDate, nil
}
