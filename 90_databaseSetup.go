package main

import (
	"database/sql"
	"fmt"
	"os"
	bootstrap "ps_meta/z_bootstrap"
	"ps_settings/def"
)

// CONSTANTS
const GcInsHeader string = "insert into psheader (" +
	"psextid, " +
	"psart, " +
	"psausgabenr, " +
	"psesd, " +
	"psrfa, " +
	"psimageuri, " +
	"psimagetyp, " +
	"psimagetypversion, " +
	"isactivated, " +
	"activatedbyuser, " +
	"activateddate, " +
	"activatedtime, " +
	"logicalnameimpsys, " +
	"importusrname, " +
	"importtime " +
	") values ("
const GcInsMeldung string = "insert into psmeldung (" +
	"psextid, " +
	"melextid, " +
	"melpsposnr, " +
	"melinhalttext, " +
	"melpsrubrik " +
	") values ("
const GcInsArticle string = "insert into psarticle (" +
	"psextid, " +
	"melextid, " +
	"extid, " +
	"psposnr, " +
	"psrubrik, " +
	"esd, " +
	"print_online, " +
	"anzahlfotos, " +
	"ausgabe, " +
	"titel, " +
	"seite1titel, " +
	"untertitel, " +
	"sonsttitel, " +
	"inhalttext, " +
	"autoabstract, " +
	"schlagwort, " +
	"autor, " +
	"bemerkung, " +
	"quellekuerzel, " +
	"quellename, " +
	"quellepubnr, " +
	"quelleseitennr, " +
	"imageuri, " +
	"imagetyp, " +
	"imagetypversion " +
	") values ("

const (
	AuthUsrAdmin               = "USRADM"
	AuthSysAdmin               = "SYSADM"
	AuthSearchWithInactive     = "INACTIVESEARCH"
	AuthSearchUnlimited        = "UNLMTDSEARCH"
	AuthSearchOneYear          = "ONEYEARSEARCH"
	AuthDataImport             = "DATAIMPORT"
	AuthUsrAdminDesc           = "Useradmin"
	AuthSysAdminDesc           = "Systemadmin"
	AuthSearchWithInactiveDesc = "Inaktive Suche"
	AuthSearchUnlimitedDesc    = "Unbegrenzte Suche"
	AuthSearchOneYearDesc      = "Suche über ein Jahr"
	AuthDataImportDesc         = "Recht zum Datenimport"
)

const (
	RoleNameSuperUser        = "SuperUser"
	RoleNameSysAdm           = "SystemAdministrator"
	RoleNameUsrAdm           = "UserAdministrator"
	RoleNameDataImporter     = "DataImporter"
	RoleNameSearchExpert     = "SearchExpert"
	RoleNameSearcher         = "Searcher"
	RoleNameSuperUserDesc    = "SuperUser"
	RoleNameSysAdmDesc       = "SystemAdministrator"
	RoleNameUsrAdmDesc       = "UserAdministrator"
	RoleNameDataImporterDesc = "DataImporter"
	RoleNameSearchExpertDesc = "SearchExpert"
	RoleNameSearcherDesc     = "Searcher"
)

func define_database_table(applConf bootstrap.ApplConfStruct, iDBHostIP, iDBHostPort string) {
	var lvTable string
	var lvFields string
	var ltValue []string
	var lvStatement def.Statement
	var ltStatement []def.Statement

	gvDatabase := "pressespiegel"
	gvConnection := applConf.DB_UserPwd + "@tcp(" + iDBHostIP + ":" + iDBHostPort + ")/"
	////println("IIIIIIIIIIIIIIIIIIIIIIIIIIIIIII   mysqldbserverRef:"+mysqldbserverRef, " IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII")
	//databaselocation = mysqldbserverRef + "/" + mysqlApplschemaName
	////println("database location:" + databaselocation)
	//gvConnection := applConf.DB_username + ":" + applConf.DB_userpw + "@tcp(" + gvConf.DB_adress + ":" + gvConf.DB_port + ")/"

	//Database
	create_database(gvConnection, gvDatabase)

	//PS
	lvTable = "psheader"
	lvFields = "(" +
		"intid int not null auto_increment, " +
		"psextid char(14), " +

		"psart varchar(30), " +
		"psausgabenr varchar(30), " +
		"psesd varchar(20), " +
		"psrfa varchar(20), " +

		"psimageuri varchar(200), " +
		"psimagetyp varchar(20), " +
		"psimagetypversion varchar(20), " +

		"isactivated tinyint, " +
		"activatedbyuser varchar(20), " +
		"activateddate varchar(20), " +
		"activatedtime varchar(20), " +
		"logicalnameimpsys varchar(20), " +
		"importusrname varchar(20), " +
		"importtime varchar(20), " +
		"primary key (intid))"
	create_table(gvConnection, gvDatabase, lvTable, lvFields)

	//Meldung
	lvTable = "psmeldung"
	lvFields = "(" +
		"intid int not null auto_increment, " +
		"psextid varchar(100), " +
		"melextid varchar(100), " +
		"melpsposnr varchar(100), " +
		"melinhalttext text, " +
		"melpsrubrik varchar(200), " +
		"primary key (intid))"
	create_table(gvConnection, gvDatabase, lvTable, lvFields)

	//Article
	lvTable = "psarticle"
	lvFields = "(" +
		"intid int not null auto_increment, " +
		"psextid varchar(100), " +
		"melextid varchar(100), " +
		"extid varchar(100), " +
		"psposnr varchar(100), " +

		"psrubrik varchar(200), " +
		"esd varchar(20), " +
		"print_online char(1), " +
		"anzahlfotos int, " +
		"ausgabe varchar(200), " +

		"titel text, " +
		"seite1titel text, " +
		"untertitel text, " +
		"sonsttitel text, " +
		"inhalttext mediumtext, " +
		"autoabstract text, " +
		"schlagwort text, " +
		"autor text, " +
		"bemerkung text, " +

		"quellekuerzel text, " +
		"quellename text, " +
		"quellepubnr text, " +
		"quelleseitennr text, " +

		"imageuri text, " +
		"imagetyp text, " +
		"imagetypversion text, " +
		"primary key (intid))"
	create_table(gvConnection, gvDatabase, lvTable, lvFields)

	//User
	lvTable = "psusr"
	lvFields = "(" +
		"intid int not null auto_increment, " +
		"loginname varchar(20), " +
		"rfa varchar(10), " +
		"name varchar(20), " +
		"pwd varchar(20), " +
		"email varchar(200), " +
		"isdeactivated TINYINT, " +
		"deactivatedby varchar(20), " +
		"deactivateddate varchar(20), " +
		"deactivatedtime varchar(20), " +
		"reactivatedby varchar(20), " +
		"reactivateddate varchar(20), " +
		"reactivatedtime varchar(20), " +
		"createddate varchar(20), " +
		"createdtime varchar(20), " +
		"createdbyusr varchar(20), " +
		"createdbycomp varchar(200), " +
		"updatedate varchar(20), " +
		"updatetime varchar(20), " +
		"updatebyusr varchar(20), " +
		"updatedbycomp varchar(200), " +
		"primary key (intid))"
		// "FOREIGN KEY (loginname, rfa) reference psusrtoken(loginname, rfa)"
		// "FOREIGN KEY (email) reference psusrtoken(loginname, rfa)"
	create_table(gvConnection, gvDatabase, lvTable, lvFields)
	insert_user_sysadmin(gvConnection, gvDatabase)
	insert_user_dokumentar(gvConnection, gvDatabase)
	insert_user_searcher(gvConnection, gvDatabase)
	insert_user_searchexpert(gvConnection, gvDatabase)

	//UserToken
	lvTable = "psusrtoken"
	lvFields = "(" +
		// "intid int not null auto_increment, "+
		"loginname char(20), " +
		"rfa char(10), " +
		"token varchar(300), " +
		"validdatefrom varchar(20), " +
		"validdateto varchar(20), " +
		"primary key (loginname, rfa))"
	create_table(gvConnection, gvDatabase, lvTable, lvFields)

	//UserRole
	lvTable = "psusrrole"
	lvFields = "(" +
		// "intid int not null auto_increment, "+
		"loginname varchar(20), " +
		"rfa varchar(10), " +
		"authrole varchar(20), " +
		"createddate varchar(20), " +
		"createdtime varchar(20), " +
		"createdby varchar(20), " +
		"primary key (loginname, rfa, authrole))"
	create_table(gvConnection, gvDatabase, lvTable, lvFields)
	/*ltValue = make([]string, 0)
	if applConf.CreateDefaultUsr {
		ltValue = append(ltValue, `("sysadmin", "wdr", "`+RoleNameSysAdm+`","","","")`)
		ltValue = append(ltValue, `("dokumentar", "wdr", "`+RoleNameDataImporter+`","","","")`)
		ltValue = append(ltValue, `("searcher", "wdr", "`+RoleNameSearcher+`","","","")`)
		ltValue = append(ltValue, `("searchexpert", "wdr", "`+RoleNameSearchExpert+`","","","")`)
	}
	insert_table_via_array(gvConnection, gvDatabase, lvTable, ltValue)
	*/
	//AuthRole
	lvTable = "psauthrole"
	lvFields = "(" +
		"name varchar(20), " +
		"description varchar(200), " +
		"primary key (name))"
	create_table(gvConnection, gvDatabase, lvTable, lvFields)
	ltValue = make([]string, 0)
	ltValue = append(ltValue, `("`+RoleNameSuperUser+`", "`+RoleNameSuperUserDesc+`")`)
	ltValue = append(ltValue, `("`+RoleNameSysAdm+`", "`+RoleNameSysAdmDesc+`")`)
	ltValue = append(ltValue, `("`+RoleNameUsrAdm+`", "`+RoleNameUsrAdmDesc+`")`)
	ltValue = append(ltValue, `("`+RoleNameDataImporter+`", "`+RoleNameDataImporterDesc+`")`)
	ltValue = append(ltValue, `("`+RoleNameSearchExpert+`", "`+RoleNameSearchExpertDesc+`")`)
	ltValue = append(ltValue, `("`+RoleNameSearcher+`", "`+RoleNameSearcherDesc+`")`)
	insert_table_via_array(gvConnection, gvDatabase, lvTable, ltValue)

	insert_table_via_array(gvConnection, gvDatabase, lvTable, ltValue)

	//AuthObjects
	lvTable = "psauthobj"
	lvFields = "(" +
		"name varchar(15), " +
		"description varchar(200), " +
		"primary key (name))"
	create_table(gvConnection, gvDatabase, lvTable, lvFields)
	ltValue = make([]string, 0)
	ltValue = append(ltValue, `("`+AuthUsrAdmin+`", "`+AuthUsrAdminDesc+`")`)
	ltValue = append(ltValue, `("`+AuthSysAdmin+`", "`+AuthSysAdminDesc+`")`)
	ltValue = append(ltValue, `("`+AuthSearchWithInactive+`", "`+AuthSearchWithInactiveDesc+`")`)
	ltValue = append(ltValue, `("`+AuthSearchUnlimited+`", "`+AuthSearchUnlimitedDesc+`")`)
	ltValue = append(ltValue, `("`+AuthSearchOneYear+`", "`+AuthSearchOneYearDesc+`")`)
	ltValue = append(ltValue, `("`+AuthDataImport+`", "`+AuthDataImportDesc+`")`)
	insert_table_via_array(gvConnection, gvDatabase, lvTable, ltValue)

	//AuthRoleObject
	lvTable = "pauthroleauthobj"
	lvFields = "(" +
		"authrolename varchar(20), " +
		"authobjname varchar(15), " +
		"primary key (authrolename, authobjname))"
	create_table(gvConnection, gvDatabase, lvTable, lvFields)
	ltValue = make([]string, 0)
	ltValue = append(ltValue, `("`+RoleNameSuperUser+`", "`+AuthSearchUnlimited+`")`)
	ltValue = append(ltValue, `("`+RoleNameSuperUser+`", "`+AuthSearchWithInactive+`")`)
	ltValue = append(ltValue, `("`+RoleNameSuperUser+`", "`+AuthSysAdmin+`")`)
	ltValue = append(ltValue, `("`+RoleNameSuperUser+`", "`+AuthUsrAdmin+`")`)
	ltValue = append(ltValue, `("`+RoleNameSuperUser+`", "`+AuthDataImport+`")`)
	ltValue = append(ltValue, `("`+RoleNameSysAdm+`", "`+AuthSysAdmin+`")`)
	ltValue = append(ltValue, `("`+RoleNameUsrAdm+`", "`+AuthUsrAdmin+`")`)
	ltValue = append(ltValue, `("`+RoleNameDataImporter+`", "`+AuthDataImport+`")`)
	ltValue = append(ltValue, `("`+RoleNameDataImporter+`", "`+AuthSearchWithInactive+`")`)
	ltValue = append(ltValue, `("`+RoleNameSearchExpert+`", "`+AuthSearchWithInactive+`")`)
	ltValue = append(ltValue, `("`+RoleNameSearchExpert+`", "`+AuthSearchUnlimited+`")`)
	ltValue = append(ltValue, `("`+RoleNameSearcher+`", "`+AuthSearchOneYear+`")`)
	insert_table_via_array(gvConnection, gvDatabase, lvTable, ltValue)

	//psentityimagedesc
	lvTable = "psentityimagedesc"
	lvFields = "(" +
		"EIDID int not null auto_increment, " +
		"entityextid  varchar(100), " +
		"entitytype varchar(10), " +
		"repopath text, " +
		"imagerepostatus varchar(200), " +
		"datereporegistration varchar(20), " +
		"timereporegistration varchar(20), " +
		"repostatusdetails varchar(300), " +
		"movedtohistoryarchivelocat text, " +
		"datemovetohistoryarchiv varchar(20), " +
		"timemovetohistoryarchiv varchar(20), " +
		"daterestore varchar(20), " +
		"timerestore varchar(20), " +
		"psesd varchar(10), " +
		"melextid  varchar(100)," +
		"primary key (EIDID))"
		// "FOREIGN KEY (loginname, rfa) reference psusrtoken(loginname, rfa)"
		// "FOREIGN KEY (email) reference psusrtoken(loginname, rfa)"
	create_table(gvConnection, gvDatabase, lvTable, lvFields)

	lvTable = "psingestorder"
	lvFields = "(" +
		"IGSID int not null auto_increment, " +
		"OP char(1), " +
		"PRIO int, " +
		"IGSEXTID  varchar(20), " +
		"ARTDATA varbinary(60000), " +
		"dateregist varchar(20), " +
		"timeregist varchar(20), " +
		"statproc varchar(10)," +
		"primary key (IGSID))"
	create_table(gvConnection, gvDatabase, lvTable, lvFields)

	lvTable = "psingeststat"
	lvFields = "(" +
		"IGSID int not null, " +
		"OP char(1), " +
		"IGSEXTID  varchar(20), " +
		"ARTDATA varbinary(60000), " +
		"dateregist varchar(20), " +
		"timeregist varchar(20), " +
		"dateingest varchar(20), " +
		"timeingest varchar(20), " +
		"statproc varchar(100)," +
		"stattxt varchar(500), " +
		"primary key (IGSID))"
	create_table(gvConnection, gvDatabase, lvTable, lvFields)
	//println(" lvTable = pscollection")

	lvTable = "pscollection"
	lvFields = "(" +
		"USRNAME  varchar(20), " +
		"COLLNAME varchar(50), " +
		"ALDAT varchar(20), " +
		"ALTIM  varchar(20), " +
		"primary key (USRNAME, COLLNAME))"
	create_table(gvConnection, gvDatabase, lvTable, lvFields)

	lvTable = "pscollactive"
	lvFields = "(" +
		"USRNAME  varchar(20), " +
		"COLLNAME varchar(50), " +
		"ACTIVEDAT varchar(20), " +
		"ACTIVETIM  varchar(20), " +
		"primary key (USRNAME))"
	create_table(gvConnection, gvDatabase, lvTable, lvFields)

	lvTable = "pscollitem"
	lvFields = "(" +
		"USRNAME  varchar(20), " +
		"COLLNAME varchar(50), " +
		"ITEMIDFIELD1 varchar(100), " +
		"ITEMIDFIELD2 varchar(100), " +
		"ALDAT varchar(20), " +
		"ALTIM  varchar(20), " +
		"primary key (USRNAME, COLLNAME,ITEMIDFIELD1,ITEMIDFIELD2))"
	create_table(gvConnection, gvDatabase, lvTable, lvFields)

	// CREATE INDEX
	ltStatement = make([]def.Statement, 0)
	lvStatement.Text = `CREATE INDEX tokenIdx ON psusrtoken (token)`
	ltStatement = append(ltStatement, lvStatement)
	lvStatement.Text = `CREATE INDEX loginnameRfaIdx ON psusr (loginname, rfa)`
	ltStatement = append(ltStatement, lvStatement)
	lvStatement.Text = `CREATE INDEX emailIdx ON psusr (email)`
	ltStatement = append(ltStatement, lvStatement)

	lvStatement.Text = `CREATE INDEX psesdIdx ON psheader (psesd)`
	ltStatement = append(ltStatement, lvStatement)
	lvStatement.Text = `CREATE INDEX psidIdx ON psmeldung (psextid)`
	ltStatement = append(ltStatement, lvStatement)
	lvStatement.Text = `CREATE INDEX psextIdx ON psarticle (psextid)`
	ltStatement = append(ltStatement, lvStatement)
	lvStatement.Text = `CREATE INDEX psmelIdx ON psarticle (psextid, melextid)`
	ltStatement = append(ltStatement, lvStatement)

	lvStatement.Text = `CREATE INDEX extidIdx ON psentityimagedesc (entityextid, entitytype)`
	ltStatement = append(ltStatement, lvStatement)
	lvStatement.Text = `CREATE INDEX psesdIdx ON psentityimagedesc (psesd)`
	ltStatement = append(ltStatement, lvStatement)
	lvStatement.Text = `CREATE INDEX psmeIdIdx ON psentityimagedesc (psesd, melextid)`
	ltStatement = append(ltStatement, lvStatement)

	lvStatement.Text = `CREATE INDEX psigsIdIdx ON psingestorder (IGSEXTID, PRIO)`
	ltStatement = append(ltStatement, lvStatement)

	lvStatement.Text = `CREATE INDEX psigstaIdx ON psingeststat (stattxt, IGSEXTID)`
	ltStatement = append(ltStatement, lvStatement)

	execute_statement(gvConnection, gvDatabase, ltStatement)
}

// create database with parametername and delete old database/tables
func create_database(connection string, database string) {
	db, err := sql.Open("mysql", connection)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	_, err = db.Exec("use " + database)
	var statement string
	if err != nil {
		fmt.Println("no database")
		statement = "create database " + database + " character set 'utf8'"
		_, err = db.Exec(statement)
		fmt.Println(statement)
	}
	db.Close()
}

// create single table
func create_table(connection string, database string, table string, fields string) {
	db, err := sql.Open("mysql", connection+database)
	// fmt.Println(connection+database)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	var statement string = "select 1 from " + table + " limit 1"
	_, err = db.Exec(statement)
	if err != nil {
		fmt.Println("*NEWTAB")
		fmt.Println("Tabelle " + table + " existiert nicht und wird neu angelegt")
		var statement string = "create table " + table
		if len(fields) > 0 {
			statement = statement + " " + fields + " character set 'utf8'"
		}
		_, err = db.Exec(statement)
		if err != nil {
			fmt.Println(err)
		}
		fmt.Println(statement)
	} else {
		fmt.Println("Tabelle " + table + " existiert bereits und wird nicht neu angelegt")
	}
	db.Close()
}

// execute generated statements from
func execute_statement(connection string, database string, statements []def.Statement) {
	db, err := sql.Open("mysql", connection+database)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	for i := 0; i < len(statements); i++ {
		_, err = db.Exec(statements[i].Text)
		if err != nil {
			fmt.Println(err)
			fmt.Println(statements[i].Text)
		} else {
			if len(statements[i].Info) > 1 {
				fmt.Println(statements[i].Info)
			}
		}
	}
	db.Close()
}

//insert without autoincrement
func insert_table_via_array(connection string, database string, table string, value []string) {
	if len(value) > 0 {
		db, err := sql.Open("mysql", connection+database)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		fmt.Println("insert into " + table)
		for i := 0; i < len(value); i++ {
			var statement string = "insert into " + table + " values " + value[i]
			_, err = db.Exec(statement)
			if err != nil {
				fmt.Println(err)
				fmt.Println(statement)
			}
		}
		db.Close()
	}
}

func insert_user_sysadmin(connection string, database string) {
	db, err := sql.Open("mysql", connection+database)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	var statement string = "insert into psusr (loginname, " +
		"rfa, " +
		"name, " +
		"pwd, " +
		"email, " +
		"isdeactivated, " +
		"deactivatedby, " +
		"deactivateddate, " +
		"deactivatedtime, " +
		"reactivatedby, " +
		"reactivateddate, " +
		"reactivatedtime, " +
		"createddate, " +
		"createdtime, " +
		"createdbyusr, " +
		"createdbycomp, " +
		"updatedate, " +
		"updatetime, " +
		"updatebyusr, " +
		"updatedbycomp) " +
		`values ("sysadmin", "wdr", "sysadmin", "sysadmin", "sysadmin@wdr.de",0,"","","","","","","","","","","","","","")`
	_, err = db.Exec(statement)
	if err != nil {
		fmt.Println(err)
		fmt.Println(statement)
	}
	db.Close()
}

func insert_user_dokumentar(connection string, database string) {
	db, err := sql.Open("mysql", connection+database)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	var statement string = "insert into psusr (loginname, " +
		"rfa, " +
		"name, " +
		"pwd, " +
		"email, " +
		"isdeactivated, " +
		"deactivatedby, " +
		"deactivateddate, " +
		"deactivatedtime, " +
		"reactivatedby, " +
		"reactivateddate, " +
		"reactivatedtime, " +
		"createddate, " +
		"createdtime, " +
		"createdbyusr, " +
		"createdbycomp, " +
		"updatedate, " +
		"updatetime, " +
		"updatebyusr, " +
		"updatedbycomp) " +
		`values ("dokumentar", "wdr", "dokumentar","dokumentar", "dokumentar@wdr.de",0,"","","","","","","","","","","","","","")`
	_, err = db.Exec(statement)
	if err != nil {
		fmt.Println(err)
		fmt.Println(statement)
	}
	db.Close()
}
func insert_user_searcher(connection string, database string) {
	db, err := sql.Open("mysql", connection+database)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	var statement string = "insert into psusr (loginname, " +
		"rfa, " +
		"name, " +
		"pwd, " +
		"email, " +
		"isdeactivated, " +
		"deactivatedby, " +
		"deactivateddate, " +
		"deactivatedtime, " +
		"reactivatedby, " +
		"reactivateddate, " +
		"reactivatedtime, " +
		"createddate, " +
		"createdtime, " +
		"createdbyusr, " +
		"createdbycomp, " +
		"updatedate, " +
		"updatetime, " +
		"updatebyusr, " +
		"updatedbycomp) " +
		`values ("searcher", "wdr", "searcher","searcher", "searcher@wdr.de",0,"","","","","","","","","","","","","","")`
	_, err = db.Exec(statement)
	if err != nil {
		fmt.Println(err)
		fmt.Println(statement)
	}

	db.Close()
}
func insert_user_searchexpert(connection string, database string) {
	db, err := sql.Open("mysql", connection+database)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	var statement string = "insert into psusr (loginname, " +
		"rfa, " +
		"name, " +
		"pwd, " +
		"email, " +
		"isdeactivated, " +
		"deactivatedby, " +
		"deactivateddate, " +
		"deactivatedtime, " +
		"reactivatedby, " +
		"reactivateddate, " +
		"reactivatedtime, " +
		"createddate, " +
		"createdtime, " +
		"createdbyusr, " +
		"createdbycomp, " +
		"updatedate, " +
		"updatetime, " +
		"updatebyusr, " +
		"updatedbycomp) " +
		`values ("searchexpert", "wdr", "searchexpert","searchexpert", "searchexpert@wdr.de",0,"","","","","","","","","","","","","","")`
	_, err = db.Exec(statement)
	if err != nil {
		fmt.Println(err)
		fmt.Println(statement)
	}

	db.Close()
}
