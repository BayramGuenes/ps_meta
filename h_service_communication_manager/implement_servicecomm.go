package h_service_communication_manager

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	bootstrap "ps_meta/z_bootstrap"
	"time"
)

const ThisMicroServiceName = "ps_meta"
const MicroServiceNameRegistrator = "ps_sysreg"
const MicroServiceNamePsImage = "ps_image"
const MicroServiceNamePsQuery = "ps_query"
const MicroServiceNamePsCollect = "ps_collect"

type ServiceCommunication interface {
	RegisterMe() (eResultRegister ResultRegisterMicroservice)
	registerMicroservice(iMikroservice string, iPort string) (eResultRegister ResultRegisterMicroservice)
	GetPortMicroservice(iMicroServiceName string) (eResultGetPort ResultGetPortMicroservice)
}

type ResultGetPortMicroservice struct {
	ResultOk      bool
	Port          string
	ResultDetails string
}

type ReqDataRegisterMicroservice struct {
	MicroserviceName string
	Port             string
}
type ResultRegisterMicroservice struct {
	ResultOk      bool
	ResultDetails string
}

type ServiceCommunicator struct {
	ThisMicroServiceName string
}

func (sc ServiceCommunicator) RegisterMe() (eResultRegister ResultRegisterMicroservice) {
	time.Sleep(10 * time.Second)
	eResultRegister = sc.registerMicroservice(ThisMicroServiceName, bootstrap.ThisPort)
	return
}

func (sc ServiceCommunicator) registerMicroservice(iMikroservice string, iPort string) (eResultRegister ResultRegisterMicroservice) {
	//	lResultGetPort := sc.GetPortMicroservice(MicroServiceNameRegistrator)
	eResultRegister = ResultRegisterMicroservice{ResultOk: true, ResultDetails: ""}

	//serviceadr := bootstrap.ThisCommunnicationProtokoll + "://" + bootstrap.ThisHostname + ":" + bootstrap.SysRegPort + "/RegisterMicroservice"
	serviceadr := bootstrap.ThisCommunnicationProtokoll + "://" + bootstrap.ServiceLocationIDSysReg + ":" + bootstrap.SysRegPort + "/RegisterMicroservice"

	reqDataRegisterMS := ReqDataRegisterMicroservice{}
	reqDataRegisterMS.MicroserviceName = iMikroservice
	reqDataRegisterMS.Port = iPort

	requestBody, err := json.Marshal(reqDataRegisterMS)

	if err != nil {
		eResultRegister.ResultOk = false
		eResultRegister.ResultDetails = "requestBody, err := json.Marshal(reqDataRegisterMS):" + err.Error()
		return
	}
	resp, err := http.Post(serviceadr, "application/json", bytes.NewBuffer(requestBody))
	if err != nil {
		eResultRegister.ResultOk = false
		eResultRegister.ResultDetails = " http.Post(serviceadr, ...Err:" + err.Error()
		return
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err = json.Unmarshal(body, &eResultRegister); err != nil {
		eResultRegister.ResultDetails = "Technical error: json.Unmarshal(body, &respStruct)) failed." + err.Error()
		eResultRegister.ResultOk = false
		return
	}
	return eResultRegister
}

func (sc ServiceCommunicator) GetPortMicroservice(iMicroServiceName string) (eResultGetPort ResultGetPortMicroservice) {

	eResultGetPort = ResultGetPortMicroservice{}
	//serviceadr := bootstrap.ThisCommunnicationProtokoll + "://" + bootstrap.ThisHostname + ":" + bootstrap.SysRegPort + "/GetPortMicroService/" + iMicroServiceName
	serviceadr := bootstrap.ThisCommunnicationProtokoll + "://" + bootstrap.ServiceLocationIDSysReg + ":" + bootstrap.SysRegPort + "/GetPortMicroService/" + iMicroServiceName

	resp, err := http.Get(serviceadr)
	if err != nil {
		//println(err.Error())
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	////println(" GetPortMicroservice:Result" + string(body))
	//  c.JSON(http.StatusOK, string(body))
	if err = json.Unmarshal(body, &eResultGetPort); err != nil {
		eResultGetPort.ResultDetails = "Technical error: json.Unmarshal(body, &respStruct)) failed." + err.Error()
		eResultGetPort.ResultOk = false
		return
	}
	return eResultGetPort
}

var ApplServiceCom = ServiceCommunicator{ThisMicroServiceName}
