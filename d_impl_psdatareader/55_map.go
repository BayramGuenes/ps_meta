package d_impl_psdatareader

import (
	"fmt"
	domain "ps_meta/a_domain"
	"strconv"
	"strings"
)

func mapToIntern(iElementName, iElementVal string, psentity *domain.Pressespiegel, psmeldung *domain.Meldung, psartikel *domain.Artikel) {
	switch iElementName {
	case "PRESSESPIEGEL":
	case "PSP_IMAGE_URI":
		if len(iElementVal) > 0 && len(psentity.PSImageURI) == 0 {
			psentity.PSImageURI = iElementVal
		}
	case "PSP_ART":
		if len(iElementVal) > 0 && len(psentity.PSArt) == 0 {
			psentity.PSArt = iElementVal
		}
	case "PSP_IMAGE_TYP":
		if len(iElementVal) > 0 && len(psentity.PSImageTyp) == 0 {
			psentity.PSImageTyp = iElementVal
		}
	case "PSP_AUSGABENR":
		if len(iElementVal) > 0 && len(psentity.PSAusgabenr) == 0 {
			psentity.PSAusgabenr = iElementVal
		}
	case "PSP_ID":
		if len(iElementVal) > 0 && len(psentity.PSExtID) == 0 {
			psentity.PSExtID = iElementVal
		}
	case "PSP_ERSCHEINUNGSDATUM":
		if len(iElementVal) > 0 && len(psentity.PSESD) == 0 {
			psentity.PSESD = iElementVal
		}
	case "PSP_IMAGE_TYPVERSION":
		if len(iElementVal) > 0 && len(psentity.PSImageTypVersion) == 0 {
			psentity.PSImageTypVersion = iElementVal
		}
	case "PSRFA":
		if len(iElementVal) > 0 && len(psentity.PSRFA) == 0 {
			psentity.PSRFA = iElementVal
		}
	case "PSP_ARD_ANSTALT":
		if len(iElementVal) > 0 && len(psentity.PSRFA) == 0 {
			psentity.PSRFA = iElementVal
		}
	case "MELDUNG_LISTE":
	case "MELDUNG":
	case "MELDUNG_ID":
		psmeldung.MelExtID = iElementVal

	case "MELDUNG_PSP_RUBRIK":
		psmeldung.MelPSRubrik = iElementVal

	case "MELDUNG_INHALTTEXT":
		psmeldung.MelInhalttext = iElementVal

	case "MELDUNG_PSP_POSNR":
		psmeldung.MelPSPosnr = iElementVal

	case "ARTIKEL_LISTE":
	case "ARTIKEL":
	case "ARTIKEL_IMAGE_TYPVERSION":
		psartikel.ImageTypVersion = iElementVal
	case "ARTIKEL_ID":
		psartikel.ExtID = iElementVal
	case "ARTIKEL_SCHLAGWORT":
		psartikel.Schlagwort = iElementVal
	case "ARTIKEL_IMAGE_TYP":
		psartikel.ImageTyp = iElementVal
	case "ARTIKEL_ANZAHL_FOTOS":
		psartikel.AnzahlFotos, _ = strconv.Atoi(iElementVal)
	case "ARTIKEL_MELDUNG_ID":
		psartikel.MelExtID = iElementVal
	case "ARTIKEL_UNTERTITEL":
		//if len(iElementVal) > 0 && len(psartikel.ArtUntertitel) > 0 {
		psartikel.Untertitel = iElementVal
		////println("psartikel.ArtUntertitel:" + psartikel.ArtUntertitel)

	case "ARTIKEL_SONST_TITEL":
		psartikel.SonstTitel = iElementVal
	case "ARTIKEL_QUELLE_NAME":
		psartikel.QuelleName = iElementVal
	case "ARTIKEL_ERSCHEINUNGSDATUM":
		psartikel.ESD = iElementVal
	case "ARTIKEL_PSP_RUBRIK":
		psartikel.PSRubrik = iElementVal
	case "ARTIKEL_TITEL":
		psartikel.Titel = iElementVal
	case "ARTIKEL_SEITE1_TITEL":
		psartikel.Seite1Titel = iElementVal
	case "ARTIKEL_INHALTTEXT":
		psartikel.Inhalttext = iElementVal
	case "ARTIKEL_AUTOABSTRACT":
		psartikel.Autoabstract = iElementVal
	case "ARTIKEL_QUELLE_PUBNR":
		psartikel.QuellePubnr = iElementVal
	case "ARTIKEL_QUELLE_SEITENNR":
		psartikel.QuelleSeitennr = iElementVal
	case "ARTIKEL_AUSGABE":
		psartikel.Ausgabe = iElementVal
	case "ARTIKEL_BEMERKUNG":
		psartikel.Bemerkung = iElementVal
	case "ARTIKEL_QUELLE_KUERZEL":
		psartikel.QuelleKuerzel = iElementVal
	case "ARTIKEL_PRINT_ONLINE":
		psartikel.Print_Online = iElementVal
	case "ARTIKEL_IMAGE_URI":
		psartikel.ImageURI = iElementVal
	case "ARTIKEL_AUTOR":
		psartikel.Autor = iElementVal
	case "ARTIKEL_PSP_POSNR":
		psartikel.PSPosnr = iElementVal
	case "":
	case "Envelope":
	case "Header":
	case "Hostname":
	case "Body":
	case "pressespiegel_request":

	default:
		fmt.Println("ilementName=" + iElementName + " nicht gemappt.")
	}

	//if iElementName == "PSP_IMAGE_TYPVERSION" {
	//	fmt.//println("iElementVal" + iElementVal)
	//}
	//fmt.//println("psentity.PSImageURI=" + psentity.PSImageURI)
	//fmt.//println("psentity.PSP_IMAGE_TYP=" + psentity.PSImageTyp)

	//return psentity
}

func startNewMeldung(iMeldung *domain.Meldung) {
	//clear(iMeldung)

	iMeldung.MelExtID = ""
	iMeldung.MelInhalttext = ""
	iMeldung.MelPSPosnr = ""
	iMeldung.MelPSRubrik = ""

}

/* ========================================================== */
/*                                                            */
/* ========================================================== */

func extractFilename(iArtImageURI string) string {
	filestringslice := strings.Split(iArtImageURI, string("/"))
	len := len(filestringslice) - 1
	filename := filestringslice[len]
	return filename
}
