package d_impl_psdatareader

import (
	"encoding/xml"
	"os"
	domain "ps_meta/a_domain"
	"strings"
)

func readToIntern(iFilePathAndName string) (ePressespiegelWrapper domain.PressespiegelWrapper, eErr error) {

	//var meldungen []Meldung
	/*
		var amInPSBlock bool
		var amInMeldungBlock bool
		var amInArtikelBlock bool
		var currentPSElement string
		var currentArtikel domain.Artikel
	*/
	var (
		currentPSElement string
		amInMeldungBlock bool
		amInArtikelBlock bool
		currentMeldung   domain.Meldung
		currentArtikel   domain.Artikel
	)
	//var currentMeldung domain.Meldung

	psentity := domain.Pressespiegel{}
	psmeldungen := domain.MeldungListe{}
	psartikellist := domain.ArtikelListe{}
	ePressespiegelWrapper = domain.PressespiegelWrapper{}
	//currentMeldung = domain.Meldung{}

	xmlFile, err := os.Open(iFilePathAndName)
	if err != nil {
		//fmt.//println("Error opening XML file")
		return ePressespiegelWrapper, err
	}
	defer xmlFile.Close()

	var (
		token          xml.Token
		elementStarted bool
		str            string
	)

	decoder := xml.NewDecoder(xmlFile)
	//count := 0
	for {
		token, err = decoder.Token()
		if token == nil {
			break
		}

		switch Element := token.(type) {
		case xml.StartElement:
			str = ""
			currentPSElement = Element.Name.Local
			////println("currentPSElement:" + currentPSElement)
			elementStarted = true

			if currentPSElement == "MELDUNG" {
				currentMeldung = domain.Meldung{}
				amInMeldungBlock = true
			}
			if currentPSElement == "ARTIKEL" {
				currentArtikel = domain.Artikel{}
				amInArtikelBlock = true
			}

		case xml.CharData:
			if elementStarted {
				str = string([]byte(Element))
			}
		case xml.EndElement:
			////println("End currentPSElement:" + Element.Name.Local)
			if len(strings.TrimSpace(currentPSElement)) > 0 {
				mapToIntern(currentPSElement, str, &psentity, &currentMeldung, &currentArtikel)
			}
			////println("currentPSElement:" + currentPSElement)
			////println("Val:" + str)
			elementStarted = false
			if Element.Name.Local == "MELDUNG" && amInMeldungBlock {
				psmeldungen = append(psmeldungen, currentMeldung)
				currentMeldung = domain.Meldung{}
				amInMeldungBlock = false
			}
			if Element.Name.Local == "ARTIKEL" && amInArtikelBlock {
				////println("readToIntern:ArtAutor:" + currentArtikel.ArtAutor)
				psartikellist = append(psartikellist, currentArtikel)
				currentArtikel = domain.Artikel{}
				amInArtikelBlock = false
			}

		}

	}
	//fmt.//println(count)
	ePressespiegelWrapper = domain.PressespiegelWrapper{psentity, psmeldungen, psartikellist}

	return ePressespiegelWrapper, nil
}
