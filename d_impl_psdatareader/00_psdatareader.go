package d_impl_psdatareader

import (
	"os"
	domain "ps_meta/a_domain"
)

type DataReaderPort struct {
}

func (r DataReaderPort) ReadPSProviderData(
	iWorkDir string, iPSProviderDataFile string) (ePressespiegelWrapper domain.PressespiegelWrapper, eImportDataFound bool, eErrorMsg domain.ProcessingInfo, ok bool) {

	// Init
	ePressespiegelWrapper = domain.PressespiegelWrapper{}
	eImportDataFound = true
	eErrorMsg = domain.ProcessingInfo{}
	ok = true
	// Init

	lFileLocation := iWorkDir + string(os.PathSeparator) + iPSProviderDataFile
	if !ExistsFile(lFileLocation) {
		eImportDataFound = false
		return
	}

	ePressespiegelWrapper, lErr := readToIntern(lFileLocation)
	if lErr != nil {
		ok = false
		eErrorMsg.DetectingComponentName = "psdatareader.ReadPSProviderData"
		eErrorMsg.InfoDescription = lErr.Error()
	}

	return

}

var DataReader DataReaderPort
