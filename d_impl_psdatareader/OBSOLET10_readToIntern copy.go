package d_impl_psdatareader

import (
	"encoding/xml"
	"os"
	domain "ps_meta/a_domain"
	"strings"
)

//var currentMeldung domain.Meldung

func readToIntern2(iFilePathAndName string) (ePressespiegelWrapper domain.PressespiegelWrapper, eErr error) {

	//var meldungen []Meldung

	var amInPSBlock bool
	var amInMeldungBlock bool
	var amInArtikelBlock bool
	var currentPSElement string
	var currentArtikel domain.Artikel
	var currentMeldung domain.Meldung

	psentity := domain.Pressespiegel{}
	psmeldungen := domain.MeldungListe{}
	psartikellist := domain.ArtikelListe{}
	ePressespiegelWrapper = domain.PressespiegelWrapper{}
	currentMeldung = domain.Meldung{}

	xmlFile, err := os.Open(iFilePathAndName)
	if err != nil {
		//fmt.//println("Error opening XML file")
		return ePressespiegelWrapper, err
	}
	defer xmlFile.Close()

	var (
		t  xml.Token
		cd xml.CharData
		se xml.StartElement
		//pi xml.ProcInst
		ee             xml.EndElement
		isElementEnd   bool
		isElementStart bool
		isData         bool
		str            string
	)

	decoder := xml.NewDecoder(xmlFile)
	//count := 0
	for {
		t, err = decoder.Token()
		if t == nil {
			break
		}

		if cd, isData = t.(xml.CharData); isData {
			str = string([]byte(cd)) //
		}

		if se, isElementStart = t.(xml.StartElement); isElementStart {
			currentPSElement = se.Name.Local
			if currentPSElement == "PRESSESPIEGEL" {

				currentPS := new(domain.Pressespiegel)
				currentPS.PSExtID = ""
				amInPSBlock = true

			}
			if currentPSElement == "MELDUNG" {
				//currentMeldung =

				currentMeldung = domain.Meldung{}
				//fmt.//println(currentMeldung.MelID)
				currentMeldung.MelExtID = ""
				amInMeldungBlock = true

			}
			if currentPSElement == "ARTIKEL" {

				currentArtikel := new(domain.Artikel)
				currentArtikel.ExtID = ""
				currentArtikel.Autor = ""
				currentArtikel.Untertitel = ""
				currentArtikel.Inhalttext = ""
				currentArtikel.Autoabstract = ""

				amInArtikelBlock = true

			}

		}

		if ee, isElementEnd = t.(xml.EndElement); isElementEnd {
			if ee.Name.Local == "PRESSESPIEGEL" && amInPSBlock {

			}
			if ee.Name.Local == "MELDUNG" && amInMeldungBlock {
				////println(" currentMeldung:" + currentMeldung.MelExtID)
				psmeldungen = append(psmeldungen, currentMeldung)
				currentMeldung = domain.Meldung{}
				currentMeldung.MelExtID = ""
				amInMeldungBlock = false
			}
			if ee.Name.Local == "ARTIKEL" && amInArtikelBlock {
				psartikellist = append(psartikellist, currentArtikel)
				currentArtikel := domain.Artikel{}
				currentArtikel.ExtID = ""
				currentArtikel.Autor = ""
				currentArtikel.Untertitel = ""
				currentArtikel.Inhalttext = ""
				currentArtikel.Autoabstract = ""
				amInArtikelBlock = false
			}

		}

		////println("currentPSElement=" + currentPSElement)
		str = string([]byte(cd)) //
		////println("str=" + str)
		if len(strings.TrimSpace(currentPSElement)) > 0 && len(strings.TrimSpace(str)) > 0 {

			mapToIntern(currentPSElement, str, &psentity, &currentMeldung, &currentArtikel)

		}

	}
	//fmt.//println(count)
	ePressespiegelWrapper = domain.PressespiegelWrapper{psentity, psmeldungen, psartikellist}

	return ePressespiegelWrapper, nil
}
