package u_utils

import (
	"strings"
	"time"
)

func DateBeforeNumDays(iDays int) string {
	currentTime := time.Now()
	newtime := currentTime.AddDate(0, 0, -(iDays)) //30 day before
	return newtime.Format("20060102")
}

func DateBeforeOneMonth() string {
	return (DateBeforeNumDays(30))
}
func DateBeforeTwoMonth() string {
	return (DateBeforeNumDays(60))
}
func DateBeforeOneYear() string {
	return (DateBeforeNumDays(365))
}
func DateLessThan(iDateLeft, iDateRight string) bool {
	leftTime, _ := time.Parse("20060102", iDateLeft)
	rightTime, _ := time.Parse("20060102", iDateRight)
	if leftTime.Before(rightTime) {
		return true
	}
	return false
}

func GetNowDateAsString() string {
	currentDate := time.Now()
	datePostfix := currentDate.Format("20060102")
	return datePostfix
}
func GetNowTimeAsString() string {
	currentTime := time.Now()
	timeAsString := currentTime.Format("15:04:05")
	return timeAsString
}

func DateDD_MM_JJJJToYYYYMMDD(iDate string) string {
	lResultDate := ""
	s := strings.Split(iDate, ".")
	if len(s) == 3 {
		for i := 0; i < len(s); i++ {
			if len(s[i]) == 1 {
				s[i] = "0" + s[i]
			}
			if i == 2 {
				for len(s[i]) < 4 {
					s[i] = "0" + s[i]
				}
			}
		}
		lResultDate = s[2] + s[1] + s[0]

	}
	//println("lResultDate:" + lResultDate)
	return lResultDate
}
