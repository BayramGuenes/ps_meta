package u_utils

import (
	domain "ps_meta/a_domain"
	"regexp"
	"strings"
)

const teaserLength int = 267

func FormatArticle(iArticle domain.Artikel) (eResult domain.IF_psArticleTextInfo) {
	lResultRow := domain.IF_psArticleTextInfo{}
	lArtikel := iArticle
	////println("lArtikel.ArtExtID:" + lArtikel.ArtExtID + " Subtitle:" + lArtikel.ArtUntertitel)

	lResultRow.Id = lArtikel.ExtID
	lArtikel.QuelleName = encodeAklContentSpecialSigns(lArtikel.QuelleName)
	lResultRow.Source = lArtikel.QuelleName
	lResultRow.Date = lArtikel.ESD

	lArtikel.Titel = encodeAklContentSpecialSigns(lArtikel.Titel)
	lResultRow.Title = lArtikel.Titel
	lArtikel.SonstTitel = encodeAklContentSpecialSigns(lArtikel.SonstTitel)
	lResultRow.Othertitle = lArtikel.SonstTitel

	lArtikel.Untertitel = encodeAklContentSpecialSigns(lArtikel.Untertitel)
	lResultRow.Subtitle = lArtikel.Untertitel
	//lResultRow.Teaser=lArtikel.
	lResultRow.Pagenrtext = lArtikel.QuelleSeitennr

	lResultRow.Teaser = lArtikel.Inhalttext
	if len(lArtikel.Inhalttext) > teaserLength {
		lOffsetLastWord, llastWord := getLastWordComplete(lArtikel.Inhalttext)
		////println("lOffsetLastWort:" + strconv.Itoa(lOffsetLastWord) + " llastWord:" + llastWord)
		lResultRow.Teaser = lResultRow.Teaser[:lOffsetLastWord] + " " + llastWord + " ..."
	}
	lResultRow.Teaser = encodeAklContentSpecialSigns(lResultRow.Teaser)
	lResultRow.Text = lArtikel.Inhalttext
	eResult = lResultRow

	return eResult
}
func encodeAklContentSpecialSigns(iInput string) string {
	eResult := iInput
	eResult = strings.Replace(eResult, "\\", "\\\\\\", -1)
	var re = regexp.MustCompile(`\t`)
	eResult = re.ReplaceAllString(eResult, " ")
	eResult = strings.Replace(eResult, "&quot;", "\"", -1)

	//var reQuot = regexp.MustCompile(`"`)
	//eResult = reQuot.ReplaceAllString(eResult, `\"`)
	//eResult = strings.Replace(eResult, "\"", "\\\"", -1)
	return eResult
}
func getLastWordComplete(iText string) (eOffsetLastword int, eLastWord string) {
	checkOfs := teaserLength - 2
	startOfs := teaserLength - 2
	lastWord := ""
	doExit := false
	for checkOfs > -1 && !doExit {
		s := iText[checkOfs:(checkOfs + 1)]
		blank := strings.TrimSpace(s) == ""
		if !blank {
			lastWord = iText[checkOfs:(checkOfs+1)] + lastWord
			startOfs = checkOfs
			checkOfs--

		} else {
			doExit = true
		}
	}
	if len(lastWord) > 0 {
		checkOfs := teaserLength - 1
		doExit := false
		for checkOfs < len(iText) && !doExit {
			s := iText[checkOfs:(checkOfs + 1)]
			blank := strings.TrimSpace(s) == ""

			if !blank {
				lastWord = lastWord + iText[checkOfs:(checkOfs+1)]
				checkOfs++
			} else {
				doExit = true
			}
		}
	}
	eOffsetLastword = startOfs
	eLastWord = lastWord
	return eOffsetLastword, eLastWord
}
