package a_domain

type PSDataProviderPort interface {
	FetchDataFromProvider(iWorkDir string) (eFileNameSet []string, eErrorMsg ProcessingInfo, ok bool)
}

type PSProviderDataReaderPort interface {
	ReadPSProviderData(iWorkDir string, iPSProviderDataFile string) (ePressespiegelWrapper PressespiegelWrapper, eImportDataFound bool, eErrorMsg ProcessingInfo, ok bool)
}
