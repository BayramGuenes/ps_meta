package a_domain

import (
	"strings"
)

const (
	AuthUsrAdmin           = "USRADM"
	AuthSysAdmin           = "SYSADM"
	AuthSearchWithInactive = "INACTIVESEARCH"
	AuthSearchUnlimited    = "UNLMTDSEARCH"
	AuthSearchOneYear      = "ONEYEARSEARCH"
	AuthDataImport         = "DATAIMPORT"
)

type AuthRoleName string

const (
	RoleNameSuperUser    = "SuperUser"
	RoleNameSysAdm       = "SystemAdministrator"
	RoleNameUsrAdm       = "UserAdministrator"
	RoleNameDataImporter = "DataImporter"
	RoleNameSearchExpert = "SearchExpert"
	RoleNameSearcher     = "Searcher"
)

type AuthRole struct {
	Name    string
	AuthSet []string
}

// AuthClass ...                       //
type AuthClass struct {
	AuthDBPort AuthDataBasePort
	Logger     PSLogPort
}

func newAuthClassInstanceGetter(iDatabase AuthDataBasePort, iLogger PSLogPort) AuthClass {
	return AuthClass{AuthDBPort: iDatabase, Logger: iLogger}
}

var authStack []AuthRole

func InitAuthStack(iDatabase AuthDataBasePort, iLogger PSLogPort) {

	logTransactionLog := iLogger.SetStartTransaction("InitAuthStack")

	authStack = []AuthRole{}
	var authClass = newAuthClassInstanceGetter(iDatabase, iLogger)
	authRoleNames, err := authClass.AuthDBPort.GetAuthRoleNames()

	if err != nil {
		var processInfo ProcessingInfo
		processInfo.DetectingComponentName = "InitAuthStack:GetAuthRoleNames"
		processInfo.InfoDescription = err.Error()

		if iLogger != nil {
			logTransactionLog = iLogger.LogStep(logTransactionLog, "GET AUTH ROLE NAMES", "")
			logTransactionLog = iLogger.LogStepResult(logTransactionLog, "GET AUTH ROLE NAMES", "FAILED FRETCH FROM DATABASE", processInfo)
		} else {
			//println("GET AUTH ROLE NAMES", "FAILED FRETCH FROM DATABASE", processInfo.DetectingComponentName, " ", processInfo.InfoDescription)
		}
	}

	for _, v := range authRoleNames {
		var authStackLine AuthRole
		authStackLine.Name = v
		authStackLine.AuthSet, err = authClass.AuthDBPort.GetAuthSet(v)
		if err != nil {
			var processInfo ProcessingInfo
			processInfo.DetectingComponentName = "InitAuthStack:.GetAuthSet"
			processInfo.InfoDescription = err.Error()
			if iLogger != nil {
				logTransactionLog = iLogger.LogStep(logTransactionLog, "GET AUTH OBJECTS OF AUTHROLES", "")
				logTransactionLog = iLogger.LogStepResult(logTransactionLog, "GET AUTH OBJECTS OF AUTHROLES", "FAILED FRETCH FROM DATABASE", processInfo)
			} else {
				//println("GET AUTH OBJECTS OF AUTHROLES", "FAILED FRETCH FROM DATABASE", processInfo.DetectingComponentName, " ", processInfo.InfoDescription)
			}
		}

		authStack = append(authStack, authStackLine)
	}
	iLogger.SetEndTransaction(logTransactionLog)

}

//PSDataBase
func CheckAuthority(iAuthObject string, iAuthRoleNames []string, iDatabase AuthDataBasePort, iLogger PSLogPort) (eAuthorised bool) {
	result := false
	InitAuthStack(iDatabase, iLogger)

	for i := 0; i < len(iAuthRoleNames); i++ {

		for j := 0; j < len(authStack); j++ {

			if authStack[j].Name == iAuthRoleNames[i] {
				for k := 0; k < len(authStack[j].AuthSet); k++ {
					if strings.Contains(authStack[j].AuthSet[k], iAuthObject) {
						result = true
						break
					}
				}

			}
		}

	}
	//println("CheckAuthority:" + strconv.FormatBool(result))
	return result
}
