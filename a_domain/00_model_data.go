package a_domain

import "strconv"

const ThisApplLogicalName = "ps_meta"

type UserToken struct {
	Loginname     string
	Rfa           string
	Token         string
	Validdatefrom string
	Validdateto   string
}

type User struct {
	Loginname       string
	Rfa             string
	Name            string
	Pwd             string
	Email           string
	IsDeactivated   bool
	DeactivatedBy   string
	DeactivatedDate string
	DeactivatedTime string
	ReactivatedBy   string
	ReactivatedDate string
	ReactivatedTime string
}

type UserRoles []string
type UserAuthKeys []string

type CalendarDate string          //yyyy-mm-dd
type CalendarDates []CalendarDate //yyyy-mm-dd

type Calendar struct {
	AvailableDates CalendarDates
	ActivatedDates CalendarDates
}

// Pressespiegel intern
type Pressespiegel struct {
	PSImageURI              string
	PSArt                   string
	PSImageTyp              string
	PSAusgabenr             string
	PSExtID                 string
	PSESD                   string
	PSImageTypVersion       string
	PSRFA                   string
	IsActivated             bool
	ActivatedByUser         string
	ActivatedDate           string
	ActivatedTime           string
	LogicalNameImportsystem string
}

// Meldung intern
type Meldung struct {
	MelExtID      string
	MelPSRubrik   string
	MelInhalttext string
	MelPSPosnr    string
}

//Artikel intern
type Artikel struct {
	MelExtID        string
	ExtID           string
	PSPosnr         string
	PSRubrik        string
	ESD             string
	Print_Online    string
	AnzahlFotos     int
	Ausgabe         string
	Titel           string
	Seite1Titel     string
	Untertitel      string
	SonstTitel      string
	Inhalttext      string
	Autoabstract    string
	Schlagwort      string
	Autor           string
	Bemerkung       string
	QuelleKuerzel   string
	QuelleName      string
	QuellePubnr     string
	QuelleSeitennr  string
	ImageURI        string
	ImageTyp        string
	ImageTypVersion string
}

// MeldungListe intern
type MeldungListe []Meldung

//ArtikelListe intern
type ArtikelListe []Artikel

//PressespiegelWrapper intern
type PressespiegelWrapper struct {
	PSHeader   Pressespiegel
	MeldungSet MeldungListe
	ArtikelSet ArtikelListe
}

type ProcessingInfo struct {
	DetectingComponentName string
	InfoDescription        string
}

type TransactionLogBufferType struct {
	TransactionKey string
	Result         string
	LogStringArray []string
}

type IF_psArticleTextInfo struct {
	Id            string
	Source        string
	Date          string
	Title         string
	Othertitle    string
	Subtitle      string
	Teaser        string
	IsDictated    string
	Pagenrtext    string
	Text          string
	IsLastArticle bool
}

type PSLogPort interface {
	SetStartTransaction(iTransactionName string) (eTransactionLog TransactionLogBufferType)
	SetEndTransaction(iTransactionLog TransactionLogBufferType)
	LogStep(iTransactionLog TransactionLogBufferType, iStepname string, iStepParam string) (eTransactionLog TransactionLogBufferType)
	LogStepResult(iTransactionLog TransactionLogBufferType, iStepname, iResult string, iDetail ProcessingInfo) (eTransactionLog TransactionLogBufferType)
	SetLocationLogs(iLocationLogs string)
}

/* ========================================================== */
/*                                                            */
/* ========================================================== */

func (usr User) IsActive() bool {
	if !usr.IsDeactivated {
		return true
	} else {
		return false
	}
}

/* ========================================================== */
/*                                                            */
/* ========================================================== */

func (ps Pressespiegel) ToString() string {
	psAsStr := "PSImageURI=" + ps.PSImageURI + "\n" +
		"PSArt=" + ps.PSArt + "\n" +
		"PSImageTyp=" + ps.PSImageTyp + "\n" +
		"PSAusgabenr=" + ps.PSAusgabenr + "\n" +
		"PSId=" + ps.PSExtID + "\n" +
		"PSErscheinungsdatum=" + ps.PSESD + "\n" +
		"PSImageTypVersion=" + ps.PSImageTypVersion + "\n" +
		"PSRFA=" + ps.PSRFA + "\n"

	return psAsStr
}

/* ========================================================== */
/*                                                            */
/* ========================================================== */

func (mellist MeldungListe) ToString() string {
	mellistAsStr := ""
	for i := 0; i < len(mellist); i++ {
		j := i + 1
		melAsStr := "********************************" + strconv.Itoa(j) + ".MELDUNG: **********************************" + "\n" +
			"MelExtID=" + mellist[i].MelExtID + "\n" +
			"PSRubrik=" + mellist[i].MelPSRubrik + "\n" +
			"Inhalttext=" + mellist[i].MelInhalttext + "\n" +
			"PosNr=" + mellist[i].MelPSPosnr + "\n"

		mellistAsStr += melAsStr
	}

	return mellistAsStr
}

/* ========================================================== */
/*                                                            */
/* ========================================================== */

func (artikellist ArtikelListe) ToString() string {
	artikellistAsStr := ""
	for i := 0; i < len(artikellist); i++ {
		j := i + 1
		artAsStr := "================================" + strconv.Itoa(j) + ".Artikel: =======================================" + "\n" +
			"ArtExtId=" + artikellist[i].ExtID + "\n" +
			"IMAGE_TYP=" + artikellist[i].ImageTyp + "\n" +
			"IMAGE_TYPVERSION=" + artikellist[i].ImageTypVersion + "\n" +
			"ArtSchlagwort=" + artikellist[i].Schlagwort + "\n" +
			"ArtAnzahlFotos=" + strconv.Itoa(artikellist[i].AnzahlFotos) + "\n" +
			"ArtMeldungID=" + artikellist[i].MelExtID + "\n" +
			"ArtUntertitel=" + artikellist[i].Untertitel + "\n" +
			"ArtSonstTitel=" + artikellist[i].SonstTitel + "\n" +
			"ArtQuelleName=" + artikellist[i].QuelleName + "\n" +
			"ArtErscheinungsdatum=" + artikellist[i].ESD + "\n" +
			"ArtPSRubrik=" + artikellist[i].PSRubrik + "\n" +
			"ArtTitel=" + artikellist[i].Titel + "\n" +
			"ArtSeite1Titel=" + artikellist[i].Seite1Titel + "\n" +
			"ArtInhalttext=" + artikellist[i].Inhalttext + "\n" +
			"ArtAutoabstract=" + artikellist[i].Autoabstract + "\n" +
			"ArtQuellePubnr=" + artikellist[i].QuellePubnr + "\n" +
			"ArtQuelleSeitennr=" + artikellist[i].QuelleSeitennr + "\n" +
			"ArtAusgabe=" + artikellist[i].Ausgabe + "\n" +
			"ArtBemerkung=" + artikellist[i].Bemerkung + "\n" +
			"ArtQuelleKuerzel=" + artikellist[i].QuelleKuerzel + "\n" +
			"ArtPrintOnline=" + artikellist[i].Print_Online + "\n" +
			"ArtImageURI=" + artikellist[i].ImageURI + "\n" +
			"ArtAutor=" + artikellist[i].Autor + "\n" +
			"ArtPSPosnr=" + artikellist[i].PSPosnr + "\n"

		artikellistAsStr += artAsStr
	}

	return artikellistAsStr
}
