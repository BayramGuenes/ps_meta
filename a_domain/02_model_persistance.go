package a_domain

import (
	"ps_meta/s_security"
	_ "ps_meta/s_security"
	"time"
)

type AuthDataBasePort interface {
	GetAuthSet(iAuthRoleName string) (eAuthObjects []string, err error)
	GetAuthRoleNames() (eAuthRoles []string, err error)
	GetUserAuthRoles(iLoginname, iRfa string) (eAuthRoles []string, err error)
	CreateUserAuthRole(iLoginname, iRfa, iAuthRoleName string) (err error)
	RemoveUserAuthRole(iLoginname, iRfa, iAuthRoleName string) (err error)
}

type UserDataBasePort interface {
	CreateUser(iLoginname string, iRfa string, iName string, iPwd string, iEmail string, iCreatedbyusr string, iCreatedbycomp string) (err error)
	SetUserToken(iLoginname string, iRfa string, iToken string, iValidFrom, iValidTo string) (err error)
	SetUserTokenViaToken(iToken string, iValidFrom, iValidTo string) (err error)
	GetUsr(iLoginname string, iRfa string) (eUser User, err error)
	GetUsrToken(iLoginname string, iRfa string) (eUserToken UserToken, err error)
	GetUsrRoles(iLoginname string, iRfa string) (eUserRoles UserRoles, err error)
	GetUsrViaToken(iToken string) (eUser User, err error)
	GetUsrViaEmail(iEmail string) (eUser User, err error)
	UpdateUser(iUser User, iUpdatebyusr string, iUpdatedbycomp string) (err error)
}

type PSDataBasePort interface {
	ExistsPS(iPSErscheinungsdatum string) (eEXTId string, eFound bool, err error)
	CreatePS(iPressespiegelWrapper PressespiegelWrapper, iLogicalNameImportsystem string, iImportusr string) (err error)
	SetPSImageLocation(iPSExtID string, iLocation string) (ok bool, err error)
	DeletePS(iPSExtID string) (err error)
	SetPSPublishmark(iPSErscheinungsdatum string, iUname string) (err error)
	GetPS(iPSErscheinungsdatum string, iAlsoInactive bool) (ePSHeader Pressespiegel, err error)
	GetPSESDsFromTo(iPSFrom string, iPSTo string, iAlsoInactive bool) (eListESD []string, err error)
	GetDateBeforePS(iAlsoInactive bool, iDateInRelationTo string) (ePSDate string, err error)
	GetDateNextPS(iAlsoInactive bool, iDateInRelationTo string) (ePSDate string, err error)
	GetDateLastPS(iAlsoInactive bool) (ePSDate string, err error)
}

type MeldungDataBasePort interface {
	GetMeldungCollection(iPSExtID string) (eCollection []Meldung, err error)
	GetMeldung(iPSExtID string, iMelExtID string) (eMeldung Meldung, err error)
	DeleteMeldung(iPSExtID string, iMelExtID string) (err error)
	CreateMeldung(iPSExtID string, iMeldung Meldung) (err error)
}

type ArticleDataBasePort interface {
	GetArticleCollection(iPSExtID string) (eCollection []Artikel, err error)
	GetArticle(iPSExtID string, iArtExtID string) (eArtikel Artikel, err error)
	GetArtViaIDAndESD(iArtExtID string, iArtESD string) (eArtikel Artikel, err error)
	DeleteArticle(iPSExtID string, iArtExtID string) (err error)
	CreateArticle(iPSExtID string, iArtikel Artikel) (err error)
	SetArticleImageLocation(iPSExtID string, iArticleEXTID string, iLocation string) (ok bool, err error)
}

/*
type PSImageArchivPort interface {
	PersistPSImage(iWorkdir string, iImageName string) (eImageLocation string, eErrorMsg ProcessingInfo, ok bool)
	PersistArticleImage(iWorkdir string, iImageName string) (eImageLocation string, eErrorMsg ProcessingInfo, ok bool)
}
type PSImageArchiv struct {
	ImageArchivPort PSImageArchivPort
}
*/

type PSDataBase struct {
	AuthDBPort    AuthDataBasePort
	UsrDBPort     UserDataBasePort
	PSDBPort      PSDataBasePort
	MeldungDBPort MeldungDataBasePort
	ArticleDBPort ArticleDataBasePort
	Init          string
}

/* ========================================================== */
/*                                                            */
/* ========================================================== */

func (d PSDataBase) PersistPS(iPressespiegelWrapper PressespiegelWrapper, iLogicalNameImportsystem string, iImportusr string) (eErrorMsg ProcessingInfo, ok bool) {
	ok = true
	eErrorMsg = ProcessingInfo{}
	////println("(d PSDataBase) PersistPS() called")
	lMelColl := []Meldung{}
	lArtColl := []Artikel{}
	lEXTID, lFound, err := d.PSDBPort.ExistsPS(iPressespiegelWrapper.PSHeader.PSESD)
	if !lFound {
		lEXTID = iPressespiegelWrapper.PSHeader.PSExtID
	}
	if lFound {
		lMelColl, err = d.MeldungDBPort.GetMeldungCollection(lEXTID)
		if err == nil {
			err = d.PSDBPort.DeletePS(lEXTID)
		}
		if err == nil {
			for i := 0; i < len(lMelColl); i++ {
				err = d.MeldungDBPort.DeleteMeldung(lEXTID, lMelColl[i].MelExtID)
				if err != nil {
					eErrorMsg.DetectingComponentName = "PersistPS: PSDBPort.DeleteMeldung()"
					eErrorMsg.InfoDescription = err.Error()
					ok = false
					return
				}
			}
		}

		if err == nil {
			lArtColl, err = d.ArticleDBPort.GetArticleCollection(lEXTID)

			if err == nil {
				for i := 0; i < len(lArtColl); i++ {
					err = d.ArticleDBPort.DeleteArticle(lEXTID, lArtColl[i].ExtID)
					if err != nil {
						eErrorMsg.DetectingComponentName = "PersistPS: PSDBPort.DeleteArticle()"
						eErrorMsg.InfoDescription = err.Error()
						ok = false
						return
					}
				}
			}
		}

	}
	if err != nil {
		eErrorMsg.DetectingComponentName = "PersistPS: PSDBPort.DeletePS()"
		eErrorMsg.InfoDescription = err.Error()
		ok = false
		return
	}

	err = d.PSDBPort.CreatePS(iPressespiegelWrapper, iLogicalNameImportsystem, iImportusr)
	if err != nil {
		eErrorMsg.DetectingComponentName = "PersistPS: PSDBPort.CreatePS()"
		eErrorMsg.InfoDescription = err.Error()
		ok = false
		return
	}

	for i := 0; i < len(iPressespiegelWrapper.MeldungSet); i++ {
		////println("iPressespiegelWrapper.MeldungSet" + iPressespiegelWrapper.MeldungSet[i].MelExtID)
		err = d.MeldungDBPort.CreateMeldung(lEXTID, iPressespiegelWrapper.MeldungSet[i])
		if err != nil {
			eErrorMsg.DetectingComponentName = "PersistPS: MeldungDBPort.CreateMeldung()"
			eErrorMsg.InfoDescription = err.Error()
			ok = false
			return
		}
	}

	for i := 0; i < len(iPressespiegelWrapper.ArtikelSet); i++ {
		err = d.ArticleDBPort.CreateArticle(lEXTID, iPressespiegelWrapper.ArtikelSet[i])
		if err != nil {
			eErrorMsg.DetectingComponentName = "PersistPS:ArticleDBPort.CreateArticle()"
			eErrorMsg.InfoDescription = err.Error()
			ok = false
			return
		}
	}

	return
}

/* ========================================================== */
/*                                                            */
/* ========================================================== */

func (u PSDataBase) GetUsrViaToken(iToken string) (eUser User, err error) {
	eUser, err = u.UsrDBPort.GetUsrViaToken(iToken)
	return
}

func (u PSDataBase) DeactivateUser(iLoginnamefromUser, iRfa, iTriggeredbyusr string, iTriggeredbycomp string) (err error) {
	lUser, err := u.UsrDBPort.GetUsr(iLoginnamefromUser, iRfa)
	if err != nil {
		return
	}
	lUser.IsDeactivated = true
	lUser.DeactivatedBy = iTriggeredbyusr
	lUser.DeactivatedDate = time.Now().Format("2006-01-02")
	lUser.DeactivatedTime = time.Now().Format("15:04:05")

	err = u.UsrDBPort.UpdateUser(lUser, iTriggeredbyusr, iTriggeredbycomp)
	return err
}

func (u PSDataBase) ActivateUser(iLoginnamefromUser, iRfa string, iTriggeredbyusr string, iTriggeredbycomp string) (err error) {
	lUser, err := u.UsrDBPort.GetUsr(iLoginnamefromUser, iRfa)
	if err != nil {
		return
	}
	lUser.IsDeactivated = false
	lUser.ReactivatedBy = iTriggeredbyusr
	lUser.ReactivatedDate = time.Now().Format("2006-01-02")
	lUser.ReactivatedTime = time.Now().Format("15:04:05")

	err = u.UsrDBPort.UpdateUser(lUser, iTriggeredbyusr, iTriggeredbycomp)
	return err
}

func (u PSDataBase) AssignExtendedSearchRights(iLoginname, iRfa string) (err error) {
	lUser, err := u.UsrDBPort.GetUsr(iLoginname, iRfa)
	//lUser.HasExtendedSearchRights = true
	err = u.UsrDBPort.UpdateUser(lUser, "admin", "admincomp")
	return err
}

func (u PSDataBase) AssignExpertRights(iLoginname, iRfa string) (err error) {
	lUser, err := u.UsrDBPort.GetUsr(iLoginname, iRfa)
	//lUser.HasExpertRights = true
	err = u.UsrDBPort.UpdateUser(lUser, "admin", "admincomp")
	return err
}

func (u PSDataBase) SubductExtendedSearchRights(iLoginname, iRfa string) (err error) {
	lUser, err := u.UsrDBPort.GetUsr(iLoginname, iRfa)
	//lUser.HasExtendedSearchRights = false
	err = u.UsrDBPort.UpdateUser(lUser, "admin", "admincomp")
	return err
}

func (u PSDataBase) SubductExpertRights(iLoginname, iRfa string) (err error) {
	lUser, err := u.UsrDBPort.GetUsr(iLoginname, iRfa)
	//lUser.HasExpertRights = false
	err = u.UsrDBPort.UpdateUser(lUser, "admin", "admincomp")
	return err
}

/* ========================================================== */
/*                                                            */
/* ========================================================== */

func (u PSDataBase) GetUsrToken(iLoginname string, iRfa string) (eUsrToken UserToken, eSavedUserToken bool, err error) {
	eSavedUserToken = false
	eUsrToken, err = u.UsrDBPort.GetUsrToken(iLoginname, iRfa)
	if err != nil {
		return UserToken{}, false, err
	}
	if len(eUsrToken.Loginname) == 0 {
		eUsrToken.Loginname = iLoginname
		eUsrToken.Rfa = iRfa
		eUsrToken.Token = s_security.CreateToken(iLoginname, iRfa)
	} else {
		eSavedUserToken = true
	}
	return eUsrToken, eSavedUserToken, nil
}
