package a_domain

type ArtikelImage struct {
	ArticleEXTID     string
	ArticleImageName string
	ArticleMelEXTID  string
}
type ArtikelImageList []ArtikelImage

const (
	repoStatImageNotFound     = "ImageNotFound"
	repoStatTransferOK        = "transferOK"
	repoStatTransferFAILED    = "transferFAILED"
	repoStatNolongerAvailable = "nolongerAvailable"
)

type EntityImageDescriptor struct {
	EIDID                      int
	Entityextid                string
	Entitytype                 string //ps, article
	RepoPath                   string
	ImageRepoStatus            string "ImageNotFound, transferOK, transferFAILED, nolongerAvailable"
	DateOfRepoRegistration     string
	TimeOfRepoRegistration     string
	RepoStatusDetails          string
	MovedToHistoryArchiveLocat string
	DateOfMoveToHistoryArchiv  string
	TimeOfMoveToHistoryArchiv  string
	DateOfRestore              string
	TimeOfRestore              string
	Psesd               string
	Melextid            string
}
type ListEntityImageDescriptor []EntityImageDescriptor

type ArtikelSelection struct {
	ArticleEXTID string
}

type DetailsTransferOfEntity struct {
	EntityExtid                 string `json:"entityextid"`
	EntityType                  string `json:"entitytype"`
	ImageRepoStatus             string `json:"imagerepostatus"`
	EntityTransferResultDetails string `json:"detailsfailedorwarning"`
}
type ListEntityTransferResults []DetailsTransferOfEntity

type ImageTransferResult struct {
	TransferOK             bool
	TransferWarningErrorText   string
	Details  ListEntityTransferResults
}

type TypMeldungslisteAsStruct struct {
	  MeldungenExtIds []string
}

type ImageCancelTransferResult struct {
	CancelOK             bool
	Details string
	NotRemovedFilesList []string
	NotUpdatedRepoDBEntries []string
}

type PSImageArchivePort interface {
	TransferImagesToImageRepProxy(iWorkdir string, iPSImageName string, iPSESD string, iPSExtID string, iArtikelImageList ArtikelImageList) (eTransferResult ImageTransferResult)
	BuildMeldungImageToImageRepProxy(iPSESD string, iMelExtID string) (eTransferResult ImageTransferResult)
	BuildMeldungenImagesToImageRepProxy(iPSESD string, iMeldungenExtIds []string) (eTransferResult ImageTransferResult)
	CancelTransferedImagesToRepProxy(iPSESD string) (eTransferCancelResult ImageCancelTransferResult)
	GetEntityImageLocationProxy(iEntityextid string, iEntititytype string) (eImageRepoStatus, eRepoPath string, err error, ok bool)
	GetPSImageLocationsProxy(iPSESD string) (eImageRepoInfo ListEntityImageDescriptor, err error, ok bool)
	GetMeldungImageLocationProxy(iPSESD, iMelExtID string) (eImageRepoInfo ListEntityImageDescriptor, err error, ok bool)
	GetCollectionImageLocationProxy(iCollectionID string) (eImageRepoInfo ListEntityImageDescriptor, err error, ok bool)
	GetTempCollectionImageLocationProxy(iTempCollection []ArtikelSelection) (eImageRepoInfo ListEntityImageDescriptor, err error, ok bool)
}
