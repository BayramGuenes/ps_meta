package b_usecases

import (
	"os"
	bootstrap "ps_meta/z_bootstrap"

	"strings"
	"time"
	//"strconv"
)




func IsInDirectory(iWorkDir, iFilename string) bool {
	return ExistsFile(iWorkDir + string(os.PathSeparator) + iFilename)
}

func GetDoneDirectory(iFatherDonePath string) (ePath string, err error) {
	exists, currentDayPath, err := ExistsDoneForCurrentDay(iFatherDonePath)
	if !exists {
		err = createPath(currentDayPath)
	}
	return currentDayPath, err
}

func ExistsDoneForCurrentDay(iFatherDonePath string) (eExists bool, ePath string, err error) {
	currentDate := time.Now()
	datePostfix := currentDate.Format("20060102")
	ePath = iFatherDonePath + string(os.PathSeparator) + datePostfix
	eExists, err = ExistsPath(ePath)
	return

}

func createPath(iPath string) (err error) {
	os.Mkdir(iPath, 0777)
	return nil
}
func moveFile(iDirSource, iDirTarget, iFilename string, iESD string) (err error) {
	////println("moveFile called:"+iDirSource, iDirTarget)

	if strings.Contains(iFilename, bootstrap.ApplConf.QueryXMLFileName) {
		return os.Rename(iDirSource+string(os.PathSeparator)+iFilename,
			iDirTarget+string(os.PathSeparator)+iESD+iFilename)
	} else {
		return os.Rename(iDirSource+string(os.PathSeparator)+iFilename,
			iDirTarget+string(os.PathSeparator)+iFilename)
	}
}

func ExistsFile(name string) bool {
	if _, err := os.Stat(name); err != nil {
		if os.IsNotExist(err) {
			return false
		}
	}
	return true
}

// exists returns whether the given file or directory exists
func ExistsPath(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return true, err
}
