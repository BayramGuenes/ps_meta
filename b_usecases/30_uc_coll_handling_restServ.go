package b_usecases

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	syscommanager "ps_meta/h_service_communication_manager"
	boot "ps_meta/z_bootstrap"
)

func GinCallGetActiveCollRestService(iLoginname string) (eResult CollectionGetActiveResult) {

	eResult = CollectionGetActiveResult{}
	lPortService := getPortPSCollectService()
	//println("GinCallGetActiveCollRestService called")
	serviceadr := boot.ThisCommunnicationProtokoll + "://" + boot.ServiceLocationIDPSCollect + ":" + lPortService + "/ps_collect/COLLECTGETACTIVE/USR/" + iLoginname

	//println(lServiceUrl)
	resp, err := http.Get(serviceadr) //<--
	if err != nil {
		eResult.ReceiveOK = false
		eResult.ReceiveWarningErrorText = "Technical error: http.Get failed:" + err.Error()
		return
	}
	////println(lServiceUrl)

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		eResult.ReceiveWarningErrorText = "Technical error: ioutil.ReadAll(resp.Body) failed." + err.Error()
		eResult.ReceiveOK = false
		return
	}

	var respStruct CollectionGetActiveResult
	if err = json.Unmarshal(body, &respStruct); err != nil {
		eResult.ReceiveWarningErrorText = "Technical error: json.Unmarshal(body, &respStruct)) failed." + err.Error()
		eResult.ReceiveOK = false
		return
	}
	eResult = respStruct

	return

}

func GinCallCollOrderRestService(iOrder CollectionOrder) (eResult CollectionObjOrderTransferResult) {

	eResult = CollectionObjOrderTransferResult{}
	lPortService := getPortPSCollectService()
	serviceadr := boot.ThisCommunnicationProtokoll + "://" + boot.ServiceLocationIDPSCollect + ":" + lPortService + "/ps_collect/COLLECTORDER"
	bytesJson, err := json.Marshal(iOrder)
	if err != nil {
		eResult.TransferOK = false
		eResult.TransferWarningErrorText = "json.Marshal failed:" + err.Error()
	}
	resp, err := http.Post(serviceadr, "application/json", bytes.NewBuffer(bytesJson))
	//resp, err := http.Get(serviceadr)

	if err != nil {
		eResult.TransferWarningErrorText = "Technical error: http.Post failed." + err.Error()
		eResult.TransferOK = false
		return

	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		eResult.TransferWarningErrorText = "Technical error: ioutil.ReadAll(resp.Body) failed." + err.Error()
		eResult.TransferOK = false
		return
	}
	var respStruct CollectionObjOrderTransferResult
	if err = json.Unmarshal(body, &respStruct); err != nil {
		eResult.TransferWarningErrorText = "Technical error: json.Unmarshal(body, &respStruct) failed." + err.Error()
		eResult.TransferOK = false
		return
	}
	eResult = respStruct
	////println("callServiceIngestPSData':eTransferResult" + eTransferResult.TransferWarningErrorText)

	return

}
func GinCallGetItemListRestService(iCollection CollectionObject) (eResult CollectionGetItemListResult) {

	eResult = CollectionGetItemListResult{}
	lPortService := getPortPSCollectService()
	serviceadr := boot.ThisCommunnicationProtokoll + "://" + boot.ServiceLocationIDPSCollect + ":" + lPortService + "/ps_collect/COLLECTITEMLIST/"
	bytesJson, err := json.Marshal(iCollection)
	if err != nil {
		eResult.ReceiveOK = false
		eResult.ReceiveWarningErrorText = "json.Marshal failed:" + err.Error()
	}
	resp, err := http.Post(serviceadr, "application/json", bytes.NewBuffer(bytesJson))
	//resp, err := http.Get(serviceadr)

	if err != nil {
		eResult.ReceiveWarningErrorText = "Technical error: http.Post failed." + err.Error()
		eResult.ReceiveOK = false
		return

	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		eResult.ReceiveWarningErrorText = "Technical error: ioutil.ReadAll(resp.Body) failed." + err.Error()
		eResult.ReceiveOK = false
		return
	}
	var respStruct CollectionGetItemListResult
	if err = json.Unmarshal(body, &respStruct); err != nil {
		eResult.ReceiveWarningErrorText = "Technical error: json.Unmarshal(body, &respStruct)) failed." + err.Error()
		eResult.ReceiveOK = false
		return
	}
	eResult = respStruct

	return

}

func GinCallCollItemOrderRestService(iOrder CollectionItemOrder) (eResult CollectionItemOrderTransferResult) {
	lPortService := getPortPSCollectService()
	serviceadr := boot.ThisCommunnicationProtokoll + "://" + boot.ServiceLocationIDPSCollect + ":" + lPortService + "/ps_collect/COLLECTITEMORDER/"

	eResult = CollectionItemOrderTransferResult{}
	bytesJson, err := json.Marshal(iOrder)
	if err != nil {
		eResult.TransferOK = false
		eResult.TransferWarningErrorText = "json.Marshal failed:" + err.Error()
	}
	resp, err := http.Post(serviceadr, "application/json", bytes.NewBuffer(bytesJson))
	//resp, err := http.Get(serviceadr)

	if err != nil {
		eResult.TransferWarningErrorText = "Technical error: http.Post failed." + err.Error()
		eResult.TransferOK = false
		return

	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		eResult.TransferWarningErrorText = "Technical error: ioutil.ReadAll(resp.Body) failed." + err.Error()
		eResult.TransferOK = false
		return
	}
	var respStruct CollectionItemOrderTransferResult
	if err = json.Unmarshal(body, &respStruct); err != nil {
		eResult.TransferWarningErrorText = "Technical error: json.Unmarshal(body, &respStruct)) failed." + err.Error()
		eResult.TransferOK = false
		return
	}
	eResult = respStruct
	////println("callServiceIngestPSData':eTransferResult" + eTransferResult.TransferWarningErrorText)

	return
}
func GinCallCollItemListOrderRestSer(iOrder CollectionItemListOrder) (eResult CollectionItemListOrderTransferResult) {
	lPortService := getPortPSCollectService()
	serviceadr := boot.ThisCommunnicationProtokoll + "://" + boot.ServiceLocationIDPSCollect + ":" + lPortService + "/ps_collect/COLLECTITEMLISTORDER/"

	eResult = CollectionItemListOrderTransferResult{}
	bytesJson, err := json.Marshal(iOrder)
	if err != nil {
		eResult.TransferOK = false
		eResult.TransferWarningErrorText = "json.Marshal failed:" + err.Error()
	}
	resp, err := http.Post(serviceadr, "application/json", bytes.NewBuffer(bytesJson))
	//resp, err := http.Get(serviceadr)

	if err != nil {
		eResult.TransferWarningErrorText = "Technical error: http.Post failed." + err.Error()
		eResult.TransferOK = false
		return

	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		eResult.TransferWarningErrorText = "Technical error: ioutil.ReadAll(resp.Body) failed." + err.Error()
		eResult.TransferOK = false
		return
	}
	var respStruct CollectionItemListOrderTransferResult
	if err = json.Unmarshal(body, &respStruct); err != nil {
		eResult.TransferWarningErrorText = "Technical error: json.Unmarshal(body, &respStruct)) failed." + err.Error()
		eResult.TransferOK = false
		return
	}
	eResult = respStruct
	////println("callServiceIngestPSData':eTransferResult" + eTransferResult.TransferWarningErrorText)

	return
}
func getPortPSCollectService() (ePSCollectPort string) {
	if boot.ApplConf.UsePSSysregAsRegistrator {
		lResultGetPortMS := syscommanager.ApplServiceCom.GetPortMicroservice(syscommanager.MicroServiceNamePsCollect)
		ePSCollectPort = lResultGetPortMS.Port
	} else {
		ePSCollectPort = boot.PortPSCollect
	}
	return
}
