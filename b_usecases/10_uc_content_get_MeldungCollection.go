package b_usecases

import (
	"ps_meta/a_domain"
)

// MeldungClass ...                       //
type MeldungCollClass struct {
	PSDataBase a_domain.PSDataBase
	Logger     a_domain.PSLogPort
}

func NewMeldungCollGetter(iDatabase a_domain.PSDataBase, iLogger a_domain.PSLogPort) MeldungCollClass {
	logger := iLogger //logger.GetLogger()
	return MeldungCollClass{PSDataBase: iDatabase, Logger: logger}
}

func (m MeldungCollClass) GetMeldungCollection(iToken string, iKindOfAuth string, iErscheinungsdatum string) (ePSHeader a_domain.Pressespiegel, eCollection []a_domain.Meldung, err error) {

	logTransactionBuffer := m.Logger.SetStartTransaction("GetMeldungCollectio")

	lStepname := "CALL  GetMeldungCollection: GetUsrInfoFromToken [m MeldungCollClass - b_usecases]"
	logTransactionBuffer = m.Logger.LogStep(logTransactionBuffer, lStepname, "iErscheinungsdatum:"+iErscheinungsdatum)

	lWithInactiv := false
	if len(iToken) > 0 {

		_, lUserRoles, err := GetUsrInfoFromToken(iToken, iKindOfAuth, m.PSDataBase.UsrDBPort)
		if err == nil {
			lWithInactiv = a_domain.CheckAuthority(a_domain.AuthSearchWithInactive, lUserRoles, m.PSDataBase.AuthDBPort, m.Logger)
		}
	}
	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "GetUsrInfoFromToken ", err, m.Logger)
	if err == nil {
		ePSHeader, err = m.PSDataBase.PSDBPort.GetPS(iErscheinungsdatum, lWithInactiv)
		////println("lPSHeader" + lPSHeader.PSExtID)
		if err == nil {
			if len(ePSHeader.PSExtID) > 0 {
				eCollection, err = m.PSDataBase.MeldungDBPort.GetMeldungCollection(ePSHeader.PSExtID)
			} else {
				eCollection = []a_domain.Meldung{}
				err = nil // errors.New("Es gibt kein Pressespiegel zum Erscheinungsdatum " + iErscheinungsdatum)
			}
		}
		////println("ePSHeader =" + ePSHeader.PSErscheinungsdatum)
		logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "GetMeldungCollection", err, m.Logger)
		m.Logger.SetEndTransaction(logTransactionBuffer)
	}
	return
}
