package b_usecases

import "ps_meta/a_domain"

const (
	coStepNameCheckingUserData        = "Checking User Data-Rights"
	coStepNameFetchingData            = "Fetching Provider Data"
	coStepNameReadingMetadataToBuffer = "Reading Metadatafile to Buffer"
	coStepNamePersistingMetadata      = "Persisting  Metadata"
	coStepNameCreateIngestDeleteOrder = "Create Ingest-Delete Order"
	coStepNameCreateIngestCreateOrder = "Create Ingest-Create Order"
	coStepNameExecIngest              = "Exec Ingesting "

	coStepNameCancellingTransferImagesBef  = "Cancelling previous Transferring Images To Rep"
	coStepNameTransferringImagesToImageRep = "Transferring ImagesTo ImageRepository"
	//coStepNameGeneratOrderArchPSImage        = "Generate Order Archive PS Image"
	//coStepNameGeneratOrderArchArtImage       = "Generate Order Archive Article Image"
	//coStepNameRunOrderArchiveImages          = "Run Order Archive Images"

	//coStepNamePersistingPSImageLocation      = "Persisting / Setting PSImageLocation"
	//coStepNamePersistingArticleImage         = "Persisting  Article Image"
	//coStepNamePersistArticleImageLocation = "Persisting / Setting Article ImageLocation"
)

var coNoError = a_domain.ProcessingInfo{}

//type articleImage image_model.ArtikelImage

type ProcessingInfo struct {
	DetectingComponentName string
	InfoDescription        string
}
