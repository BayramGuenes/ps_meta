package b_usecases

import (
	"errors"
	"os"
	"path/filepath"
	domain_model "ps_meta/a_domain"
	bootstrap "ps_meta/z_bootstrap"
	"strconv"
	"strings"
	//image_archiv "ps_meta/i_model_image_interface"
)

/* ============================================= */

type ImportServiceResponseStruct struct {
	MessageType string //Info,Error
	PspEsd      string
	MessageText string
	Details     string
}

// PSDataImporterClass ...                       //
type PSDataImporterClass struct {
	DataFetcher     domain_model.PSDataProviderPort
	DataReader      domain_model.PSProviderDataReaderPort
	Repository      domain_model.PSDataBase
	ImageRepository domain_model.PSImageArchivePort
	TransferLogger  domain_model.PSLogPort
}

func NewPSDataImporter(
	iDataFetcher domain_model.PSDataProviderPort,
	iDataReader domain_model.PSProviderDataReaderPort,
	iRepository domain_model.PSDataBase,
	iImageRepository domain_model.PSImageArchivePort,
	iTransferLogger domain_model.PSLogPort) PSDataImporterClass {
	return PSDataImporterClass{
		DataFetcher:     iDataFetcher,
		DataReader:      iDataReader,
		Repository:      iRepository,
		ImageRepository: iImageRepository,
		TransferLogger:  iTransferLogger,
	}
}

func (u PSDataImporterClass) ImportPSData(iImportusr string, iSkipFTPAndReadDataWorkDir bool, iTriggerBuildMeldungImages bool, iWorkDir string, iPSProviderDataFile string, iLogicalNameImportsystem string) (eServiceResult ImportServiceResponseStruct,
	eErrorMsgs []domain_model.ProcessingInfo, ok bool) {
	// init data --------------------------------------------
	err := *new(error)
	ok = true
	eErrorMsgs = []domain_model.ProcessingInfo{}
	eServiceResult = ImportServiceResponseStruct{}
	lImportDataFound := false
	lErrMsg := domain_model.ProcessingInfo{}
	////println("IRFA=" + iRFA)
	logTransactionBuffer := u.TransferLogger.SetStartTransaction("ImportPSData")
	// checking user rights ---------------------------------------------
	logTransactionBuffer = u.TransferLogger.LogStep(logTransactionBuffer, coStepNameCheckingUserData, "")
	////println("IRFA=" + iRFA)

	// fetching ---------------------------------------------

	logTransactionBuffer = u.TransferLogger.LogStep(logTransactionBuffer, coStepNameFetchingData, "")
	lOk := true
	ltFetchedFileNameSet := []string{}
	if !iSkipFTPAndReadDataWorkDir {
		ltFetchedFileNameSet, lErrMsg, lOk = u.DataFetcher.FetchDataFromProvider(iWorkDir)
	} else {
		err = filepath.Walk(iWorkDir, func(path string, info os.FileInfo, err error) error {
			////println("filepath.Walk")
			ltFetchedFileNameSet = append(ltFetchedFileNameSet, path)
			return nil
		})
		if err != nil {
			lOk = false
			lErrMsg.DetectingComponentName = coStepNameFetchingData
			lErrMsg.InfoDescription = err.Error()
		}
	}
	if !lOk {
		eErrorMsgs = append(eErrorMsgs, lErrMsg)
		logTransactionBuffer = u.TransferLogger.LogStepResult(logTransactionBuffer, coStepNameFetchingData, "FAILED", lErrMsg)
		ok = false
	}

	// reading in struct in buffer -----------------------------
	lPressespiegelWrapper := domain_model.PressespiegelWrapper{}

	if ok {
		logTransactionBuffer = u.TransferLogger.LogStep(logTransactionBuffer, coStepNameReadingMetadataToBuffer, "")
		lPressespiegelWrapper, lImportDataFound, lErrMsg, lOk = u.DataReader.ReadPSProviderData(iWorkDir, iPSProviderDataFile)
		if !lOk {
			eErrorMsgs = append(eErrorMsgs, lErrMsg)
			logTransactionBuffer = u.TransferLogger.LogStepResult(logTransactionBuffer, coStepNameReadingMetadataToBuffer, "FAILED", lErrMsg)
			ok = false

		} else {
			logTransactionBuffer = u.TransferLogger.LogStepResult(logTransactionBuffer, coStepNameReadingMetadataToBuffer, "OK", coNoError)
		}

	}

	// persist metadata -----------------------------------------
	if ok && lImportDataFound && len(lPressespiegelWrapper.PSHeader.PSExtID) > 0 {

		logTransactionBuffer = u.TransferLogger.LogStep(logTransactionBuffer, coStepNamePersistingMetadata, "")
		lErrMsg, lOk = u.Repository.PersistPS(lPressespiegelWrapper, iLogicalNameImportsystem, iImportusr)
		if !lOk {
			eErrorMsgs = append(eErrorMsgs, lErrMsg)
			logTransactionBuffer = u.TransferLogger.LogStepResult(logTransactionBuffer, coStepNamePersistingMetadata, "FAILED", lErrMsg)
			ok = false
		} else {
			////println("persist")
			logTransactionBuffer = u.TransferLogger.LogStepResult(logTransactionBuffer, coStepNamePersistingMetadata, "OK", coNoError)
		}
	}
	if !lImportDataFound {
		logTransactionBuffer = u.TransferLogger.LogStepResult(logTransactionBuffer, coStepNameFetchingData+" kein QueryXML gefunden", "OK", coNoError)
	}
	// Create Order ElasticSearch-Index delete article Records //
	//go func(){
	if ok {
		logTransactionBuffer = u.TransferLogger.LogStep(logTransactionBuffer, coStepNameCreateIngestDeleteOrder, "ESD:"+lPressespiegelWrapper.PSHeader.PSESD)
		ingestOrderResult := BuildIngestOrder(u.Repository, u.TransferLogger, lPressespiegelWrapper.PSHeader.PSESD, "D", 10)
		//ingestOrderResult.TransferOK = false
		if !ingestOrderResult.TransferOK {
			eServiceResult.MessageType = "Error"
			eServiceResult.MessageText = "Pressespiegel zum Erscheinungsdatum " + lPressespiegelWrapper.PSHeader.PSESD + " konnte nicht importiert  werden."
			eServiceResult.Details = "Übertragung der Indizierungsdaten - Delete  fehlgeschlagen:" + ingestOrderResult.TransferWarningErrorText
			err = errors.New("Ingest Order Transfer Error!")
			lErrMsg.DetectingComponentName = coStepNameCreateIngestDeleteOrder
			lErrMsg.InfoDescription = eServiceResult.Details
			logTransactionBuffer = u.TransferLogger.LogStepResult(logTransactionBuffer, coStepNameCreateIngestDeleteOrder, "FAILED", lErrMsg)
			ok = false
		} else {
			logTransactionBuffer = u.TransferLogger.LogStepResult(logTransactionBuffer, coStepNameCreateIngestDeleteOrder, "OK", coNoError)
		}
	}
	//}()

	// TransferImagesToImageRepProxy() ----------------------------------------

	lImageTransferFailed := false
	lImagesTransferOK := true
	lImagesTransferResultString := ""
	lImagesTransferDetailsString := ""

	lImagesTransferResult := domain_model.ImageTransferResult{}
	lImageCancelTransferResult := domain_model.ImageCancelTransferResult{}
	lImageCancelTransferResult.CancelOK = true
	if ok && lImportDataFound {
		logTransactionBuffer = u.TransferLogger.LogStep(logTransactionBuffer, coStepNameCancellingTransferImagesBef, "")
		lImageCancelTransferResult = u.ImageRepository.CancelTransferedImagesToRepProxy(lPressespiegelWrapper.PSHeader.PSESD)

		if !lImageCancelTransferResult.CancelOK {
			lprocessinginfo := domain_model.ProcessingInfo{}
			lprocessinginfo.DetectingComponentName = "u.ImageRepository.CancelTransferedImagesToRepProxy"
			lprocessinginfo.InfoDescription = lImageCancelTransferResult.Details
			logTransactionBuffer = u.TransferLogger.LogStepResult(logTransactionBuffer, coStepNameCancellingTransferImagesBef, "FAILED", lprocessinginfo)
		} else {
			logTransactionBuffer = u.TransferLogger.LogStepResult(logTransactionBuffer, coStepNameCancellingTransferImagesBef, "OK", coNoError)
		}

		logTransactionBuffer = u.TransferLogger.LogStep(logTransactionBuffer, coStepNameTransferringImagesToImageRep, "")
		lPSImagename := extractImageName(lPressespiegelWrapper.PSHeader.PSImageURI)
		ltArticleImages := extractSetOfArticleImageName(lPressespiegelWrapper)

		lImagesTransferResult =
			u.ImageRepository.TransferImagesToImageRepProxy(iWorkDir, lPSImagename, lPressespiegelWrapper.PSHeader.PSESD,
				lPressespiegelWrapper.PSHeader.PSExtID, ltArticleImages)

			////println("IMAGE TRANSFER RESULT"+lImagesTransferResult.TransferWarningDetails)
		lImageTransferFailed = !lImagesTransferResult.TransferOK
		if lImageTransferFailed {
			lImagesTransferOK = false
			lImagesTransferDetailsString = lImagesTransferResult.TransferWarningErrorText + "\n"

			if lImagesTransferResult.Details != nil {
				for _, row := range lImagesTransferResult.Details {
					entity := ""
					if row.EntityType == "article" {
						entity = getArticle(u, lPressespiegelWrapper.PSHeader.PSExtID, row.EntityExtid)
					} else {
						entity = row.EntityExtid
					}
					lImagesTransferDetailsString = lImagesTransferDetailsString + " Entity: < " + entity + ">"
					lImagesTransferDetailsString = lImagesTransferDetailsString + " EntityType: <" + row.EntityType + ">"
					lImagesTransferDetailsString = lImagesTransferDetailsString + " EntityTransferResultDetails: " + row.EntityTransferResultDetails
					lImagesTransferDetailsString = lImagesTransferDetailsString + "\n"
				}
			}

			lprocessinginfo := domain_model.ProcessingInfo{
				DetectingComponentName: "ImageArchivPort.TransferImagesToImageRepProxy",
				InfoDescription:        "Error executing Transfer Images To ImageRep via Proxy: " + lImagesTransferDetailsString,
			}
			eErrorMsgs = append(eErrorMsgs, lprocessinginfo)
			logTransactionBuffer = u.TransferLogger.LogStepResult(logTransactionBuffer, coStepNameTransferringImagesToImageRep, "FAILED", lprocessinginfo)
			////println("FAIILED Import Images")
		} else {
			logTransactionBuffer = u.TransferLogger.LogStepResult(logTransactionBuffer, coStepNameTransferringImagesToImageRep, "OK", coNoError)
		}
		/*if ok && lImportDataFound&&  lImagesTransferOK &&iTriggerBuildMeldungImages{
					for _,row:=range lPressespiegelWrapper.MeldungSet{
		    		 //go NICHT IM Concurrent Mode laufen lassen => entsteht sonst Map Race Condition
					 u.ImageRepository.BuildMeldungImagesToImageRepProxy(lPressespiegelWrapper.PSHeader.PSErscheinungsdatum,
						  row.MelExtID   )
		    }*/
		if ok && lImportDataFound && lImagesTransferOK && iTriggerBuildMeldungImages {
			lMelExtidList := []string{}
			for _, row := range lPressespiegelWrapper.MeldungSet {
				lMelExtidList = append(lMelExtidList, row.MelExtID)
			}
			go u.ImageRepository.BuildMeldungenImagesToImageRepProxy(lPressespiegelWrapper.PSHeader.PSESD,
				lMelExtidList)

		}
	}

	// remove query.xml from work directory ------------------------------------------
	targetDir, _ := GetDoneDirectory(bootstrap.ApplConf.DataDonePath)
	filename := strings.Trim(iPSProviderDataFile, "/")
	if IsInDirectory(iWorkDir, filename) {
		moveFile(iWorkDir, targetDir, filename, lPressespiegelWrapper.PSHeader.PSESD)
	}

	// return results ------------------------------------------------

	if ok && !lImportDataFound {
		eServiceResult.MessageType = "Info"
		eServiceResult.MessageText = "Neue Pressespiegel Daten aus " + bootstrap.ApplConf.ImportFromSys + " liegen nicht vor."
		eServiceResult.Details = "Es wurden keine importierbaren Dateien beim Pressespiegel-Generierungssystem " +
			bootstrap.ApplConf.ImportFromSys + " gefunden."
	}
	if ok && lImportDataFound {

		if lImageTransferFailed {

			eServiceResult.PspEsd = lPressespiegelWrapper.PSHeader.PSESD
			eServiceResult.MessageType = "Warning"
			eServiceResult.MessageText = "Der Import des Pressespiegels aus " + bootstrap.ApplConf.ImportFromSys + " war teilweise erfolgreich." +
				" Nicht alle PDFs wurden importiert."
			eServiceResult.Details = "Folgender Pressespiegel konnte nur mit Warnungen erstellt werden:" +
				"Ausgabenummer:" + lPressespiegelWrapper.PSHeader.PSAusgabenr + ",\n" +
				"ESD: " + lPressespiegelWrapper.PSHeader.PSESD + ",\n" +
				"PS Art: " + lPressespiegelWrapper.PSHeader.PSArt + ",\n" +
				"ID PROD: " + lPressespiegelWrapper.PSHeader.PSExtID + ",\n" +
				"Warnungen:" + lImagesTransferResultString + ",\n" +
				"Details:" + lImagesTransferDetailsString

		} else {
			eServiceResult.PspEsd = lPressespiegelWrapper.PSHeader.PSESD
			eServiceResult.MessageType = "Info"
			eServiceResult.MessageText = "Der Import des Pressespiegels aus " + bootstrap.ApplConf.ImportFromSys + " war erfolgreich."
			eServiceResult.Details = "Folgender Pressespiegel konnte erstellt werden:" +
				"Ausgabenummer:" + lPressespiegelWrapper.PSHeader.PSAusgabenr + ",\n" +
				"ESD: " + lPressespiegelWrapper.PSHeader.PSESD + ",\n" +
				"PS Art: " + lPressespiegelWrapper.PSHeader.PSArt + ",\n" +
				"ID PROD: " + lPressespiegelWrapper.PSHeader.PSExtID
		}

	}
	if !ok {
		eServiceResult.PspEsd = lPressespiegelWrapper.PSHeader.PSESD
		eServiceResult.MessageType = "Info"
		eServiceResult.MessageText = "Achtung! Der Import des Pressespiegels aus " + bootstrap.ApplConf.ImportFromSys + " ist fehlgeschlagen."
		for i := 0; i < len(eErrorMsgs); i++ {
			eServiceResult.Details += "[" + strconv.Itoa(i) + "] " + eErrorMsgs[i].InfoDescription + "\n"
		}
	}
	if lImportDataFound && (!lImageCancelTransferResult.CancelOK) {
		eServiceResult.Details = eServiceResult.Details + "\n" + "Beim  Entfernen der vorher importierten PDFs kam es zu Fehlern: " +
			lImageCancelTransferResult.Details
		if len(lImageCancelTransferResult.NotRemovedFilesList) > 0 {
			eServiceResult.Details = eServiceResult.Details + "\n" + "Nicht entfernte Dateien:"

			for _, row := range lImageCancelTransferResult.NotRemovedFilesList {
				eServiceResult.Details = eServiceResult.Details + row
			}
		}
		if len(lImageCancelTransferResult.NotUpdatedRepoDBEntries) > 0 {
			eServiceResult.Details = eServiceResult.Details + "\n" + "Nicht aktualisierte Repo-DB-Einträge:"

			for _, row := range lImageCancelTransferResult.NotUpdatedRepoDBEntries {
				eServiceResult.Details = eServiceResult.Details + row
			}
		}
	}

	u.TransferLogger.SetEndTransaction(logTransactionBuffer)

	return
}

func getArticle(u PSDataImporterClass, iPSExtId string, iArtExtID string) string {
	article := NewArticleGetter(u.Repository, u.TransferLogger)
	akl, _ := article.GetArticle(iPSExtId, iArtExtID)
	if len(akl.Titel) > 30 {
		return akl.Titel[0:30] + "..."
	} else {
		return akl.Titel
	}
}
