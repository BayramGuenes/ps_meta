package b_usecases

import (
	"ps_meta/a_domain"
)

// ArticleCollClass ...                       //
type ArticleClass struct {
	RepoArticle a_domain.ArticleDataBasePort
	Logger      a_domain.PSLogPort
}

func NewArticleGetter(iDatabase a_domain.PSDataBase, iLogger a_domain.PSLogPort) ArticleClass {
	logger := iLogger //logger.GetLogger()
	return ArticleClass{RepoArticle: iDatabase.ArticleDBPort, Logger: logger}
}

func (a ArticleClass) GetArticle(iPSExtId string, iArtExtId string) (eArtikel a_domain.Artikel, err error) {
	lStepname := "CALL  GetArticle [a ArticleClass - b_usecases]"
	logTransactionBuffer := a.Logger.SetStartTransaction("GetArticle")

	logTransactionBuffer = a.Logger.LogStep(logTransactionBuffer, lStepname, "iPSExtId:"+iPSExtId+" iArtExtId:"+iArtExtId)

	eArtikel, err = a.RepoArticle.GetArticle(iPSExtId, iArtExtId)

	////println("ePSHeader =" + ePSHeader.PSErscheinungsdatum)
	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "GetArticle", err, a.Logger)
	a.Logger.SetEndTransaction(logTransactionBuffer)
	return
}

func (a ArticleClass) GetArtViaIDAndESD(iArtExtId string, iArtESD string) (eArtikel a_domain.Artikel, err error) {
	lStepname := "CALL  GetArticle [a ArticleClass - b_usecases]"
	logTransactionBuffer := a.Logger.SetStartTransaction("GetArticleViaArtIDAndArtESD")

	logTransactionBuffer = a.Logger.LogStep(logTransactionBuffer, lStepname, "iArtExtId:"+iArtExtId+" iArtESD:"+iArtESD)

	eArtikel, err = a.RepoArticle.GetArtViaIDAndESD(iArtExtId, iArtESD)

	////println("ePSHeader =" + ePSHeader.PSErscheinungsdatum)
	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "GetArticleViaArtIDAndArtESD", err, a.Logger)
	a.Logger.SetEndTransaction(logTransactionBuffer)
	return
}
