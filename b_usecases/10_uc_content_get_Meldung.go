package b_usecases

import (
	"ps_meta/a_domain"
)

// MeldungClass ...                       //
type MeldungClass struct {
	RepoPS      a_domain.PSDataBasePort
	RepoMeldung a_domain.MeldungDataBasePort
	Logger      a_domain.PSLogPort
}

func NewMeldungGetter(iDatabase a_domain.PSDataBase, iLogger a_domain.PSLogPort) MeldungClass {
	logger := iLogger //logger.GetLogger()
	return MeldungClass{RepoPS: iDatabase.PSDBPort, RepoMeldung: iDatabase.MeldungDBPort, Logger: logger}
}

func (m MeldungClass) GetMeldung(iPSExtId string, iMelExtId string) (eMeldung a_domain.Meldung, err error) {
	
	eMeldung, err = m.RepoMeldung.GetMeldung(iPSExtId, iMelExtId)
	////println("lPSHeader" + lPSHeader.PSExtID)

	////println("ePSHeader =" + ePSHeader.PSErscheinungsdatum)

	//logResult(lStepname, "GetMeldung", err, m.Logger)
	return
}
