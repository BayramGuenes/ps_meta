package b_usecases

import (
	"ps_meta/a_domain"
	model "ps_meta/a_domain"
	utils "ps_meta/u_utils"
	boot "ps_meta/z_bootstrap"
	"strconv"
	"strings"
)

// PSEntityClass ...                       //
type PSEntityClass struct {
	Repository a_domain.PSDataBase
	Logger     a_domain.PSLogPort
}

func NewPSGetter(iDatabase a_domain.PSDataBase, iLogger a_domain.PSLogPort) PSEntityClass {
	logger := iLogger //logger.GetLogger()
	return PSEntityClass{Repository: iDatabase, Logger: logger}
}

func (u PSEntityClass) GetPS(iPSErscheinungsdatum string, iToken string, iKindOfAuth string) (ePSHeader a_domain.Pressespiegel, err error) {

	logTransactionBuffer := u.Logger.SetStartTransaction("GetPS")

	lStepname := "CALL  GetPS [u PSEntityClass]"
	logTransactionBuffer = u.Logger.LogStep(logTransactionBuffer, lStepname, "iPSErscheinungsdatum:"+iPSErscheinungsdatum)
	_, lUserRoles, err := GetUsrInfoFromToken(iToken, iKindOfAuth, u.Repository.UsrDBPort)
	if err != nil {
		logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "GetPS:GetUsrInfoFromToken", err, u.Logger)
		return a_domain.Pressespiegel{}, err
	}
	lWithInactive := a_domain.CheckAuthority(a_domain.AuthSearchWithInactive, lUserRoles, u.Repository.AuthDBPort, u.Logger)
	ePSHeader, err = u.Repository.PSDBPort.GetPS(iPSErscheinungsdatum, lWithInactive)

	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "GetPS", err, u.Logger)
	u.Logger.SetEndTransaction(logTransactionBuffer)
	return

}

func (u PSEntityClass) ExistsPS(iPSErscheinungsdatum string) (eEXTId string, eFound bool, err error) {
	logTransactionBuffer := u.Logger.SetStartTransaction("ExistsPS")
	lStepname := "CALL ExistsPS  [u PSEntityClass]"
	logTransactionBuffer = u.Logger.LogStep(logTransactionBuffer, lStepname, "iPSErscheinungsdatum:"+iPSErscheinungsdatum)

	eEXTId, eFound, err = u.Repository.PSDBPort.ExistsPS(iPSErscheinungsdatum)
	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "ExistsPS", err, u.Logger)
	u.Logger.SetEndTransaction(logTransactionBuffer)
	return
}

func (u PSEntityClass) GetDateBeforePS(iToken string, iKindOfAuth string, iDateInRelationTo string) (ePSDate string, err error) {

	logTransactionBuffer := u.Logger.SetStartTransaction("GetDateBeforePS")

	lWithInactiv := false
	lUserRoles := []string{}
	lUser := model.User{}
	// Just for Tests and Dev:
	dateCompare := utils.DateBeforeNumDays(boot.ApplConf.AnonymUsrDefaultAccessInterval)
	// use later
	// dateCompare := utils.DateBeforeOneMonth()
	if len(iToken) > 0 {
		lStepname := "CALL GetDateBeforePS:GetUsrInfoFromToken()  [u PSEntityClass]"
		logTransactionBuffer = u.Logger.LogStep(logTransactionBuffer, lStepname, "iToken:"+iToken)
		lUser, lUserRoles, err = GetUsrInfoFromToken(iToken, iKindOfAuth, u.Repository.UsrDBPort)
		logTransactionBuffer = logResult(logTransactionBuffer, lStepname, strings.Join(lUserRoles, ","), err, u.Logger)
		if err != nil {
			return "", err
		}
		lStepname = "CALL GetDateBeforePS:CheckAuthority  [u PSEntityClass]"
		logTransactionBuffer = u.Logger.LogStep(logTransactionBuffer, lStepname, "User:"+lUser.Loginname)
		lWithInactiv = a_domain.CheckAuthority(a_domain.AuthSearchWithInactive, lUserRoles, u.Repository.AuthDBPort, u.Logger)
		lOneYearSearch := a_domain.CheckAuthority(a_domain.AuthSearchOneYear, lUserRoles, u.Repository.AuthDBPort, u.Logger)
		lUnrestrictedSearch := a_domain.CheckAuthority(a_domain.AuthSearchUnlimited, lUserRoles, u.Repository.AuthDBPort, u.Logger)
		resultAuthCheck := "withInactive:" + strconv.FormatBool(lWithInactiv) +
			"; unrestrictedSearch:" + strconv.FormatBool(lUnrestrictedSearch) +
			"; oneYearSearch:" + strconv.FormatBool(lOneYearSearch)
		logTransactionBuffer = logResult(logTransactionBuffer, lStepname, resultAuthCheck, nil, u.Logger)
		if lUnrestrictedSearch {
			dateCompare = ""
		} else if lOneYearSearch {
			dateCompare = utils.DateBeforeOneYear()
		}
	}

	lStepname := "CALL GetDateBeforePS  [u PSEntityClass]"
	lRefDate := iDateInRelationTo
	logTransactionBuffer = u.Logger.LogStep(logTransactionBuffer, lStepname, "iDateInRelationTo:"+lRefDate)
	lPSDate, err := u.Repository.PSDBPort.GetDateBeforePS(lWithInactiv, lRefDate)
	if utils.DateLessThan(lPSDate, dateCompare) {
		lPSDate = ""
	}
	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, lPSDate, err, u.Logger)
	if err != nil {
		return "", err
	}
	u.Logger.SetEndTransaction(logTransactionBuffer)
	return lPSDate, nil
}

func (u PSEntityClass) GetDateNextPS(iToken, iKindOfAuth string, iDateInRelationTo string) (ePSDate string, ePSIsActive bool, err error) {
	logTransactionBuffer := u.Logger.SetStartTransaction("GetDateNextPS")
	lStepname := "CALL  GetDateNextPS  [u PSEntityClass]"
	logTransactionBuffer = u.Logger.LogStep(logTransactionBuffer, lStepname, "iDateInRelationTo:"+iDateInRelationTo)

	lWithInactiv := false
	if len(iToken) > 0 {

		_, lUserRoles, err := GetUsrInfoFromToken(iToken, iKindOfAuth, u.Repository.UsrDBPort)
		if err == nil {

			lWithInactiv = model.CheckAuthority(a_domain.AuthSearchWithInactive, lUserRoles, u.Repository.AuthDBPort, u.Logger)
			if contains(lUserRoles, "DataImporter") {
				lWithInactiv = true
			}
		}
		logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "GetUsrInfoFromToken", err, u.Logger)

	}
	if err == nil {
		ePSDate, err = u.Repository.PSDBPort.GetDateNextPS(lWithInactiv, iDateInRelationTo)
		logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "GetDateNextPS"+":"+ePSDate, err, u.Logger)
		if err == nil {
			lStepname := "CALL  GetDateNextPS:GetPS  [u PSEntityClass]"
			lPS, err := u.Repository.PSDBPort.GetPS(ePSDate, lWithInactiv)
			ePSIsActive = lPS.IsActivated
			logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "GetDateNextPS:GetPS"+":"+ePSDate, err, u.Logger)
		}
		u.Logger.SetEndTransaction(logTransactionBuffer)
	}
	return
}

func (u PSEntityClass) GetDateLastPS(iToken string, iKindOfAuth string) (ePSDate string, err error) {
	logTransactionBuffer := u.Logger.SetStartTransaction("GetDateLastPS")
	lStepname := "CALL GetDateLastPS: GetUsrInfoFromToken [u PSEntityClass]"
	logTransactionBuffer = u.Logger.LogStep(logTransactionBuffer, lStepname, "")

	lWithInactiv := false
	if len(iToken) > 0 {

		_, lUserRoles, err := GetUsrInfoFromToken(iToken, iKindOfAuth, u.Repository.UsrDBPort)
		if err == nil {

			lWithInactiv = a_domain.CheckAuthority(a_domain.AuthSearchWithInactive, lUserRoles, u.Repository.AuthDBPort, u.Logger)
			if contains(lUserRoles, "DataImporter") {
				lWithInactiv = true
			}
		}
		logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "GetUsrInfoFromToken", err, u.Logger)

	}
	if err == nil {
		ePSDate, err = u.Repository.PSDBPort.GetDateLastPS(lWithInactiv)
		logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "GetDateLastPS"+":"+ePSDate, err, u.Logger)
		u.Logger.SetEndTransaction(logTransactionBuffer)
	}
	return
}

func contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}
