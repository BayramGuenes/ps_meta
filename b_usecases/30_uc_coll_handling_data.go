package b_usecases

import (
	"ps_meta/a_domain"
	model "ps_meta/a_domain"
)

type CollectionObject struct {
	Loginname      string
	Collectionname string
}

type CollectionItem struct {
	ItemIdField1 string
	ItemIdField2 string
}

const (
	COColOrderGet          = "GET"
	COColOrderCheckNewPerm = "NEWPERM"
	COColOrderNew          = "NEW"
	COColOrderDel          = "DEL"
	COColOrderActivate     = "ACT"
)

type CollectionOrder struct {
	Ordertype  string
	Collection CollectionObject
}

const (
	COColItemNew = "NEW"
	COColItemDel = "DEL"
)

type CollectionItemOrder struct {
	Ordertype  string
	Collection CollectionObject
	Item       CollectionItem
}

type CollectionItemList []CollectionItem

type CollectionObjOrderTransferResult struct {
	TransferOK               bool
	TransferWarningErrorText string
	TransferredData          CollectionOrder
	CollectionList           []string
}

type CollectionItemOrderTransferResult struct {
	TransferOK               bool
	TransferWarningErrorText string
	TransferredData          CollectionItemOrder
}

type GUICollItemListOrder struct {
	Token     string
	Ordertype string
	Itemlist  CollectionItemList
}

type CollectionItemListOrder struct {
	Ordertype  string
	Collection CollectionObject
	Itemlist   CollectionItemList
}
type CollectionItemListOrderTransferResult struct {
	TransferOK               bool
	TransferWarningErrorText string
	TransferredData          CollectionItemListOrder
}

type CollectionGetItemListResult struct {
	ReceiveOK               bool
	ReceiveWarningErrorText string
	Collection              string
	Itemlist                CollectionItemList
}
type CollectionGetArticleListResult struct {
	CollectionGetItemListResult
	Articlelist []model.IF_psArticleTextInfo
}
type CollectionGetActiveResult struct {
	ReceiveOK               bool
	ReceiveWarningErrorText string
	Collection              CollectionObject
}

/* =================================================*/
// PSEIngestOrderClass ...                       //
/* =================================================*/
type PSCollectionClass struct {
	Logger     a_domain.PSLogPort
	PSDataBase a_domain.PSDataBase
}

func NewCollectionHandler(iLogger a_domain.PSLogPort, iPSDataBase a_domain.PSDataBase) PSCollectionClass {
	logger := iLogger //logger.GetLogger()
	return PSCollectionClass{Logger: logger, PSDataBase: iPSDataBase}
}
