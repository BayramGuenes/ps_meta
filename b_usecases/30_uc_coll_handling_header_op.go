package b_usecases

import "errors"

func (col PSCollectionClass) HandleGetUsrCollections(iToken string, iKindOfAuth string) (eHandleResult CollectionObjOrderTransferResult) {

	eHandleResult = CollectionObjOrderTransferResult{}
	logTransactionBuffer := col.Logger.SetStartTransaction("GetUsrCollections")
	lStepname := "CALL CollectionsGet:GetUsrInfoFromToken [u uc_collection_HandleGetUsrCollections]"
	logTransactionBuffer = col.Logger.LogStep(logTransactionBuffer, lStepname, " iToken:"+iToken)

	lUser, _, err := GetUsrInfoFromToken(iToken, iKindOfAuth, col.PSDataBase.UsrDBPort)

	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "CollectionsGet:GetUsrInfoFromToken", err, col.Logger)
	if err != nil {
		eHandleResult.TransferWarningErrorText = "Benutzer konnte nicht identifiziert werden:" + err.Error()
		return
	}
	if len(lUser.Loginname) == 0 {
		eHandleResult.TransferOK = true
		eHandleResult.TransferWarningErrorText = "Kein Benutzer angegeben (anonymer Aufruf)"
		return
	}
	lStepname = "CALL CollectionsGet:GinCallCollOrderRestService [u uc_collection_HandleGetUsrCollections]"
	logTransactionBuffer = col.Logger.LogStep(logTransactionBuffer, lStepname, " Usrname:"+lUser.Loginname)
	lColOrder := CollectionOrder{}
	lColOrder.Ordertype = COColOrderGet
	lColOrder.Collection.Loginname = lUser.Loginname
	lColOrder.Collection.Collectionname = " "
	eHandleResult = GinCallCollOrderRestService(lColOrder)
	if !eHandleResult.TransferOK {
		err = errors.New(eHandleResult.TransferWarningErrorText)
	}
	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "CollectionsGet:GinCallCollOrderRestService ", err, col.Logger)

	col.Logger.SetEndTransaction(logTransactionBuffer)

	return eHandleResult
}

func (col PSCollectionClass) HandleNewCollPermitted(iToken string, iKindOfAuth string, iCollname string) (eHandleResult CollectionObjOrderTransferResult) {

	eHandleResult = CollectionObjOrderTransferResult{}
	logTransactionBuffer := col.Logger.SetStartTransaction("CollectionNewCollPermitted")
	lStepname := "CALL CollectionNewPermitted:GetUsrInfoFromToken [u uc_collection_HandleNewCollPermitted]"
	logTransactionBuffer = col.Logger.LogStep(logTransactionBuffer, lStepname, " iToken:"+iToken)

	lUser, _, err := GetUsrInfoFromToken(iToken, iKindOfAuth, col.PSDataBase.UsrDBPort)
	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "CollectionNewPermitted:GetUsrInfoFromToken", err, col.Logger)
	if err != nil {
		eHandleResult.TransferWarningErrorText = "Benutzer konnte nicht identifiziert werden:" + err.Error()
		return
	}
	if len(lUser.Loginname) == 0 {
		eHandleResult.TransferOK = true
		eHandleResult.TransferWarningErrorText = "Kein Benutzer angegeben (anonymer Aufruf)"
		return
	}

	lStepname = "CALL CollectionNewPermitted:GinCallCollOrderRestService [u uc_collection_HandleNewCollPermitted]"
	logTransactionBuffer = col.Logger.LogStep(logTransactionBuffer, lStepname, " Usrname:"+lUser.Loginname)
	lColOrder := CollectionOrder{}
	lColOrder.Ordertype = COColOrderCheckNewPerm
	lColOrder.Collection.Loginname = lUser.Loginname
	lColOrder.Collection.Collectionname = iCollname
	eHandleResult = GinCallCollOrderRestService(lColOrder)
	if !eHandleResult.TransferOK {
		err = errors.New(eHandleResult.TransferWarningErrorText)
	}
	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "CollectionNewPermitted:GinCallCollOrderRestService ", err, col.Logger)

	col.Logger.SetEndTransaction(logTransactionBuffer)

	return eHandleResult

}
func (col PSCollectionClass) HandleNewColl(iToken, iKindOfAuth, iCollName string) (eHandleResult CollectionObjOrderTransferResult) {

	eHandleResult = CollectionObjOrderTransferResult{}
	logTransactionBuffer := col.Logger.SetStartTransaction("CollectionNew")
	lStepname := "CALL CollectionNew:GetUsrInfoFromToken [u uc_collection_HandleNewColl]"
	logTransactionBuffer = col.Logger.LogStep(logTransactionBuffer, lStepname, " iToken:"+iToken)

	lUser, _, err := GetUsrInfoFromToken(iToken, iKindOfAuth, col.PSDataBase.UsrDBPort)
	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "CollectionNew:GetUsrInfoFromToken", err, col.Logger)
	if err != nil {
		eHandleResult.TransferWarningErrorText = "Benutzer konnte nicht identifiziert werden:" + err.Error()
		return
	}
	if len(lUser.Loginname) == 0 {
		eHandleResult.TransferOK = true
		eHandleResult.TransferWarningErrorText = "Kein Benutzer angegeben (anonymer Aufruf)"

		return
	}

	lStepname = "CALL CollectionNew:CollectionNewPermitted-GinCallCollOrderRestService [u uc_collection_HandleNewColl]"
	logTransactionBuffer = col.Logger.LogStep(logTransactionBuffer, lStepname, " Usrname:"+lUser.Loginname)
	lColOrder := CollectionOrder{}
	lColOrder.Ordertype = COColOrderCheckNewPerm
	lColOrder.Collection.Loginname = lUser.Loginname
	lColOrder.Collection.Collectionname = iCollName
	eHandleResult = GinCallCollOrderRestService(lColOrder)
	if !eHandleResult.TransferOK {
		err = errors.New(eHandleResult.TransferWarningErrorText)
	}
	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "CollectionNew:NewPermitted:GinCallCollOrderRestService ", err, col.Logger)
	if err != nil {
		eHandleResult.TransferWarningErrorText = "Kollektion existiert bereits. Bitte überprüfen. "
		return
	}

	lStepname = "CALL CollectionNew:GinCallCollOrderRestService [u uc_collection_HandleNewColl]"
	logTransactionBuffer = col.Logger.LogStep(logTransactionBuffer, lStepname, " iCollname:"+iCollName)
	lColOrder = CollectionOrder{}
	lColOrder.Ordertype = COColOrderNew
	lColOrder.Collection.Loginname = lUser.Loginname
	lColOrder.Collection.Collectionname = iCollName
	eHandleResult = GinCallCollOrderRestService(lColOrder)
	if !eHandleResult.TransferOK {
		err = errors.New(eHandleResult.TransferWarningErrorText)
	}
	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "CollectionNew:GinCallCollOrderRestService ", err, col.Logger)

	col.Logger.SetEndTransaction(logTransactionBuffer)

	return eHandleResult
}

func (col PSCollectionClass) HandleDelColl(iToken, iKindOfAuth, iCollName string) (eHandleResult CollectionObjOrderTransferResult) {
	eHandleResult = CollectionObjOrderTransferResult{}
	logTransactionBuffer := col.Logger.SetStartTransaction("CollectionDelete")

	/* --------------------*/
	lStepname := "CALL CollectionDelete:GetUsrInfoFromToken [u uc_collection_HandleDelColl]"
	logTransactionBuffer = col.Logger.LogStep(logTransactionBuffer, lStepname, " iToken:"+iToken)

	lUser, _, err := GetUsrInfoFromToken(iToken, iKindOfAuth, col.PSDataBase.UsrDBPort)
	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "CollectionDelete:GetUsrInfoFromToken", err, col.Logger)
	if err != nil {
		eHandleResult.TransferWarningErrorText = "Benutzer konnte nicht identifiziert werden:" + err.Error()
		return
	}
	if len(lUser.Loginname) == 0 {
		eHandleResult.TransferOK = true
		eHandleResult.TransferWarningErrorText = "Kein Benutzer angegeben (anonymer Aufruf)"

		return
	}

	/* --------------------*/
	lStepname = "CALL CollectionDelete:GinCallGetActiveCollRestService [u uc_collection_HandleGetItems]"
	logTransactionBuffer = col.Logger.LogStep(logTransactionBuffer, lStepname, " Usr:"+lUser.Loginname)
	lResGetActive := GinCallGetActiveCollRestService(lUser.Loginname)
	if !lResGetActive.ReceiveOK {
		err = errors.New(lResGetActive.ReceiveWarningErrorText)
	}
	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "CollectionDelete:GinCallGetActiveCollRestService", err, col.Logger)
	if err != nil {
		eHandleResult.TransferWarningErrorText = "Aktive Kollektion konnte nicht identifiziert werden:" + err.Error()
		return
	}

	/* --------------------*/
	lStepname = "CALL CollectionDelete:GinCallGetItemListRestService [u uc_collection_HandleGetItems]"
	logTransactionBuffer = col.Logger.LogStep(logTransactionBuffer, lStepname, " Collname:"+lResGetActive.Collection.Collectionname)
	lReceiveResult := GinCallGetItemListRestService(lResGetActive.Collection)
	if !lReceiveResult.ReceiveOK {
		err = errors.New(lReceiveResult.ReceiveWarningErrorText)
	}
	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "CollectionGetItems:GinCallGetItemListRestService", err, col.Logger)
	if err != nil {
		eHandleResult.TransferWarningErrorText = "Itemlist konnte nicht gelesen werden:" + lReceiveResult.ReceiveWarningErrorText
		return
	}

	/* --------------------*/
	lStepname = "CALL CollectionDelete:GinCallCollItemListOrderRestSer( [u uc_collection_HandleDelCollItemList]"
	logTransactionBuffer = col.Logger.LogStep(logTransactionBuffer, lStepname, " Collname:"+lResGetActive.Collection.Collectionname)
	lColItemListOrder := CollectionItemListOrder{}
	lColItemListOrder.Ordertype = COColItemDel
	lColItemListOrder.Collection = lResGetActive.Collection
	for i := 0; i < len(lReceiveResult.Itemlist); i++ {
		lColItemListOrder.Itemlist = append(lColItemListOrder.Itemlist, lReceiveResult.Itemlist[i])
	}
	lHandleResult := GinCallCollItemListOrderRestSer(lColItemListOrder)
	if !lHandleResult.TransferOK {
		err = errors.New(lHandleResult.TransferWarningErrorText)
	}
	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "CollectionDelete:GinCallCollItemListOrderRestSer", err, col.Logger)
	if err != nil {
		eHandleResult.TransferWarningErrorText = "Itemlist konnte nicht entfernt werden:" + lHandleResult.TransferWarningErrorText
		return
	}

	/* --------------------*/
	lStepname = "CALL CollectionDelete:GinCallCollOrderRestService [u uc_collection_HandleDelColl]"
	logTransactionBuffer = col.Logger.LogStep(logTransactionBuffer, lStepname, " iCollname:"+iCollName)
	lColOrder := CollectionOrder{}
	lColOrder.Ordertype = COColOrderDel
	lColOrder.Collection.Loginname = lUser.Loginname
	lColOrder.Collection.Collectionname = iCollName
	eHandleResult = GinCallCollOrderRestService(lColOrder)
	if !eHandleResult.TransferOK {
		err = errors.New(eHandleResult.TransferWarningErrorText)
	}
	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "CollectionDelete:GinCallCollOrderRestService ", err, col.Logger)

	col.Logger.SetEndTransaction(logTransactionBuffer)
	return eHandleResult

}
func (col PSCollectionClass) HandleActivateColl(iToken, iKindOfAuth, iCollName string) (eHandleResult CollectionObjOrderTransferResult) {
	eHandleResult = CollectionObjOrderTransferResult{}
	logTransactionBuffer := col.Logger.SetStartTransaction("CollectionActivate")
	lStepname := "CALL CollectionActivate:GetUsrInfoFromToken [u uc_collection_HandleActivateColl]"
	logTransactionBuffer = col.Logger.LogStep(logTransactionBuffer, lStepname, " iToken:"+iToken)

	lUser, _, err := GetUsrInfoFromToken(iToken, iKindOfAuth, col.PSDataBase.UsrDBPort)
	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "CollectionActivate:GetUsrInfoFromToken", err, col.Logger)
	if err != nil {
		eHandleResult.TransferWarningErrorText = "Benutzer konnte nicht identifiziert werden:" + err.Error()
		return
	}
	if len(lUser.Loginname) == 0 {
		eHandleResult.TransferOK = true
		eHandleResult.TransferWarningErrorText = "Kein Benutzer angegeben (anonymer Aufruf)"

		return
	}
	lStepname = "CALL CollectionActivate:GinCallCollOrderRestService [u uc_collection_HandleActivateColl]"
	logTransactionBuffer = col.Logger.LogStep(logTransactionBuffer, lStepname, " iCollname:"+iCollName)
	lColOrder := CollectionOrder{}
	lColOrder.Ordertype = COColOrderActivate
	lColOrder.Collection.Loginname = lUser.Loginname
	lColOrder.Collection.Collectionname = iCollName
	eHandleResult = GinCallCollOrderRestService(lColOrder)
	if !eHandleResult.TransferOK {
		err = errors.New(eHandleResult.TransferWarningErrorText)
	}
	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "CollectionActivate:GinCallCollOrderRestService ", err, col.Logger)

	col.Logger.SetEndTransaction(logTransactionBuffer)
	return eHandleResult
}

func (col PSCollectionClass) HandleGetActivColl(iToken, iKindOfAuth string) (eHandleResult CollectionGetActiveResult) {
	eHandleResult = CollectionGetActiveResult{}
	logTransactionBuffer := col.Logger.SetStartTransaction("CollectionGetActive")
	lStepname := "CALL CollectionGetActive:GetUsrInfoFromToken [u uc_collection_HandleGetActiveColl]"
	logTransactionBuffer = col.Logger.LogStep(logTransactionBuffer, lStepname, " iToken:"+iToken)

	lUser, _, err := GetUsrInfoFromToken(iToken, iKindOfAuth, col.PSDataBase.UsrDBPort)
	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "CollectionGetActive:GetUsrInfoFromToken", err, col.Logger)
	if err != nil {
		eHandleResult.ReceiveWarningErrorText = "Benutzer konnte nicht identifiziert werden:" + err.Error()
		return
	}
	if len(lUser.Loginname) == 0 {
		eHandleResult.ReceiveOK = true
		eHandleResult.ReceiveWarningErrorText = "Kein Benutzer angegeben (anonymer Aufruf)"

		return
	}

	lStepname = "CALL CollectionGetActive:GinCallGetActiveCollRestService [u uc_collection_HandleGetActiveColl]"
	logTransactionBuffer = col.Logger.LogStep(logTransactionBuffer, lStepname, "Usr:"+lUser.Loginname)

	eHandleResult = GinCallGetActiveCollRestService(lUser.Loginname)
	if !eHandleResult.ReceiveOK {
		err = errors.New(eHandleResult.ReceiveWarningErrorText)
	}
	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "CollectionGetActive:GinCallGetActiveCollRestService ", err, col.Logger)

	col.Logger.SetEndTransaction(logTransactionBuffer)
	return eHandleResult
}
