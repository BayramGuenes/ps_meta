package b_usecases

import (
	"errors"
	"ps_meta/a_domain"
)

func (u PSEntityClass) MarkPublishable(iPSErscheinungsdatum string, iToken string, iKindOfAuth string) (eServiceResult ImportServiceResponseStruct) {
	////println("called:(u PSEntityClass) GetPS(iPSErscheinungsdatum string):" + iPSErscheinungsdatum)
	//ePSHeader = a_domain.Pressespiegel{}
	lPSErscheinungsdatum := formatDateYYYYminMMminDD(iPSErscheinungsdatum)
	eServiceResult = ImportServiceResponseStruct{
		MessageType: "Info",
		PspEsd:      iPSErscheinungsdatum,
		MessageText: "Pressespiegel zum Erscheinungsdatum " + lPSErscheinungsdatum + " wurde aktiviert.",
		Details:     "",
	}
	logTransactionBuffer := u.Logger.SetStartTransaction("ActivatePS")

	lStepname := "CALL  MarkPublishable [u PSEntityClass]"
	logTransactionBuffer = u.Logger.LogStep(logTransactionBuffer, lStepname, "iPSErscheinungsdatum:"+iPSErscheinungsdatum)

	lStepname = "CALL  MarkPublishable:GetUsrInfoFromToken [u PSEntityClass]"
	logTransactionBuffer = u.Logger.LogStep(logTransactionBuffer, lStepname, " iToken:"+iToken)

	lUser, lUserRoles, err := GetUsrInfoFromToken(iToken, iKindOfAuth, u.Repository.UsrDBPort)

	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "MarkPublishable:GetUsrInfoFromToken", err, u.Logger)
	if err != nil {
		eServiceResult.MessageType = "Error"
		eServiceResult.MessageText = "Pressespiegel zum Erscheinungsdatum " + lPSErscheinungsdatum + " konnte nicht aktiviert werden."
		eServiceResult.Details = "Benutzer konnte nicht identifiziert werden:" + err.Error()
	}

	if err == nil {
		lStepname = "CALL  MarkPublishable:CheckAuthority [u PSEntityClass]"
		lAuthorised := a_domain.CheckAuthority(a_domain.AuthDataImport, lUserRoles, u.Repository.AuthDBPort, u.Logger)
		if !lAuthorised {
			eServiceResult.MessageType = "Error"
			eServiceResult.MessageText = "Pressespiegel zum Erscheinungsdatum " + lPSErscheinungsdatum + " konnte nicht aktiviert werden."
			eServiceResult.Details = "Benutzer ist nicht authorisert, Importe durchzuführen bzw  zu aktivieren"
			err = errors.New("User is not authorised to import or to activate imports!")
		}
		logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "MarkPublishable:GetUsrRoles", err, u.Logger)
	}

	// 2. Exec domain function
	if err == nil {
		lStepname = "CALL  MarkPublishable:SetPSPublishmark [u PSEntityClass]"
		logTransactionBuffer = u.Logger.LogStep(logTransactionBuffer, lStepname, "")
		err = u.Repository.PSDBPort.SetPSPublishmark(iPSErscheinungsdatum, lUser.Loginname)
		logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "", err, u.Logger)
		if err != nil {
			eServiceResult.MessageType = "Error"
			eServiceResult.MessageText = "Pressespiegel zum Erscheinungsdatum " + lPSErscheinungsdatum + " konnte nicht aktiviert werden."
			eServiceResult.Details = "Datenbank-Fehler - PSDBPort.SetPSPublishmark:" + err.Error()
			logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "", err, u.Logger)
		}
	}
	//go func() {
	if err == nil {
		lStepname = "CALL MarkPublishable:" + coStepNameCreateIngestCreateOrder + "[u PSEntityClass]"
		logTransactionBuffer = u.Logger.LogStep(logTransactionBuffer, lStepname, "")

		ingestOrderResult := BuildIngestOrder(u.Repository, u.Logger, iPSErscheinungsdatum, "C", 1)
		//ingestOrderResult.TransferOK = false
		if !ingestOrderResult.TransferOK {
			eServiceResult.MessageType = "Error"
			eServiceResult.MessageText = "Pressespiegel zum Erscheinungsdatum " + lPSErscheinungsdatum + " konnte nicht aktiviert werden."
			eServiceResult.Details = "Übertragung der Indizierungsdaten fehlgeschlagen:" + ingestOrderResult.TransferWarningErrorText
			err = errors.New("Ingest Order Transfer Error:" + ingestOrderResult.TransferWarningErrorText)
		}
		logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "", err, u.Logger)
	}

	if err == nil {
		lStepname = "CALL MarkPublishable:" + coStepNameExecIngest + "[u PSEntityClass]"
		logTransactionBuffer = u.Logger.LogStep(logTransactionBuffer, lStepname, "")

		lExecRes := ExecOrderToQueryMicroService(1500)
		if !lExecRes.ExecIngestOK {
			eServiceResult.MessageType = "Error"
			eServiceResult.MessageText = "Pressespiegel zum Erscheinungsdatum " + lPSErscheinungsdatum + " konnte nicht aktiviert werden."
			eServiceResult.Details = "Ausführung der Indizierungs fehlgeschlagen:" + lExecRes.ExecIngestWarningErrorText
			err = errors.New("Exec Ingest Error:" + lExecRes.ExecIngestWarningErrorText)
		}
		logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "", err, u.Logger)
	}
	//}()
	////println("ePSHeader =" + ePSHeader.PSErscheinungsdatum)
	logTransactionBuffer = logResult(logTransactionBuffer, "CALL  MarkPublishable [u PSEntityClass]", "MarkPublishable:MarkPublishable", err, u.Logger)
	u.Logger.SetEndTransaction(logTransactionBuffer)
	return

}
