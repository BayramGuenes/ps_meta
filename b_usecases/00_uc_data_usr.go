package b_usecases

import (
	"ps_meta/a_domain"
	sec "ps_meta/s_security"
)

// UsrClass ...                       //
type UsrClass struct {
	Repository a_domain.PSDataBase
	Logger     a_domain.PSLogPort
}

func NewUsrClassInstanceGetter(iDatabase a_domain.PSDataBase, iLogger a_domain.PSLogPort) UsrClass {
	logger := iLogger
	return UsrClass{Repository: iDatabase, Logger: logger}
}

func (u UsrClass) GetUsr(iLoginname, iRfa, iEmail string) (eUser a_domain.User, eUserAuthkeys a_domain.UserAuthKeys, err error) {
	logTransactionBuffer := u.Logger.SetStartTransaction("GetUsr")
	lStepname := "CALL  GetUsr [u UsrClass - b_usecases]"
	logTransactionBuffer = u.Logger.LogStep(logTransactionBuffer, lStepname, "iLoginname:"+iLoginname)

	if len(iLoginname) > 0 {
		eUser, err = u.Repository.UsrDBPort.GetUsr(iLoginname, iRfa)
	} else {
		if len(iEmail) > 0 {
			eUser, err = u.Repository.UsrDBPort.GetUsrViaEmail(iEmail)
		}
	}

	if err != nil {
		logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "GetUsr", err, u.Logger)
		return
	}

	lStepname = "CALL  GetUsr-GetUsrRoles [u UsrClass - b_usecases]"
	logTransactionBuffer = u.Logger.LogStep(logTransactionBuffer, lStepname, "iLoginname:"+iLoginname)
	lUserRoles, err := u.Repository.UsrDBPort.GetUsrRoles(eUser.Loginname, eUser.Rfa)
	if err != nil {
		logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "GetUsr-GetUsrRoles", err, u.Logger)
		return
	}

	lStepname = "CALL  GetUsr-GetAuthSet [u UsrClass - b_usecases]"
	logTransactionBuffer = u.Logger.LogStep(logTransactionBuffer, lStepname, "iLoginname:"+iLoginname)

	for i := 0; i < len(lUserRoles); i++ {
		lAuthSeterr, err := u.Repository.AuthDBPort.GetAuthSet(lUserRoles[i])
		if err != nil {
			logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "GetUsr-GetAuthSet", err, u.Logger)
			return eUser, a_domain.UserAuthKeys{}, err
		}
		eUserAuthkeys = append(eUserAuthkeys, lAuthSeterr...)
	}

	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "GetUsr", err, u.Logger)
	u.Logger.SetEndTransaction(logTransactionBuffer)
	return
}

func (u UsrClass) GetUsrViaToken(iToken string, iKindOfAuth string) (eUser a_domain.User, eUserAuthkeys a_domain.UserAuthKeys, err error) {

	eUser = a_domain.User{}
	eUserAuthkeys = a_domain.UserAuthKeys{}
	lStepname := ""
	lUserRoles := []string{}
	err = *new(error)
	if len(iToken) == 0 {
		return
	}
	if len(iToken) > 0 {

		logTransactionBuffer := u.Logger.SetStartTransaction("GetUsrViaToken")

		lStepname = "CALL  sec.GetUsrInfoFromToken(iToken)"
		logTransactionBuffer = u.Logger.LogStep(logTransactionBuffer, lStepname, "iToken:"+iToken)

		eUser, lUserRoles, err = GetUsrInfoFromToken(iToken, iKindOfAuth, u.Repository.UsrDBPort)
		if err != nil {
			logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "GetUsrViaToken", err, u.Logger)
			return
		}

		if err == nil {
			lStepname = "CALL  GetUsrViaToken-GetAuthSet [u UsrClass - b_usecases]"
			logTransactionBuffer = u.Logger.LogStep(logTransactionBuffer, lStepname, "iLoginname:"+eUser.Loginname)

			for i := 0; i < len(lUserRoles); i++ {
				lAuthSeterr, err := u.Repository.AuthDBPort.GetAuthSet(lUserRoles[i])
				if err != nil {
					logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "GetUsrViaToken-GetAuthSet", err, u.Logger)
					return eUser, a_domain.UserAuthKeys{}, err
				}
				eUserAuthkeys = append(eUserAuthkeys, lAuthSeterr...)
			}

			logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "GetUsrViaToken", err, u.Logger)
			u.Logger.SetEndTransaction(logTransactionBuffer)
		}
	}
	return
}

func (u UsrClass) CreateUsr(
	iLoginname,
	iRfa,
	iName,
	iPwd,
	iEmail string,
	iCreateComp string) (ok bool, err error) {

	logTransactionBuffer := u.Logger.SetStartTransaction("CreateUsr")

	lStepname := "CALL  CreateUsr [u UsrClass - b_usecases]"

	logTransactionBuffer = u.Logger.LogStep(logTransactionBuffer, lStepname, "iLoginname:"+iLoginname)

	var lUser a_domain.User
	lUser.Name = iName
	lUser.Loginname = iLoginname
	lUser.Rfa = iRfa
	lUser.Email = iEmail

	err = u.Repository.UsrDBPort.CreateUser(
		iLoginname, iRfa, iName, iPwd, iEmail, iLoginname, iCreateComp)

	if err == nil {

	}
	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "CreateUsr", err, u.Logger)
	u.Logger.SetEndTransaction(logTransactionBuffer)
	return
}

func (u UsrClass) LoginUsr(iLoginname, iRfa, iEmail string) (eUser a_domain.User, eUserToken a_domain.UserToken, eUserAuthkeys a_domain.UserAuthKeys, err error) {
	eUserToken = a_domain.UserToken{}
	err = nil

	logTransactionBuffer := u.Logger.SetStartTransaction("LoginUsr")

	lStepname := "CALL LoginUsr [u UsrClass - b_usecases]"
	logTransactionBuffer = u.Logger.LogStep(logTransactionBuffer, lStepname, "iLoginname:"+iLoginname)

	eUser, eUserAuthkeys, err = u.GetUsr(iLoginname, iRfa, iEmail)
	if err != nil {
		logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "LoginUsr", err, u.Logger)
		return
	}

	//lData := sec.TestTokenStruct{Loginname: lUser.Loginname, Rfa: lUser.Rfa}
	if len(eUser.Loginname) > 0 {
		lToken := sec.CreateToken(iLoginname, iRfa)
		lValidFrom, lValidTo := sec.GetValidFromTo()

		u.Repository.UsrDBPort.SetUserToken(eUser.Loginname, eUser.Rfa, lToken, lValidFrom, lValidTo)
		lStepname = "CALL LoginUsr SetUserToken [u UsrClass - b_usecases]"
		logTransactionBuffer = u.Logger.LogStep(logTransactionBuffer, lStepname, "iLoginname:"+iLoginname)

		if err != nil {
			logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "iLoginname", err, u.Logger)
			return
		}
		logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "LoginUsr", err, u.Logger)
		eUserToken.Loginname = iLoginname
		eUserToken.Rfa = iRfa
		eUserToken.Token = lToken
		eUserToken.Validdatefrom = lValidFrom
		eUserToken.Validdateto = lValidTo

	}
	u.Logger.SetEndTransaction(logTransactionBuffer)
	return eUser, eUserToken, eUserAuthkeys, err
}

func (u UsrClass) GetNewToken(iToken string) (eToken string, err error) {
	eToken = ""
	err = nil
	logTransactionBuffer := u.Logger.SetStartTransaction("GetNewToken")

	lStepname := "CALL GetNewToken [u UsrClass - b_usecases]"
	logTransactionBuffer = u.Logger.LogStep(logTransactionBuffer, lStepname, "iToken:"+iToken)

	lUser, err := u.Repository.UsrDBPort.GetUsrViaToken(iToken)
	if err != nil {
		logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "GetNewToken", err, u.Logger)
		return
	}

	//lData := sec.TestTokenStruct{Loginname: lUser.Loginname, Rfa: lUser.Rfa}
	eToken = sec.CreateToken(lUser.Loginname, lUser.Rfa)
	lValidFrom, lValidTo := sec.GetValidFromTo()

	u.Repository.UsrDBPort.SetUserTokenViaToken(eToken, lValidFrom, lValidTo)

	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "GetNewToken", err, u.Logger)
	u.Logger.SetEndTransaction(logTransactionBuffer)
	return eToken, err
}

func (u UsrClass) UpdateUsr(
	iUser a_domain.User) (ok bool, err error) {

	logTransactionBuffer := u.Logger.SetStartTransaction("UpdateUsr")

	lStepname := "CALL  UpdateUsr [u UsrClass - b_usecases]"
	logTransactionBuffer = u.Logger.LogStep(logTransactionBuffer, lStepname, "iLoginname:"+iUser.Loginname)

	err = u.Repository.UsrDBPort.UpdateUser(iUser, iUser.Loginname, "web")
	if err == nil {
	}
	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "GetUsr", err, u.Logger)
	u.Logger.SetEndTransaction(logTransactionBuffer)
	return
}

func (u UsrClass) DeactivateUsr(
	iLoginname string, iRfa string, iTriggeredbyusr string, iTriggeredbycomp string) (ok bool, err error) {

	logTransactionBuffer := u.Logger.SetStartTransaction("DeactivateUsr")

	lStepname := "CALL DeactivateUsr [u UsrClass - b_usecases]"
	logTransactionBuffer = u.Logger.LogStep(logTransactionBuffer, lStepname, "iLoginname:"+iLoginname)

	err = u.Repository.DeactivateUser(iLoginname, iRfa, iTriggeredbyusr, iTriggeredbycomp)
	if err == nil {

	}
	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "DeactivateUsr", err, u.Logger)
	u.Logger.SetEndTransaction(logTransactionBuffer)
	return
}

func (u UsrClass) ActivateUsr(
	iLoginname string, iRfa string, iTriggeredbyusr string, iTriggeredbycomp string) (ok bool, err error) {

	logTransactionBuffer := u.Logger.SetStartTransaction("ActivateUsr")

	lStepname := "CALL ActivateUsr [u UsrClass - b_usecases]"
	logTransactionBuffer = u.Logger.LogStep(logTransactionBuffer, lStepname, "iLoginname:"+iLoginname)

	err = u.Repository.ActivateUser(iLoginname, iRfa, iTriggeredbyusr, iTriggeredbycomp)
	if err == nil {

	}
	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "ActivateUsr", err, u.Logger)
	u.Logger.SetEndTransaction(logTransactionBuffer)
	return
}

func (u UsrClass) AssignExtendedSearchRights(iLoginname, iRfa, iEmail string) (ok bool, err error) {
	lUser, _, err := u.GetUsr(iLoginname, iRfa, iEmail)
	//lUser.HasExtendedSearchRights = true
	ok, err = u.UpdateUsr(lUser)
	return ok, err
}

func (u UsrClass) AssignExpertRights(iLoginname, iRfa, iEmail string) (ok bool, err error) {
	lUser, _, err := u.GetUsr(iLoginname, iRfa, iEmail)
	//lUser.HasExpertRights = true
	ok, err = u.UpdateUsr(lUser)
	return ok, err
}

func (u UsrClass) SubductExtendedSearchRights(iLoginname, iRfa, iEmail string) (ok bool, err error) {
	lUser, _, err := u.GetUsr(iLoginname, iRfa, iEmail)
	//lUser.HasExtendedSearchRights = false
	ok, err = u.UpdateUsr(lUser)
	return ok, err
}

func (u UsrClass) SubductExpertRights(iLoginname, iRfa, iEmail string) (ok bool, err error) {
	lUser, _, err := u.GetUsr(iLoginname, iRfa, iEmail)
	//lUser.HasExpertRights = false
	ok, err = u.UpdateUsr(lUser)
	return ok, err
}
