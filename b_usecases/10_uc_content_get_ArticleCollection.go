package b_usecases

import (
	"errors"
	"ps_meta/a_domain"
)

// ArticleCollClass ...                       //
type ArticleCollClass struct {
	PSDataBase a_domain.PSDataBase
	Logger     a_domain.PSLogPort
}

func NewArticleCollGetter(iDatabase a_domain.PSDataBase, iLogger a_domain.PSLogPort) ArticleCollClass {
	logger := iLogger
	return ArticleCollClass{PSDataBase: iDatabase, Logger: logger}
}

func (a ArticleCollClass) GetArticleCollection(iToken string, iKindOfAuth string, iErscheinungsdatum string) (ePSHeader a_domain.Pressespiegel, eCollection []a_domain.Artikel, err error) {
	logTransactionBuffer := a.Logger.SetStartTransaction("GetArticleCollection")

	lStepname := "CALL  GetArticleCollection: GetUsrInfoFromToken [a ArticleCollClass - b_usecases]"
	logTransactionBuffer = a.Logger.LogStep(logTransactionBuffer, lStepname, "iErscheinungsdatum:"+iErscheinungsdatum)
	err = *new(error)
	lWithInactiv := false
	if len(iToken) > 0 {
		_, lUserRoles, err := GetUsrInfoFromToken(iToken, iKindOfAuth, a.PSDataBase.UsrDBPort)
		if err == nil {
			lWithInactiv = a_domain.CheckAuthority(a_domain.AuthSearchWithInactive, lUserRoles, a.PSDataBase.AuthDBPort,
				a.Logger)
		}
	}
	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "GetUsrInfoFromToken", err, a.Logger)
	if err == nil {

		ePSHeader, err = a.PSDataBase.PSDBPort.GetPS(iErscheinungsdatum, lWithInactiv)

		if err == nil {
			if len(ePSHeader.PSExtID) > 0 {
				eCollection, err = a.PSDataBase.ArticleDBPort.GetArticleCollection(ePSHeader.PSExtID)
			} else {
				err = errors.New("Es gibt kein Pressespiegel zum Erscheinungsdatum " + iErscheinungsdatum)
			}
		}

		logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "GetArticleCollection", err, a.Logger)
	}
	a.Logger.SetEndTransaction(logTransactionBuffer)
	return
}
