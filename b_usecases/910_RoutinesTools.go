package b_usecases

import (
	"ps_meta/a_domain"
	model "ps_meta/a_domain"
	sec "ps_meta/s_security"
	"strings"
)

func GetUsrInfoFromToken(iToken string, iKindOfAuth string, iUsrRepo a_domain.UserDataBasePort) (eUser model.User,
	eUserRoles model.UserRoles, err error) {
	eUser = model.User{}
	err = *new(error)
	eUserRoles = model.UserRoles{}
	lPreferredName, eUserRoles, err := sec.GetUsrInfoFromToken(iToken, iKindOfAuth)
	if err != nil {
		//println("err != nil && boot.ApplConf.StrictTokenValidateOn ")
		return model.User{}, model.UserRoles{}, err
	}
	if len(lPreferredName) == 0 {
		eUser, err = iUsrRepo.GetUsrViaToken(iToken)
		if err == nil {
			eUserRoles, err = iUsrRepo.GetUsrRoles(eUser.Loginname, eUser.Rfa)
		}
	} else {
		eUser.Loginname = lPreferredName
	}
	return
}

func extractImageName(iImageURI string) string {
	////println("extractImageName:"+iImageURI)
	splittedString := strings.Split(iImageURI, "/")
	lenArr := len(splittedString)
	////println("lenArr:"+strconv.Itoa(lenArr))

	if lenArr > 0 {
		lenArr = lenArr - 1
		////println("extractImageName:"+splittedString[lenArr])

		return splittedString[lenArr]
	} else {
		////println("extractImageName:")
		return ""
	}
}

func extractSetOfArticleImageName(iPressespiegelWrapper model.PressespiegelWrapper) model.ArtikelImageList {
	lArr := model.ArtikelImageList{}
	for _, artikel := range iPressespiegelWrapper.ArtikelSet {
		lImageArtikel := extractImageName(artikel.ImageURI)
		lImageInfo := model.ArtikelImage{}
		lImageInfo.ArticleEXTID = artikel.ExtID
		lImageInfo.ArticleImageName = lImageArtikel
		lImageInfo.ArticleMelEXTID = artikel.MelExtID
		lArr = append(lArr, lImageInfo)
	}

	return lArr
}

func logResult(iTransactionBuffer model.TransactionLogBufferType, iStepname string, iCompoName string, iErr error, iLogger model.PSLogPort) (eTransactionBuffer model.TransactionLogBufferType) {
	lDetail := model.ProcessingInfo{}
	lDetail.DetectingComponentName = iCompoName
	eTransactionBuffer = iTransactionBuffer
	if iErr != nil {
		lDetail.InfoDescription = iErr.Error()
		eTransactionBuffer = iLogger.LogStepResult(eTransactionBuffer, iStepname, " FAILED", lDetail)
	} else {
		lDetail.InfoDescription = iCompoName

		eTransactionBuffer = iLogger.LogStepResult(eTransactionBuffer, iStepname, " OK", lDetail)
	}
	return
}
func formatDateYYYYminMMminDD(iDate string) string {

	var result = ""
	////println("formatDateYYYYminMMminDD=" + iDate)
	if !strings.Contains(iDate, "-") && len(iDate) >= 8 {
		result = iDate[:4] + "-" + iDate[4:6] + "-" + iDate[6:8]
		strings.TrimSpace(result)
	} else {
		result = iDate
	}
	return result
}

func normalizeTextForSearch(iString string) string {
	lNormalized := strings.ReplaceAll(iString, "ö", "oe")
	lNormalized = strings.ReplaceAll(lNormalized, "Ö", "Oe")
	lNormalized = strings.ReplaceAll(lNormalized, "ü", "ue")
	lNormalized = strings.ReplaceAll(lNormalized, "Ü", "Ue")
	lNormalized = strings.ReplaceAll(lNormalized, "ä", "ae")
	lNormalized = strings.ReplaceAll(lNormalized, "Ä", "Ae")
	return lNormalized
}
