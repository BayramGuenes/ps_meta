package b_usecases

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"ps_meta/a_domain"
	syscommanager "ps_meta/h_service_communication_manager"
	boot "ps_meta/z_bootstrap"
	"strconv"
)

// PSEIngestOrderClass ...                       //
type PSIngestOrderClass struct {
	Repository a_domain.PSDataBase
	Logger     a_domain.PSLogPort
}
type IngestOrderTransferResult struct {
	TransferOK               bool
	TransferWarningErrorText string
	TransferredData          IngestOrderList
}

type ArticleTitel struct {
	HTIT      string
	SEITE1TIT string
	UTIT      string
	SOTIT     string
}

type ArticleSTW struct {
	TITEL     ArticleTitel
	INHALTTXT string
	ABSTRACT  string
	SCHLAGW   string
}
type ArticleData struct {
	EXTID   string
	PSEXTID string
	PSESD   int
	ESD     int
	QUELLE  string
	AUTOR   string
	RUBRIK  string
	MELTXT  string
	MEDIUM  string
	TITEL   ArticleTitel
	STW     ArticleSTW
}

type ArticleIngestOrder struct {
	OP       string //C,U,D (Create,Update,Delete)
	PRIO     int
	IGSEXTID string
	Article  ArticleData
}
type IngestOrderList []ArticleIngestOrder

func BuildIngestOrder(iDatabase a_domain.PSDataBase, iLogger a_domain.PSLogPort, iPSESD string, iOp string, iPrio int) (eOrderBuildResult IngestOrderTransferResult) {
	eOrderBuildResult = IngestOrderTransferResult{TransferOK: true, TransferWarningErrorText: ""}
	ingestOrderObj := NewPSIngestOrder(iDatabase, iLogger)
	lOrderDataStruc, err := ingestOrderObj.BuildOrderData(iPSESD, iOp, iPrio)
	if err != nil {
		eOrderBuildResult.TransferOK = false
		eOrderBuildResult.TransferWarningErrorText = "BuildOrderData failed:" + err.Error()
		return
	}

	if len(lOrderDataStruc) > 0 {
		bytesJson, err := json.Marshal(lOrderDataStruc)
		if err != nil {
			eOrderBuildResult.TransferOK = false
			eOrderBuildResult.TransferWarningErrorText = "json.Marshal failed:" + err.Error()
		}
		lOrderDataBytes := bytesJson //string(bytesJson)
		eOrderBuildResult = ingestOrderObj.TransferOrderToQueryMicroService(lOrderDataBytes)
	}
	return
}

func BuildIngestOrderInterval(iDatabase a_domain.PSDataBase, iLogger a_domain.PSLogPort, iPSESDFrom, iPSESDTo string, iOp string) (eOrderBuildResult IngestOrderTransferResult) {
	eOrderBuildResult = IngestOrderTransferResult{TransferOK: true, TransferWarningErrorText: ""}
	ingestOrderObj := NewPSIngestOrder(iDatabase, iLogger)
	lOrderDataStruc, err := ingestOrderObj.BuildOrderDataOfInterval(iPSESDFrom, iPSESDTo, iOp)
	if err != nil {
		eOrderBuildResult.TransferOK = false
		eOrderBuildResult.TransferWarningErrorText = "BuildOrderData failed:" + err.Error()
		return
	}
	bytesJson, err := json.Marshal(lOrderDataStruc)
	if err != nil {
		eOrderBuildResult.TransferOK = false
		eOrderBuildResult.TransferWarningErrorText = "json.Marshal failed:" + err.Error()
	}
	lOrderDataBytes := bytesJson //string(bytesJson)
	eOrderBuildResult = ingestOrderObj.TransferOrderToQueryMicroService(lOrderDataBytes)

	return
}

func BuildIngestOrderArticle(iDatabase a_domain.PSDataBase, iLogger a_domain.PSLogPort, iPSESD, iARTEXTID string, iOp string) (eOrderBuildResult IngestOrderTransferResult) {
	eOrderBuildResult = IngestOrderTransferResult{TransferOK: true, TransferWarningErrorText: ""}
	ingestOrderObj := NewPSIngestOrder(iDatabase, iLogger)
	lOrderDataStruc, err := ingestOrderObj.BuildOrderDataArticle(iPSESD, iARTEXTID, iOp)
	if err != nil {
		eOrderBuildResult.TransferOK = false
		eOrderBuildResult.TransferWarningErrorText = "BuildOrderData failed:" + err.Error()
		return
	}
	bytesJson, err := json.Marshal(lOrderDataStruc)
	if err != nil {
		eOrderBuildResult.TransferOK = false
		eOrderBuildResult.TransferWarningErrorText = "json.Marshal failed:" + err.Error()
	}
	lOrderDataBytes := bytesJson //string(bytesJson)
	eOrderBuildResult = ingestOrderObj.TransferOrderToQueryMicroService(lOrderDataBytes)

	return
}

func NewPSIngestOrder(iDatabase a_domain.PSDataBase, iLogger a_domain.PSLogPort) PSIngestOrderClass {
	logger := iLogger //logger.GetLogger()
	return PSIngestOrderClass{Repository: iDatabase, Logger: logger}
}

func (io PSIngestOrderClass) BuildOrderData(iPSESD string, iOp string, iPrio int) (eIngestOrderList IngestOrderList, err error) {

	logTransactionBuffer := io.Logger.SetStartTransaction("BuildOrderData")

	lStepname := "CALL  BuildOrderData [io PSIngestOrderClass]"
	logTransactionBuffer = io.Logger.LogStep(logTransactionBuffer, lStepname, "iPSESD:"+iPSESD)

	lPS, err := io.Repository.PSDBPort.GetPS(iPSESD, true)
	if err != nil {
		return IngestOrderList{}, err
	}
	lListMeldung, err := io.Repository.MeldungDBPort.GetMeldungCollection(lPS.PSExtID)
	if err != nil {
		return IngestOrderList{}, err
	}

	lListArticle, err := io.Repository.ArticleDBPort.GetArticleCollection(lPS.PSExtID)
	if err != nil {
		return IngestOrderList{}, err
	}
	eIngestOrderList = IngestOrderList{}
	for i := 0; i < len(lListArticle); i++ {
		lArtikel := lListArticle[i]
		lIngestOrder := ArticleIngestOrder{}
		lIngestOrder.OP = iOp
		lIngestOrder.PRIO = iPrio
		lIngestOrder.IGSEXTID = lArtikel.ExtID
		lIngestOrder.Article = ArticleData{}
		lIngestOrder.Article.EXTID = lArtikel.ExtID
		lIngestOrder.Article.PSEXTID = lPS.PSExtID
		lPSESD, _ := strconv.Atoi(iPSESD)
		lIngestOrder.Article.PSESD = lPSESD
		lESD, _ := strconv.Atoi(lArtikel.ESD)
		lIngestOrder.Article.ESD = lESD

		lIngestOrder.Article.TITEL.HTIT = normalizeTextForSearch(lArtikel.Titel)
		lIngestOrder.Article.TITEL.SEITE1TIT = normalizeTextForSearch(lArtikel.Seite1Titel)
		lIngestOrder.Article.TITEL.UTIT = normalizeTextForSearch(lArtikel.Untertitel)
		lIngestOrder.Article.TITEL.SOTIT = normalizeTextForSearch(lArtikel.SonstTitel)

		lIngestOrder.Article.STW.TITEL = lIngestOrder.Article.TITEL
		lIngestOrder.Article.STW.INHALTTXT = normalizeTextForSearch(lArtikel.Inhalttext)
		lIngestOrder.Article.STW.ABSTRACT = normalizeTextForSearch(lArtikel.Autoabstract)
		lIngestOrder.Article.STW.SCHLAGW = normalizeTextForSearch(lArtikel.Schlagwort)

		lIngestOrder.Article.QUELLE = normalizeTextForSearch(lArtikel.QuelleKuerzel) + " " +
			normalizeTextForSearch(lArtikel.QuelleName)
		lIngestOrder.Article.AUTOR = normalizeTextForSearch(lArtikel.Autor)

		lIngestOrder.Article.RUBRIK = normalizeTextForSearch(lArtikel.PSRubrik)
		if len(lArtikel.Print_Online) > 0 {
			a := []rune(lArtikel.Print_Online)
			lIngestOrder.Article.MEDIUM = string(a[0:1])
		}

		for j := 0; j < len(lListMeldung); j++ {
			if lListMeldung[j].MelExtID == lListArticle[i].MelExtID {
				lIngestOrder.Article.MELTXT = normalizeTextForSearch(lListMeldung[j].MelInhalttext)
				break
			}
		}
		eIngestOrderList = append(eIngestOrderList, lIngestOrder)
	}

	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "BuildOrderData", err, io.Logger)
	io.Logger.SetEndTransaction(logTransactionBuffer)
	return

}

func (io PSIngestOrderClass) BuildOrderDataArticle(iPSESD, iARTEXTID string, iOp string) (eIngestOrderList IngestOrderList, err error) {

	logTransactionBuffer := io.Logger.SetStartTransaction("BuildOrderDataArticle")

	lStepname := "CALL  BuildOrderDataArticle [io PSIngestOrderClass]"
	logTransactionBuffer = io.Logger.LogStep(logTransactionBuffer, lStepname, "iPSESD:"+iPSESD+"; iARTEXTID:"+iARTEXTID)

	lPS, err := io.Repository.PSDBPort.GetPS(iPSESD, false)
	if err != nil {
		return IngestOrderList{}, err
	}
	lListMeldung, err := io.Repository.MeldungDBPort.GetMeldungCollection(lPS.PSExtID)
	if err != nil {
		return IngestOrderList{}, err
	}
	lArtikel, err := io.Repository.ArticleDBPort.GetArticle(lPS.PSExtID, iARTEXTID)
	if err != nil {
		return IngestOrderList{}, err
	}

	eIngestOrderList = IngestOrderList{}
	lIngestOrder := ArticleIngestOrder{}
	lIngestOrder.OP = iOp
	lIngestOrder.PRIO = 1
	lIngestOrder.IGSEXTID = lArtikel.ExtID
	lIngestOrder.Article = ArticleData{}
	lIngestOrder.Article.EXTID = lArtikel.ExtID
	lIngestOrder.Article.PSEXTID = lPS.PSExtID
	lPSESD, _ := strconv.Atoi(iPSESD)
	lIngestOrder.Article.PSESD = lPSESD
	lESD, _ := strconv.Atoi(lArtikel.ESD)
	lIngestOrder.Article.ESD = lESD

	lIngestOrder.Article.TITEL.HTIT = normalizeTextForSearch(lArtikel.Titel)
	lIngestOrder.Article.TITEL.SEITE1TIT = normalizeTextForSearch(lArtikel.Seite1Titel)
	lIngestOrder.Article.TITEL.UTIT = normalizeTextForSearch(lArtikel.Untertitel)
	lIngestOrder.Article.TITEL.SOTIT = normalizeTextForSearch(lArtikel.SonstTitel)

	lIngestOrder.Article.STW.TITEL = lIngestOrder.Article.TITEL
	lIngestOrder.Article.STW.INHALTTXT = normalizeTextForSearch(lArtikel.Inhalttext)
	lIngestOrder.Article.STW.ABSTRACT = normalizeTextForSearch(lArtikel.Autoabstract)
	lIngestOrder.Article.STW.SCHLAGW = normalizeTextForSearch(lArtikel.Schlagwort)

	lIngestOrder.Article.QUELLE = normalizeTextForSearch(lArtikel.QuelleKuerzel) + " " +
		normalizeTextForSearch(lArtikel.QuelleName)
	lIngestOrder.Article.AUTOR = lArtikel.Autor

	lIngestOrder.Article.RUBRIK = normalizeTextForSearch(lArtikel.PSRubrik)
	if len(lArtikel.Print_Online) > 0 {
		a := []rune(lArtikel.Print_Online)
		lIngestOrder.Article.MEDIUM = string(a[0:1])
	}
	for j := 0; j < len(lListMeldung); j++ {
		if lListMeldung[j].MelExtID == lArtikel.MelExtID {
			lIngestOrder.Article.MELTXT = normalizeTextForSearch(lListMeldung[j].MelInhalttext)
			break
		}
	}
	eIngestOrderList = append(eIngestOrderList, lIngestOrder)

	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "BuildOrderDataArticle", err, io.Logger)
	io.Logger.SetEndTransaction(logTransactionBuffer)
	return

}

func (io PSIngestOrderClass) BuildOrderDataOfInterval(iPSESDFrom, iPSESDTo string, iOp string) (eIngestOrderList IngestOrderList, err error) {

	logTransactionBuffer := io.Logger.SetStartTransaction("BuildOrderDataOfInterval")

	lStepname := "CALL  BuildOrderData [io PSIngestOrderClass]"
	logTransactionBuffer = io.Logger.LogStep(logTransactionBuffer, lStepname, "iPSESDFrom:"+iPSESDFrom+" iPSESDTo:"+iPSESDTo)

	lListESD, err := io.Repository.PSDBPort.GetPSESDsFromTo(iPSESDFrom, iPSESDTo, false)
	////println("lListESD:")
	eIngestOrderList = IngestOrderList{}
	for i := 0; i < len(lListESD); i++ {
		////println(lListESD[i])
		lOrderList, err := io.BuildOrderData(lListESD[i], iOp, 0)
		if err != nil {
			return IngestOrderList{}, err
		}
		for k := 0; k < len(lOrderList); k++ {
			eIngestOrderList = append(eIngestOrderList, lOrderList[k])
		}
	}

	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "BuildOrderDataOfInterval", err, io.Logger)
	io.Logger.SetEndTransaction(logTransactionBuffer)
	return

}

func (io PSIngestOrderClass) TransferOrderToQueryMicroService(iOrderData []byte) (eTransferResult IngestOrderTransferResult) {

	portPSQueryService := getPortPSQueryService()
	//    //println("lParamAsJson:"+lParamAsJson)
	eTransferResult = callServiceIngestPSData(iOrderData, portPSQueryService)

	return
}

func getPortPSQueryService() (ePSQueryPort string) {
	if boot.ApplConf.UsePSSysregAsRegistrator {
		lResultGetPortMS := syscommanager.ApplServiceCom.GetPortMicroservice(syscommanager.MicroServiceNamePsQuery)
		ePSQueryPort = lResultGetPortMS.Port
	} else {
		ePSQueryPort = boot.PortPSQuery
	}
	return
}

func callServiceIngestPSData(iOrderData []byte, iPortOfService string) (eTransferResult IngestOrderTransferResult) {

	eTransferResult = IngestOrderTransferResult{}

	body := []byte(iOrderData) // []byte{} //iOrderData
	serviceadr := boot.ThisCommunnicationProtokoll + "://" + boot.ServiceLocationIDPSQuery + ":" + iPortOfService + "/ps_query/getOrderBuildIngest/"
	resp, err := http.Post(serviceadr, "application/json", bytes.NewBuffer(body))
	//resp, err := http.Get(serviceadr)

	if err != nil {
		eTransferResult.TransferWarningErrorText = "Technical error: http.Post failed." + err.Error()
		eTransferResult.TransferOK = false
		return

	}
	defer resp.Body.Close()
	body, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		eTransferResult.TransferWarningErrorText = "Technical error: ioutil.ReadAll(resp.Body) failed." + err.Error()
		eTransferResult.TransferOK = false
		return
	}
	var respStruct IngestOrderTransferResult
	if err = json.Unmarshal(body, &respStruct); err != nil {
		eTransferResult.TransferWarningErrorText = "Technical error: json.Unmarshal(body, &respStruct)) failed." + err.Error()
		eTransferResult.TransferOK = false
		return
	}
	eTransferResult = respStruct
	////println("callServiceIngestPSData':eTransferResult" + eTransferResult.TransferWarningErrorText)
	return eTransferResult
}
