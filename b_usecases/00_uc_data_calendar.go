package b_usecases

import (
	"ps_meta/a_domain"
	utils "ps_meta/u_utils"
	boot "ps_meta/z_bootstrap"
	"strings"

	//"strconv"
	"time"
)

var ZEITRAUM_STANDARD_USER int //= boot.ApplConf.AnonymUsrDefaultAccessInterval

const (
	ZEITRAUM_SEARCHONEYEAR_USER int = 365
	ZEITRAUM_SEARCHUNLMTDR_USER int = 72500
)

// CalendarClass ...                       //
type CalendarClass struct {
	Database a_domain.PSDataBase
	RepoUsr  a_domain.UserDataBasePort
	RepoPS   a_domain.PSDataBasePort
	Logger   a_domain.PSLogPort
}

func NewCalendarInstanceGetter(iDatabase a_domain.PSDataBase, iLogger a_domain.PSLogPort) CalendarClass {
	logger := iLogger //logger.GetLogger()
	return CalendarClass{Database: iDatabase, RepoUsr: iDatabase.UsrDBPort, RepoPS: iDatabase.PSDBPort, Logger: logger}
}

func (c CalendarClass) GetCalendarViaOneSelect(iToken, iKindOfAuth string, iDataFromDay, iDataToDay string) (eCalendar a_domain.Calendar, err error) {
	ZEITRAUM_STANDARD_USER = boot.ApplConf.AnonymUsrDefaultAccessInterval
	//println("ZEITRAUM_STANDARD_USER:" + strconv.Itoa(ZEITRAUM_STANDARD_USER))

	lStepname := "CALL  GetCalendarViaOneSelect [c CalendarClass - b_usecases]"

	logTransactionBuffer := c.Logger.SetStartTransaction("GetCalendarViaOneSelect")

	logTransactionBuffer = c.Logger.LogStep(logTransactionBuffer, lStepname, "iToken:"+iToken)
	////println(	logTransactionBuffer,lStepname, "iToken:"+iToken)

	eCalendar = a_domain.Calendar{}

	_, lUserRoles, err := GetUsrInfoFromToken(iToken, iKindOfAuth, c.RepoUsr)
	//println("GetCalendarViaOneSelect:Token:" + iToken)
	//println("GetCalendarViaOneSelect:Roles:" + strings.Join(lUserRoles, ","))

	if err != nil {
		logTransactionBuffer = logResult(logTransactionBuffer, lStepname, " GetCalendarViaOneSelect:GetUsrInfoFromToken", err, c.Logger)
		return a_domain.Calendar{}, err
	}

	//lWithInactive := a_domain.CheckAuthority(a_domain.AuthSearchWithInactive, lUserRoles)
	lListESD := []string{}
	if err == nil {
		////println("iDataFromDay:" + iDataFromDay)
		////println("iDataToDay:" + iDataToDay)

		lStartDate := time.Time{}
		lSearchWithinOneYear := a_domain.CheckAuthority(a_domain.AuthSearchOneYear, lUserRoles, c.Database.AuthDBPort, c.Logger)
		lSearchUnlimited := a_domain.CheckAuthority(a_domain.AuthSearchUnlimited, lUserRoles, c.Database.AuthDBPort, c.Logger)
		if lSearchUnlimited {
			lStartDate = time.Now().AddDate(0, 0, -ZEITRAUM_SEARCHUNLMTDR_USER)
			//lStartDate,_=time.Parse("20060102", iDataFromDay)
		} else {
			if lSearchWithinOneYear {
				lStartDate = time.Now().AddDate(0, 0, -ZEITRAUM_SEARCHONEYEAR_USER)
			} else {
				lStartDate = time.Now().AddDate(0, 0, -ZEITRAUM_STANDARD_USER)
			}
		}

		lStartDateAsString := lStartDate.Format("20060102")

		currentDate := time.Now()
		currentDateAsString := currentDate.Format("20060102")

		lWithInactive := a_domain.CheckAuthority(a_domain.AuthSearchWithInactive, lUserRoles, c.Database.AuthDBPort, c.Logger)
		if !lSearchUnlimited {
			lListESD, err = c.Database.PSDBPort.GetPSESDsFromTo(lStartDateAsString, currentDateAsString, lWithInactive)
			for i := 0; i < len(lListESD); i++ {
				////println("lListESD[i]:" + lListESD[i])
				eCalendar.AvailableDates = append(eCalendar.AvailableDates, a_domain.CalendarDate(convertDateStringToCalFormat(lListESD[i])))
			}
		} else {
			eCalendar.AvailableDates = []a_domain.CalendarDate{}
		}
	}
	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "GetCalendarViaOneSelect:"+strings.Join(lListESD[:], ","), err, c.Logger)
	c.Logger.SetEndTransaction(logTransactionBuffer)
	return eCalendar, err
}

func (c CalendarClass) GetCalendar(iToken, iKindOfAuth, iDataFromDay, iDataToDay string) (eCalendar a_domain.Calendar, err error) {

	lStepname := "CALL  GetCalendar [c CalendarClass - b_usecases]"

	logTransactionBuffer := c.Logger.SetStartTransaction("GetCalendar")

	logTransactionBuffer = c.Logger.LogStep(logTransactionBuffer, lStepname, "iToken:"+iToken)
	////println(	logTransactionBuffer,lStepname, "iToken:"+iToken)

	eCalendar = a_domain.Calendar{}

	_, lUserRoles, err := GetUsrInfoFromToken(iToken, iKindOfAuth, c.RepoUsr)

	if err != nil {
		logTransactionBuffer = logResult(logTransactionBuffer, lStepname, " GetCalendar:GetUsrInfoFromToken", err, c.Logger)
		return a_domain.Calendar{}, err
	}

	//lWithInactive := a_domain.CheckAuthority(a_domain.AuthSearchWithInactive, lUserRoles)

	if err == nil {
		////println("iDataFromDay:" + iDataFromDay)
		////println("iDataToDay:" + iDataToDay)

		lStartDate := time.Time{}
		lSearchWithinOneYear := a_domain.CheckAuthority(a_domain.AuthSearchOneYear, lUserRoles, c.Database.AuthDBPort, c.Logger)
		lSearchUnlimited := a_domain.CheckAuthority(a_domain.AuthSearchUnlimited, lUserRoles, c.Database.AuthDBPort, c.Logger)

		if lSearchUnlimited {
			lStartDate = time.Now().AddDate(0, 0, -ZEITRAUM_SEARCHUNLMTDR_USER)
			//lStartDate,_=time.Parse("20060102", iDataFromDay)
		} else {
			if lSearchWithinOneYear {
				lStartDate = time.Now().AddDate(0, 0, -ZEITRAUM_SEARCHONEYEAR_USER)
			} else {
				lStartDate = time.Now().AddDate(0, 0, -ZEITRAUM_STANDARD_USER)
			}
		}

		lStartDateAsString := lStartDate.Format("20060102")
		if iDataFromDay != "" && utils.DateLessThan(lStartDateAsString, iDataFromDay) {
			lStartDate, _ = time.Parse("20060102", iDataFromDay)
		}

		indexDateOld := lStartDate

		toDateSelected, _ := time.Parse("20060102", iDataToDay)
		currentDate := time.Now()
		currentDateAsString := currentDate.Format("20060102")

		RunUpToDate := currentDate

		if iDataToDay != "" && utils.DateLessThan(iDataToDay, currentDateAsString) {
			RunUpToDate = toDateSelected
		}

		lRun := true
		if lSearchUnlimited {
			lRun = false //Anderes Handling !!!
		}

		for lRun {

			//if !lSearchUnlimited && !lSearchWithinOneYear {
			indexDate, isActivePS := c.getNextPSDate(indexDateOld, iToken, iKindOfAuth)
			//} else {
			//	indexDate = indexDateOld.AddDate(0, 0, 1) // next  day
			//}
			calendarDate := convertDateToCalendarFormat(indexDate)

			if !strings.Contains(indexDate.Format("2006-01-02"), "0001") {
				eCalendar.AvailableDates = append(eCalendar.AvailableDates, calendarDate)
				if isActivePS {

					////println("AvailableDates, calendarDate:" + calendarDate)
					eCalendar.ActivatedDates = append(eCalendar.ActivatedDates, calendarDate)
				}
			}
			if strings.Contains(indexDate.Format("2006-01-02"), "0001") {
				lRun = false
			}
			if indexDate.Equal(RunUpToDate) || indexDate.After(RunUpToDate) {
				lRun = false
			}
			indexDateOld = indexDate
		}

	}

	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "GetCalendar", err, c.Logger)
	c.Logger.SetEndTransaction(logTransactionBuffer)
	return eCalendar, err
}
func (c CalendarClass) getNextPSDate(indexDate time.Time, iToken string, iKindOfAuth string) (eDate time.Time, ePSIsActive bool) {
	lStepname := "CALL  GetCalendar: getNextPSDate [c CalendarClass - b_usecases]"

	logTransactionBuffer := c.Logger.SetStartTransaction("CalendarClassGetNextPSDate")
	logTransactionBuffer = c.Logger.LogStep(logTransactionBuffer, lStepname, "iToken:"+iToken)

	eDate = time.Time{}
	ps := NewPSGetter(c.Database, c.Logger)
	lvDate := indexDate.Format("20060102")

	/*_, lUserRoles, err := GetUsrInfoFromToken(iToken, iKindOfAuth, c.RepoUsr)
	if err != nil {
		logTransactionBuffer = logResult(logTransactionBuffer, lStepname, " GetCalendar:getNextPSDate:GetUsrInfoFromToken", err, c.Logger)
		return eDate, true
	}
	lWithInactive := a_domain.CheckAuthority(a_domain.AuthSearchWithInactive, lUserRoles, c.Database.AuthDBPort, c.Logger)
	*/
	lvDate, ePSIsActive, err := ps.GetDateNextPS(iToken, iKindOfAuth, lvDate)

	eDate, _ = time.Parse("20060102", lvDate)

	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, " GetCalendar:getNextPSDate", err, c.Logger)
	c.Logger.SetEndTransaction(logTransactionBuffer)
	////println("Date:"+lvDate, " ePSIsActive:"+strconv.FormatBool(ePSIsActive))
	return
}
func convertDateToCalendarFormat(indexDate time.Time) (eDate a_domain.CalendarDate) {
	eDate = a_domain.CalendarDate("")
	eDate = a_domain.CalendarDate(indexDate.Format("2006-01-02"))
	return eDate
}
func convertDateStringToCalFormat(iDateString string) (eDate string) {
	eDate = iDateString[0:4] + "-" + iDateString[4:6] + "-" + iDateString[6:8]

	return eDate
}
