package b_usecases

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
	"ps_meta/a_domain"
	model "ps_meta/a_domain"
	syscommanager "ps_meta/h_service_communication_manager"
	psutil "ps_meta/u_utils"
	utils "ps_meta/u_utils"
	boot "ps_meta/z_bootstrap"
	bootstrap "ps_meta/z_bootstrap"
	"strings"
	"time"
)

type ResultArtikel struct {
	EXTID   string
	PSEXTID string
	ESD     int
}

type ResponseStructSearchEngine struct {
	ProcessOK         bool //Info,Error
	Details           string
	CountResultsTotal string
	ListArtikel       []ResultArtikel
}

type ResponseStructGetSingleArticleData struct {
	ProcessOK   bool //Info,Error
	Details     string
	ArtikelData model.IF_psArticleTextInfo
}
type ResponseStructGetQueryArticleData struct {
	ProcessOK   bool //Info,Error
	Details     string
	ListArtikel []model.IF_psArticleTextInfo
}

type QueryItem struct {
	ItemName string
	ItemVal  string
}
type Query []QueryItem

func ExecArticleQueryPlainSearch(iDatabase a_domain.PSDataBase, iLogger a_domain.PSLogPort, iQueryParamString string, iToken string, iKindOfAuth string) (eSearchResult ResponseStructSearchEngine) {

	result := ResponseStructGetQueryArticleData{}
	queryESDFrom, queryESDTo, lErrQueryDefaultESDFrom := "", "", ""
	redirectServiceExecQuery := ""
	lToken := iToken
	lQueryString := normalizeTextForSearch(iQueryParamString)

	logTransactionBuffer := iLogger.SetStartTransaction("ArticleQueryPlainSearch")
	logTransactionBuffer = iLogger.LogStep(logTransactionBuffer, "Call-PsQuery", "QueryFor:"+lQueryString)
	resGetPort := syscommanager.ResultGetPortMicroservice{}
	if bootstrap.ApplConf.UsePSSysregAsRegistrator {
		resGetPort = syscommanager.ApplServiceCom.GetPortMicroservice(syscommanager.MicroServiceNamePsQuery)
	} else {
		resGetPort.Port = bootstrap.PortPSQuery
	}

	urlPathPsQuery := "/ps_query/execQuery/PlainSearch/"
	redirectServiceExecQuery = ":" + resGetPort.Port + urlPathPsQuery + lQueryString

	queryESDFrom, queryESDTo, logTransactionBuffer, lErrQueryDefaultESDFrom = getQueryDefaultESDFromTo(lToken, iKindOfAuth, iDatabase, iLogger, logTransactionBuffer)
	if len(lErrQueryDefaultESDFrom) > 0 {
		result.ProcessOK = false
		result.Details = "Error: getQueryDefaultESDFromTo:" + lErrQueryDefaultESDFrom
		return
	}
	redirectServiceExecQuery += "/ESDFrom/" + queryESDFrom + "/ESDTo/" + queryESDTo

	/*==========================================================*/
	/*  Call GinCallQueryRestService
	/*==========================================================*/

	//result.ProcessOK = true
	respSearchEngine := GinCallQueryRestService(redirectServiceExecQuery)
	lDetail := model.ProcessingInfo{}
	lDetail.DetectingComponentName = "GinCallQueryRestServicePOST"
	if respSearchEngine.ProcessOK {
		lDetail.InfoDescription = ": Count results:" + respSearchEngine.CountResultsTotal
		logTransactionBuffer = iLogger.LogStepResult(logTransactionBuffer, "Call-PsQuery", "OK", lDetail)

	} else {
		//result.ProcessOK = false
		//result.Details = respSearchEngine.Details
		logTransactionBuffer = iLogger.LogStepResult(logTransactionBuffer, "Call-PsQuery", "FAILED", lDetail)

		//println("respSearchEngine.Details:" + respSearchEngine.Details)

	}

	eSearchResult = respSearchEngine
	iLogger.SetEndTransaction(logTransactionBuffer)
	return

}

func ExecArticleQueryExtendedSearch(iDatabase a_domain.PSDataBase, iLogger a_domain.PSLogPort, iQueryParamString string, iToken string, iKindOfAuth string) (eSearchResult ResponseStructSearchEngine) {

	result := ResponseStructGetQueryArticleData{}
	redirectServiceExecQuery := ""
	lQueryString := normalizeTextForSearch(iQueryParamString)

	logTransactionBuffer := iLogger.SetStartTransaction("ArticleQuery ExtendedSearch")
	logTransactionBuffer = iLogger.LogStep(logTransactionBuffer, "Call-PsQuery", "QueryFor:"+lQueryString)
	resGetPort := syscommanager.ResultGetPortMicroservice{}
	if bootstrap.ApplConf.UsePSSysregAsRegistrator {
		resGetPort = syscommanager.ApplServiceCom.GetPortMicroservice(syscommanager.MicroServiceNamePsQuery)
	} else {
		resGetPort.Port = bootstrap.PortPSQuery
	}

	queryMaxESDFrom, queryMaxESDTo, logTransactionBuffer, lErrQueryDefaultESDFrom := getQueryDefaultESDFromTo(iToken, iKindOfAuth, iDatabase, iLogger, logTransactionBuffer)
	if len(lErrQueryDefaultESDFrom) > 0 {
		result.ProcessOK = false
		result.Details = "Error: getQueryDefaultESDFromTo:" + lErrQueryDefaultESDFrom
		return
	}

	lQueryString = strings.ReplaceAll(lQueryString, "QueryFor:", "")
	queries := strings.Split(lQueryString, "&")

	thisQuery := Query{}
	for _, query := range queries {
		fiedNameVal := strings.Split(query, "=")
		fieldName := fiedNameVal[0]
		fieldVal := fiedNameVal[1]
		//println("fieldName :" + fiedNameVal[0] + "fieldVal:" + fiedNameVal[1])
		fieldVal, _ = url.QueryUnescape(fieldVal)
		thisQueryItem := QueryItem{}
		thisQueryItem.ItemName = fieldName
		thisQueryItem.ItemVal = fieldVal
		if fieldName == "dateFrom" || fieldName == "dateTo" {
			thisQueryItem.ItemVal = utils.DateDD_MM_JJJJToYYYYMMDD(thisQueryItem.ItemVal)

		}

		thisQuery = append(thisQuery, thisQueryItem) ////println(fieldName + "=" + fieldVal)
	}
	thisQuery = append(thisQuery, QueryItem{ItemName: "USERMAXESDFROM", ItemVal: queryMaxESDFrom})
	thisQuery = append(thisQuery, QueryItem{ItemName: "USERMAXESDTO", ItemVal: queryMaxESDTo})

	urlPathPsQuery := "/ps_query/execQuery/ExtendedSearch/"
	redirectServiceExecQuery = ":" + resGetPort.Port + urlPathPsQuery //+ lQueryString

	/*==========================================================*/
	/*  Call GinCallQueryRestService
	/*==========================================================*/

	result.ProcessOK = true
	respSearchEngine := GinCallQueryRestServiceExtSearch(redirectServiceExecQuery, thisQuery)
	lDetail := model.ProcessingInfo{}
	lDetail.DetectingComponentName = "GinCallQueryRestServicePOST"
	if respSearchEngine.ProcessOK {
		lDetail.InfoDescription = ": Count results:" + respSearchEngine.CountResultsTotal
		logTransactionBuffer = iLogger.LogStepResult(logTransactionBuffer, "Call-PsQuery", "OK", lDetail)

	} else {
		logTransactionBuffer = iLogger.LogStepResult(logTransactionBuffer, "Call-PsQuery", "FAILED", lDetail)
		result.ProcessOK = false
		result.Details = respSearchEngine.Details
	}

	eSearchResult = respSearchEngine
	iLogger.SetEndTransaction(logTransactionBuffer)
	return

}

func GinCallQueryRestServiceExtSearch(iServiceUrl string, iQuery Query) (eResult ResponseStructSearchEngine) {
	////println("GinCallRestService:lServiceUrl" + iServiceUrl)

	eResult = ResponseStructSearchEngine{}
	//client := &http.Client{}
	//lServiceUrl := bootstrap.ThisServerAddr + iServiceUrl
	lServiceUrl := bootstrap.ThisCommunnicationProtokoll + "://" + bootstrap.ServiceLocationIDPSQuery + iServiceUrl

	/*for i := 0; i < len(iQuery); i++ {
		println(iQuery[i].ItemName + "=" + iQuery[i].ItemVal)
	}*/
	b, err := json.Marshal(iQuery)
	if err != nil {
		eResult.ProcessOK = false //Info,Error
		eResult.Details = err.Error()
		return eResult
	}
	resp, err := http.Post(lServiceUrl, "application/json", bytes.NewBuffer(b)) //<--
	defer resp.Body.Close()
	if err != nil {
		eResult.ProcessOK = false //Info,Error
		eResult.Details = err.Error()
		return eResult
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		eResult.ProcessOK = false //Info,Error
		eResult.Details = err.Error()
		return eResult
	}

	err = json.Unmarshal(body, &eResult)
	////println("string(body):" + string(body))
	if err != nil {
		eResult.ProcessOK = false //Info,Error
		eResult.Details = err.Error()
		return eResult
	}

	return eResult
}

func getQueryDefaultESDFromTo(iToken string, iKindOfAuth string, iDatabase a_domain.PSDataBase, iLogger a_domain.PSLogPort, iTransactionLog model.TransactionLogBufferType) (
	eESDFrom, eESDTo string, eTransactionLog model.TransactionLogBufferType, errString string) {

	currentDate := time.Now()
	eESDTo = currentDate.Format("20060102")
	errString = ""

	lStepname := "CALL  ArticleQuery:GetUsrInfoFromToken [u uc_query_execSearch.]"
	logTransactionBuffer := iTransactionLog
	logTransactionBuffer = iLogger.LogStep(logTransactionBuffer, lStepname, " iToken:"+iToken)
	lUser, lUserRoles, err := GetUsrInfoFromToken(iToken, iKindOfAuth, iDatabase.UsrDBPort)

	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "ArticleQuery:GetUsrRoles", err, iLogger)
	if err != nil {
		return "", "", logTransactionBuffer, "Benutzerrolle konnte nicht identifiziert werden:" + err.Error()
	}
	if err == nil {
		lStepname = "CALL  ArticleQuery:CheckAuthority  [u uc_query_execSearch.]"
		logTransactionBuffer = iLogger.LogStep(logTransactionBuffer, lStepname, " lUser.Loginname:"+lUser.Loginname)

		lAuthorisedSearchUnlimited := a_domain.CheckAuthority(a_domain.AuthSearchUnlimited, lUserRoles, iDatabase.AuthDBPort, iLogger)
		lAuthorisedSearchOneYear := a_domain.CheckAuthority(a_domain.AuthSearchOneYear, lUserRoles, iDatabase.AuthDBPort, iLogger)
		if lAuthorisedSearchUnlimited {
			logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "Authority: SearchUnlimited", err, iLogger)
			eESDFrom = "00000000"
		} else if lAuthorisedSearchOneYear {
			logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "Authority: SearchOneYear", err, iLogger)
			eESDFrom = psutil.DateBeforeOneYear()
		} else {
			logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "Authority: SearchOneMonth/Default", err, iLogger)
			eESDFrom = psutil.DateBeforeNumDays(boot.ApplConf.AnonymUsrDefaultAccessInterval)
		}

	}

	eTransactionLog = logTransactionBuffer
	return
}

func GinCallQueryRestService(iServiceUrl string) (eResult ResponseStructSearchEngine) {
	////println("GinCallRestService:lServiceUrl" + iServiceUrl)

	eResult = ResponseStructSearchEngine{}
	//client := &http.Client{}
	//lServiceUrl := bootstrap.ThisServerAddr + iServiceUrl
	lServiceUrl := bootstrap.ThisCommunnicationProtokoll + "://" + bootstrap.ServiceLocationIDPSQuery + iServiceUrl
	////println(lServiceUrl)
	resp, err := http.Get(lServiceUrl) //<--
	if err != nil {
		eResult.ProcessOK = false //Info,Error
		eResult.Details = err.Error()
		return eResult
	}

	////println(lServiceUrl)

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		//println("err:" + err.Error())
		eResult.ProcessOK = false //Info,Error
		eResult.Details = err.Error()
		return eResult
	}
	err = json.Unmarshal(body, &eResult)
	////println("string(body):" + string(body))
	if err != nil {
		eResult.ProcessOK = false //Info,Error
		eResult.Details = err.Error()
		return eResult
	}
	return eResult
}
