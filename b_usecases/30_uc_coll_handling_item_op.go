package b_usecases

import (
	"errors"
	utils "ps_meta/u_utils"
)

func (col PSCollectionClass) HandleNewCollItem(iToken, iKindOfAuth, iItemIDField1, iItemIDField2 string) (eHandleResult CollectionItemOrderTransferResult) {

	eHandleResult = CollectionItemOrderTransferResult{}
	logTransactionBuffer := col.Logger.SetStartTransaction("CollectionItemNew")
	lStepname := "CALL CollectionItemNew:GetUsrInfoFromToken [u uc_collection_HandleNewCollItem]"
	logTransactionBuffer = col.Logger.LogStep(logTransactionBuffer, lStepname, " iToken:"+iToken)

	lUser, _, err := GetUsrInfoFromToken(iToken, iKindOfAuth, col.PSDataBase.UsrDBPort)
	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "CollectionItemNew:GetUsrInfoFromToken", err, col.Logger)
	if err != nil {
		eHandleResult.TransferWarningErrorText = "Benutzer konnte nicht identifiziert werden:" + err.Error()
		return
	}

	lStepname = "CALL CollectionItemNew:GinCallGetActiveCollRestService [u uc_collection_HandleNewCollItem]"
	logTransactionBuffer = col.Logger.LogStep(logTransactionBuffer, lStepname, " Usr:"+lUser.Loginname)
	lResGetActive := GinCallGetActiveCollRestService(lUser.Loginname)
	if !lResGetActive.ReceiveOK {
		err = errors.New(lResGetActive.ReceiveWarningErrorText)
	}
	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "CollectionItemNew:GinCallGetActiveCollRestService ", err, col.Logger)
	if err != nil {
		eHandleResult.TransferWarningErrorText = "Aktive Kollektion konnte nicht identifiziert werden:" + err.Error()
		return
	}

	lStepname = "CALL CollectionItemNew:GinCallCollItemOrderRestService [u uc_collection_HandleNewCollItem]"
	logTransactionBuffer = col.Logger.LogStep(logTransactionBuffer, lStepname, " Collname:"+lResGetActive.Collection.Collectionname)
	lColItemOrder := CollectionItemOrder{}
	lColItemOrder.Ordertype = COColItemNew
	lColItemOrder.Collection = lResGetActive.Collection
	lColItemOrder.Item.ItemIdField1 = iItemIDField1
	lColItemOrder.Item.ItemIdField2 = iItemIDField2
	eHandleResult = GinCallCollItemOrderRestService(lColItemOrder)
	if !eHandleResult.TransferOK {
		err = errors.New(eHandleResult.TransferWarningErrorText)
	}
	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "CollectionItemNew:GinCallCollItemOrderRestService", err, col.Logger)

	col.Logger.SetEndTransaction(logTransactionBuffer)

	return eHandleResult

}
func (col PSCollectionClass) HandleDelCollItem(iToken, iKindOfAuth, iItemIDField1, iItemIDField2 string) (eHandleResult CollectionItemOrderTransferResult) {
	eHandleResult = CollectionItemOrderTransferResult{}
	logTransactionBuffer := col.Logger.SetStartTransaction("CollectionItemDelete")
	lStepname := "CALL CollectionItemDelete:GetUsrInfoFromToken [u uc_collection_HandleDelCollItem]"
	logTransactionBuffer = col.Logger.LogStep(logTransactionBuffer, lStepname, " iToken:"+iToken)

	lUser, _, err := GetUsrInfoFromToken(iToken, iKindOfAuth, col.PSDataBase.UsrDBPort)
	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "CollectionItemDelete:GetUsrInfoFromToken", err, col.Logger)
	if err != nil {
		eHandleResult.TransferWarningErrorText = "Benutzer konnte nicht identifiziert werden:" + err.Error()
		return
	}

	lStepname = "CALL CollectionItemDelete:GinCallGetActiveCollRestService [u uc_collection_HandleDelCollItem]"
	logTransactionBuffer = col.Logger.LogStep(logTransactionBuffer, lStepname, " Usr:"+lUser.Loginname)
	lResGetActive := GinCallGetActiveCollRestService(lUser.Loginname)
	if !lResGetActive.ReceiveOK {
		err = errors.New(lResGetActive.ReceiveWarningErrorText)
	}
	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "CollectionItemDelete:GinCallGetActiveCollRestService ", err, col.Logger)
	if err != nil {
		eHandleResult.TransferWarningErrorText = "Aktive Kollektion konnte nicht identifiziert werden:" + err.Error()
		return
	}

	lStepname = "CALL CollectionItemDelete:GinCallCollItemOrderRestService [u uc_collection_HandleDelCollItem]"
	logTransactionBuffer = col.Logger.LogStep(logTransactionBuffer, lStepname, " Collname:"+lResGetActive.Collection.Collectionname)
	lColItemOrder := CollectionItemOrder{}
	lColItemOrder.Ordertype = COColItemDel
	lColItemOrder.Collection = lResGetActive.Collection
	lColItemOrder.Item.ItemIdField1 = iItemIDField1
	lColItemOrder.Item.ItemIdField2 = iItemIDField2
	eHandleResult = GinCallCollItemOrderRestService(lColItemOrder)
	if !eHandleResult.TransferOK {
		err = errors.New(eHandleResult.TransferWarningErrorText)
	}
	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "CollectionItemDelete:GinCallCollItemOrderRestService", err, col.Logger)

	col.Logger.SetEndTransaction(logTransactionBuffer)

	return eHandleResult

}

func (col PSCollectionClass) HandleDelCollItemList(iToken, iKindOfAuth string, iItemlist CollectionItemList) (
	eHandleResult CollectionItemListOrderTransferResult) {
	eHandleResult = CollectionItemListOrderTransferResult{}

	logTransactionBuffer := col.Logger.SetStartTransaction("CollectionItemListDelete")
	lStepname := "CALL CollectionItemListDelete:GetUsrInfoFromToken [u uc_collection_HandleDelCollItemList]"
	logTransactionBuffer = col.Logger.LogStep(logTransactionBuffer, lStepname, " iToken:"+iToken)

	lUser, _, err := GetUsrInfoFromToken(iToken, iKindOfAuth, col.PSDataBase.UsrDBPort)
	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "CollectionItemListDelete:GetUsrInfoFromToken", err, col.Logger)
	if err != nil {
		eHandleResult.TransferWarningErrorText = "Benutzer konnte nicht identifiziert werden:" + err.Error()
		return
	}

	lStepname = "CALL CollectionItemListDelete:GinCallGetActiveCollRestService [u uc_collection_HandleDelCollItemList]"
	logTransactionBuffer = col.Logger.LogStep(logTransactionBuffer, lStepname, " Usr:"+lUser.Loginname)
	lResGetActive := GinCallGetActiveCollRestService(lUser.Loginname)
	if !lResGetActive.ReceiveOK {
		err = errors.New(lResGetActive.ReceiveWarningErrorText)
	}
	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "CollectionItemListDelete:GinCallGetActiveCollRestService ", err, col.Logger)
	if err != nil {
		eHandleResult.TransferWarningErrorText = "Aktive Kollektion konnte nicht identifiziert werden:" + err.Error()
		return
	}

	lStepname = "CALL CollectionItemListDelete:GinCallCollItemListOrderRestSer( [u uc_collection_HandleDelCollItemList]"
	logTransactionBuffer = col.Logger.LogStep(logTransactionBuffer, lStepname, " Collname:"+lResGetActive.Collection.Collectionname)
	lColItemListOrder := CollectionItemListOrder{}
	lColItemListOrder.Ordertype = COColItemDel
	lColItemListOrder.Collection = lResGetActive.Collection
	for i := 0; i < len(iItemlist); i++ {
		lColItemListOrder.Itemlist = append(lColItemListOrder.Itemlist, iItemlist[i])
	}
	eHandleResult = GinCallCollItemListOrderRestSer(lColItemListOrder)
	if !eHandleResult.TransferOK {
		err = errors.New(eHandleResult.TransferWarningErrorText)
	}
	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "CollectionItemListDelete:GinCallCollItemListOrderRestSer", err, col.Logger)

	col.Logger.SetEndTransaction(logTransactionBuffer)

	return eHandleResult

}

func (col PSCollectionClass) HandleGetItems(iToken, iKindOfAuth string) (eReceiveResult CollectionGetItemListResult) {
	eReceiveResult = CollectionGetItemListResult{}
	logTransactionBuffer := col.Logger.SetStartTransaction("CollectionGetItems")
	lStepname := "CALL CollectionGetItems:GetUsrInfoFromToken [u uc_collection_HandleGetItems]"
	logTransactionBuffer = col.Logger.LogStep(logTransactionBuffer, lStepname, " iToken:... ") //+iToken)

	lUser, _, err := GetUsrInfoFromToken(iToken, iKindOfAuth, col.PSDataBase.UsrDBPort)
	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "CollectionGetItems:GetUsrInfoFromToken:lUser.Loginname:"+lUser.Loginname, err, col.Logger)
	if err != nil {
		eReceiveResult.ReceiveWarningErrorText = "Benutzer konnte nicht identifiziert werden:" + err.Error()
		return
	}

	lStepname = "CALL CollectionGetItems:GinCallGetActiveCollRestService [u uc_collection_HandleGetItems]"
	logTransactionBuffer = col.Logger.LogStep(logTransactionBuffer, lStepname, " Usr:"+lUser.Loginname)
	lResGetActive := GinCallGetActiveCollRestService(lUser.Loginname)
	if !lResGetActive.ReceiveOK {
		err = errors.New(lResGetActive.ReceiveWarningErrorText)
	}
	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "CollectionGetItems:GinCallGetActiveCollRestService ", err, col.Logger)
	if err != nil {
		eReceiveResult.ReceiveWarningErrorText = "Aktive Kollektion konnte nicht identifiziert werden:" + err.Error()
		return
	}

	lStepname = "CALL CollectionGetItems:GinCallGetItemListRestService [u uc_collection_HandleGetItems]"
	logTransactionBuffer = col.Logger.LogStep(logTransactionBuffer, lStepname, " Collname:"+lResGetActive.Collection.Collectionname)
	eReceiveResult = GinCallGetItemListRestService(lResGetActive.Collection)

	if !eReceiveResult.ReceiveOK {
		err = errors.New(eReceiveResult.ReceiveWarningErrorText)
	}
	logTransactionBuffer = logResult(logTransactionBuffer, lStepname, "CollectionGetItems:GinCallGetItemListRestService", err, col.Logger)

	col.Logger.SetEndTransaction(logTransactionBuffer)

	return eReceiveResult

}
func (col PSCollectionClass) HandleGetCollArticleList(iToken string, iKindOfAuth string) (eReceiveResult CollectionGetArticleListResult) {
	eReceiveResult = CollectionGetArticleListResult{}
	eReceiveResult.CollectionGetItemListResult = col.HandleGetItems(iToken, iKindOfAuth)
	if !eReceiveResult.CollectionGetItemListResult.ReceiveOK {
		return
	}
	for i := 0; i < len(eReceiveResult.CollectionGetItemListResult.Itemlist); i++ {
		lItem := eReceiveResult.CollectionGetItemListResult.Itemlist[i]
		a := NewArticleGetter(col.PSDataBase, col.Logger)

		lArtikel, err := a.RepoArticle.GetArtViaIDAndESD(lItem.ItemIdField1, lItem.ItemIdField2)

		if err != nil {
			eReceiveResult.ReceiveOK = false
			eReceiveResult.ReceiveWarningErrorText = "Failed:a.RepoArticle.GetArtViaIDAndESD:" + err.Error()
			return
		}
		lFormattedArt := utils.FormatArticle(lArtikel)
		eReceiveResult.Articlelist = append(eReceiveResult.Articlelist, lFormattedArt)
	}
	return

}

func (col PSCollectionClass) HandleGetItemlistArticles(iItemlist CollectionItemList) (eReceiveResult CollectionGetArticleListResult) {
	eReceiveResult = CollectionGetArticleListResult{}
	for i := 0; i < len(iItemlist); i++ {
		lItem := iItemlist[i]
		a := NewArticleGetter(col.PSDataBase, col.Logger)
		//println("HandleGetItemlistArticles:"+lItem.ItemIdField1, lItem.ItemIdField2)

		lArtikel, err := a.RepoArticle.GetArtViaIDAndESD(lItem.ItemIdField1, lItem.ItemIdField2)

		if err != nil {
			//println("err")
			eReceiveResult.ReceiveOK = false
			eReceiveResult.ReceiveWarningErrorText = "Failed:a.RepoArticle.GetArtViaIDAndESD:" + err.Error()
			return
		}
		lFormattedArt := utils.FormatArticle(lArtikel)
		//println("lFormattedArt:" + lFormattedArt.Title)
		eReceiveResult.ReceiveOK = true
		eReceiveResult.Articlelist = append(eReceiveResult.Articlelist, lFormattedArt)
	}
	return

}
