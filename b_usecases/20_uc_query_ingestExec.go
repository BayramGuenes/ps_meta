package b_usecases

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	boot "ps_meta/z_bootstrap"
	"strconv"
)

type IngestOrderDB struct {
	IGSID      string
	OP         string //C,U,D (Create,Update,Delete)
	PRIO       int
	IGSEXTID   string
	Article    ArticleData
	dateregist string
	timeregist string
	Statproc   string `json:"statproc"`
}
type ExecIngestResult struct {
	ExecIngestOK               bool
	ExecIngestWarningErrorText string
	IngestedData               IngestOrderList
	FailedIngestData           []IngestOrderDB
}

func ExecOrderToQueryMicroService(iNumOrders int) (eExecIngestResult ExecIngestResult) {

	portPSQueryService := getPortPSQueryService()
	eExecIngestResult = callServiceExecIngest(iNumOrders, portPSQueryService)

	return
}

func callServiceExecIngest(iNumOrders int, iPortOfService string) (eExecIngestResult ExecIngestResult) {

	eExecIngestResult = ExecIngestResult{}

	serviceadr := boot.ThisCommunnicationProtokoll + "://" + boot.ServiceLocationIDPSQuery + ":" + iPortOfService + "/ps_query/execIngest/" + strconv.Itoa(iNumOrders)
	resp, err := http.Get(serviceadr)
	//resp, err := http.Get(serviceadr)

	if err != nil {
		eExecIngestResult.ExecIngestWarningErrorText = "Technical error: http.Get failed." + err.Error()
		eExecIngestResult.ExecIngestOK = false
		return

	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		eExecIngestResult.ExecIngestWarningErrorText = "Technical error: ioutil.ReadAll(resp.Body) failed." + err.Error()
		eExecIngestResult.ExecIngestOK = false
		return
	}
	var respStruct ExecIngestResult
	if err = json.Unmarshal(body, &respStruct); err != nil {
		eExecIngestResult.ExecIngestWarningErrorText = "Technical error: json.Unmarshal(body, &respStruct)) failed." + err.Error()
		eExecIngestResult.ExecIngestOK = false
		return
	}
	for _, v := range eExecIngestResult.FailedIngestData {
		println(v.Statproc)
	}

	eExecIngestResult = respStruct
	////println("callServiceIngestPSData':eTransferResult" + eTransferResult.TransferWarningErrorText)
	return eExecIngestResult
}
