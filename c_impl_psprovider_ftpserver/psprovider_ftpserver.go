package c_impl_psprovider_ftpserver

import (
	"fmt"
	"io"
	"os"
	domain "ps_meta/a_domain"
	bootstrap "ps_meta/z_bootstrap"
	"strings"

	"github.com/dutchcoders/goftp"
)

const coThisComponentName string = "psproviderViaFTPServer"

type FTPConfig struct {
	HostName string
	HostPort int
	FTPUSR   string
	FTPPWD   string
}

type ProviderFTPServerPort struct{}

func (p ProviderFTPServerPort) FetchDataFromProvider(iWorkDir string) (eFileNameSet []string, eErrorMsg domain.ProcessingInfo, ok bool) {

	var err error
	var ftp *goftp.FTP

	//  Init  ------------------------- //
	ok = false
	eFileNameSet = []string{}
	eErrorMsg = domain.ProcessingInfo{}
	ftpserverAdr := bootstrap.ApplConf.FTPHostName + string(':') + bootstrap.ApplConf.FTPHostPort
	//fmt.//println("ftpserver=" + ftpserver)

	if ftp, err = goftp.Connect(ftpserverAdr); err != nil {
		eErrorMsg.DetectingComponentName = coThisComponentName
		eErrorMsg.InfoDescription = err.Error()
		////println("Err ProviderFTPServerPort:" + err.Error())
		return eFileNameSet, eErrorMsg, ok
	}
	defer ftp.Close()

	//fmt.//println("Successfully connected to server")

	// Username / password authentication
	//fmt.//println("ftpConfigData.ftpusr" + ftpConfigData.FTPUSR)
	if err = ftp.Login(bootstrap.ApplConf.FTPUsr, bootstrap.ApplConf.FTPPwd); err != nil {
		eErrorMsg.DetectingComponentName = coThisComponentName
		eErrorMsg.InfoDescription = err.Error()
		////println("Err ProviderFTPServerPort:" + err.Error())
		return eFileNameSet, eErrorMsg, ok
	}

	if err = ftp.Cwd("/"); err != nil {
		eErrorMsg.DetectingComponentName = coThisComponentName
		eErrorMsg.InfoDescription = err.Error()
		//	//println("Err ProviderFTPServerPort:" + err.Error())
		return eFileNameSet, eErrorMsg, ok
	}

	fileclientabspath := iWorkDir

	err = ftp.Walk("/", func(path string, info os.FileMode, err error) error {
		_, err = ftp.Retr(path, func(r io.Reader) error {

			filename := fmt.Sprintf("%s", path)
			strings.SplitAfter(filename, "/")
			fmt.Println("Walk" + filename)

			//Dateipfad bestimmen

			targetpath := fileclientabspath

			filenameClient := targetpath + string(os.PathSeparator) + filename
			filenameClient = strings.Replace(filenameClient, "//", "/", -1)
			//fmt.//println("filenameClient=" + filenameClient)
			// Reader-Content in Datei schreiben
			fileclient, _ := os.Create(filenameClient)
			if _, err = io.Copy(fileclient, r); err != nil {
				//fmt.//println("Err:os.Create-filenameClient", err.Error(), " EndErr")
				//	//println("Err ProviderFTPServerPort:" + err.Error())
				return err
			}
			eFileNameSet = append(eFileNameSet, filename)
			defer fileclient.Close()

			return nil
		})
		if err != nil {
			eErrorMsg.DetectingComponentName = coThisComponentName
			eErrorMsg.InfoDescription = err.Error()
			////println("Err ProviderFTPServerPort:" + err.Error())
		}
		return nil
	})

	if len(eErrorMsg.InfoDescription) == 0 && bootstrap.ApplConf.FTPDeleteDataFromServerAfterFetch {
		for _, v := range eFileNameSet {

			filecomp := strings.SplitN(v, ";", 10)
			len := len(filecomp) - 1
			file := strings.TrimSpace(filecomp[len])
			//fmt.//println(file)
			err := ftp.Dele(file)
			if err != nil {
				eErrorMsg.DetectingComponentName = coThisComponentName
				eErrorMsg.InfoDescription = err.Error()
				//	//println("Err ProviderFTPServerPort:" + err.Error())
			}
		}
	}

	if err == nil {
		ok = true
	}
	////println("eFileNameSet[0]:" + eFileNameSet[0])
	return eFileNameSet, eErrorMsg, ok
}

func existsSubpath(iPathBase string, iSubpath string) bool {
	filepath := iPathBase + string(os.PathSeparator) + iSubpath
	_, err := os.Stat(filepath)
	if err == nil {
		return true
	}
	if os.IsNotExist(err) {
		return false
	}
	return true

}
