package s_security

import (
	"net/http"
	"net/url"

	"github.com/gin-gonic/gin"
)

func GolangKeyCloakLogoutGin(c *gin.Context) {
	initBackendGo()
	//resetAuthCookie(c)/ via JS!

	target := c.Query("target")
	if len(target) > 0 {
		c.Redirect(http.StatusFound, createOauthServiceURI("/protocol/openid-connect/logout?redirect_uri="+url.QueryEscape(target)))
	} else {
		c.Redirect(http.StatusFound, coDefaultCaller)
	}

}
