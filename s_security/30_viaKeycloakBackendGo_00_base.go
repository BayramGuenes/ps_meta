package s_security

import (
	"context"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"net/url"
	boot "ps_meta/z_bootstrap"
	"strings"

	oidc "github.com/coreos/go-oidc"
	"github.com/gin-gonic/gin"
	"golang.org/x/oauth2"
)

var (
	configBackendAuthGo TypConfigClient
	oauth2Config        oauth2.Config
	state               string
	oidcConfig          *oidc.Config
	//configURL           string
)

var (
	cookieAuthAccessToken  = boot.FrontConsKCloakCookieAccessTok  // "AUTHPSACCESSTOKEN"
	cookieAuthRefreshToken = boot.FrontConsKCloakCookieRefreshTok //"AUTHPSREFRESHTOKEN"
	cookieCallerLogin      = boot.FrontConsKCloakCookieCaller     //"PSAUTHLOGIN"
	coUseCookiePath        = boot.FrontConsKCloakPathCookies      //   "/"
	coUseCookieDomain      = boot.FrontConsKCloakDomainCookies    //"localhost"
	coDefaultCaller        = boot.FrontConsKCloakDef              //"http://localhost:3000"
)

func initBackendGo() {
	configBackendAuthGo = TypConfigClient{
		ConfigURLScheme: boot.ServerKCloakConfigURLScheme, //"http",
		ConfigURL:       boot.ServerKCloakConfigURL,       // "172.17.0.1:8090/auth/realms/idefix",
		ClientID:        boot.ServerKCloakClientID,        // "psmeta_auth_keycloak",
		ClientSecret:    boot.ServerKCloakClioentSecret,   // "f6528cc9-9cfc-494b-8e2e-7ccd76f0945c",
		RedirectURL:     "http://localhost:" + boot.ThisPort + "/ps_meta/authViaKeycloak/oidc/callback",
		// "http://localhost:8800/ps_meta/authViaKeycloak/oidc/callback"
	}
	ctx := context.Background()

	provider, err := oidc.NewProvider(ctx, configBackendAuthGo.ConfigURLScheme+"://"+configBackendAuthGo.ConfigURL)
	if err != nil {
		println("provider, err := oidc.NewProvider(ctx, configURL):" + err.Error())
		panic(err)
	}

	// Configure an OpenID Connect aware OAuth2 client.
	oauth2Config = oauth2.Config{
		ClientID:     configBackendAuthGo.ClientID,
		ClientSecret: configBackendAuthGo.ClientSecret,
		RedirectURL:  configBackendAuthGo.RedirectURL,
		// Discovery returns the OAuth2 endpoints.
		Endpoint: provider.Endpoint(),
		// "openid" is a required scope for OpenID Connect flows.
		Scopes: []string{oidc.ScopeOpenID, "profile", "email"},
	}
	state = "somestate"

	oidcConfig = &oidc.Config{
		ClientID: configBackendAuthGo.ClientID,
	}
}

func getAuthToken(c *gin.Context) (*oauth2.Token, error) {
	token, err := getAuthTokenViaCookie(c)
	if err != nil {
		return nil, err
	}

	ctx := context.Background()

	provider, err := oidc.NewProvider(ctx, configBackendAuthGo.ConfigURLScheme+"://"+configBackendAuthGo.ConfigURL)
	if err != nil {
		println("getAuthToken 5" + err.Error())
		return nil, err
	}
	verifier := provider.Verifier(oidcConfig)

	_, err = verifier.Verify(ctx, token.AccessToken)
	//_, err = verifier.Verify(ctx, parts[1])

	if err != nil {
		println("getAuthToken 7" + err.Error())
		return nil, err
	}

	return token, nil

}

func getAuthTokenViaAuthHeader(c *gin.Context) (*oauth2.Token, error) {

	rawAccessToken := ""
	if len(c.Request.Header) > 0 && len(c.Request.Header["Authorization"]) > 0 {
		rawAccessToken = c.Request.Header["Authorization"][0]
	}
	if rawAccessToken == "" {
		return nil, errors.New("no authorization token was found in the request Authorization Header")
	}

	parts := strings.Split(rawAccessToken, " ")
	if len(parts) != 2 {

		return nil, errors.New("deformed token was found in the request Authorization Header")

	}

	token := &oauth2.Token{
		AccessToken: parts[1],
	}

	return token, nil
}

func getAuthTokenViaCookie(c *gin.Context) (*oauth2.Token, error) {
	cookie, _ := c.Request.Cookie(cookieAuthAccessToken)
	if cookie != nil {

		token := &oauth2.Token{
			AccessToken: cookie.Value,
		}

		return token, nil
	}

	return nil, errors.New("no authorization token was found in the request cookies")

}

func resetAuthCookie(c *gin.Context) {
	println("Delete cookie !")
	c.SetCookie(cookieAuthAccessToken, "XXXX", -1, coUseCookiePath, coUseCookieDomain, false, true)

}

func setCookie(iName string, iSubToken string, c *gin.Context) {
	//println("setCookie")
	c.SetCookie(iName, iSubToken, 1200, coUseCookiePath, coUseCookieDomain, false, false)

}

func getCookie(iName string, c *gin.Context) (eCaller string) {
	//println("getCallerCookie:" + eCaller)
	cookie, err := c.Request.Cookie(iName)
	if err != nil {
		println("getCallerCookie-Err:" + err.Error())
		return ""
	}
	eCaller, _ = url.QueryUnescape(cookie.Value)
	return

}

func getUserInfo(client *http.Client) (userInfo, error) {

	resp, err := client.Get(createOauthServiceURI("/protocol/openid-connect/userinfo"))

	if err != nil {
		return userInfo{}, err
	}

	defer resp.Body.Close()

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return userInfo{}, err
	}

	var result userInfo
	if err := json.Unmarshal(data, &result); err != nil {
		return userInfo{}, err
	}

	return result, nil
}
