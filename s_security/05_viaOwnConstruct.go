package s_security

import (
	"strconv"
	"time"
)

type TestTokenStruct struct {
	Loginname string
	Rfa       string
}

func CreateToken(iLoginname string, iRfa string) string {
	tim := time.Now().Format("2006-01-02T15:04:05-0700")
	result := iLoginname + iRfa + tim
	return result
}

func GetValidFromTo() (eValidFrom string, eValidTo string) {
	currentTime := time.Now()
	thisYear := currentTime.Year()
	thisMonth := strconv.Itoa(int(currentTime.Month()))

	if len(thisMonth) < 10 {
		thisMonth += "0"
	}

	thisDay := strconv.Itoa(int(currentTime.Day()))
	if len(thisDay) < 10 {
		thisDay += "0"
	}

	eValidFrom = currentTime.Format("2006-01-02 15:04:05")
	eValidTo = strconv.Itoa(thisYear) + "-" + thisMonth + "-" + thisDay + "23:59:29"

	return
}
