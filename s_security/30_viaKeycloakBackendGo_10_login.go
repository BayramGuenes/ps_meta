package s_security

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"golang.org/x/net/context"
	"golang.org/x/oauth2"
)

func GolangKeyCloakLoginGin(c *gin.Context) {
	//println("func AuthViaKeyCloakRootGin CALLED 1")

	//solved via client cookie:
	//unescaped, _ := url.QueryUnescape(c.Query("Caller"))
	//println("callerURL:" + unescaped)

	initBackendGo()

	token, err := getAuthToken(c)
	if err != nil {
		//println(err.Error())
		//println("auth2Config.AuthCodeURL(state):" + oauth2Config.AuthCodeURL(state))
		c.Redirect(http.StatusFound, oauth2Config.AuthCodeURL(state))
		c.Abort()

	} else {

		redirectToCaller(oauth2Config, token, c)

	}
}

func redirectToCaller(config oauth2.Config, token *oauth2.Token, c *gin.Context) {

	tokenSource := config.TokenSource(context.Background(), token)

	client := oauth2.NewClient(context.Background(), tokenSource)

	userInfo, err := getUserInfo(client)
	println(userInfo.Name)

	//var content string
	if err != nil {
		// if there is an error, the authentication must be invalid or expired
		resetAuthCookie(c)
		//content = createLoginLink(config)

	} else {
		//content = "Hello " + userInfo.Name + "! <br><br><a href=\"/logout?target=app\">log out app</a> / <a href=\"/logout?target=all\">log out all</a>"
	}

	//c.JSON(http.StatusOK, "hello world")
	//sayHelloToUser(config, token, w)
}
