package s_security

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	boot "ps_meta/z_bootstrap"

	"github.com/coreos/go-oidc"
	"golang.org/x/net/context"
	"golang.org/x/oauth2"
)

//============================================================================
//============================================================================
type KeycloakUsrInfoAdr struct {
	oauthServiceURL string
	clientID        string
	clientSecret    string
	serverAdr       string
}

/*var (
	configRestServerGo TypConfigClient
)*/

type userInfo struct {
	Sub               string `json:"sub"`
	Name              string `json:"name"`
	GivenName         string `json:"given_name"`
	FamilyName        string `json:"family_name"`
	Profile           string `json:"profile"`
	Picture           string `json:"picture"`
	Email             string `json:"email"`
	EmailVerified     bool   `json:"email_verified"`
	Gender            string `json:"gender"`
	PreferredUsername string `json:"preferred_username"`
}

type TypAuthenticationResponse struct {
	OAuth2Token   *oauth2.Token
	IDTokenClaims *json.RawMessage // ID Token payload is just JSON.
}

type ResourceAccess struct {
	Roles []string `json:"roles"`
}

type JWTPayload struct {
	RealmAccess struct {
		Roles []string `json:"roles"`
	} `json:"realm_access"`

	ResourcesAccess map[string]ResourceAccess `json:"resource_access"`

	Scope             string `json:"scope"`
	EmailVerified     bool   `json:"email_verified"`
	Name              string `json:"name"`
	PreferredUsername string `json:"preferred_username"`
	GivenName         string `json:"given_name"`
	FamilyName        string `json:"family_name"`
	Email             string `json:"email"`
}

//============================================================================
//============================================================================

func KC_initOAuth(iConfigClient TypConfigClient) (oauth2.Config, *oidc.IDTokenVerifier, error) {

	ctx := context.Background()

	config := oauth2.Config{
		ClientID:     iConfigClient.ClientID,
		ClientSecret: iConfigClient.ClientSecret,
		RedirectURL:  iConfigClient.ConfigURLScheme + "://" + iConfigClient.RedirectURL,
		Scopes:       []string{oidc.ScopeOpenID, "profile", "email"},
	}
	provider, err := oidc.NewProvider(ctx, iConfigClient.ConfigURLScheme+"://"+iConfigClient.ConfigURL)

	if err != nil {
		return config, nil, fmt.Errorf("Error while getting new provider: %v", err)
	}

	config.Endpoint = provider.Endpoint()

	oidcConfig := &oidc.Config{
		ClientID: iConfigClient.ClientID,
	}

	verifier := provider.Verifier(oidcConfig)

	return config, verifier, nil
}

func KC_getAuthToken(iToken string) *oauth2.Token {

	token := &oauth2.Token{
		AccessToken: iToken,
	}

	return token
}

func KC_decodeToken(token *oauth2.Token, iConfigClient TypConfigClient) (ePreferredName string,
	eTabUsrRole []string, err error) {

	eTabUsrRole = []string{}
	ePreferredName = ""
	config, verifier, err := KC_initOAuth(iConfigClient)
	if err != nil {
		//log.Fatal(err)
		err = errors.New("KC_decodeToken-failed: KC_initOAuth:" + err.Error())
		return

	}
	tokenSource := config.TokenSource(context.Background(), token)

	client := oauth2.NewClient(context.Background(), tokenSource)

	userInfo, err := KC_getUserInfo(client)
	if err != nil {
		println(err.Error())

		err = errors.New("KC_decodeToken-failed: KC_getUserInfo(client):" + err.Error())
		return
	}

	ePreferredName = userInfo.PreferredUsername

	//rawPayload := strings.Split(authResponse.OAuth2Token.AccessToken, ".")[1]
	rawPayload := strings.Split(token.AccessToken, ".")[1]
	ctx := context.Background()

	//https://stackoverflow.com/questions/53550321/keycloak-gatekeeper-aud-claim-and-client-id-do-not-match
	if boot.ApplConf.StrictTokenValidateOn {
		println("verifier.Verify(ctx, token.AccessToken)") //	token.AccessToken = "jdkjsdksjdksdj"
		_, err = verifier.Verify(ctx, token.AccessToken)
		if err != nil {
			println(err.Error())
			err = errors.New("KC_decodeToken-failed: verifier.Verify():" + err.Error())
			return
		}
	}
	payload, err := base64.RawURLEncoding.DecodeString(rawPayload)

	if err != nil {
		println(err.Error())

		err = errors.New("KC_decodeToken-failed: base64.RawURLEncoding.DecodeString:" + err.Error())
		return
	}

	jwtPayload := JWTPayload{}
	err = json.Unmarshal(payload, &jwtPayload)

	if err != nil {
		println(err.Error())

		err = errors.New("KC_decodeToken-failed: json.Unmarshal(payload):" + err.Error())
		return
	}
	if len(userInfo.Name) == 0 {
		ePreferredName = jwtPayload.PreferredUsername
	}

	//jwtPayload
	eTabUsrRole = jwtPayload.RealmAccess.Roles
	return

}
func KC_getUserInfo(client *http.Client) (userInfo, error) {

	resp, err := client.Get(createOauthServiceURI("/protocol/openid-connect/userinfo"))

	if err != nil {
		println("KC_getUserInfo:client.Get(createOauthServiceURI " + err.Error())
		return userInfo{}, err
	}

	defer resp.Body.Close()

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		println("KC_getUserInfo:ioutil.ReadAll( " + err.Error())
		return userInfo{}, err
	}
	//println("KC_getUserInfo-data:" + string(data))

	var result userInfo
	if err := json.Unmarshal(data, &result); err != nil {
		return userInfo{}, err
	}
	//println("KC_getUserInfo:" + result.PreferredUsername)

	return result, nil
}

func createOauthServiceURI(uri string) string {

	return configBackendAuthGo.ConfigURLScheme + "://" + configBackendAuthGo.ConfigURL + uri
}
