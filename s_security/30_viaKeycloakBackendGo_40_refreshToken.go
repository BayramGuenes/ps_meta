package s_security

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"

	"github.com/gin-gonic/gin"
)

type Type_ResultKCHttpService = struct {
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
}
type Type_ServicePCRefreshToken = struct {
	AccessToken  string
	RefreshToken string
	ErrorText    string
}

func GolangKeyCloakRefreshTokenGin(c *gin.Context) {
	//https://stackoverflow.com/questions/51386337/refresh-access-token-via-refresh-token-in-keycloak

	initBackendGo()
	refreshToken := c.PostForm("RefreshToken")
	//http://172.17.0.1:8090/auth/realms/idefix","
	refreshServiceURL := configBackendAuthGo.ConfigURLScheme + "://" + configBackendAuthGo.ConfigURL + "/protocol/openid-connect/token"

	lKCResult := Type_ResultKCHttpService{}
	lPSResult := Type_ServicePCRefreshToken{}

	if len(refreshToken) > 0 {
		lServiceUrl := refreshServiceURL

		resp, err := http.PostForm(lServiceUrl,
			url.Values{"grant_type": {"refresh_token"},
				"refresh_token": {refreshToken},
				"client_id":     {configBackendAuthGo.ClientID},
				"client_secret": {configBackendAuthGo.ClientSecret}})
		defer resp.Body.Close()
		body, err := ioutil.ReadAll(resp.Body)
		err = json.Unmarshal(body, &lKCResult)
		if err != nil {
			lPSResult.ErrorText = err.Error()

		} else {
			lPSResult.AccessToken = lKCResult.AccessToken
			lPSResult.RefreshToken = lKCResult.RefreshToken //string(body)
		}

	}
	c.JSON(http.StatusOK, lPSResult)

}
