package s_security

import "encoding/json"

func extractPayloadFromParsedToken(iToken string) (ePreferredName string,
	eTabUsrRole []string) {
	m := map[string]interface{}{}

	// Parsing/Unmarshalling JSON encoding/json
	err := json.Unmarshal([]byte(iToken), &m)

	if err != nil {
		panic(err)
	}
	_, _, ePreferredName, eTabUsrRole = parseMap(m, false, false, ePreferredName, eTabUsrRole)
	return
}

func parseMap(aMap map[string]interface{}, inRealmAccessBlock bool, inRoleBlock bool, iPreferredName string, iTabUsrRole []string) (eRealmAccessBlock bool, eRoleBlock bool, ePreferredName string, eTabUsrRole []string) {

	useInRoleBlock := inRoleBlock
	useInRealmAccessBlock := inRealmAccessBlock
	ePreferredName = iPreferredName
	for i := 0; i < len(iTabUsrRole); i++ {
		eTabUsrRole = append(eTabUsrRole, iTabUsrRole[i])
	}

	for key, val := range aMap {
		if key == "realm_access" {
			useInRealmAccessBlock = true
			eRealmAccessBlock = true
			//	println("IN REALM ACCESS BLOCK")
		}
		if useInRealmAccessBlock && key == "roles" {
			useInRoleBlock = true
			eRoleBlock = true
			//println("IN ROLE BLOCK")
		}
		switch concreteVal := val.(type) {
		//switch val.(type) {
		case map[string]interface{}:
			//fmt.Println(key)
			//if key=="hits"
			eRealmAccessBlock, eRoleBlock, ePreferredName, eTabUsrRole = parseMap(val.(map[string]interface{}), useInRealmAccessBlock, useInRoleBlock, ePreferredName, eTabUsrRole)

		case []interface{}:
			//fmt.Println(key)
			eRealmAccessBlock, eRoleBlock, ePreferredName, eTabUsrRole = parseArray(val.([]interface{}), useInRealmAccessBlock, useInRoleBlock, ePreferredName, eTabUsrRole)
		case map[bool]interface{}:
		case map[float64]interface{}:

		default:
			if key == "preferred_username" {
				if len(concreteVal.(string)) > 0 {
					ePreferredName = concreteVal.(string)
				}
				//println("value:" + s)
			}
			if useInRoleBlock {
				println("concreteVal" + concreteVal.(string))
				//fmt.Println(key, ":", concreteVal)
			}

		}
	}

	return
}

func parseArray(anArray []interface{}, inRealmAccessBlock bool, inRoleBlock bool, iPreferredName string, iTabUsrRole []string) (eRealmAccessBlock bool, eRoleBlock bool, ePreferredName string, eTabUsrRole []string) {
	for i := 0; i < len(iTabUsrRole); i++ {
		eTabUsrRole = append(eTabUsrRole, iTabUsrRole[i])
	}
	ePreferredName = iPreferredName
	for _, val := range anArray {
		//switch concreteVal := val.(type) {
		if inRoleBlock && val.(string) != ePreferredName {
			eTabUsrRole = append(eTabUsrRole, val.(string))
		}
		//	println(val.(string))
		switch val.(type) {
		case map[string]interface{}:
			//fmt.Println("Index:", i)
			eRealmAccessBlock, eRoleBlock, ePreferredName, eTabUsrRole = parseMap(val.(map[string]interface{}), inRealmAccessBlock, inRoleBlock, ePreferredName, eTabUsrRole)
		case []interface{}:
			//fmt.Println("Index:", i)
			eRealmAccessBlock, eRoleBlock, ePreferredName, eTabUsrRole = parseArray(val.([]interface{}), inRealmAccessBlock, inRoleBlock, ePreferredName, eTabUsrRole)
		case string:
			//println(val)

		default:
			//fmt.Println("Index", i, ":", concreteVal)
		}
	}

	return
}

/*func contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}
*/
