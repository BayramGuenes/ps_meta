package s_security

import (
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/mux"
)

type TypConfigClient = struct {
	ConfigURLScheme string
	ConfigURL       string
	ClientID        string
	ClientSecret    string
	RedirectURL     string
	//UseCookieName   string
	//UseCookieDomain string
}

func ExtractTokenFromHTTPRequest(r *http.Request) (eToken string) {

	eToken = r.FormValue("Token")
	if eToken == "" {
		vars := mux.Vars(r)
		for i, v := range vars {
			switch i {
			case "Token":
				lstringTab := strings.SplitAfter(v, "=")
				if len(lstringTab) > 1 {
					eToken = lstringTab[1]
				} else {
					eToken = v
				}
			}
			////println(i, v)
		}
	}

	return eToken
}
func ExtractTokenFromHTTPRequestGin(c *gin.Context) (eToken string, eKindOfAuth string) {

	eToken = c.PostForm("PSUsr.Token")
	if eToken == "" {
		eToken = c.PostForm("Token")
	}
	if eToken == "" {
		splitArr := strings.Split(c.GetHeader("Authorization"), "Bearer ")
		if len(splitArr) > 0 {
			eToken = splitArr[len(splitArr)-1]
		}
		if eToken == "Bearer" {
			eToken = ""
		}

	}
	if eToken == "" {
		eToken = c.Param("Token")
	}

	eKindOfAuth = c.Query("KindOfAuth")
	if eKindOfAuth == "" {
		eKindOfAuth = c.PostForm("KindOfAuth")
	}

	return eToken, eKindOfAuth
}

func GetUsrInfoFromToken(iToken string, iKindOfAuth string) (ePreferredName string,
	eTabUsrRole []string, err error) {
	ePreferredName = ""
	eTabUsrRole = []string{}
	switch iKindOfAuth {
	case "WithoutKC":
		//force use DB-Get:
		ePreferredName = ""
		eTabUsrRole = []string{}
		//err = nil
		err = *new(error)
		break
	case "TokenParsed":
		if len(iToken) > 0 && strings.HasPrefix(iToken, "{") && strings.HasSuffix(iToken, "}") {

			ePreferredName, eTabUsrRole = extractPayloadFromParsedToken(iToken)

		}
		break

	case "KCViaGoBackend":
		if len(iToken) > 0 {
			initBackendGo()
			auth2Token := KC_getAuthToken(iToken)
			ePreferredName, eTabUsrRole, err = KC_decodeToken(auth2Token, configBackendAuthGo)
			strings.Join(eTabUsrRole, ",")

		}

		break
	}

	return
}
