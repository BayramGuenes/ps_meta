package s_security

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"net/http"
	"strings"

	"github.com/coreos/go-oidc"
	"github.com/gin-gonic/gin"
	"golang.org/x/net/context"
	"golang.org/x/oauth2"
)

func GolangKeyCloakOIDCCallbackGin(c *gin.Context) {

	lCaller := getCookie(cookieCallerLogin, c)
	authResponse, httpStatus, err := authenticate(c)
	if err != nil {
		c.JSON(httpStatus, gin.H{"error": err.Error()})
		return
	}

	err = payloadDataToCookie(authResponse.OAuth2Token, authResponse, c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.Redirect(301, lCaller)
	//data := jsonAuthResponse
	c.JSON(http.StatusOK, "")
	//w.Write(data)
}
func payloadDataToCookie(iToken *oauth2.Token, iAuthenticationResponse TypAuthenticationResponse,
	c *gin.Context) (err error) {
	rawPayload := strings.Split(iToken.AccessToken, ".")[1]
	payload, err := base64.RawURLEncoding.DecodeString(rawPayload)

	if err != nil {
		return
	}

	jwtPayload := JWTPayload{}
	err = json.Unmarshal(payload, &jwtPayload)

	if err != nil {
		return
	}
	setCookie(cookieAuthAccessToken, iToken.AccessToken, c)
	setCookie(cookieAuthRefreshToken, iToken.RefreshToken, c)

	return nil
}
func toJSONBytes(authResponse TypAuthenticationResponse) ([]byte, error) {
	return json.MarshalIndent(authResponse, "", "    ")
}

func authenticate(c *gin.Context) (eAuthenticationResponse TypAuthenticationResponse,
	ehttpStatus int, err error) {

	eAuthenticationResponse = TypAuthenticationResponse{}
	ctx := context.Background()
	provider, err := oidc.NewProvider(ctx, configBackendAuthGo.ConfigURLScheme+"://"+configBackendAuthGo.ConfigURL)

	verifier := provider.Verifier(oidcConfig)

	if c.Query("state") != state {
		return eAuthenticationResponse, http.StatusBadRequest, errors.New("state did not match")
	}

	oauth2Token, err := oauth2Config.Exchange(ctx, c.Query("code"))
	if err != nil {
		return eAuthenticationResponse, http.StatusInternalServerError, errors.New("Failed to exchange token: " + err.Error())
	}

	rawIDToken, ok := oauth2Token.Extra("id_token").(string)
	if !ok {
		return eAuthenticationResponse, http.StatusInternalServerError, errors.New("No id_token field in oauth2 token.")
	}

	idToken, err := verifier.Verify(ctx, rawIDToken)
	if err != nil {
		return eAuthenticationResponse, http.StatusInternalServerError, errors.New("Failed to verify ID Token: " + err.Error())
	}

	eAuthenticationResponse.OAuth2Token = oauth2Token
	eAuthenticationResponse.IDTokenClaims = new(json.RawMessage)
	if err = idToken.Claims(&eAuthenticationResponse.IDTokenClaims); err != nil {
		return eAuthenticationResponse, http.StatusInternalServerError, err

	}

	return eAuthenticationResponse, 0, nil
}
