package y_impl_transferlogger

import (
	"io/ioutil"
	"os"
	domain "ps_meta/a_domain"

	//	"strconv"
	boot "ps_meta/z_bootstrap"
	"strings"
	"time"
	"unicode"

	uid "github.com/lithammer/shortuuid"
)

type LogPort struct {
	LogOnlyErr bool
}

func (l LogPort) LogStep(iTransactionLog domain.TransactionLogBufferType, iStepname string, iStepParam string) (eTransactionLog domain.TransactionLogBufferType) {
	eTransactionLog = CollectThis(iTransactionLog, "Step:"+iStepname+" "+iStepParam)
	return //Logthis("Step:" + iStepname + " " + iStepParam)
}

func (l LogPort) LogStepResult(iTransactionLog domain.TransactionLogBufferType, iStepname, iResult string, iDetail domain.ProcessingInfo) (eTransactionLog domain.TransactionLogBufferType) {

	if len(iDetail.InfoDescription) > 0 {
		eTransactionLog = CollectThisResult(iTransactionLog, iResult, iStepname+" Details:"+iDetail.DetectingComponentName+" "+iDetail.InfoDescription)
		//Logthis("Result:" + iResult + " " + iStepname + " Details:" + iDetail.DetectingComponentName + " " + iDetail.InfoDescription)
	} else {
		eTransactionLog = CollectThisResult(iTransactionLog, iResult, iStepname)
		//Logthis("Result:" + iResult + " " + iStepname)
	}
	if strings.Contains(iResult, "FAILED") {
		l.SetEndTransaction(eTransactionLog)
	}
	return
}

func GetLogger(iLogOnlyErr bool) LogPort {
	return LogPort{LogOnlyErr: iLogOnlyErr}
}

func (l LogPort) SetLocationLogs(iLocationLogs string) {
	coPathLogFile = iLocationLogs + string(os.PathSeparator)
}

// =================================================================== //
// Buffered Logger                                                     //
// =================================================================== //
//
/*type transactionLogBufferType struct {
	result         string
	logStringArray []string
}

type TypLogBuffer map[string]transactionLogBufferType

var logBuffer map[string]transactionLogBufferType

func init() {
	logBuffer = make(map[string]transactionLogBufferType)
}
*/
func (l LogPort) SetStartTransaction(iTransactionName string) (eTransactionLog domain.TransactionLogBufferType) {
	eTransactionLog = domain.TransactionLogBufferType{}
	eTransactionLog.TransactionKey = generateTransactionKey(iTransactionName)
	eTransactionLog = l.LogStep(eTransactionLog, "<NEW-LINE>", "")
	eTransactionLog = l.LogStep(eTransactionLog, "<NEW-LINE>", "")

	eTransactionLog = l.LogStep(eTransactionLog, "START Transaction ==========================="+iTransactionName+" ================================", "")
	return
}
func generateTransactionKey(iTransactionName string) string {
	t := time.Now()

	//nanosec := t.UnixNano()
	//millisec := t.UnixNano() / int64(time.Millisecond)

	formatinfo := t.Format("20060102T150405")

	lUid := uid.New()
	//lKey := formatinfo + "-" + (strconv.FormatInt(nanosec, 10)) + "-" + iTransactionName
	//lKey := formatinfo + "-" + (strconv.FormatInt(millisec, 10)) + "-" + iTransactionName
	lKey := formatinfo + "-" + iTransactionName + "-" + lUid

	////println("lKey:"+lKey)
	return lKey
}
func (l LogPort) SetEndTransaction(iTransactionLog domain.TransactionLogBufferType) {
	lTransactionLog := l.LogStep(iTransactionLog, "END  Transaction ==============================="+iTransactionLog.TransactionKey, "")
	////println("len(iTransactionLog.LogStringArray):" + strconv.Itoa(len(iTransactionLog.LogStringArray)))
	if len(iTransactionLog.LogStringArray) > 4 {
		writeBufferTransactionToLogDir(lTransactionLog, l.LogOnlyErr)
	}
}

func CollectThis(iTransactionLog domain.TransactionLogBufferType, iString string) (eTransactionLog domain.TransactionLogBufferType) {
	t := time.Now()
	////println(t.Format("2006-01-02 15:04:05"))
	logString := "INFO" + "|" + t.Format("2006-01-02 15:04:05") + "|" + iString + "\n"
	////println("logBuffer[iTransactionKey] BEFORE")
	eTransactionLog = iTransactionLog
	eTransactionLog.LogStringArray = append(eTransactionLog.LogStringArray, logString)
	////println("logBuffer[iTransactionKey] AFTER II")
	return
}

func CollectThisResult(iTransactionLog domain.TransactionLogBufferType, iResult string, iString string) (eTransactionLog domain.TransactionLogBufferType) {
	t := time.Now()
	////println(t.Format("2006-01-02 15:04:05"))
	logString := "INFO" + "|" + t.Format("2006-01-02 15:04:05") + "|" + "Result:" + iResult + " " + iString + "\n"
	eTransactionLog = iTransactionLog
	eTransactionLog.Result = iResult
	eTransactionLog.LogStringArray = append(eTransactionLog.LogStringArray, logString)
	return

}

func writeBufferTransactionToLogDir(iTransactionLog domain.TransactionLogBufferType, iLogOnlyErr bool) {
	//println("writeBufferTransactionToLogDir CALLED:" + iTransactionLog.TransactionKey)
	if !iLogOnlyErr || (iLogOnlyErr && !strings.Contains(iTransactionLog.Result, "OK")) ||
		(boot.ApplConf.AlwaysLogImportActivate && strings.Contains(iTransactionLog.TransactionKey, "ImportPSData")) ||
		(boot.ApplConf.AlwaysLogImportActivate && strings.Contains(iTransactionLog.TransactionKey, "ActivatePS")) ||
		(boot.ApplConf.AlwaysLogExecQueryArticle && strings.Contains(iTransactionLog.TransactionKey, "ArticleQuery")) {
		//println("writeBufferTransactionToLogDir WITHIN")

		transactionLogFilename := getTransactionLogFilename(iTransactionLog.TransactionKey, iTransactionLog.Result)
		err := builTransactionLogFile(transactionLogFilename)
		if err != nil {
			panic(err)
		}

		f, err := os.OpenFile(transactionLogFilename, os.O_APPEND|os.O_WRONLY, 0777)
		if err != nil {
			panic(err)
		}
		defer f.Close()

		for i := 0; i < len(iTransactionLog.LogStringArray); i++ {
			logString := iTransactionLog.LogStringArray[i]
			print(logString)
			if _, err = f.WriteString(logString); err != nil {
				panic(err)
			}
		}

	}
}

func getTransactionLogFilename(iTransactionFileName string, iTransactionStatus string) string {
	if len(coPathLogFile) == 0 {
		coPathLogFile = GetLogPath()
	}
	logPath := coPathLogFile
	logFile := logPath + //string(os.PathSeparator) +
		"TRA-" + iTransactionStatus + "-" + iTransactionFileName + ".txt"
	stripSpaces(logFile)
	return logFile
}

// BuildNewLogFileIf ...//
func builTransactionLogFile(iTransactionLogFilename string) (err error) {

	if _, err := os.Stat(iTransactionLogFilename); os.IsNotExist(err) {

		err = ioutil.WriteFile(iTransactionLogFilename, []byte(""), 0777)

	}

	return err
}
func stripSpaces(str string) string {
	return strings.Map(func(r rune) rune {
		if unicode.IsSpace(r) {
			// if the character is a space, drop it
			return -1
		}
		// else keep it in the string
		return r
	}, str)
}
