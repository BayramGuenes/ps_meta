package y_impl_transferlogger

import (
	"io/ioutil"
	"log"
	"os"

	//"path/filepath"
	//"strings"
	"time"
)

var (
	// Log ... //
	Log           *log.Logger
	coPathLogFile string
	// CurrentLogFile ... //
	CurrentLogFile      string
	informedLogFileName bool
)

func init() {
	//BuildNewLogFileIf()
}

// BuildNewLogFileIf ...//
func BuildNewLogFileIf() (eLogFile string) {
	CurrentLogFile := getCurrentLogFilename()

	if _, err := os.Stat(CurrentLogFile); os.IsNotExist(err) {

		err := ioutil.WriteFile(CurrentLogFile, []byte(""), 0777)
		if err != nil {
			panic(err)
		}
		//println("INFO", CurrentLogFile) //buildNewLogFile()
	}

	return CurrentLogFile
}

// GetLogPath ... //
func GetLogPath() string {

	return coPathLogFile
}

// //println ... //
func Println(iMsgType string, iString string) {
	f, err := os.OpenFile(CurrentLogFile, os.O_APPEND|os.O_WRONLY, 0777)
	if err != nil {
		panic(err)
	}

	defer f.Close()

	t := time.Now()
	////println(t.Format("2006-01-02 15:04:05"))

	logString := iMsgType + "|" + t.Format("2006-01-02 15:04:05") + "|" + iString + "\n"
	if _, err = f.WriteString(logString); err != nil {
		panic(err)
	}

}

/* ************************  local functions **************************** */

func getCurrentLogFilename() string {
	if len(coPathLogFile) == 0 {
		coPathLogFile = GetLogPath()
	}
	logPath := coPathLogFile
	currentDate := time.Now()
	datePostfix := currentDate.Format("20060102")
	dateinfo := datePostfix
	logFile := logPath + //string(os.PathSeparator) +
		"log" + dateinfo + ".txt"
	CurrentLogFile = logFile
	return logFile
}

func Logthis(iString string) {
	//logger.DeleteOldLogFiles(1)
	//BuildNewLogFileIf()

	if !informedLogFileName {
		println("Logging into:" + CurrentLogFile)
	}
	//logger.Log.//println(iString)
	Println("INFO", iString)
	//logger.CurrentLogFile.Close()
	log.Println(iString)
	informedLogFileName = true
}
